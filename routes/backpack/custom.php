<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', 'RequireAdminRole', config('backpack.base.middleware_key', 'admin')],

    // 'middleware' => array_merge(
    //     (array) config('backpack.base.web_middleware', 'web'),
    //     (array) config('backpack.base.middleware_key', 'admin'),
    //     (array) 'RequireAdminRole'
    // ),
    // 'middleware' => array_merge(
    //     (array) config('backpack.base.web_middleware', 'web'),
    //     (array) config('backpack.base.middleware_key', 'admin'),
    //     array('roleCheck:pdma')
    // ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('province', 'ProvinceCrudController');
    Route::crud('tehsil', 'TehsilCrudController');
    Route::crud('uc', 'UcCrudController');
    Route::crud('district', 'DistrictCrudController');
    Route::crud('area_expertise', 'Area_expertiseCrudController');
    Route::crud('volunteer', 'VolunteerCrudController');

    Route::crud('relief_camps', 'ReliefCampCrudController');
    Route::crud('disaster', 'DisasterCrudController');
    Route::crud('category_type', 'Category_typeCrudController');
    Route::crud('item_type', 'Item_typeCrudController');
    Route::crud('stock', 'StockCrudController');
    Route::crud('network', 'NetworkCrudController');
    Route::crud('affiliation', 'AffiliationCrudController');
    Route::crud('hazard_areas', 'Hazard_areasCrudController');
    // Route::crud('hazardareas', 'HazardProneAreaController');
    Route::crud('funding_agency', 'FundingAgencyCrudController');
    Route::crud('organization_natures', 'OrganizationNaturesCrudController');
    Route::crud('ngo_contact', 'Ngo_contactCrudController');
    Route::crud('human_resource', 'Human_resourceCrudController');
    Route::crud('thematic_areas', 'ThematicAreaCrudController');
    Route::crud('project_category', 'ProjectCategoryCrudController');
    Route::crud('projecttype', 'ProjectTypeCrudController');
    Route::crud('contact_pdma', 'Contact_pdmaCrudController');
    Route::crud('emergency', 'EmergencyCrudController');
    Route::crud('emergency-item', 'EmergencyItemCrudController');
//    Route::resource('emergency-item/{id}/list', 'EmergencyItemCrudController');
    Route::crud('needassesmentresponse', 'NeedassesmentresponseCrudController');
    Route::crud('riskmanagement', 'RiskmanagementCrudController');
    Route::crud('project_modality', 'Project_modalityCrudController');
    Route::crud('implementing_organization_type', 'Implementing_organization_typeCrudController');
    Route::crud('sector_cluster', 'Sector_ClusterCrudController');
    Route::crud('implementingpartner', 'ImplementingPartnerCrudController');
    Route::crud('ngo', 'NgoCrudController');
    Route::crud('affectedarea', 'AffectedAreaCrudController');
    Route::crud('donor', 'DonorCrudController');
    // Route::crud('user', 'UserCrudController');

    Route::get('notifications', 'AdminNotificationController@index');
    Route::get('notifications/profileApproval/{id}', 'AdminNotificationController@profileApproval');
    Route::post('assingProfileApproval', 'AdminNotificationController@assingProfileApproval')->name('assingProfileApproval');

    Route::get('notifications/nocApproval/{id}', 'AdminNotificationController@nocApproval');
    Route::post('assignNocApproval', 'AdminNotificationController@assignNocApproval')->name('assignNocApproval');
    Route::post('pdmaNocReview', 'AdminNotificationController@pdmaNocReview')->name('pdmaNocReview');

    Route::post('pdmaProfileReview', 'AdminNotificationController@pdmaProfileReview')->name('pdmaProfileReview');

    Route::crud('project', 'ProjectCrudController');
    Route::crud('project-reporting', 'ProjectReportingCrudController');
    Route::crud('hazard_type', 'HazardTypeCrudController');
    Route::crud('hazard_prone_areas', 'HazardProneAreasCrudController');
    Route::crud('announcement', 'AnnouncementCrudController');
    Route::crud('forecastof', 'ForecastofCrudController');
    Route::crud('reliefcampcontact', 'ReliefcampcontactCrudController');
    Route::crud('medicalfacilities', 'MedicalFacilitiesCrudController');
    Route::crud('personstreated', 'PersonsTreatedCrudController');
    Route::crud('foodpackage', 'FoodPackageCrudController');
    Route::crud('livestockcamp', 'LivestockCampCrudController');
    Route::crud('terms-content', 'TermsContentCrudController');
    Route::crud('ngo-registration-law', 'NgoRegistrationLawCrudController');
}); // this should be the absolute last line of this file