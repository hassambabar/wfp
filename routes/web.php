<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Auth::routes(['verify' => true]);


Route::get('user-profile', 'UserProfileController@index');
Route::post('user-profile', 'UserProfileController@store')->name('user-profile');


Route::middleware(['auth','verified','roleCheck:NGO'])->group(function () {

	Route::get('/', 'HomeController@index')->name('home');


	Route::get('/home', 'HomeController@index')->name('home');
	// Route::get('/volunteers', 'VolunteersController@index')->name('volunteers');
	Route::resource('volunteers', 'VolunteersController');
//	Route::resource('/admin/hazard_prone_area', 'HazardProneAreaController');
	Route::get('/volunteers/{id}/delete', 'VolunteersController@delete');
	Route::resource('stocks', 'StocksController');
	Route::get('/stocks/{id}/delete', 'StocksController@delete');
    Route::get('/stocks/detail/{id}', 'StocksController@show');
	Route::resource('affiliations', 'AffiliationsController');
	Route::get('/affiliations/{id}/delete', 'AffiliationsController@delete');
    Route::get('/affiliations/detail/{id}', 'AffiliationsController@show');



	//NGO Profile
    Route::get('/profile/request-approval', 'Ngo\NgoProfileController@approval');
    Route::get('/profile/create','Ngo\NgoProfileController@create');
    Route::post('/profile/create','Ngo\NgoProfileController@store')->name('profile.store');
    Route::get('/profile/approval/request-update', 'Ngo\NgoProfileController@update_approval');
	Route::resource('profile','Ngo\NgoProfileController');

	//NGO MOU
	Route::resource('mous','Ngo\NgoMouController');
	Route::get('/mou/create','Ngo\NgoMouController@create');
    Route::get('/mou/{id}/delete', 'Ngo\NgoMouController@delete');

	//NGO Projects
	Route::resource('projects','Ngo\NgoProjectsController');
	Route::get('/project/create','Ngo\NgoProjectsController@create');
    Route::get('/noc/approval/request-update', 'Ngo\NgoProjectsController@update_approval');
	Route::get('/project/{id}/delete', 'Ngo\NgoProjectsController@delete');


	//NGO Donors
	Route::resource('donors','Ngo\NgoDonorsController');

	//NGO NOC Requests
	Route::resource('noc-requests','Ngo\NgoNocRequestsController');
    Route::get('/noc-apply/create','Ngo\NgoProjectsController@nocApply');

	//Emergencies Responded
	Route::resource('past-emergencies','Ngo\NgoPastEmergenciesController');
	Route::get('/past-emergency/create','Ngo\NgoPastEmergenciesController@create');
	Route::get('/past-emergency/{id}/delete','Ngo\NgoPastEmergenciesController@delete');
    Route::get('/past-emergency/detail/{id}', 'Ngo\NgoPastEmergenciesController@show');

	//NGO Contact
	Route::resource('ngocontact', 'NgoContactController');
	Route::get('ngocontact/{id}/delete', 'NgoContactController@delete');
    Route::get('ngocontact/detail/{id}', 'NgoContactController@show');

	//Contact For PDMA
	Route::resource('contacpdma', 'ContactPdmaController');
	Route::get('contacpdma/{id}/delete', 'ContactPdmaController@delete');


	// Emergencies
	Route::get('emergencies', 'EmergenciesController@index');
	// Route::get('need-assesment', 'NeedAssesmentController@index');
	Route::resource('need-assesment', 'NeedAssesmentController');
	Route::post('needassesmentresponses', 'NeedAssesmentController@needassesmentresponses')->name('needassesmentresponses');


	//Risk Management
	Route::resource('risk-management', 'RiskManagementController');
	Route::get('risk-management/{id}/delete', 'RiskManagementController@delete');

	//Reaminder
	Route::get('readremainder/{id}', 'HomeController@readremainder')->name('readremainder');


	Route::resource('human-resource', 'HumanResourceController');
	Route::get('/human-resource/{id}/delete', 'HumanResourceController@delete');
	Route::get('/human-resource/detail/{id}', 'HumanResourceController@show');



	//NGO Reporting
	Route::resource('reporting','Ngo\NgoReportingController');
	Route::get('/reporting/create','Ngo\NgoReportingController@create')->name('reporting.new');
	Route::get('/reporting/{id}/delete', 'Ngo\NgoReportingController@delete');


	// Notification Listing
	Route::get('notification','Ngo\NotificationController@index')->name('notification.index');
	Route::get('notification/emergencies','Ngo\NotificationController@emergencies')->name('notification.emergencies');

	// Tehsil Listing
    Route::get('/getTehsils/{id}','AjaxController@getTehsils');
    Route::get('/getUcs/{id}','AjaxController@getUcs');
    Route::get('/getProjectDetail/{id}','AjaxController@getProjectDetail');
    Route::post('accept-terms/{id}','AjaxController@acceptTerms');


});

Route::resource('admin/comments', 'CommentController');

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', 'RequireAdminRole', config('backpack.base.middleware_key', 'admin')],

    'namespace'  => 'Admin',
], function () {
	   Route::crud('user', 'UserCrudController');
	   Route::get('dashboard', 'DashboardController@index');

    Route::get('api/getTehsils','AjaxController@getTehsilsBackpack');
});

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', 'RequireAdminRole', config('backpack.base.middleware_key', 'admin')],
], function () {
    Route::get('api/getTehsils','AjaxController@getTehsilsBackpack');
    Route::get('api/getUcs','AjaxController@getUcsBackpack');
    Route::get('api/getUcs','AjaxController@getUcsBackpack');
    Route::post('emergency-item/send-emg-notification','AjaxController@sendEmgNotification');
    Route::get('project/show-report-summary/{id}','AjaxController@showSummaryReport');
});

// middleware


