<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Affiliations extends Model
{
    //
    use CrudTrait;

    protected $fillable = [
        'network_id', 'joining_date','ngo_id','status'
    ];


    public function network()
    {
        return $this->belongsTo('App\Models\Network', 'network_id','id');

    }
}
