<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class ProjectsNoc extends Model
{
    //
    use CrudTrait;
    protected $table = 'projects_noc';

    protected $fillable = [
        'project_id', 'comments', 'status'
    ];

    public function project()
    {
        return $this->belongsTo(NgoProjects::class,'project_id','id');
    }
}
