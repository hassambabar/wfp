<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Riskmanagement extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'riskmanagement';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function ngo()
    {
        return $this->belongsTo('App\User', 'ngo_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function getTitleNgo()
    {
        if ($this->ngo_id) {
            return '<a href="user/' . $this->ngo->id . '/edit">' . $this->ngo->name . ' - ' . $this->ngo->email . '</a>';
        } else {
            return '';
        }
    }
    public function getimages()
    {

        $image = !empty($this->image) ? unserialize($this->image) : '';
        $retunImage = '';
        foreach ($image as $key => $value) {
            $url = url('uploads/risk-management/' . $value);
            $retunImage .= '<br><a href=' . $url . '>' . $value . '</a>';
        }
        return $retunImage;
    }
    public function getDocument()
    {

        $document = !empty($this->document) ? unserialize($this->document) : '';
        $retunImage = '';
        foreach ($document as $key => $value) {
            $url = url('uploads/risk-management/document/' . $value);
            $retunImage .= '<br><a href=' . $url . '>' . $value . '</a>';
        }
        return $retunImage;
    }
    
}
