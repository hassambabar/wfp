<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class HazardProneAreas extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'hazard_prone_areas';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function hazardType(){
        return $this->belongsTo(HazardType::class, 'hazard_type_id');
    }

    public function districtName()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
    public function tehsilName()
    {
        return $this->belongsTo(Tehsil::class, 'tehsil_id');
    }

    public function UcName()
    {
        return $this->belongsTo(Uc::class, 'uc_id');
    }

    public function getHazardTypeName(){
        if($this->hazardType){
            return strip_tags($this->hazardType->name);
        }else{
            return '';
        }
    }

    public function getDistrictTitle() {
        if($this->district_id){
            return strip_tags($this->districtName->name);
        }else{
            return '';
        }
    }

    public function getTitleUc() {
        if($this->uc_id){
            return strip_tags($this->UcName->name);
        }else{
            return '';
        }
    }

    public function getTitleTehsil() {
        if($this->tehsil_id){
            return strip_tags($this->tehsilName->name);
        }else{
            return '';
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
