<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class NgoPastEmergencies extends Model
{
    //
    use CrudTrait;
    protected $table = 'emergencies_responded';

    protected $fillable = [
        'year', 'type_of_emergency', 'province', 'district','tehsil','uc','village_details','response_type', 'desc_deliverables','status'
    ];

    public function ngo()
    {
        return $this->belongsTo('App\User','ngo_id','id');
    }

    public function project()
    {
        return $this->belongsTo(NgoProjects::class,'project_id','id');
    }

    public function disaster()
    {
        return $this->belongsTo(Disaster::class,'type_of_emergency');
    }

    public function provinces(){
        return $this->belongsTo(Province::class,'province');
    }

    public function districts(){
        return $this->belongsTo(District::class,'district','id');
    }

    public function tehsils(){
        return $this->belongsTo(Tehsil::class,'tehsil','id');
    }

    public function ucs(){
        return $this->belongsTo(Uc::class,'uc','id');
    }
}
