<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ngo_projects';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

   /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getNgoName() {
        if($this->ngo){
            return $this->ngo->name;
        }else{
            return '';
        }
     }

     public function getdistrictTitle() {
        if($this->district){
            return $this->district->name;
        }else{
            return '';
        }
     }

     public function getFileUrl(){
        if($this->source_of_funds){
            return '<a target="_blank" href="'.url($this->source_of_funds).'">View File</a>';
        }else{
            return '--';
        }
     }

     public function getthematicAreaTitle() {
        if($this->thematicArea){
            return $this->thematicArea->area_name;
        }elseif ($this->projectThematicAreas){
            $thematicAreas = $this->projectThematicAreas->map(function ($thareas){
                return $thareas->thematicArea->area_name;
            })->toArray();
            if(is_array($thematicAreas) && count($thematicAreas) > 0){
                return implode(', ',$thematicAreas);
            }else{
                return '--';
            }
        }
        else{
            return '';
        }
     }


     public function getsectorTitle() {
        if($this->Sector){
            return $this->Sector->title;
        }else{
            return '';
        }
     }
     public function getcategoryTitle() {
        if($this->category){
            return $this->category->category_name;
        }else{
            return '';
        }
     }

     public function getImplementingPartnerTitle() {
        if($this->partner){
            return $this->partner->title;
        }else{
            return '';
        }
     }
     public function getImplementingOrgtypeTitle() {
        if($this->organization_type){
            return $this->organization_type->title;
        }else{
            return '';
        }
     }

     public function getprovTitle() {
        if($this->province){
            return $this->province->name;
        }else{
            return '';
        }
     }

     public function getptehsilTitle() {
        if($this->tehsil){
            return $this->tehsil->name;
        }else{
            return '';
        }
     }
     public function getucTitle() {
        if($this->uc){
            return $this->uc->name;
        }else{
            return '';
        }
     }
     public function getproject_modalityTitle() {
        if($this->modality){
            return $this->modality->title;
        }else{
            return '';
        }
     }
     public function getprojectdoneTitle() {
        if($this->fundingAgency){
            return $this->fundingAgency->name;
        }else{
            return '';
        }
     }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function ngo()
    {
        return $this->belongsTo('App\User','ngo_id','id');
    }

    public function activities()
    {
        return $this->hasMany(ProjectActivity::class,'project_id');
    }

    public function province(){
        return $this->belongsTo(Province::class,'project_province','id');
    }

    public function district(){
        return $this->belongsTo(District::class,'project_district','id');
    }

    public function tehsil(){
        return $this->belongsTo(Tehsil::class,'project_tehsil','id');
    }

    public function uc(){
        return $this->belongsTo(Uc::class,'project_uc','id');
    }

    public function fundingAgency(){
        return $this->belongsTo(FundingAgency::class,'donor','id');
    }
    public function category(){
        return $this->belongsTo(ProjectCategory::class,'project_category','id');
    }
    public function modality(){
        return $this->belongsTo(Project_modality::class,'project_modality','id');
    }
    public function thematicArea(){
        return $this->belongsTo(ThematicArea::class,'thematic_area','id');
    }
    public function Sector(){
        return $this->belongsTo(Sector_Cluster::class,'project_sector','id');
    }
    public function partner(){
        return $this->belongsTo(ImplementingPartner::class,'implementing_partner','id');
    }
    public function organization_type(){
        return $this->belongsTo(Implementing_organization_type::class,'implementing_org_type','id');
    }

    public function noc(){
        return $this->hasMany(ProjectsNoc::class,'project_id');
    }

    public function comment()
    {
        return $this->hasMany(Comment::class,'post_id')->where('type','projects')->whereNull('parent_id');
    }

    public function projectAreas(){
        return $this->hasMany(ProjectAreas::class,'project_id');
    }

    public function projectThematicAreas(){
        return $this->hasMany(ProjectThematicAreas::class,'project_id');
    }

    public function reports(){
        return $this->hasMany(NgoReporting::class, 'report_project');
    }


    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
