<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class NgoProfile extends Model
{
    //
    use CrudTrait;
    protected $table = 'ngo_profile';

    protected $fillable = [
        'legal_status', 'reg_law', 'reg_date', 'reg_no','profile','have_mou', 'hq_origin_country', 'hq_reg_law', 'hq_reg_date',
        'hq_reg_no', 'org_full_name', 'acronym', 'ngo_goal' ,'mission_statement','objectives','registration_certificate'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function mous()
    {
        return $this->hasMany(NgoMou::class,'ngo_id','user_id');
    }

    public function natureOfOrg(){
        return $this->belongsTo(OrganizationNatures::class,'nature_of_org','id');
    }

    public function ngoOrgNatures(){
        return $this->hasMany(NgoOrgNatures::class,'ngo_id');
    }

    public function projects(){
        return $this->hasMany(NgoProjects::class,'ngo_id','user_id');
    }

    public function reporting(){
        return $this->hasMany(NgoReporting::class,'ngo_id','user_id');
    }

    public function pastEmergencies(){
        return $this->hasMany(NgoPastEmergencies::class,'ngo_id','user_id');
    }
}
