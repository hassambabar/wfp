<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use phpDocumentor\Reflection\Project;

class NgoProjects extends Model
{
    //
    use CrudTrait;
    protected $table = 'ngo_projects';

    protected $fillable = [
        'project_title', 'project_category', 'project_owner_name', 'project_code','start_date','end_date','donor', 'project_modality', 'project_owner_org_type', 'implementing_partner',
        'implementing_org_type', 'project_sector', 'project_province', 'project_village','target_number','achieved_number',
        'activity_unit', 'assist_type', 'families_targeted', 'women_targeted', 'men_targeted' ,'girls_targeted','boys_targeted', 'female_headed','families_reached','women_reached','men_reached','girls_reached','boys_reached','with_disabilities',
        'contact_person', 'contact_number', 'contact_email', 'comments','funds_amount','funds_donor_name','project_status','prev_extension','status'
    ];

    public function ngo()
    {
        return $this->belongsTo('App\User','ngo_id','id');
    }

    public function activities()
    {
        return $this->hasMany(ProjectActivity::class,'project_id');
    }

    public function province(){
        return $this->belongsTo(Province::class,'project_province','id');
    }

    public function district(){
        return $this->belongsTo(District::class,'project_district','id');
    }

    public function tehsil(){
        return $this->belongsTo(Tehsil::class,'project_tehsil','id');
    }

    public function uc(){
        return $this->belongsTo(Uc::class,'project_uc','id');
    }

    public function fundingAgency(){
        return $this->belongsTo(FundingAgency::class,'donor','id');
    }

    public function thematicArea(){
        return $this->belongsTo(ThematicArea::class,'thematic_area','id');
    }

    public function noc(){
        return $this->hasMany(ProjectsNoc::class,'project_id');
    }

    public function category(){
        return $this->belongsTo(ProjectCategory::class,'project_category');
    }

    public function modality(){
        return $this->belongsTo(Project_modality::class,'project_modality');
    }

    public function implementingPartner(){
        return $this->belongsTo(ImplementingPartner::class,'implementing_partner');
    }

    public function implementingOrgType(){
        return $this->belongsTo(Implementing_organization_type::class,'implementing_org_type');
    }

    public function projectSector(){
        return $this->belongsTo(Sector_Cluster::class,'project_sector');
    }

    public function projectAreas(){
        return $this->hasMany(ProjectAreas::class,'project_id');
    }

    public function projectThematicAreas(){
        return $this->hasMany(ProjectThematicAreas::class,'project_id');
    }

    public function reports(){
        return $this->hasMany(NgoReporting::class, 'report_project');
    }
}
