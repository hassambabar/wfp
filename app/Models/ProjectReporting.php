<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class ProjectReporting extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ngo_reportings';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getNgoName() {
        if($this->ngo){
            return $this->ngo->name;
        }else{
            return '';
        }
    }
    public function getsectorTitle() {
        if($this->Sector){
            return $this->Sector->title;
        }else{
            return '';
        }
    }
    public function getcategoryTitle() {
        if($this->category){
            return $this->category->category_name;
        }else{
            return '';
        }
    }

    public function getImplementingPartnerTitle() {
        if($this->partner){
            return $this->partner->title;
        }else{
            return '';
        }
    }
    public function getImplementingOrgtypeTitle() {
        if($this->organization_type){
            return $this->organization_type->title;
        }else{
            return '';
        }
    }
    public function getproject_modalityTitle() {
        if($this->modality){
            return $this->modality->title;
        }else{
            return '';
        }
    }
    public function getprojectdoneTitle() {
        if($this->fundingAgency){
            return $this->fundingAgency->name;
        }else{
            return '';
        }
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function ngo()
    {
        return $this->belongsTo('App\User','ngo_id','id');
    }

    public function activities()
    {
        return $this->hasMany(NgoReportingActivity::class,'report_id');
    }

    public function modality(){
        return $this->belongsTo(Project_modality::class,'project_modality');
    }

    public function category(){
        return $this->belongsTo(ProjectCategory::class,'project_category','id');
    }

    public function implementingPartner(){
        return $this->belongsTo(ImplementingPartner::class,'implementing_partner');
    }

    public function implementingOrgType(){
        return $this->belongsTo(Implementing_organization_type::class,'implementing_org_type');
    }

    public function projectSector(){
        return $this->belongsTo(Sector_Cluster::class,'project_sector');
    }

    public function fundingAgency(){
        return $this->belongsTo(FundingAgency::class,'donor','id');
    }

    public function project(){
        return $this->belongsTo(NgoProjects::class,'report_project','id');
    }
    public function Sector(){
        return $this->belongsTo(Sector_Cluster::class,'project_sector','id');
    }
    public function partner(){
        return $this->belongsTo(ImplementingPartner::class,'implementing_partner','id');
    }
    public function organization_type(){
        return $this->belongsTo(Implementing_organization_type::class,'implementing_org_type','id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
