<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class NgoMou extends Model
{
    //
    use CrudTrait;
    protected $table = 'ngo_mous';

    protected $fillable = [
        'mou_status', 'mou_signed_date', 'mou_expiry_date', 'num_of_years'
    ];

    public function ngo()
    {
        return $this->belongsTo('App\User','ngo_id','id');
    }

    public function natureOfOrg(){
        return $this->belongsTo(OrganizationNatures::class,'nature_of_org','id');
    }

    public function fundingAgency(){
        return $this->belongsTo(FundingAgency::class,'funding_agency','id');
    }
}
