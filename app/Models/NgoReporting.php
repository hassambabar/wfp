<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use phpDocumentor\Reflection\Project;

class NgoReporting extends Model
{
    //
    use CrudTrait;
    protected $table = 'ngo_reportings';

    protected $fillable = [
        'project_category','donor', 'project_code',  'project_modality',  'project_title',  'project_owner_name',  'project_owner_org_type',  'implementing_partner',  'implementing_org_type',  'project_sector',  'target_number',  'achieved_number',  'activity_unit',  'women_targeted',  'men_targeted',  'girls_targeted',  'boys_targeted',  'report_duration',  'start_date',  'end_date',  'female_headed',  'women_reached',  'men_reached',  'girls_reached',  'boys_reached',  'contact_person',  'contact_number',  'contact_email',  'comments',  'updated_date', 'report_status','report_project','families_targeted','disabled_women_targeted','disabled_men_targeted','disabled_boys_targeted','disabled_girls_targeted'
    ];

    public function ngo()
    {
        return $this->belongsTo('App\User','ngo_id','id');
    }

    public function activities()
    {
        return $this->hasMany(NgoReportingActivity::class,'report_id');
    }

    public function modality(){
        return $this->belongsTo(Project_modality::class,'project_modality');
    }

    public function category(){
        return $this->belongsTo(ProjectCategory::class,'project_category','id');
    }

    public function implementingPartner(){
        return $this->belongsTo(ImplementingPartner::class,'implementing_partner');
    }

    public function implementingOrgType(){
        return $this->belongsTo(Implementing_organization_type::class,'implementing_org_type');
    }

    public function projectSector(){
        return $this->belongsTo(Sector_Cluster::class,'project_sector');
    }

    public function fundingAgency(){
        return $this->belongsTo(FundingAgency::class,'donor','id');
    }

    public function project(){
        return $this->belongsTo(NgoProjects::class,'report_project','id');
    }

}
