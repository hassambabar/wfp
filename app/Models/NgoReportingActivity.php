<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class NgoReportingActivity extends Model
{
    //
    use CrudTrait;
    protected $table = 'ngo_reporting_activities';

    protected $fillable = [
        'activity', 'description', 'activity_start_date', 'activity_end_date','activity_province',
        'activity_district', 'activity_tehsil', 'activity_uc', 'activity_village'
    ];

    public function reporting()
    {
        return $this->belongsTo(NgoReporting::class,'report_id','id');
    }

    public function province(){
        return $this->belongsTo(Province::class,'activity_province','id');
    }

    public function district(){
        return $this->belongsTo(District::class,'activity_district','id');
    }

    public function tehsil(){
        return $this->belongsTo(Tehsil::class,'activity_tehsil','id');
    }

    public function uc(){
        return $this->belongsTo(Uc::class,'activity_uc','id');
    }
}
