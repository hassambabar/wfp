<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class ProjectAreas extends Model
{
    //
    use CrudTrait;
    protected $table = 'project_areas';

    protected $fillable = [
        'project_id', 'district_id', 'tehsil_id', 'uc_id'
    ];

    public function project()
    {
        return $this->belongsTo(NgoProjects::class,'project_id','id');
    }

    public function district(){
        return $this->belongsTo(District::class,'district_id','id');
    }

    public function tehsil(){
        return $this->belongsTo(Tehsil::class,'tehsil_id','id');
    }

    public function uc(){
        return $this->belongsTo(Uc::class,'uc_id','id');
    }
}
