<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class ProjectThematicAreas extends Model
{
    //
    use CrudTrait;
    protected $table = 'project_thematic_areas';

    protected $fillable = [
        'project_id', 'thematic_area'
    ];

    public function project()
    {
        return $this->belongsTo(NgoProjects::class,'project_id','id');
    }

    public function thematicArea(){
        return $this->belongsTo(ThematicArea::class,'thematic_area','id');
    }
}
