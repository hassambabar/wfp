<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;// <---------------------- and this one
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use CrudTrait; // <----- this
    use HasRoles; // <------ and this
    use Notifiable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'users';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = [ 'name', 'email', 'password','status'];
     protected $hidden = [  'password', 'remember_token','email_verified_at'];
    // protected $dates = [];


    public function focalperson(){
         return $this->belongsTo('App\Models\Ngo_contact', 'id','ngo_id');
    }

    public function reporting(){
         return $this->belongsTo('App\Models\NgoReporting', 'id','ngo_id');
    }

    public function commentsNoc()
    {
        return $this->hasMany(Comment::class,'post_id')->where('type','noc')->whereNull('parent_id');
    }


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
