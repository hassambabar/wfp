<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class NgoOrgNatures extends Model
{
    use CrudTrait;
    protected $table = 'ngo_org_nature';

    protected $fillable = [
        'ngo_id', 'org_nature_id'
    ];

    public function ngo()
    {
        return $this->belongsTo(NgoProfile::class,'ngo_id','id');
    }

    public function natureOfOrg(){
        return $this->belongsTo(OrganizationNatures::class,'org_nature_id','id');
    }
}
