<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class ReliefCamp extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'relief_camps';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];


    // function rel_district(){
    //      return $this->belongsTo('App\Models\District', 'district_id', 'id');
    // }


    public function rel_uc()
    {
        return $this->belongsTo('App\Models\Uc', 'uc_id', 'id');

    }
    public function rel_district()
    {
        return $this->belongsTo('App\Models\District', 'district_id', 'id');

    }
    public function rel_tehsil()
    {
        return $this->belongsTo('App\Models\Tehsil', 'tehsil_id', 'id');

    }


    // public function Reliefcampcontact()
    // {
    //     return $this->hasMany('App\Models\Reliefcampcontact', 'relief_camp_id', 'id');

    // }

    // public function setContactAttribute($value)
    // {
    //   dd($value);
    // }

    public function getDrinkingWater(){ return ($this->has_drinking_water)?'Yes':'No'; }
    public function getElectricity(){ return ($this->has_electricity)?'Yes':'No'; }
    public function getWashroom(){ return ($this->has_electricity)?'Yes':'No'; }
    public function getMedicalAid(){ return ($this->has_medical_aid)?'Yes':'No'; }


    public function getcontact(){

        if(!empty($this->contact)){
             $contact = json_decode($this->contact);

           $retun = '<table>
           <tr>
             <th>Focal Person</th>
             <th>Designation</th>
             <th>Contact No</th>
           </tr>';
            foreach($contact as $item){
                $retun .=" <tr>
                  <td>$item->focal_person</td>
                  <td>$item->designation</td>
                  <td>$item->contact_no</td>
                </tr>
              "; 

            }
            $retun .='</table>';
            return $retun;
        }else{
            return '--';
        }
     

    }


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
