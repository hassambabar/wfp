<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class ProjectActivity extends Model
{
    //
    use CrudTrait;
    protected $table = 'project_activities';

    protected $fillable = [
        'project_activity', 'project_description', 'activity_start_date', 'activity_end_date','month_assistance','activity_province',
        'activity_district', 'activity_tehsil', 'activity_uc', 'activity_village','latitude','longitude'
    ];

    public function project()
    {
        return $this->belongsTo(NgoProjects::class,'project_id','id');
    }

    public function province(){
        return $this->belongsTo(Province::class,'project_province','id');
    }

    public function district(){
        return $this->belongsTo(District::class,'project_district','id');
    }

    public function tehsil(){
        return $this->belongsTo(Tehsil::class,'project_tehsil','id');
    }

    public function uc(){
        return $this->belongsTo(Uc::class,'project_uc','id');
    }
}
