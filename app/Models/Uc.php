<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Uc extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ucs';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getProvinceTitle() {
        if($this->province){
            return $this->province->name;
        }else{
            return '';
        }
     }
     public function getdistrictTitle() {
        if($this->district){
            return $this->district->name;
        }else{
            return '';
        }
     }
     public function gettehsilTitle() {
        if($this->tehsil){
            return $this->tehsil->name;
        }else{
            return '';
        }
     }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function province()
    {
        return $this->belongsTo('App\Province', 'province_id', 'id');

    }
    public function district()
    {
        return $this->belongsTo('App\District', 'district_id', 'id');

    }
    public function tehsil()
    {
        return $this->belongsTo('App\Tehsil', 'tehsil_id', 'id');

    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
