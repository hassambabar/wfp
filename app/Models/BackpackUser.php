<?php

namespace App\Models;

use App\User;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Spatie\Permission\Traits\HasRoles;// <---------------------- and this one
use Illuminate\Notifications\Notifiable;


class BackpackUser extends User
{
    use CrudTrait; // <----- this
    use Notifiable;

    protected $table = 'users';

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    public function focalperson(){
        return $this->belongsTo('App\Models\Ngo_contact', 'id','ngo_id');
   }

   public function reporting(){
        return $this->belongsTo('App\Models\NgoReporting', 'id','ngo_id');
   }

   public function district(){
    return $this->belongsTo('App\Models\District', 'district_id','id');
}
   public function partner(){
    return $this->belongsTo(ImplementingPartner::class,'implementing_partner','id');
}
    public function comment()
    {
        return $this->hasMany(Comment::class,'post_id')->where('type','profile')->whereNull('parent_id');
    }
    public function getSlugWithLink() {
        return '<a href="/admin/volunteer?ngo_id='.$this->id.'">Show</a>';
    }
    public function getcontactPDMALink() {
        return '<a href="/admin/contact_pdma?ngo_id='.$this->id.'">Show</a>';
    }
    public function ngo_contact() {
        return '<a href="/admin/ngo_contact?ngo_id='.$this->id.'">Show</a>';
    }
    public function ngo_affiliation() {
        return '<a href="/admin/affiliation?ngo_id='.$this->id.'">Show</a>';
    }
    public function ngo_Stocks() {
        return '<a href="/admin/stock?ngo_id='.$this->id.'">Show</a>';
    }
    public function ngo_HR() {
        return '<a href="/admin/human_resource?ngo_id='.$this->id.'">Show</a>';
    }

    public function getproject_modalityTitle() {
        if(!empty($this->reporting->modality)){
            return $this->reporting->modality->title;
        }else{
            return '';
        }
    }

    public function getprojectCategory() {
        if(!empty($this->reporting->category)){
            return $this->reporting->category->category_name;
        }else{
            return '';
        }
    }

    public function getprojectDonor() {
        if(!empty($this->reporting->fundingAgency)){
            return $this->reporting->fundingAgency->name;
        }else{
            return '';
        }
    }

    public function getprojectImplementingPartner() {
        if(!empty($this->reporting->implementingPartner)){
            return $this->reporting->implementingPartner->title;
        }else{
            return '';
        }
    }

    public function getprojectImplementingOrgtype() {
        if(!empty($this->reporting->implementingOrgType)){
            return $this->reporting->implementingOrgType->title;
        }else{
            return '';
        }
    }

}
