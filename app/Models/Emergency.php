<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Emergency extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'emergency';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function listEmergencyItems($crud = false)
    {
        return '<a class="btn btn-sm btn-link" target="_blank" href="'.backpack_url('emergency-item/list?emergency_id='.$this->id.'').'">View Items</a>';
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function district()
    {
        return $this->belongsTo('App\District', 'district_id','id');

    }
    public function tehsil()
    {
        return $this->belongsTo('App\Tehsil', 'tehsil_id');

    }

    public function Uc()
    {
        return $this->belongsTo('App\Uc', 'uc_id');

   }

    public function supportDistrict()
    {
        return $this->belongsTo('App\District', 'support_district_id','id');

    }
    public function supportTehsil()
    {
        return $this->belongsTo('App\Tehsil', 'support_tehsil_id');

    }

    public function supportUc()
    {
        return $this->belongsTo('App\Uc', 'support_uc_id');

    }

    public function hazardType(){
        return $this->belongsTo(HazardType::class, 'type_of_hazard');
    }

    public function emergencyItems(){
        return $this->hasMany(EmergencyItem::class,'emergency_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setDocumentAttribute($value)
    {
        $attribute_name = "document";
        $disk = "public";
        $destination_path = "emergency/documents";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }

    public function getTitle() {
        if($this->district_id){
            return strip_tags($this->district->name);
        }else{
            return '';
        }
    }
    public function getTitleUc() {
        if($this->uc_id){
            return strip_tags($this->Uc->name);
        }else{
            return '';
        }
    }
    public function getTitleTehsil() {
        if($this->tehsil){
            return strip_tags($this->tehsil->name);
        }else{
            return '';
        }
    }

}
