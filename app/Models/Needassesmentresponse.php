<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Needassesmentresponse extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'needassesmentresponses';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getHazardType(){
        $hazardType = isset($this->emergency->hazardType) ? $this->emergency->hazardType->name : ' null ';
        return $hazardType;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function ngo()
    {
        return $this->belongsTo('App\User', 'ngo_id','id');

    }
    public function ngofocal()
    {
        return $this->hasMany('App\Models\Ngo_contact', 'ngo_id','ngo_id');

    }
    public function emergency()
    {
        return $this->belongsTo('App\Models\Emergency', 'emergency_id','id');

    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function getTitleNgo() {
        if($this->ngo_id){
            return '<a href="user/'.$this->ngo->id.'/edit">'.$this->ngo->name.' - '.$this->ngo->email.'</a>';
        }else{
            return '';
        }
    }
    public function getTitleNgoFocalPerson() {

   //   dd($this->ngofocal);
        if($this->ngo_id){
            if($this->ngofocal){

            foreach ($this->ngofocal as $key => $value) {
              //  dd($value);
                $returndata = $value->designation.' '.$value->contact_person_name.' - '.$value->email.' - '.$value->cell.'<br>.';
            }
            return $returndata;
        }else{
            return '';
        }
        }else{
            return '';
        }
    }
    public function getEmergencydistrict() {
        if($this->emergency_id){
            // dd($this->emergency->type_of_emergency);
            if($this->emergency){
                 return $this->emergency->district->name;

            }else{
                return '';

            }
        }else{
            return '';
        }
    }
    public function getTitleEmergency() {
        if($this->emergency_id){
            // dd($this->emergency->type_of_emergency);
            if($this->emergency){
                 return '<a href="emergency/'.$this->emergency->id.'/edit">'.$this->emergency->type_of_emergency.' - '.$this->getHazardType().'</a>';

            }else{
                return '';

            }
        }else{
            return '';
        }
    }

    public function getEmergencyItem() {
        if($this->emergency_item_id){
            // dd($this->emergency->type_of_emergency);
            if($this->emergency){
                return '<a href="'.backpack_url("emergency-item/$this->emergency_item_id/show").'" target="_blank">View Item</a>';

            }else{
                return '';

            }
        }else{
            return '';
        }
    }

}
