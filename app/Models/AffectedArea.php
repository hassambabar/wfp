<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class AffectedArea extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'affected_areas';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];


    public function rel_province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id', 'id');

    }

     public function rel_district()
    {
        return $this->belongsTo('App\Models\District', 'district_id', 'id');

    }
    public function district()
    {
        return $this->belongsTo('App\District', 'affected_district','id');

    }
    public function tehsil()
    {
        return $this->belongsTo('App\Tehsil', 'affected_talukas','id');

    }

    

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function getTitleTehsil() {
        if($this->affected_talukas){
            return strip_tags($this->tehsil->name);
        }else{
            return '';
        }
    }
    public function getdistrictTitle() {
        if($this->affected_district){
            return strip_tags($this->district->name);
        }else{
            return '';
        }
    }
    public function getProvince() {
        if($this->province_id){
            return strip_tags($this->rel_province->name);
        }else{
            return '';
        }
    }
}
