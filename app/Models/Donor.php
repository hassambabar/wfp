<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'ngo_projects';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getNgoName() {
        if($this->ngo){
            return $this->ngo->name;
        }else{
            return '';
        }
     }

     public function getdistrictTitle() {
        if($this->district){
            return $this->district->name;
        }else{
            return '';
        }
     }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function ngo()
    {
        return $this->belongsTo('App\User','ngo_id','id');
    }

    public function activities()
    {
        return $this->hasMany(ProjectActivity::class,'project_id');
    }

    public function province(){
        return $this->belongsTo(Province::class,'project_province','id');
    }

    public function district(){
        return $this->belongsTo(District::class,'project_district','id');
    }

    public function tehsil(){
        return $this->belongsTo(Tehsil::class,'project_tehsil','id');
    }

    public function uc(){
        return $this->belongsTo(Uc::class,'project_uc','id');
    }

    public function fundingAgency(){
        return $this->belongsTo(FundingAgency::class,'donor','id');
    }

    public function thematicArea(){
        return $this->belongsTo(ThematicArea::class,'thematic_area','id');
    }

    public function noc(){
        return $this->hasMany(ProjectsNoc::class,'project_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
