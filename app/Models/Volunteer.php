<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'volunteers';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getdistrictTitle() {
        if($this->district){
            return $this->district->name;
        }else{
            return '';
        }
     }
     public function gettehsilTitle() {
        if($this->tehsil){
            return $this->tehsil->name;
        }else{
            return '';
        }
     }

     public function getNgoName() {
        if($this->ngo){
            return $this->ngo->name;
        }else{
            return '';
        }
     }
     public function getUcTitle() {
        if($this->uc){
            return $this->uc->name;
        }else{
            return '';
        }
     }

     public function getImageUrl(){
        if($this->image){
            return '<a target="_blank" href="'.url($this->image).'">View Image</a>';
        }else{
            return '--';
        }
     }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function district()
    {
        return $this->belongsTo('App\District', 'district_id', 'id');

    }
    public function tehsil()
    {
        return $this->belongsTo('App\Tehsil', 'tehsil_id', 'id');

    }
    public function uc()
    {
        return $this->belongsTo('App\Models\Uc', 'uc_id', 'id');

    }
    public function ngo()
    {
        return $this->belongsTo('App\User', 'ngo_id', 'id');

    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */



}
