<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'stocks';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getNgoName() {
        if($this->ngo){
            return $this->ngo->name;
        }else{
            return '';
        }
     }

     public function getCatName() {
        if($this->category){
            return $this->category->name;
        }else{
            return '';
        }
     }

     public function getItemName() {
        if($this->type){
            return $this->type->name;
        }else{
            return '';
        }
     }
     public function getlocationName() {
        if($this->location){
            return $this->location->name;
        }else{
            return '';
        }
     }
    public function getptehsilTitle() {
        if($this->tehsil){
            return $this->tehsil->name;
        }else{
            return '';
        }
    }
    public function getucTitle() {
        if($this->uc){
            return $this->uc->name;
        }else{
            return '';
        }
    }
    public function getdistrictTitle() {
        if($this->district){
            return $this->district->name;
        }else{
            return '';
        }
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function ngo()
    {
        return $this->belongsTo('App\User', 'ngo_id', 'id');

    }
    public function category()
    {
        return $this->belongsTo('App\Models\Category_type', 'category_id', 'id');

    }

    public function type()
    {
        return $this->belongsTo('App\Models\Item_type', 'item_type_id', 'id');

    }
    public function location()
    {
        return $this->belongsTo('App\Models\Province', 'location_id', 'id');

    }

    public function district(){
        return $this->belongsTo(District::class,'district_id','id');
    }

    public function tehsil(){
        return $this->belongsTo(Tehsil::class,'tehsil_id','id');
    }

    public function uc(){
        return $this->belongsTo(Uc::class,'uc_id','id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
