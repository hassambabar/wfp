<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Uc extends Model
{
    //
    use CrudTrait;

    public function tehsil()
    {
        return $this->belongsTo('App\Models\Tehsil', 'tehsil_id', 'id');

    }

}
