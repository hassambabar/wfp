<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Stocks extends Model
{
    //
    use CrudTrait;

    protected $fillable = [
        'name', 'category_id', 'quantity', 'item_type_id', 'availability', 'location_id', 'remarks','ngo_id','district_id','tehsil_id','uc_id','office_warehouse_address'
    ];


    public function category()
    {
        return $this->belongsTo('App\Models\Category_type', 'category_id','id');
    }
    public function type()
    {
        return $this->belongsTo('App\Models\Item_type', 'item_type_id', 'id');

    }
    public function location()
    {
        return $this->belongsTo('App\Models\Province', 'location_id', 'id');

    }

    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id', 'id');

    }

    public function tehsil()
    {
        return $this->belongsTo('App\Models\Tehsil', 'tehsil_id', 'id');

    }

    public function uc()
    {
        return $this->belongsTo('App\Models\Uc', 'uc_id', 'id');

    }
}
