<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Volunteer extends Model
{
    use CrudTrait;
    protected $fillable = [
        'f_name', 'l_name', 'cnic', 'email', 'age', 'gender', 'district_id', 'tehsil_id', 'uc_id','contact_no','address','address_org','availability','area_of_expertise','physical_limitation','ngo_id','image','whats_app','name_of_ngo','email_of_org','contact_of_org_focal_person','education','occupation','status','availability_end_date'
    ];

    public function district()
    {
        return $this->belongsTo('App\District', 'district_id','id');

    }
    public function tehsil()
    {
        return $this->belongsTo('App\Tehsil', 'tehsil_id');

    }

    public function Uc()
    {
        return $this->belongsTo('App\Uc', 'uc_id');

    }
}
