<?php

namespace App\Http\Controllers;

use App\Models\NgoProfile;
use Illuminate\Http\Request;
use Auth;
use App\Models\Riskmanagement;

class RiskManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Volunteers.');
        }
        $Riskmanagement = Riskmanagement::where('ngo_id', $userId)->get();
        return view('Riskmanagement.index', compact('Riskmanagement','ngoProfile'));
    }

    public function create()
    {

        return view('Riskmanagement.add');
    }




    public function store(Request $request)
    {
         $request->validate([
            'title' => 'required',
            'subject' => 'required',
            'description' => 'required',
            ]);
        $userId = Auth::id();

        $attach_file = $request['image'];
        $image_attachments = array();
        if (!empty($request['image'])) {
           foreach ($attach_file as $key => $value) {

            $destinationPath = 'uploads/risk-management';
            $filename = time().'-'.$value->getClientOriginalName();
            $value->move($destinationPath, $filename);

            $image_attachments[] = $filename;
           }
         //  $MinutesOfMeeting->attachments = serialize($image_attachments);
       }

       $document_file = $request['document'];
       $document_attachments = array();
       if (!empty($request['document'])) {
          foreach ($document_file as $key => $value) {

           $destinationPath = 'uploads/risk-management/document';
           $filename = time().'-'.$value->getClientOriginalName();
           $value->move($destinationPath, $filename);

           $document_attachments[] = $filename;
          }
        //  $MinutesOfMeeting->attachments = serialize($image_attachments);
      }



        $Riskmanagement = new Riskmanagement([
            'title' => $request->get('title'),
            'subject' => $request->get('subject'),
            'description' => $request->get('description'),
            'image' => serialize($image_attachments),
            'document' => serialize($document_attachments),

            'ngo_id' => $userId,
        ]);
       $Riskmanagement->save();

       return redirect('/risk-management')->with('message', 'Saved!');
    }



    public function delete($id){

        $userId = Auth::id();
        $Riskmanagement = Riskmanagement::where('id', $id)->where('ngo_id',$userId)->first();
        if($Riskmanagement){
            $Human_resourceDelete = Riskmanagement::find($Riskmanagement->id);
            $Human_resourceDelete->delete();

        }

        return redirect('/risk-management')->with('message', 'Deleted successfully!');
      }

      public function show($id)
      {
          $userId = Auth::id();

          $Riskmanagement = Riskmanagement::where('id',$id)->first();
          $image = !empty($Riskmanagement->image) ? unserialize($Riskmanagement->image) : '';
          $document = !empty($Riskmanagement->document) ? unserialize($Riskmanagement->document) : '';

          return view('Riskmanagement.Show',compact('Riskmanagement','image','document'));
      }

}
