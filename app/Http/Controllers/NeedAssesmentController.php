<?php

namespace App\Http\Controllers;

use App\Models\EmergencyItem;
use Illuminate\Http\Request;
use Auth;
use App\Models\Emergency;
use App\Models\Needassesmentresponse;
use App\Notifications\EmergenciesRespond;
use Illuminate\Support\Facades\Notification;
use App\User;

class NeedAssesmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $Emergencies = Emergency::orderBy("id", "desc")->get();
        foreach ($Emergencies as $key => $value) {
            $Needassesmentresponse = Needassesmentresponse::where('emergency_id',$value->id)->where('ngo_id',$userId)->first();
            if($Needassesmentresponse){
                $Emergencies[$key]->Needassesmentresponse = 1;
            }else{
                $Emergencies[$key]->Needassesmentresponse = 0;
            }

          }
        return view('NeedAssesment.index', compact('Emergencies'));
    }
    public function show($id)
    {
        $userId = Auth::id();

        $EmergencyItem = EmergencyItem::where('id',$id)->first();
        if(!$EmergencyItem){
            return redirect('/need-assesment')->with('message', 'error|No record found.');
        }
        $Needassesmentresponse = Needassesmentresponse::where('emergency_item_id',$EmergencyItem->id)->where('ngo_id',$userId)->first();

        return view('NeedAssesment.Show',compact('EmergencyItem','Needassesmentresponse'));
    }

    public function needassesmentresponses(Request $request)
    {
        $request->validate([
            'response' => 'required',
            'emergency_id' => 'required',
            'emergency_item_id' => 'required',

        ]);
        $userId = Auth::id();

        $Needassesmentresponse = new Needassesmentresponse([
            'response' => $request->get('response'),
            'response_quantity' => $request->get('response_quantity'),
            'emergency_id' =>  $request->get('emergency_id'),
            'emergency_item_id' =>  $request->get('emergency_item_id'),
            'ngo_id' => $userId,
        ]);
       $Needassesmentresponse->save();

       $users = User::select("users.*")
       ->join("model_has_roles","model_has_roles.model_id","=","users.id")->where('model_has_roles.role_id',4)->where('users.status',1)
       ->get();

        Notification::send($users, new EmergenciesRespond($Needassesmentresponse));

       return redirect('/need-assesment')->with('success', 'Saved!');
    }



}
