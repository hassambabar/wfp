<?php

namespace App\Http\Controllers;

use App\Models\Emergency;
use App\Models\NgoProjects;
use App\Models\Tehsil;
use App\Models\Uc;
use App\Models\User;
use App\Notifications\EmergenciesCreated;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;

class AjaxController extends Controller
{
    public function getTehsils($id){

        // Fetch Tehsil by district_id
        $tehsils = Tehsil::where('district_id',$id)->get()->map(function($item, $index) {
            return [
                "id" => $item["id"],
                "name" => $item["name"]
            ];
        });
        return response()->json($tehsils);

    }

    public function getTehsilsBackpack(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = Tehsil::query();

        // if no category has been selected, show no options
        if (! $form['district_id']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['district_id']) {
            $options = $options->where('district_id', $form['district_id']);
        }

        if ($search_term) {
            $results = $options->where('name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $options->paginate(10);
    }

    public function getUcsBackpack(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = Uc::query();

        // if no category has been selected, show no options
        if (! $form['tehsil_id']) {
            return [];
        }

        // if a category has been selected, only show articles in that category
        if ($form['tehsil_id']) {
            $options = $options->where('tehsil_id', $form['tehsil_id']);
        }

        if ($search_term) {
            $results = $options->where('name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $options->paginate(10);
    }

    public function getUcs($id){

        // Fetch Tehsil by district_id
        $ucs = Uc::where('tehsil_id',$id)->get()->map(function($item, $index) {
            return [
                "id" => $item["id"],
                "name" => $item["name"]
            ];
        });
        return response()->json($ucs);

    }

    public function sendEmgNotification(Request $request){
        $eid = $request->input('emgID');
        if ($eid){
            $emergency = Emergency::find($eid);
            $users = User::select("users.*")
                ->join("model_has_roles","model_has_roles.model_id","=","users.id")->where('model_has_roles.role_id',1)
                ->where('status', 1)
                ->get();

            Notification::send($users, new EmergenciesCreated($emergency));
        }

    }

    public function getProjectDetail($id){

        // Fetch Tehsil by district_id
        $project = NgoProjects::where('id',$id)->first();
        return response()->json($project);

    }

    public function showSummaryReport($id){
        $project = NgoProjects::where('id', $id)->with('reports')->get()->first();
        $html = '<h5>No reports found.</h5>';
        if($project && $project->reports->count() > 0){
            $html = '<div class="row">';
            $html .= '<div class="col-md-6"><p><b>Total Reports:</b> '.$project->reports->count().'</p></div>';
            $html .= '<div class="col-md-6"><p><b>Project Target:</b> '.$project->target_number.'</p></div>';
            $html .= '<div class="col-md-6"><p><b>Targets Achieved:</b> '.$project->reports->sum('achieved_number').'</p></div>';
            $html .= '<div class="col-md-6"><p><b>Total Families Targeted:</b> '.$project->reports->sum('families_targeted').'</p></div>';
            $html .= '<div class="col-md-6"><p><b>Total Women Targeted:</b> '.$project->reports->sum('women_targeted').'</p></div>';
            $html .= '<div class="col-md-6"><p><b>Total Men Targeted:</b> '.$project->reports->sum('men_targeted').'</p></div>';
            $html .= '<div class="col-md-6"><p><b>Total Girls Targeted:</b> '.$project->reports->sum('girls_targeted').'</p></div>';
            $html .= '<div class="col-md-6"><p><b>Total Boys Targeted:</b> '.$project->reports->sum('boys_targeted').'</p></div>';
            $html .= '<div class="col-md-12"><hr></div>';
            $html .= '<div class="col-md-6"><p><b>Total Disabled Women:</b> '.$project->reports->sum('disabled_women_targeted').'</p></div>';
            $html .= '<div class="col-md-6"><p><b>Total Disabled Men:</b> '.$project->reports->sum('disabled_men_targeted').'</p></div>';
            $html .= '<div class="col-md-6"><p><b>Total Disabled Boys:</b> '.$project->reports->sum('disabled_boys_targeted').'</p></div>';
            $html .= '<div class="col-md-6"><p><b>Total Disabled Girls:</b> '.$project->reports->sum('disabled_girls_targeted').'</p></div>';
            $html .= '</div>';
        }
        return $html;
    }

    public function acceptTerms($id){
        if($id){
            $user = User::find($id);
            $user->terms_accepted = 1;
            $user->save();
        }
    }
}
