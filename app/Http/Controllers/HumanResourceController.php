<?php

namespace App\Http\Controllers;

use App\Models\NgoProfile;
use Illuminate\Http\Request;
use Auth;
use App\Models\Human_resource;

class HumanResourceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Organization HR.');
        }
        $Human_resource = Human_resource::where('ngo_id', $userId)->get();
        return view('Human_resource.index', compact('Human_resource','ngoProfile'));
    }

    public function create()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Organization HR.');
        }
        return view('Human_resource.add');
    }

    public function edit($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Organization HR.');
        }
        $Human_resource = Human_resource::where('id', $id)->first();
        return view('Human_resource.Edit', compact('Human_resource'));
    }

    public function show($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Organization HR.');
        }
        $Human_resource = Human_resource::where('id', $id)->first();
        $Human_resource->nationality = $Human_resource->nationality == 'other' ? 'Foreign National' : 'pakistani';
        return view('Human_resource.show', compact('Human_resource','ngoProfile'));
    }


    public function store(Request $request)
    {
         $request->validate([
            'name' => 'required',
             'cnic' => 'required_if:nationality,pakistani',
            'email' => 'required',
            'phone_no' => 'required',
            ]);
        $userId = Auth::id();

        $Human_resource = new Human_resource([
            'name' => $request->get('name'),
            // 'total_national_staff' => $request->get('total_national_staff'),
            // 'total_frn_national_staff' => $request->get('total_frn_national_staff'),
            'nationality' => $request->get('nationality'),
            'cnic' => $request->get('cnic'),
            'email' => $request->get('email'),
            'phone_no' => $request->get('phone_no'),
            'father_name' => $request->get('father_name'),
            'address' => $request->get('address'),
            'temp_address' => $request->get('temp_address'),
            'country_home_add' => $request->get('country_home_add'),
            'address_in_pakistan' => $request->get('address_in_pakistan'),
            'ngo_id' => $userId,
        ]);

       $Human_resource->save();

       if($request->hasFile('passport') && $request->file('passport')->isValid()){
           $destinationPath = 'uploads/hr/document';
           $filename = time().'-'.$request->file('passport')->getClientOriginalName();
           $request->file('passport')->move($destinationPath, $filename);
           $Human_resource->passport = $destinationPath.'/'.$filename;
           $Human_resource->update();
       }
       if($request->submit_type && $request->submit_type == 'save_new'){
           return redirect('/human-resource/create')->with('message', 'Saved!');
       }
       return redirect('/human-resource')->with('message', 'Saved!');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'cnic' => 'required_if:nationality,pakistani',
            'email' => 'required',
            'phone_no' => 'required',
            ]);

        $Human_resource = Human_resource::find($id);
        $Human_resource->name =  $request->get('name');
        // $Human_resource->total_national_staff =  $request->get('total_national_staff');
        // $Human_resource->total_frn_national_staff =  $request->get('total_frn_national_staff');
        $Human_resource->nationality =  $request->get('nationality');
        $Human_resource->cnic =  $request->get('cnic');
        $Human_resource->email =  $request->get('email');
        $Human_resource->phone_no =  $request->get('phone_no');
        $Human_resource->father_name =  $request->get('father_name');
        $Human_resource->address =  $request->get('address');
        $Human_resource->temp_address =  $request->get('temp_address');
        $Human_resource->country_home_add =  $request->get('country_home_add');
        $Human_resource->address_in_pakistan =  $request->get('address_in_pakistan');
        $Human_resource->save();

        if($request->hasFile('passport') && $request->file('passport')->isValid()){
            $destinationPath = 'uploads/hr/document';
            $filename = time().'-'.$request->file('passport')->getClientOriginalName();
            $request->file('passport')->move($destinationPath, $filename);
            $Human_resource->passport = $destinationPath.'/'.$filename;
            $Human_resource->update();
        }

        return redirect('/human-resource')->with('message', 'Update successfully!');
    }

    public function delete($id){

        $userId = Auth::id();
        $Human_resource = Human_resource::where('id', $id)->where('ngo_id',$userId)->first();
        if($Human_resource){
            $Human_resourceDelete = Human_resource::find($Human_resource->id);
            $Human_resourceDelete->delete();

        }

        return redirect('/human-resource')->with('message', 'Deleted successfully!');    }
}
