<?php

namespace App\Http\Controllers;

use App\Models\NgoProfile;
use Illuminate\Http\Request;
use App\Volunteer;
use Auth;

use App\District;
use App\Tehsil;
use App\Uc;

class VolunteersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Volunteers.');
        }
        $Volunteers = Volunteer::where('ngo_id', $userId)->get();
        return view('volunteers.index', compact('Volunteers','ngoProfile'));
    }

    public function create()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Volunteers.');
        }
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Uc = Uc::where('province_id',1)->get();
        return view('volunteers.add', compact('District','Tehsil','Uc'));
    }

    public function show($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Stocks.');
        }
        $volunteers = Volunteer::where('id', $id)->first();
        return view('volunteers.show', compact('volunteers','ngoProfile'));
    }

    public function edit($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Volunteers.');
        }
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Uc = Uc::where('province_id',1)->get();
        $volunteers = Volunteer::where('id', $id)->first();
        return view('volunteers.Edit', compact('volunteers','District','Tehsil','Uc'));
    }


    public function store(Request $request)
    {
         $request->validate([
            'f_name' => 'required',
            'l_name' => 'required',
            'cnic' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'district_id' => 'required',
            'contact_no' => 'required',
            ]);
        $userId = Auth::id();

        $Volunteer = new Volunteer([
            'f_name' => $request->get('f_name'),
            'l_name' => $request->get('l_name'),
            'cnic' => $request->get('cnic'),
            'email' => $request->get('email'),
            'age' => $request->get('age'),
            'gender' => $request->get('gender'),
            'district_id' => $request->get('district_id'),
            'tehsil_id' => $request->get('tehsil_id'),
            'uc_id' => $request->get('uc_id'),
            'contact_no' => $request->get('contact_no'),
            'address' => $request->get('address'),
            'address_org' => $request->get('address_org'),
            'availability' => $request->get('availability'),
            'availability_end_date' => $request->get('availability_end_date'),
            'area_of_expertise' => $request->get('area_of_expertise'),
            'physical_limitation' => $request->get('physical_limitation'),
            'whats_app' => $request->get('whats_app'),
            'name_of_ngo' => $request->get('name_of_ngo'),
            'email_of_org' => $request->get('email_of_org'),
            'contact_of_org_focal_person' => $request->get('contact_of_org_focal_person'),
            'education' => $request->get('education'),
            'occupation' => $request->get('occupation'),
            'status' => $request->get('status'),
            'ngo_id' => $userId,
        ]);
       $Volunteer->save();


        try{
            if($Volunteer){
                $disks = 'uploads_dir';
                $fileName = '';
                if ($request->hasFile('image') && $request->file('image')->isValid()) {
                    /*$name = $request->file('image');
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                    $fileSystem = \Storage::disk($disks);
                    $filePath = 'uploads/volunteers/' . $Volunteer->id . '/contents/' . $fileName;
                    $fileSystem->put($filePath, file_get_contents($request->file('image')), 'public');
                    $Volunteer->image = $filePath;
                    $Volunteer->update();*/

                    $name = $request->file('image');
                    $destinationPath = 'uploads/volunteers/' . $Volunteer->id . '/contents/';
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                    $request->file('image')->move($destinationPath, $fileName);
                    $Volunteer->image = $destinationPath.'/'.$fileName;
                    $Volunteer->update();
                }
            }
        }catch (\Exception $e){
            return redirect('volunteers/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }

        if($request->submit_type && $request->submit_type == 'save_new'){
            return redirect('/volunteers/create')->with('message', 'Saved!');
        }
       return redirect('/volunteers')->with('message', 'Saved!');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'f_name' => 'required',
            'l_name' => 'required',
            'cnic' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'district_id' => 'required',
            'contact_no' => 'required',
            ]);

        $Volunteer = Volunteer::find($id);
        $Volunteer->f_name =  $request->get('f_name');
        $Volunteer->l_name =  $request->get('l_name');
        $Volunteer->cnic =  $request->get('cnic');
        $Volunteer->email =  $request->get('email');
        $Volunteer->age =  $request->get('age');
        $Volunteer->gender =  $request->get('gender');
        $Volunteer->district_id =  $request->get('district_id');
        $Volunteer->tehsil_id =  $request->get('tehsil_id');
        $Volunteer->uc_id =  $request->get('uc_id');
        $Volunteer->contact_no =  $request->get('contact_no');
        $Volunteer->address =  $request->get('address');
        $Volunteer->address_org =  $request->get('address_org');
        $Volunteer->availability =  $request->get('availability');
        $Volunteer->availability_end_date =  $request->get('availability_end_date');
        $Volunteer->area_of_expertise =  $request->get('area_of_expertise');
        $Volunteer->physical_limitation =  $request->get('physical_limitation');
        $Volunteer->whats_app =  $request->get('whats_app');
        $Volunteer->name_of_ngo =  $request->get('name_of_ngo');
        $Volunteer->email_of_org =  $request->get('email_of_org');
        $Volunteer->contact_of_org_focal_person =  $request->get('contact_of_org_focal_person');
        $Volunteer->education =  $request->get('education');
        $Volunteer->occupation =  $request->get('occupation');
        $Volunteer->status =  $request->get('status');

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            /*$name = $request->file('image');
            $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
            $fileSystem = \Storage::disk($disks);
            $filePath = 'uploads/volunteers/' . $Volunteer->id . '/contents/' . $fileName;
            $fileSystem->put($filePath, file_get_contents($request->file('image')), 'public');
            $Volunteer->image = $filePath;
            $Volunteer->update();*/

            $name = $request->file('image');
            $destinationPath = 'uploads/volunteers/' . $Volunteer->id . '/contents/';
            $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
            $request->file('image')->move($destinationPath, $fileName);
            $Volunteer->image = $destinationPath.'/'.$fileName;
            $Volunteer->update();
        }

        $Volunteer->save();
        return redirect('/volunteers')->with('message', 'Update successfully!');
    }

    public function delete($id){

        $userId = Auth::id();
        $Volunteer = Volunteer::where('id', $id)->where('ngo_id',$userId)->first();
        if($Volunteer){
            $VolunteerDelete = Volunteer::find($Volunteer->id);
            $VolunteerDelete->delete();

        }

        return redirect('/volunteers')->with('message', 'Deleted successfully!');
    }
}
