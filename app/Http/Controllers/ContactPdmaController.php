<?php

namespace App\Http\Controllers;

use App\Models\NgoProfile;
use Illuminate\Http\Request;
use Auth;
use App\Models\Contact_pdma;

class ContactPdmaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding PDMA Contacts.');
        }
        $Contact_pdma = Contact_pdma::where('ngo_id', $userId)->get();
        return view('Contact_pdma.index', compact('Contact_pdma','ngoProfile'));
    }

    public function create()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding PDMA Contacts.');
        }
        return view('Contact_pdma.add');
    }

    public function edit($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding PDMA Contacts.');
        }
        $Contact_pdma = Contact_pdma::where('id', $id)->first();
        return view('Contact_pdma.Edit', compact('Contact_pdma'));
    }


    public function store(Request $request)
    {
         $request->validate([
            'cell_1' => 'required',
            'email_1' => 'required',

            ]);
        $userId = Auth::id();

        $Contact_pdma = new Contact_pdma([
            'cell_1' => $request->get('cell_1'),
            'cell_2' => $request->get('cell_2'),
            'email_1' => $request->get('email_1'),
            'email_2' => $request->get('email_2'),
            'ngo_id' => $userId,
        ]);
       $Contact_pdma->save();
       return redirect('/contacpdma')->with('message', 'Saved!');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'cell_1' => 'required',
            'email_1' => 'required',
            ]);

        $Contact_pdma = Contact_pdma::find($id);
        $Contact_pdma->cell_1 =  $request->get('cell_1');
        $Contact_pdma->cell_2 =  $request->get('cell_2');
        $Contact_pdma->email_1 =  $request->get('email_1');
        $Contact_pdma->email_2 =  $request->get('email_2');

        $Contact_pdma->save();
        return redirect('/contacpdma')->with('message', 'Update successfully!');
    }

    public function delete($id){

        $userId = Auth::id();
        $Contact_pdma = Contact_pdma::where('id', $id)->where('ngo_id',$userId)->first();
        if($Contact_pdma){
            $Contact_pdmaDelete = Contact_pdma::find($Contact_pdma->id);
            $Contact_pdmaDelete->delete();

        }

        return redirect('/contacpdma')->with('message', 'Deleted successfully!');    }
}
