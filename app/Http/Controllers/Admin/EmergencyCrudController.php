<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EmergencyRequest;
use App\Models\HazardType;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Notifications\EmergenciesCreated;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\District;
use App\Tehsil;
use App\Uc;

/**
 * Class EmergencyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EmergencyCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Emergency::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/emergency');
        CRUD::setEntityNameStrings('Hazards & Emergencies', 'Hazards & Emergencies');
        $this->addCustomCrudFilters();
        $this->crud->enableExportButtons();

        // Colums
        $this->crud->addColumn([
            'name'  => 'created_at',
            'label' => 'Date of Emergency Occurrence',
            'type'  => 'date',
        ]);
        $this->crud->addColumn([
			'name'  => 'type_of_emergency',
            'label' => 'Level of Hazard/Emergency',
            'limit'  => 100,
        ]);
        $this->crud->addColumn([
			'name'  => 'type_of_hazard',
            'label' => 'Type of Hazard/Emergency',
            'type'         => 'relationship',
            'entity'    => 'hazardType',
        ]);

         $this->crud->addColumn([
			'name'  => 'district_id',
            'label' => 'District',
            'type'  => 'model_function',
            'function_name' => 'getTitle',
        ]);

        $this->crud->addColumn([
			'name'  => 'tehsil_id',
            'label' => 'Tehsil',
            'type'  => 'model_function',
            'function_name' => 'getTitleTehsil',
        ]);
        $this->crud->addColumn([
			'name'  => 'uc_id',
            'label' => 'UC',
            'type'  => 'model_function',
            'function_name' => 'getTitleUc',
        ]);

        $this->crud->addColumn([
            'name'  => 'affected_population',
            'label' => 'Estimated Affected Persons',
        ]);
        $this->crud->addColumn([
            'name'  => 'affected_hhs',
            'label' => 'Estimated Affected HHs',
        ]);
        $this->crud->addColumn([
            'name'  => 'affected_livestock',
            'label' => 'Estimated Affected Livestock',
        ]);
        $this->crud->addColumn([
            'name'  => 'area_affected',
            'label' => 'Estimated Affected Area',
        ]);
        $this->crud->addColumn([
            'name'  => 'when_needed',
            'label' => 'When Needed',
        ]);

        // Main Information

        $this->crud->addField([
            'name'      => 'type_of_emergency',
            'label'      => 'Level of Hazard/Emergency',
            'tab'             => 'Hazards & Emergencies Information',
            'type'        => 'select2_from_array',
            'options'         => ['low' => 'Low','medium' => 'Medium','high' => 'High'],
            'allows_null'     => false,
            'default' => 'low',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'type_of_hazard',
            'tab'             => 'Hazards & Emergencies Information',
            'label'      => 'Type of Hazard/Emergency',
            'type'        => 'select2_from_array',
            'options'     => $this->getHazardTypes(),
            'allows_null'     => false,
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'district_id',
            'tab'             => 'Hazards & Emergencies Information',
            'type'        => 'select2_from_array',
            'options'     => $this->Getdistrict(),
            'allows_null'     => false,
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        /*$this->crud->addField([
            'name'      => 'tehsil_id',
            'tab'             => 'Main Information',
            'type'        => 'select2_from_array',
            'options'     => $this->GetTehsil(),
            'allows_null'     => false,
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);*/
        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_ajax',
            'label' => 'Taluka',
            'tab'             => 'Hazards & Emergencies Information',
            'name'      => 'tehsil_id',
            'data_source' => url("admin/api/getTehsils"),
            'dependencies'            => ['district_id'], // when a dependency changes, this select2 is reset to null
            'include_all_form_fields' => true,
            'allows_null'     => false,
            'placeholder'             => "Select a Taluka",
            'minimum_input_length'    => 0,
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'uc_id',
            'tab'             => 'Hazards & Emergencies Information',
            'type'        => 'select2_from_ajax',
            'data_source' => url("admin/api/getUcs"),
            'allows_null'     => false,
            'dependencies'            => ['tehsil_id'], // when a dependency changes, this select2 is reset to null
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'include_all_form_fields' => true,
            'placeholder'             => "Select a UC",
            'minimum_input_length'    => 0,
        ]);
        /*$this->crud->addField([
            'name'      => 'taluka_id',
            'tab'             => 'Main Information',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);*/
        $this->crud->addField([
            'name'      => 'village',
            'tab'             => 'Hazards & Emergencies Information',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([
            'name'      => 'affected_population',
            'label'      => 'Estimated Affected Persons',
            'tab'             => 'Hazards & Emergencies Information',
            'type'        => 'select2_from_array',
            'options'         => ['Household' => 'Household'],
            'allows_null'     => true,
            'default' => 'Household',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'affected_hhs',
            'label'      => 'Estimated Affected HHs',
            'tab'             => 'Hazards & Emergencies Information',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'affected_livestock',
            'label'      => 'Estimated Affected Livestock',
            'tab'             => 'Hazards & Emergencies Information',
            'type'        => 'select2_from_array',
            'options'         => ['small livestock' => 'Small livestock','large livestock' => 'Large livestock'],
            'allows_null'     => false,
            'default' => 'small livestock',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'area_affected',
            'label'      => 'Estimated Affected Area',
            'tab'             => 'Hazards & Emergencies Information',
            'allows_null'     => true,
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        /*$this->crud->addField([
            'name'      => 'document',
            'tab'             => 'Main Information',
            'type'      => 'upload',
            'upload'    => true,
            'disk'      => 'uploads',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);*/
        $this->crud->addField([
            'name'      => 'description',
            'tab'             => 'Hazards & Emergencies Information',
            'type'          => 'ckeditor',

            'wrapperAttributes' => ['class' => 'form-group col-md-12'],
        ]);
        // Emergency Support/Response Requirement
        $this->crud->addField([   // CustomHTML
            'name'  => 'heading',
            'type'  => 'custom_html',
            'tab'             => 'Emergency Support/Response Requirement',
            'value' => '<h3>Location/camp where emergency support is required</h3>'
        ])->beforeField('support_district_id');
        $this->crud->addField([
            'name'      => 'support_district_id',
            'label'     => 'District',
            'tab'             => 'Emergency Support/Response Requirement',
            'type'        => 'select2_from_array',
            'options'     => $this->Getdistrict(),
            'allows_null'     => false,
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'support_tehsil_id',
            'label'     => 'Tehsil/Taluka',
            'tab'       => 'Emergency Support/Response Requirement',
            'type'        => 'select2_from_array',
            'options'     => $this->GetTehsil(),
            'allows_null'     => true,
            'default' => 'one',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        /*$this->crud->addField([
            'name'      => 'support_tehsil_id',
            'tab'       => 'Emergency Support/Response Requirement',
            'label' => 'Tehsil/Taluka',
            'data_source' => url("admin/api/getTehsils"),
            'dependencies'            => ['support_district_id'], // when a dependency changes, this select2 is reset to null
            'include_all_form_fields' => true,
            'allows_null'     => false,
            'placeholder'             => "Select a Tehsil/Taluka",
            'minimum_input_length'    => 0,
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'support_uc_id',
            'label'     => 'UC',
            'tab'       => 'Emergency Support/Response Requirement',
            'type'        => 'select2_from_ajax',
            'data_source' => url("admin/api/getUcs"),
            'allows_null'     => false,
            'dependencies'            => ['support_tehsil_id'], // when a dependency changes, this select2 is reset to null
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'include_all_form_fields' => true,
            'placeholder'             => "Select a UC",
            'minimum_input_length'    => 0,
        ]);*/
        $this->crud->addField([
            'name'      => 'support_uc_id',
            'label'     => 'UC',
            'tab'       => 'Emergency Support/Response Requirement',
            'type'        => 'select2_from_array',
            'options'     => $this->GetUc(),
            'allows_null'     => true,
            'default' => 'one',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'support_area_name',
            'label'     => 'Location/Area Name',
            'tab'       => 'Emergency Support/Response Requirement',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'latitude',
            'label'     => 'Latitude',
            'tab'       => 'Emergency Support/Response Requirement',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'longitude',
            'label'     => 'Longitude',
            'tab'       => 'Emergency Support/Response Requirement',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([   // CustomHTML
            'name'  => 'separator',
            'type'  => 'custom_html',
            'tab'             => 'Emergency Support/Response Requirement',
            'value' => '<hr>'

        ])->afterField('longitude');
        $this->crud->addField([
            'name'      => 'contact_person_one',
            'tab'       => 'Emergency Support/Response Requirement',
            'label'     => 'Contact person-1 at Site',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'contact_person_one_designation',
            'tab'       => 'Emergency Support/Response Requirement',
            'label'     => 'Designation',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'contact_person_one_email',
            'tab'       => 'Emergency Support/Response Requirement',
            'label'     => 'Email',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'contact_person_one_phone',
            'tab'       => 'Emergency Support/Response Requirement',
            'label'     => 'Phone',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([   // CustomHTML
            'name'  => 'separator',
            'type'  => 'custom_html',
            'tab'             => 'Emergency Support/Response Requirement',
            'value' => '<hr>'
        ])->beforeField('contact_person_two');
        $this->crud->addField([
            'name'      => 'contact_person_two',
            'tab'       => 'Emergency Support/Response Requirement',
            'label'     => 'Contact person-2 at Site',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'contact_person_two_designation',
            'tab'       => 'Emergency Support/Response Requirement',
            'label'     => 'Designation',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'contact_person_two_email',
            'tab'       => 'Emergency Support/Response Requirement',
            'label'     => 'Email',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'contact_person_two_phone',
            'tab'       => 'Emergency Support/Response Requirement',
            'label'     => 'Phone',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);


        // Requirement Sheet *if required

        /*$this->crud->addField([
            'name'      => 'rescue_items',
            'tab'             => 'Requirement Sheet *if required',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'quantity',
            'tab'             => 'Requirement Sheet *if required',
            'type' => 'number',

            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'requirement_location',
            'tab'             => 'Requirement Sheet *if required',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'when_needed',
            'tab'             => 'Requirement Sheet *if required',
            'type'  => 'date_picker',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'other_requirement',
            'tab'             => 'Requirement Sheet *if required',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);*/


        // Risk Management
        /*$this->crud->addField([
            'name'      => 'proposal_commets_from_ac_relevant',
            'tab'             => 'Risk Management',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'referred_to_for_technical_proposal_comments',
            'tab'             => 'Risk Management',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'expert_opinion',
            'tab'             => 'Risk Management',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'progress_status_update_by_dc_ac_pdma',
            'label'      => 'Progress Status update by DC/AC PDMA',
            'tab'             => 'Risk Management',
            'type' => 'ckeditor',
            'wrapperAttributes' => ['class' => 'form-group col-md-12'],
        ]);*/
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeButton('delete');
        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('delete');

        }
        $this->crud->addButtonFromModelFunction('line', 'emergency_item', 'listEmergencyItems', 'beginning');

     //   CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(EmergencyRequest::class);

        CRUD::setFromDb(); // fields
        $this->crud->removeFields([
            'safe_places_after_rescue_for_camp','safe_places_after_rescue_for_camp','resilience_district_id',
            'resilience_tehsil_id','resilience_uc_id','resilience_taluka_id','resilience_village','location_of_camp','taluka_id','document',
            'rescue_items','quantity','requirement_location','when_needed','other_requirement','proposal_commets_from_ac_relevant','referred_to_for_technical_proposal_comments','expert_opinion','progress_status_update_by_dc_ac_pdma'
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->removeButton('delete');
        CRUD::setFromDb(); // fields
        $this->crud->modifyColumn('progress_status_update_by_dc_ac_pdma', array(
            'label'      => 'Progress Status update by DC/AC PDMA',
        ));
        $this->crud->removeColumn('taluka_id');
        $this->crud->removeColumn('village');
        $this->crud->addColumn([
            'name'            => 'taluka_id',
            'visibleInTable'  => false, // no point, since it's a large text
            'visibleInModal'  => false, // would make the modal too big
            'visibleInExport' => false, // not important enough
            'visibleInShow'   => false, // sure, why not
        ]);
        $this->crud->addColumn([
            'name'            => 'village',
            'visibleInTable'  => false, // no point, since it's a large text
            'visibleInModal'  => false, // would make the modal too big
            'visibleInExport' => false, // not important enough
            'visibleInShow'   => false, // sure, why not
        ]);
    }

    public function Getdistrict()
    {
        $entries = District::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }

    public function GetTehsil()
    {
        $entries = Tehsil::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }
    public function GetUc()
    {
        $entries = Uc::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }

    public function getHazardTypes()
    {
        $entries = HazardType::all();
        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {
            $tab[$entry->id] = $entry->name;
        }
        return $tab;
    }


    public function store()
    {
       //dd($users);
        $response = $this->traitStore();
        $date = $this->data['entry'];

        /*don't sent notifications from here...*/
        /*$users = User::select("users.*")
       ->join("model_has_roles","model_has_roles.model_id","=","users.id")->where('model_has_roles.role_id',1)
       ->where('status', 1)
       ->get();



        Notification::send($users, new EmergenciesCreated($date));*/
       //  auth()->user()->notify(new EmergenciesCreated($response));

// dd($this->crud->getRequest());
         return $response;
    }


    public function addCustomCrudFilters()
    {

        $this->crud->addFilter([
            'name' => 'type_of_emergency',
            'type' => 'dropdown',
            'label'=> 'Level of Emergency',
          ], ['low' => 'Low','medium' => 'Medium','high' => 'High'], function ($value) {
              $this->crud->addClause('where', 'type_of_emergency', $value);
          });

        $this->crud->addFilter([
            'name' => 'type_of_hazard',
            'type' => 'dropdown',
            'label'=> 'Type of Hazard',
        ], function (){
            return HazardType::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function ($value) {
            $this->crud->addClause('where', 'type_of_hazard', $value);
        });

        $this->crud->addFilter([ // select2 filter
            'name' => 'district_id',
            'type' => 'select2',
            'label'=> 'District'
        ], function() {

            return District::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'district_id', $value);
        });
        $this->crud->addFilter([ // sel
        'name' => 'tehsil_id',
        'type' => 'select2',
        'label'=> 'Tehsil'
      ], function() {

        return Tehsil::all()->keyBy('id')->pluck('name', 'id')->toArray();
    }, function($value) { // if the filter is active
        $this->crud->addClause('where', 'tehsil_id', $value);
    });

    $this->crud->addFilter([ // select2 filter
        'name' => 'uc_id',
        'type' => 'select2',
        'label'=> 'UC'
    ], function() {

        return Uc::all()->keyBy('id')->pluck('name', 'id')->toArray();
    }, function($value) { // if the filter is active
        $this->crud->addClause('where', 'uc_id', $value);
    });


    }


}
