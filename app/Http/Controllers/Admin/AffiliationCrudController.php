<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AffiliationRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\User;
use App\Models\Network;

/**
 * Class AffiliationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AffiliationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Affiliation::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/affiliation');
        CRUD::setEntityNameStrings('affiliation', 'Details of Memberships & Affiliations with Networks / Forums');
        $this->addCustomCrudFilters();
        $this->crud->enableExportButtons();
        $this->crud->addField([
            'name' => 'ngo_id',
            'label' => 'NGO',
            'type' => "relationship",
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'entity' => 'ngo',
            'attribute' => 'name',
           
        ]);  
        $this->crud->addField([
            'name' => 'network_id',
            'label' => 'Name of Network or Forum',
            'type' => "relationship",
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'entity' => 'network',
            'attribute' => 'name',
           
        ]); 

        $this->crud->addField([
            'name' => 'joining_date',
            'label' => 'Membership / Joining Date',
            'type' => "date_picker",
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
          
        ]); 

        $this->crud->addField([
            'name'  => 'status',
            'label' => 'Status',
            'type'  => 'select_from_array',
            'options' => [ 0 => 'In-Active', 1 => 'Active'],
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => false,
        ]);

        $this->crud->addColumn([
            'label' => 'NGO',
            'type' => 'model_function',
            'name' => 'ngo_id',
            'function_name' => 'getNgoName',
            'attribute' => 'name',
            'model' => "App\Models\Affiliation",
        ]);
        $this->crud->addColumn([
            'label' => 'Name of Network or Forum',
            'type' => 'model_function',
            'name' => 'network_id',
            'function_name' => 'getNetworkName',
            'attribute' => 'name',
            'model' => "App\Models\Affiliation",
        ]);
        $this->crud->addColumn([
            'name'  => 'joining_date',
            'label' => 'Membership / Joining Date',

        ]);

        $this->crud->addColumn([
            'name'  => 'status',
            'label' => 'Membership status',
            'type'  => 'boolean',
            'options' => [1 => 'Active', 0 => 'Inactive'],
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],

        ]);

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        if(backpack_user()->hasRole('Admin')){
            //$this->crud->removeButton('create');

        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }
  
       

        $this->crud->addColumn([
            'label' => 'NGO',
            'type' => 'model_function',
            'name' => 'ngo_id',
            'function_name' => 'getNgoName',
            'attribute' => 'name',
            'model' => "App\Models\Affiliation",
        ]);
        $this->crud->addColumn([
            'label' => 'Name of Network or Forum',
            'type' => 'model_function',
            'name' => 'network_id',
            'function_name' => 'getNetworkName',
            'attribute' => 'name',
            'model' => "App\Models\Affiliation",
        ]);
        $this->crud->addColumn([
            'name'  => 'joining_date',
            'label' => 'Membership / Joining Date',

        ]);

        $this->crud->addColumn([
            'name'  => 'status',
            'label' => 'Membership status',
            'type'  => 'boolean',
            'options' => [1 => 'Active', 0 => 'Inactive'],
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],

        ]);
      //  CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AffiliationRequest::class);
        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }
        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }

    }

    public function addCustomCrudFilters()
    {

        $this->crud->addFilter([ // dropdown filter
            'name' => 'status',
            'type' => 'dropdown',
            'label'=> 'Status',
          ], [ 0 => 'In-Active', 1 => 'Active'], function ($value) {
              // if the filter is active
              $this->crud->addClause('where', 'status', $value);
          });
  

        $this->crud->addFilter([
            'name' => 'ngo_id',
            'type' => 'select2',
            'label'=> 'NGO'
          ], function() {

            $user = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->where('model_has_roles.role_id',1)
            ->pluck('users.name', 'users.id','model_has_roles.role_id','model_has_roles.model_id')
            ->all();

            return $user;
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'ngo_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'network_id',
            'type' => 'select2',
            'label'=> 'Network'
          ], function() {
             
              return Network::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'network_id', $value);
          });


    }
}
