<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VolunteerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\User;
use App\Models\District;
use App\Models\Tehsil;
use App\Models\Uc;


/**
 * Class VolunteerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VolunteerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Volunteer::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/volunteer');
        CRUD::setEntityNameStrings('volunteer', 'volunteers');
        $this->addCustomCrudFilters();

        if(backpack_user()->hasRole('Admin')){

        }else{
            $district_id = backpack_user()->district_id;
            if(!empty($district_id)){
                $this->crud->addClause('where', 'district_id', '=', $district_id);
            }

        }
        $this->crud->addField([
            'name'      => 'status',
            'tab'             => 'Status',
            'type'        => 'select2_from_array',
            'options'         => ['1' => 'Active','0' => 'Inactive'],
            'allows_null'     => false,
            'default' => 'low',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addColumn([
            'name'  => 'status',
            'label' => 'Status',
            'type'  => 'boolean',
            'options' => [1 => 'Active', 0 => 'Inactive'],
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],

        ]);
        $this->crud->addField([
            'name' => 'ngo_id',
            'label' => 'NGO',
            'type' => "relationship",
            'entity' => 'ngo',
            'attribute' => 'name',

        ]);


        $this->crud->addColumn([
            'label' => 'First Name',
            'name' => 'f_name',
        ]);
        $this->crud->addColumn([
            'label' => 'Last Name',
            'name' => 'l_name',

        ]);
        $this->crud->addColumn([
            'label' => 'NGO',
            'type' => 'model_function',
            'name' => 'ngo_id',
            'function_name' => 'getNgoName',
            'attribute' => 'name',
            'model' => "App\Models\Volunteer",
        ]);
        $this->crud->addColumn([
            'label' => 'District',
            'type' => 'model_function',
            'name' => 'district_id',
            'function_name' => 'getdistrictTitle',
            'attribute' => 'name',
            'model' => "App\Models\Volunteer",
        ]);
        $this->crud->addColumn([
            'label' => 'Tehsil',
            'type' => 'model_function',
            'name' => 'tehsil_id',
            'function_name' => 'gettehsilTitle',
            'attribute' => 'name',
            'model' => "App\Models\Volunteer",
        ]);
        $this->crud->addColumn([
            'label' => 'UC',
            'type' => 'model_function',
            'name' => 'uc_id',
            'function_name' => 'getUcTitle',
            'attribute' => 'name',
            'model' => "App\Models\Volunteer",
        ]);

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        if(backpack_user()->hasRole('Admin')){
            $this->crud->removeButton('create');

            }else{
                $this->crud->removeButton('update');
                $this->crud->removeButton('create');
                $this->crud->removeButton('delete');

            }

        $this->crud->enableExportButtons();
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    protected function setupShowOperation()
    {
        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }
        CRUD::setFromDb(); // fields
        $this->crud->removeColumn('image');
        $this->crud->modifyColumn('name_of_ngo', array(
            'label' => 'Name of NGO',
        ));
        $this->crud->modifyColumn('whats_app', array(
            'label' => 'WhatsApp',
        ));

        $this->crud->addColumn([
            'label' => 'Image',
            'type' => 'model_function',
            'name' => 'image',
            'function_name' => 'getImageUrl',
            'attribute' => 'name',
            'model' => "App\Models\Volunteer",
        ]);

    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(VolunteerRequest::class);
        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'District',
            'name'      => 'district_id',
            'options'     => $this->getDistrict(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'Tehsil',
            'name'      => 'tehsil_id',
            'options'     => $this->getTehsil(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'UC',
            'name'      => 'uc_id',
            'options'     => $this->getUcTitle(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function addCustomCrudFilters()
    {
        $this->crud->addFilter([
            'name' => 'ngo_id',
            'type' => 'select2',
            'label'=> 'NGO'
          ], function() {

            $user = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->where('model_has_roles.role_id',1)
            ->pluck('users.name', 'users.id','model_has_roles.role_id','model_has_roles.model_id')
            ->all();

            return $user;
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'ngo_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'district_id',
            'type' => 'select2',
            'label'=> 'District'
          ], function() {

              return District::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'district_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'tehsil_id',
            'type' => 'select2',
            'label'=> 'Tehsil'
          ], function() {

              return Tehsil::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'tehsil_id', $value);
          });

          $this->crud->addFilter([ // dropdown filter
            'name' => 'status',
            'type' => 'dropdown',
            'label'=> 'Status',
          ], [1 => 'Active', 0 => 'Inactive'], function ($value) {
              // if the filter is active
              $this->crud->addClause('where', 'status', $value);
          });

          $this->crud->addFilter([
            'type'  => 'text',
            'name'  => 'area_of_expertise',
            'label' => 'Area Of Expertise'
          ],
          false,
          function($value) { // if the filter is active
             $this->crud->addClause('where', 'area_of_expertise', 'LIKE', "%$value%");
          });

    }


    public function getDistrict()
	{
		$entries = District::all();
		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {

          //  echo "<pre>"; print_r($entry->id); exit;

			$tab[$entry->id] = $entry->name;


		}

		return $tab;
    }
    public function getTehsil()
	{
		$entries = Tehsil::all();
		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {
			$tab[$entry->id] = $entry->name;
		}
		return $tab;
    }
    public function getUcTitle()
	{
		$entries = Uc::all();
		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {
			$tab[$entry->id] = $entry->name;
		}
		return $tab;
    }


}
