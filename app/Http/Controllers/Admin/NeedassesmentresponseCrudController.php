<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NeedassesmentresponseRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Notifications\NeedAssesmentResponseUpdated;
use App\User;
use App\Models\Emergency;
use Illuminate\Support\Facades\Notification;

/**
 * Class NeedassesmentresponseCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class NeedassesmentresponseCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */

    public function setup()
    {
        $user_login = backpack_user();

        CRUD::setModel(\App\Models\Needassesmentresponse::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/needassesmentresponse');
        CRUD::setEntityNameStrings('Emergency Support/Response', 'Emergency Support/Response by Organizations');

        $this->addCustomCrudFilters();

        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'NGO',
            'name'      => 'ngo_id',
            'options'     => $this->getNgoName(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'Emergency',
            'name'      => 'emergency_id',
            'options'     => $this->getemergencyName(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'status',
            'tab'             => 'Status',
            'type'        => 'select2_from_array',
            'options'         => ['1' => 'Accept','0' => 'Pending','-1' => 'Reject'],
            'allows_null'     => false,
            'default' => 'low',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'emergency_item_id',
            'type'        => 'hidden',
        ]);
        $this->crud->addColumn([
			'name'  => 'ngo_id',
            'label' => 'NGO',
            'type'  => 'model_function',
            'function_name' => 'getTitleNgo',
        ]);

        $this->crud->addColumn([
			'name'  => 'emergency_id',
            'label' => 'Emergency',
            'type'  => 'model_function',
            'function_name' => 'getTitleEmergency',
        ]);
        $this->crud->addColumn([
			'name'  => 'status',
            'label' => 'Status',
            'type'    => 'boolean',
            'options'         => ['1' => 'Accept','0' => 'Pending','-1' => 'Reject'],
        ]);
        $this->crud->addColumn([
			'name'  => 'response_quantity',
            'label' => 'Response Quantity',

        ]);


    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeButton('delete');
        CRUD::setFromDb(); // columns
        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(NeedassesmentresponseRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */

    public function update()
    {
        $response = $this->traitUpdate();

        $date = $this->data['entry'];
        if(!empty($date->ngo_id)){
        $ngo_id  =$date->ngo_id;
        $user = User::where("id",$ngo_id)->first();
        Notification::send($user, new NeedAssesmentResponseUpdated($date));
        }

        return $response;

    }
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function setupShowOperation()
    {
        $this->crud->removeButton('delete');
        $this->crud->addColumn([
            'name' => 'focal_person',
            'label' => 'Focal Person Name',
            'type'  => 'model_function',
            'function_name' => 'getTitleNgoFocalPerson',

        ]);
        $this->crud->addColumn([
            'name' => 'district_id',
            'label' => 'District',
            'type'  => 'model_function',
            'function_name' => 'getEmergencydistrict',

        ]);
        $this->crud->addColumn([
            'name' => 'emergency_item_id',
            'label' => 'Emergency Item',
            'type'  => 'model_function',
            'function_name' => 'getEmergencyItem',

        ]);

    }


    public function getemergencyName()
	{
		$entries = Emergency::all();
		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {

          //  echo "<pre>"; print_r($entry->id); exit;
            $hazardType = isset($entry->hazardType->name) ? $entry->hazardType->name : ' null ';

			$tab[$entry->id] = $entry->type_of_emergency.' - '.$hazardType;


		}

		return $tab;
    }

    public function getNgoName()
	{
		//$entries = User::all();
        $entries = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->where('model_has_roles.role_id',1)->where('status', 1)->get();

		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {

          //  echo "<pre>"; print_r($entry->id); exit;

			$tab[$entry->id] = $entry->name;


		}

		return $tab;
    }
    public function addCustomCrudFilters()
    {

        $this->crud->addFilter([ // dropdown filter
            'name' => 'status',
            'type' => 'dropdown',
            'label'=> 'Status',
          ], [1 => 'Accept', 0 => 'Pending', -1 => 'Reject'], function ($value) {
              // if the filter is active
              $this->crud->addClause('where', 'status', $value);
          });


        $this->crud->addFilter([ // select2 filter
            'name' => 'emergency_id',
            'type' => 'select2',
            'label'=> 'Emergency'
          ], function() {

              return User::all()->keyBy('id')->pluck('type_of_emergency', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'emergency_id', $value);
          });


    }

}
