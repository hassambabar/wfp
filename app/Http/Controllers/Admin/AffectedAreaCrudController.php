<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AffectedAreaRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Province;
use App\District;
use App\Tehsil;
use App\Uc;
/**
 * Class AffectedAreaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AffectedAreaCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AffectedArea::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/affectedarea');
        CRUD::setEntityNameStrings('Losses/Damages Reports', 'Losses/Damages Reports');

        $this->addCustomCrudFilters();
        $this->crud->enableExportButtons();

        $this->crud->addColumn([
            'name'  => 'report_from',
            'label' => 'Report From',
        ]);

        $this->crud->addColumn([
            'name'  => 'report_to',
            'label' => 'Report To',
        ]);

        $this->crud->addColumn([
			'name'  => 'province_id',
            'label' => 'Province',
            'type'  => 'model_function',
            'function_name' => 'getProvince',
        ]);

        $this->crud->addColumn([
			'name'  => 'affected_district',
            'label' => 'District',
            'type'  => 'model_function',
            'function_name' => 'getdistrictTitle',
        ]);
        $this->crud->addColumn([
			'name'  => 'affected_talukas',
            'label' => 'Tehsil',
            'type'  => 'model_function',
            'function_name' => 'getTitleTehsil',
        ]);
        $this->crud->addColumn([
            'name'  => 'affected_union_council_name',
            'label' => 'Affected UCs Name',
        ]);
        $this->crud->addColumn([
            'name'  => 'population_affected',
            'label' => 'Population Affected',
        ]);
        $this->crud->addColumn([
            'name'  => 'relief_camps_established',
            'label' => 'Relief Camps Established',
        ]);
        $this->crud->addColumn([
            'name'  => 'max_persons_in_relief_camps',
            'label' => 'Max. Persons Recorded in Relief Camps',
        ]);
        $this->crud->addColumn([
            'name'  => 'persons_teated',
            'label' => 'Persons Returned',
        ]);

        $this->crud->addField([
            'type'        => 'date_picker',
            'label'     => 'Report From',
            'name'      => 'report_from',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([
            'type'        => 'date_picker',
            'label'     => 'Report To',
            'name'      => 'report_to',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([
            'type'        => 'select2_from_array',
            'label'     => 'Province',
            'name'      => 'province_id',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'options'     => $this->GetProvinces()
        ]);

        /*$this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_2',
            'value'     => '<h3><strong>Damage Affected</strong></h3><hr />',
            'wrapperAttributes' => ['class' => 'col-md-12 mt-4 pt-2'],
        ]);*/

        $this->crud->addField([
            'type'        => 'select2_from_array',
            'label'     => 'District',
            'name'      => 'affected_district',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'options'     => $this->Getdistrict()

        ]);

        $this->crud->addField([
            'type'        => 'select2_from_array',
            'label'     => 'Talukas Affected',
            'name'      => 'affected_talukas',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'options'     => $this->GetTehsil()

        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Union Council Names',
            'name'      => 'affected_union_council_name',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Affected Villages Name',
            'name'      => 'affected_villages_name',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Population Affected',
            'name'      => 'population_affected',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Relief Camps Established',
            'name'      => 'relief_camps_established',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_3',
            'value'     => '<h3><strong>Human Losses & Injured</strong></h3><hr/>',
            'wrapperAttributes' => ['class' => 'col-md-12 mt-4 pt-2'],
        ]);

        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_4',
            'value'     => '<p><strong>Persons Died</strong></p>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Male',
            'name'      => 'male_died',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Female',
            'name'      => 'female_died',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Children',
            'name'      => 'children_died',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Total',
            'name'      => 'total_died',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);


        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_5',
            'value'     => '<p><strong>Persons Injured</strong></p>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Male',
            'name'      => 'male_injured',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Female',
            'name'      => 'female_injured',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Children',
            'name'      => 'children_injured',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Total',
            'name'      => 'total_injured',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);



         $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_6',
            'value'     => '<p><strong>Displaced Persons (DPs)</strong></p>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Male',
            'name'      => 'male_displaced',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Female',
            'name'      => 'female_displaced',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Children',
            'name'      => 'children_displaced',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Total',
            'name'      => 'total_displaced',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);


        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_7',
            'value'     => '<p><strong>Persons in Relief Camps</strong></p>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Male',
            'name'      => 'male_in_relief_camps',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Female',
            'name'      => 'female_in_relief_camps',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Children',
            'name'      => 'children_in_relief_camps',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Total',
            'name'      => 'total_in_relief_camps',
            'wrapperAttributes' => ['class' => 'form-group col-md-3'],
            'allows_null'     => true,
        ]);


        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_8',
            'value'     => '<h3><strong>Private Property/Livehood Losses & Damage</strong></h3><hr/>',
            'wrapperAttributes' => ['class' => 'col-md-12 mt-4 pt-2'],
        ]);
       /* $this->crud->addField([
            'label' => 'District',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
            'type'        => 'select2_from_array',
            'name'      => 'damage_district',
            'allows_null'     => true,
            'options'     => $this->Getdistrict()
        ]);*/

        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_9',
            'value'     => '<p><strong>Houses Damaged</strong></p>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Partially',
            'name'      => 'partially_houses_damage',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Fully',
            'name'      => 'fully_houses_damage',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Total',
            'name'      => 'total_houses_damage',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);


        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_10',
            'value'     => '<p><strong>Villages Affected</strong></p>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Partially',
            'name'      => 'partially_villages_affected',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Fully',
            'name'      => 'fully_villages_affected',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Total',
            'name'      => 'total_villages_affected',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);


        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Crops Area Damaged (Acres)',
            'name'      => 'crops_area_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Cattle Head Perished',
            'name'      => 'cattle_head_perished',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_11',
            'value'     => '<br/>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);


        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_12',
            'value'     => '<h3><strong>Public Infrastructure Damages</strong></h3><hr />',
            'wrapperAttributes' => ['class' => 'col-md-12 mt-4 pt-2'],
        ]);

        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_13',
            'value'     => '<p><strong>School / Collages Damaged</strong></p>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Partially',
            'name'      => 'partially_school_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Fully',
            'name'      => 'fully_school_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Total',
            'name'      => 'total_school_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);


        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_14',
            'value'     => '<p><strong>Government Buildings Damaged (Less Schools / Colleges)</strong></p>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Partially',
            'name'      => 'partially_building_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Fully',
            'name'      => 'fully_building_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Total',
            'name'      => 'total_building_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);


        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_15',
            'value'     => '<p><strong>Communication Infrastructure  & Private Property Damaged</strong></p>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Roads',
            'name'      => 'roads_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Railways',
            'name'      => 'railways_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Bridges',
            'name'      => 'bridges_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);

         $this->crud->addField([
            'type'        => 'text',
            'label' => 'Shops',
            'name'      => 'shops_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Hotels',
            'name'      => 'hotels_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Masjid/Religious places',
            'name'      => 'religious_places_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Power Houses/ Electric Installations',
            'name'      => 'power_houses_damaged',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
            'allows_null'     => true,
        ]);

       /* $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_16',
            'value'     => '<h3><strong>Rescue Effort at Provincial/State Level</strong></h3><hr />',
            'wrapperAttributes' => ['class' => 'col-md-12 mt-4 pt-2'],
        ]);*/

        $this->crud->addField([
            'type'        => 'custom_html',
            'name'      => 'chtml_17',
            'value'     => '<hr>',
            'wrapperAttributes' => ['class' => 'col-md-12'],
        ]);
        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Max. Persons Recorded in Relief Camps',
            'name'      => 'max_persons_in_relief_camps',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
        ]);


        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Persons Returned ',
            'name'      => 'persons_teated',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
        ]);

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeButton('delete');
        $this->crud->enableExportButtons();

        if(backpack_user()->hasRole('Admin')){

        }else{
            //$this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }

//        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AffectedAreaRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->removeButton('delete');
    }


    public function GetProvinces()
    {
        $entries = Province::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }
    public function Getdistrict()
    {
        $entries = District::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }

    public function GetTehsil()
    {
        $entries = Tehsil::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }


    public function addCustomCrudFilters()
    {

        $this->crud->addFilter([ // select2 filter
            'name' => 'province_id',
            'type' => 'select2',
            'label'=> 'Province'
        ], function() {

            return Province::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'province_id', $value);
        });


        $this->crud->addFilter([ // select2 filter
            'name' => 'district_id',
            'type' => 'select2',
            'label'=> 'District'
        ], function() {

            return District::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'district_id', $value);
        });
        $this->crud->addFilter([ // sel
        'name' => 'affected_talukas',
        'type' => 'select2',
        'label'=> 'Tehsil'
    ], function() {

        return Tehsil::all()->keyBy('id')->pluck('name', 'id')->toArray();
    }, function($value) { // if the filter is active
        $this->crud->addClause('where', 'affected_talukas', $value);
    });



    }

}
