<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Ngo_contactRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\User;
use App\Province;
use App\District;
/**
 * Class Ngo_contactCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class Ngo_contactCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Ngo_contact::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/ngo_contact');
        CRUD::setEntityNameStrings('NGO Contact', 'NGO Contacts');
        $this->addCustomCrudFilters();
        $this->crud->enableExportButtons();


        $this->crud->addField([
            'name' => 'ngo_id',
            'label' => 'NGO',
            'type' => "relationship",
            'entity' => 'ngo',
            'attribute' => 'name',

        ]);
        $this->crud->addField([
            'name' => 'province_id',
            'label' => 'Province',
            'type' => "relationship",
            'entity' => 'province',
            'attribute' => 'name',

        ]);      $this->crud->addField([
            'name' => 'district_id',
            'label' => 'District',
            'type' => "relationship",
            'entity' => 'district',
            'attribute' => 'name',

        ]);

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        if(backpack_user()->hasRole('Admin')){
            $this->crud->removeButton('delete');


        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }

        $this->crud->removeButton('create');
        $this->crud->addColumn([
            'label' => 'NGO',
            'type' => 'model_function',
            'name' => 'ngo_id',
            'function_name' => 'getNgoName',
            'attribute' => 'name',
            'model' => "App\Models\Ngo_contact",
        ]);
        $this->crud->removeColumn('contact_person_name');
        $this->crud->addColumn([
            'name' => 'contact_person_name',
            'label' => 'Contact Person Name'
        ])->afterColumn('ngo_id');
        $this->crud->removeColumn('designation');
        $this->crud->addColumn([
            'name' => 'designation',
            'label' => 'Designation'
        ])->afterColumn('contact_person_name');
        $this->crud->addColumn([
            'label' => 'Province',
            'type' => 'model_function',
            'name' => 'province_id',
            'function_name' => 'getprovinceTitle',
            'attribute' => 'name',
            'model' => "App\Models\Ngo_contact",
        ]);
        $this->crud->addColumn([
            'label' => 'District',
            'type' => 'model_function',
            'name' => 'district_id',
            'function_name' => 'getdistrictTitle',
            'attribute' => 'name',
            'model' => "App\Models\Ngo_contact",
        ]);
        CRUD::setFromDb(); // columns

        $this->crud->removeColumn('city_id');
        $this->crud->removeColumn('province_id');
        $this->crud->removeColumn('office');
        $this->crud->modifyColumn('can_pdma_contact', array(
            'label' => 'Can PDMA Contact?',
            'type' => 'select_from_array',
            'options' => [0 => 'No', 1 => 'Yes'],
        ));
        $this->crud->addColumn([
            'name'  => 'office',
            'label' => 'Office Location',
            'type'     => 'closure',
            'function' => function($entry) {
                if($entry->office == 'Head Office'){
                    return 'Main Office / Head Office';
                }elseif($entry->office == 'Sindh Office'){
                    return 'Sindh Province Main Office';
                }
                return '--';
            }
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(Ngo_contactRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function setupShowOperation()
    {
        $this->crud->removeButton('create');
        if(backpack_user()->hasRole('Admin')){


        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }
        CRUD::setFromDb(); // fields
        $this->crud->removeColumns(['mailing_address','office','contact_person_name','designation','province_id','district_id','landline','cell','fax','email','city_id']);

        $this->crud->modifyColumn('can_pdma_contact', array(
            'label' => 'Can PDMA Contact?',
            'type' => 'select_from_array',
            'options' => [0 => 'No', 1 => 'Yes'],
        ));

        $this->crud->addColumn([
            'name'  => 'office',
            'label' => 'Office Location',
            'type'     => 'closure',
            'function' => function($entry) {
                if($entry->office == 'Head Office'){
                    return 'Main Office / Head Office';
                }elseif($entry->office == 'Sindh Office'){
                    return 'Sindh Province Main Office';
                }
                return '--';
            }
        ])->makeFirstColumn();

        $this->crud->addColumn([
            'name'  => 'contact_person_name',
            'label' => 'Contact Person Name'
        ])->afterColumn('office');

        $this->crud->addColumn([
            'name'  => 'designation',
            'label' => 'Designation/Title'
        ])->afterColumn('contact_person_name');

        $this->crud->addColumn([
            'name'  => 'province_id',
            'label' => 'Province',
            'type'     => 'closure',
            'function' => function($entry) {
                return $entry->getprovinceTitle();
            }
        ])->afterColumn('designation');

        $this->crud->addColumn([
            'name'  => 'district_id',
            'label' => 'District',
            'type'     => 'closure',
            'function' => function($entry) {
                return $entry->getdistrictTitle();
            }
        ])->afterColumn('province_id');

        $this->crud->addColumn([
            'name'  => 'mailing_address',
            'label' => 'Mailing Address'
        ])->afterColumn('district_id');

        $this->crud->addColumn([
            'name'  => 'landline',
            'label' => 'Phone - Landline'
        ])->afterColumn('mailing_address');

        $this->crud->addColumn([
            'name'  => 'cell',
            'label' => 'Phone - Cell'
        ])->afterColumn('landline');

        $this->crud->addColumn([
            'name'  => 'fax',
            'label' => 'Fax'
        ])->afterColumn('cell');

        $this->crud->addColumn([
            'name'  => 'email',
            'label' => 'Email',
            'type'  => 'email'
        ])->afterColumn('fax');

        $this->crud->addColumn([
            'name'            => 'city_id',
            'visibleInShow'   => false, // sure, why not
        ]);

    }
    public function addCustomCrudFilters()
    {
        $this->crud->addFilter([
            'name' => 'ngo_id',
            'type' => 'select2',
            'label'=> 'NGO'
          ], function() {

            $user = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->where('model_has_roles.role_id',1)
            ->pluck('users.name', 'users.id','model_has_roles.role_id','model_has_roles.model_id')
            ->all();

            return $user;
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'ngo_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'province_id',
            'type' => 'select2',
            'label'=> 'Province'
        ], function() {

            return Province::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'province_id', $value);
        });


        $this->crud->addFilter([ // select2 filter
            'name' => 'district_id',
            'type' => 'select2',
            'label'=> 'District'
        ], function() {

            return District::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'district_id', $value);
        });


    }
}
