<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use Carbon\Carbon;
use App\Models\NgoProjects;
use App\Models\ProjectsNoc;
use App\Notifications\NgoProfileReviewByPdma;
use App\Notifications\PdmaEmpAssignmentNoc;
use App\Notifications\ProjectNocReviewByPdma;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\NgoProfile;
use Illuminate\Support\Facades\Notification;
use App\Notifications\PdmaEmpAssignmentProfile;
use App\Notifications\ProfileApproved;


class AdminNotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware(config('backpack.base.middleware_key', 'admin'));


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        return view('pdma.notifications.list');
        //pdma/notifications/list
    }


    public function profileApproval($id){
       $ngoProfile =  NgoProfile::where('user_id', $id)->first();
       $ngoUserInfo =  \App\Models\BackpackUser::where('id',$id)->first();
//dd(backpack_user()->hasRole('PDMA'));
       $pdma_employee_role_id = 5;
       /*$pdma_employees = \App\Models\BackpackUser::whereHas('roles', function ($query) use ($pdma_employee_role_id) {
                $query->where('role_id', '=', $pdma_employee_role_id);
        })->get();*/
        $pdma_employees = \App\Models\BackpackUser::select("users.*")
            ->join("model_has_roles", "model_has_roles.model_id", "=", "users.id")->whereIn('model_has_roles.role_id',[3,5])
            ->where('users.status',1)
            ->where('users.id','!=', backpack_user()->id)
            ->get();

       return view('pdma.notifications.profileApproval', compact('ngoProfile','pdma_employees','ngoUserInfo'));
       // pdma/notifications/profileApproval
    }


    public function assingProfileApproval(Request $request){


        $ngoUserInfo = \App\User::where('id',$request->ngo_user_id)->first();
        $ngoProfile =  NgoProfile::where('id', $request->ngo_id)->first();

        // if status or employee_assign_id value changed the send notification
        $sendProfileStatusNotif = ($ngoUserInfo->status != $request->status)?true:false;
        $sendPdmaEmpAssignNotif = ($ngoUserInfo->employee_assign_id != $request->pdma_employee_assign)?true:false;

        $ngoUserInfo->status = $request->status;
        $ngoUserInfo->employee_assign_id = $request->pdma_employee_assign;
        $ngoUserInfo->save();

        // if assign to pdma_employee then send notification to him.
        if($sendPdmaEmpAssignNotif){
          $pdma_emp =  \App\User::where('id',$request->pdma_employee_assign)->first();
          Notification::send($pdma_emp, new PdmaEmpAssignmentProfile($ngoProfile));
        }

        //if status changed and made approved then notify ngo.
        if($sendProfileStatusNotif && ($ngoUserInfo->status == 1))
          Notification::send($ngoUserInfo, new ProfileApproved($ngoProfile));


        return redirect(url('/admin/notifications/profileApproval/'.$request->ngo_user_id));
    }

    public function pdmaProfileReview(Request $request){
        $ngoUserInfo = \App\User::where('id',$request->ngo_user_id)->first();
        $ngoProfile =  NgoProfile::where('id', $request->ngo_id)->first();
        $data = $request->all();

        if($data['pdma_profile_review_decision'] == 'deferred'){
            $ngoUserInfo->enable_edit = 1;
        }else{
            $ngoUserInfo->enable_edit = 0;
        }
        $ngoUserInfo->pdma_profile_review_decision = $data['pdma_profile_review_decision'];
        $ngoUserInfo->pdma_profile_review_comment = $data['pdma_profile_review_comment'];
        $ngoUserInfo->save();

        Notification::send($ngoUserInfo, new NgoProfileReviewByPdma($ngoProfile));

        return redirect(url('/admin/notifications'))->with('message', 'success|Notification sent successfully.');
        //dd($request->all());
    }

    public function nocApproval($id){
        $project = NgoProjects::where('id', $id)->first();
        if(isset($project) && $project !== null){
            $ngoProfile =  NgoProfile::where('user_id', $project->ngo_id)->first();
            $ngoUserInfo =  \App\Models\BackpackUser::where('id',$id)->first();
//dd(backpack_user()->hasRole('PDMA'));
            /*$pdma_employee_role_id = 5;
            $pdma_employees = \App\Models\BackpackUser::whereHas('roles', function ($query) use ($pdma_employee_role_id) {
                $query->where('role_id', '=', $pdma_employee_role_id);
            })->get();*/
            $pdma_employees = \App\Models\BackpackUser::select("users.*")
                ->join("model_has_roles", "model_has_roles.model_id", "=", "users.id")->whereIn('model_has_roles.role_id',[3,5])
                ->where('users.status',1)
                ->where('users.id','!=', backpack_user()->id)
                ->get();
        }else{
            return redirect('notifications')->with('message', 'error|Project not found.');
        }
        return view('pdma.notifications.nocApproval', compact('ngoProfile','pdma_employees','project'));
        // pdma/notifications/profileApproval
    }

    public function assignNocApproval(Request $request){


        $projectNoc = ProjectsNoc::where('project_id',$request->project_id)->first();
        $ngoProject =  NgoProjects::where('id', $request->project_id)->first();
        $ngoUserInfo = \App\User::where('id',$request->ngo_user_id)->first();

        // if status or employee_assign_id value changed the send notification
        $sendProfileStatusNotif = ($projectNoc->status != $request->status)?true:false;

        $projectNoc->status = $request->status;
        $projectNoc->save();

        // if assign to pdma_employee then send notification to him.
       if($request->pdma_employee_assign){
            $pdma_emp =  \App\User::where('id',$request->pdma_employee_assign)->first();
            Notification::send($pdma_emp, new PdmaEmpAssignmentNoc($ngoProject));
        }

        //if status changed and made approved then notify ngo.
         /*if($sendProfileStatusNotif && ($projectNoc->status == 'accepted'))
            Notification::send($ngoUserInfo, new ProfileApproved($ngoProject));*/


        return redirect(url('/admin/notifications/nocApproval/'.$request->project_id));
    }

    public function pdmaNocReview(Request $request){
        $ngoUserInfo = \App\User::where('id',$request->ngo_user_id)->first();
        $ngoProfile =  NgoProfile::where('id', $request->ngo_id)->first();
        $ngoProject =  NgoProjects::where('id', $request->project_id)->first();

        $noc = $ngoProject->noc->where('status','pending')->first();
        $data = $request->all();

        if($data['pdma_noc_review_decision'] == 'deferred'){
            $ngoUserInfo->noc_enable_edit = 1;
        }else{
            $ngoUserInfo->noc_enable_edit = 0;
        }

        if($noc){
            $noc->comments = $data['pdma_noc_review_comment'];
            $noc->save();
        }

        $ngoUserInfo->pdma_noc_review_decision = $data['pdma_noc_review_decision'];
        $ngoUserInfo->pdma_noc_review_comment = $data['pdma_noc_review_comment'];
        $ngoUserInfo->save();

        Notification::send($ngoUserInfo, new ProjectNocReviewByPdma($noc));

//        return redirect(url('/admin/notifications'))->with('message', 'success|Notification sent successfully.');
        return redirect(url('/admin/notifications/nocApproval/'.$request->project_id));
        //dd($request->all());
    }
}
