<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReliefCampRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

use App\Models\District;
use App\Models\Tehsil;
use App\Models\Uc;

/**
 * Class ReliefCampCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReliefCampCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ReliefCamp::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/relief_camps');
        CRUD::setEntityNameStrings('relief camp', 'relief camps');
        $this->addCustomCrudFilters();
        $this->crud->enableExportButtons();
        $this->crud->setListView('vendor.backpack.crud.listAnnouncements');

        if(backpack_user()->hasRole('Admin')){

        }else{
            $district_id = backpack_user()->district_id;
            if(!empty($district_id)){
                $this->crud->addClause('where', 'district_id', '=', $district_id);
            }

        }

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Title',
            'name'      => 'title',
            'wrapperAttributes' => ['class' => 'form-group col-md-12'],
            'allows_null'     => false,
        ]);


        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'District',
            'name'      => 'district_id',
            'options'     => $this->getDistricts(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
        ]);



        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'UC',
            'name'      => 'uc_id',
            'options'     => $this->getUcList(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
        ]);

        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'Taluka',
            'name'      => 'tehsil_id',
            'options'     => $this->getTehsilList(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
        ]);

        $this->crud->addField([ // select_from_array
            'type'        => 'number',
            'label' => 'No. of camps installed',
            'name'      => 'capacity',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Location/Site name',
            'name'      => 'site_name',
            'wrapperAttributes' => ['class' => 'form-group col-md-12'],
            'allows_null'     => true,
        ]);

        $this->crud->addField([
            'type'        => 'radio',
            'label'     => 'Drinking water facility',
            'name'      => 'has_drinking_water',
            'options'     => [
                0 => "No",
                1 => "Yes"
            ],
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'inline' => true,
            'default' => '0',
        ]);

        $this->crud->addField([
            'type'        => 'radio',
            'label'     => 'Electricity',
            'name'      => 'has_electricity',
            'options'     => [
                0 => "No",
                1 => "Yes"
            ],
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'inline' => true,
            'default' => '0',
        ]);

        $this->crud->addField([
            'type'        => 'radio',
            'label'     => 'Wash room availability',
            'name'      => 'has_washroom',
            'options'     => [
                0 => "No",
                1 => "Yes"
            ],
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'inline' => true,
            'default' => '0',
        ]);

        $this->crud->addField([
            'type'        => 'radio',
            'label'     => 'Medical/First aid facility',
            'name'      => 'has_medical_aid',
            'options'     => [
                0 => "No",
                1 => "Yes"
            ],
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'inline' => true,
            'default' => '0',
        ]);

         $this->crud->addField([
            'type'        => 'text',
            'label' => 'Add other facility',
            'name'      => 'other_facility',
            'wrapperAttributes' => ['class' => 'form-group col-md-12'],
            'allows_null'     => true,
        ]);

        // $this->crud->addField([
        //     'type'        => 'text',
        //     'label' => 'First person name',
        //     'name'      => 'focal_person',
        //     'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        //     'allows_null'     => true,
        // ]);

        // $this->crud->addField([
        //     'type'        => 'text',
        //     'label' => 'Contact number',
        //     'name'      => 'contact_no',
        //     'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        //     'allows_null'     => true,
        // ]);

        // $this->crud->addField([
        //     'type'        => 'text',
        //     'label' => 'Designation',
        //     'name'      => 'designation',
        //     'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        //     'allows_null'     => true,
        // ]);

        // $this->crud->addField([
        //     'type'        => 'custom_html',
        //     'name'      => '',
        //     'value'     => '<br/>'
        // ]);

        $this->crud->addField([ // select_from_array

            // repeatable
              'name'  => 'contact',
              'label' => 'Focal person',
              'type'  => 'repeatable',
              'pivot' => true,

              'fields' => [
                  [
                      'name'    => 'focal_person',
                      'type'    => 'text',
                      'label'   => 'Focal person Name',
                      'wrapper' => ['class' => 'form-group col-md-4'],
                  ],
                  [
                      'name'    => 'designation',
                      'type'    => 'text',
                      'label'   => 'Designation',
                      'wrapper' => ['class' => 'form-group col-md-4'],
                  ],
                  [
                      'name'    => 'contact_no',
                      'type'    => 'text',
                      'label'   => 'Contact no',
                      'wrapper' => ['class' => 'form-group col-md-4'],
                  ]
              ],

              // optional
              'new_item_label'  => 'Add Another', // customize the text of the button
              'init_rows' => 1, // number of empty rows to be initialized, by default 1
              'min_rows' => 1, // minimum rows allowed, when reached the "delete" buttons will be hidden
              'max_rows' => 10, // maximum rows allowed, when reached the "new item" button will be hidden

          ]);
        // $this->crud->addField([
        //     'type'        => 'text',
        //     'label' => 'Second person name',
        //     'name'      => 'focal_person_2',
        //     'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        //     'allows_null'     => true,
        // ]);

        // $this->crud->addField([
        //     'type'        => 'text',
        //     'label' => 'Contact number',
        //     'name'      => 'contact_no_2',
        //     'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        //     'allows_null'     => true,
        // ]);

        // $this->crud->addField([
        //     'type'        => 'text',
        //     'label' => 'Designation',
        //     'name'      => 'designation_2',
        //     'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        //     'allows_null'     => true,
        // ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Latitude',
            'name'      => 'latitude',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
        ]);

        $this->crud->addField([
            'type'        => 'text',
            'label' => 'Longitude',
            'name'      => 'longitude',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
        ]);

    }


    protected function setupShowOperation()
    {
        $this->crud->removeButton('delete');
        $this->crud->addColumn([
            'label' => 'District',
            'type' => 'select',
            'name' => 'district_id', // the db column for the foreign key
            'entity' => 'rel_district', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\ReliefCamp" // foreign key model
        ]);

         $this->crud->addColumn([
            'label' => 'UC',
            'type' => 'select',
            'name' => 'uc_id', // the db column for the foreign key
            'entity' => 'rel_uc', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\ReliefCamp" // foreign key model
        ]);

        $this->crud->addColumn([
            'label' => 'Tehsil',
            'type' => 'select',
            'name' => 'tehsil_id', // the db column for the foreign key
            'entity' => 'rel_tehsil', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\ReliefCamp" // foreign key model
        ]);


       $this->crud->addColumn([
            'label' => 'Drinking water facility',
            'type' => 'model_function',
            'name' => 'has_drinking_water', // the db column for the foreign key
            'function_name' => 'getDrinkingWater',
        ]);

       $this->crud->addColumn([
            'label' => 'Electricity',
            'type' => 'model_function',
            'name' => 'has_electricity', // the db column for the foreign key
            'function_name' => 'getElectricity',
        ]);

        $this->crud->addColumn([
            'label' => 'Wash room availability',
            'type' => 'model_function',
            'name' => 'has_washroom', // the db column for the foreign key
            'function_name' => 'getWashroom',
        ]);


        $this->crud->addColumn([
            'label' => 'Medical/First aid facility',
            'type' => 'model_function',
            'name' => 'has_medical_aid', // the db column for the foreign key
            'function_name' => 'getMedicalAid',
        ]);

        $this->crud->addColumn([
            'name' => 'contact',
            'label' => 'Contact',
            'type'  => 'model_function',
            'function_name' => 'getcontact',

        ]);


    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns
        $this->crud->removeButton('delete');
        if(backpack_user()->hasRole('Admin')){

        }else{
           // $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }

        //$this->crud->removeColumn('has_drinking_water');
//        $this->crud->removeColumn('has_electricity');
//        $this->crud->removeColumn('has_washroom');
//        $this->crud->removeColumn('has_medical_aid');
//        $this->crud->removeColumn('other_facility');
        $this->crud->removeColumn('facilities');
        $this->crud->removeColumn('contact_no');
//        $this->crud->removeColumn('latitude');
//        $this->crud->removeColumn('longitude');
//        $this->crud->removeColumn('contact');

        $this->crud->setColumnDetails('has_drinking_water', [
            'exportOnlyField' => true,
            'visibleInTable'  => false,
            'visibleInExport' => true,
            'type'     => 'closure',
            'function' => function($entry) {
                return $entry->has_drinking_water == 1 ? 'Yes' : 'No';
            }
        ]);
        $this->crud->setColumnDetails('has_electricity', [
            'exportOnlyField' => true,
            'visibleInTable'  => false,
            'visibleInExport' => true,
            'type'     => 'closure',
            'function' => function($entry) {
                return $entry->has_electricity == 1 ? 'Yes' : 'No';
            }
        ]);
        $this->crud->setColumnDetails('has_washroom', [
            'exportOnlyField' => true,
            'visibleInTable'  => false,
            'visibleInExport' => true,
            'type'     => 'closure',
            'function' => function($entry) {
                return $entry->has_washroom == 1 ? 'Yes' : 'No';
            }
        ]);
        $this->crud->setColumnDetails('has_medical_aid', [
            'exportOnlyField' => true,
            'visibleInTable'  => false,
            'visibleInExport' => true,
            'type'     => 'closure',
            'function' => function($entry) {
                return $entry->has_medical_aid == 1 ? 'Yes' : 'No';
            }
        ]);
        $this->crud->setColumnDetails('other_facility', [
            'exportOnlyField' => true,
            'visibleInTable'  => false,
            'visibleInExport' => true
        ]);
        $this->crud->setColumnDetails('latitude', [
            'exportOnlyField' => true,
            'visibleInTable'  => false,
            'visibleInExport' => true
        ]);
        $this->crud->setColumnDetails('longitude', [
            'exportOnlyField' => true,
            'visibleInTable'  => false,
            'visibleInExport' => true
        ]);
        $this->crud->setColumnDetails('contact', [
            'exportOnlyField' => true,
            'visibleInTable'  => false,
            'visibleInExport' => true,
            'type'     => 'closure',
            'function' => function($entry) {
                $contact = json_decode($entry->contact);
                //dd($contact);
                if($contact && sizeof($contact) > 0){
                    $contact_html = '';
                    foreach ($contact as $cinfo){
                        $contact_html .= 'Focal Person: '.$cinfo->focal_person.';';
                        $contact_html .= 'Designation: '.$cinfo->designation.';';
                        $contact_html .= 'Contact No: '.$cinfo->contact_no.';';
                    }
                    return $contact_html;
                }
                return '--';
            }
        ]);

        $this->crud->setColumnDetails('district_id',[
            'label' => 'District',
            'type' => 'select',
            'name' => 'district_id', // the db column for the foreign key
            'entity' => 'rel_district', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\ReliefCamp" // foreign key model
        ]);

        $this->crud->setColumnDetails('tehsil_id',[
            'label' => 'Tehsil',
            'type' => 'select',
            'name' => 'tehsil_id', // the db column for the foreign key
            'entity' => 'rel_tehsil', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\ReliefCamp" // foreign key model
        ]);

         $this->crud->setColumnDetails('uc_id',[
            'label' => 'UC',
            'type' => 'select',
            'name' => 'uc_id', // the db column for the foreign key
            'entity' => 'rel_uc', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\ReliefCamp" // foreign key model
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ReliefCampRequest::class);

      //  CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }




    public function getDistricts()
    {
        $entries = District::all();
        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {
            $tab[$entry->id] = $entry->name;
        }
        return $tab;
    }

    public function getUcList()
    {
        $entries = Uc::all();
        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {
            $tab[$entry->id] = $entry->name;
        }
        return $tab;
    }

    public function getTehsilList()
    {
        $entries = Tehsil::all();
        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {
            $tab[$entry->id] = $entry->name;
        }
        return $tab;
    }


    // public function store()
    // {
    //    //dd($users);
    //     $response = $this->traitStore();
    //     dd($response);
    // }



    public function addCustomCrudFilters()
    {




        $this->crud->addFilter([ // select2 filter
            'name' => 'district_id',
            'type' => 'select2',
            'label'=> 'District'
          ], function() {

              return District::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'district_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'tehsil_id',
            'type' => 'select2',
            'label'=> 'Tehsil'
          ], function() {

              return Tehsil::all()->keyBy('id')->pluck('name', 'id')->toArray();
          },
          function($value) { // if the filter is active
            $this->crud->addClause('where', 'tehsil_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'uc_id',
            'type' => 'select2',
            'label'=> 'Uc'
          ], function() {

              return Uc::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'uc_id', $value);
          });

    }

}
