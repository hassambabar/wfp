<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RiskmanagementRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class RiskmanagementCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RiskmanagementCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Riskmanagement::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/riskmanagement');
        CRUD::setEntityNameStrings('risk management', 'risk managements');

        $this->crud->addColumn([
			'name'  => 'ngo_id',
            'label' => 'NGO',
            'type'  => 'model_function',
            'function_name' => 'getTitleNgo',
        ]);
        $this->crud->addColumn([
			'name'  => 'title',
            'label' => 'Title',

        ]);
        $this->crud->addColumn([
			'name'  => 'subject',
            'label' => 'Subject',

        ]);

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeButton('delete');
       // CRUD::setFromDb(); // columns
       if(backpack_user()->hasRole('Admin')){

    }else{
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');

    }

       $this->crud->removeButton('create');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(RiskmanagementRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->removeButton('delete');
        $this->crud->addColumn([
            'name' => 'image',
            'label' => 'Images',
            'type'  => 'model_function',
            'function_name' => 'getimages',

        ]);

        $this->crud->addColumn([
            'name' => 'document',
            'label' => 'Document',
            'type'  => 'model_function',
            'function_name' => 'getDocument',

        ]);

    }

}
