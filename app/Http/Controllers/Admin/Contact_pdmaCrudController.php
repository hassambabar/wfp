<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Contact_pdmaRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\User;

/**
 * Class Contact_pdmaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class Contact_pdmaCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Contact_pdma::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/contact_pdma');
        CRUD::setEntityNameStrings('contact pdma', 'contact For PDMA');
        $this->addCustomCrudFilters();

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        /*$this->crud->denyAccess('create');
$this->crud->denyAccess('delete');
$this->crud->denyAccess('update');*/
        $this->crud->removeButton('delete');
if(backpack_user()->hasRole('Admin')){
    $this->crud->removeButton('create');

}else{
    $this->crud->removeButton('create');
    $this->crud->removeButton('delete');

}


$this->crud->enableExportButtons();

$this->crud->addColumn([
    'label' => 'NGO',
    'type' => 'model_function',
    'name' => 'ngo_id',
    'function_name' => 'getNgoName',
    'attribute' => 'name',
    'model' => "App\Models\Contact_pdma",
]);

        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(Contact_pdmaRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    protected function setupShowOperation()
    {
        $this->crud->removeButton('delete');
    }
    public function addCustomCrudFilters()
    {
        $this->crud->addFilter([
            'name' => 'ngo_id',
            'type' => 'select2',
            'label'=> 'NGO'
          ], function() {

            $user = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->where('model_has_roles.role_id',1)
            ->pluck('users.name', 'users.id','model_has_roles.role_id','model_has_roles.model_id')
            ->all();

            return $user;
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'ngo_id', $value);
          });



    }
}
