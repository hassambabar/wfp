<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProjectRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\User;
use App\Models\District;
use App\Models\ThematicArea;
use App\Models\Sector_Cluster;
use App\Tehsil;
use App\Uc;

use App\Models\ProjectCategory;
use App\Models\ProjectActivity;
use App\Models\Province;
use App\Models\FundingAgency;
use App\Models\Project_modality;
use App\Models\ImplementingPartner;
use App\Models\Implementing_organization_type;
use App\Models\ProjectsNoc;
use Illuminate\Support\Facades\Route;


/**
 * Class ProjectCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProjectCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {

        CRUD::setModel(\App\Models\Project::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/project');
        CRUD::setEntityNameStrings('project', 'projects');
        $this->crud->enableExportButtons();
        $this->crud->addClause('where', 'status', '=', 1);
        if(backpack_user()->hasRole('Admin')){

        }else{
            $district_id = backpack_user()->district_id;
            if(!empty($district_id)){
                $this->crud->addClause('where', 'project_district', '=', $district_id);
            }
        }


        $this->addCustomCrudFilters();
        $this->crud->setShowView('vendor.backpack.crud.showProject');
        $this->crud->setListView('vendor.backpack.crud.listProject');

        $this->crud->addColumn([
          'label' => 'NGO',
          'type' => 'model_function',
          'name' => 'ngo_id',
          'function_name' => 'getNgoName',
          'attribute' => 'name',
          'model' => "App\Models\Donor",
      ]);

      $this->crud->addColumn([
          'label' => 'Project Title',
          'name' => 'project_title',

      ]);
      $this->crud->addColumn([
          'label' => 'Start Date ',
          'name' => 'start_date',

      ]);
      $this->crud->addColumn([
          'label' => 'End Date',
          'name' => 'end_date',

      ]);
        $this->crud->addColumn([
          'label' => 'Sector',
          'name' => 'project_sector',
          'type' => 'model_function',
          'function_name' => 'getsectorTitle',
          'attribute' => 'name',
          'model' => "App\Models\Project",

      ]);

      $this->crud->addColumn([
          'label' => 'Thematic Area',
          'name' => 'thematic_area',
          'type' => 'model_function',
          'function_name' => 'getthematicAreaTitle',
          'attribute' => 'name',
          'model' => "App\Models\Project",

      ]);
      $this->crud->addColumn([
          'label' => 'District',
          'type' => 'model_function',
          'name' => 'project_district',
          'function_name' => 'getdistrictTitle',
          'attribute' => 'name',
          'model' => "App\Models\Project",
      ]);
        $this->crud->addColumn([
            'label' => 'UC',
            'type' => 'model_function',
            'name' => 'project_uc',
            'function_name' => 'getucTitle',
            'attribute' => 'name',
            'model' => "App\Models\Project",
        ]);
      $this->crud->addColumn([
        'label' => 'Donor',
        'name' => 'donor',
        'type' => 'model_function',
        'function_name' => 'getprojectdoneTitle',
        'attribute' => 'name',
        'model' => "App\Models\Project",

      ]);

      $this->crud->addColumn([
        'name'  => 'status',
        'label' => 'Status',
        'type'    => 'select_from_array',
        'options'         => [ 0 => 'Pending', 1 => 'Accept']
    ]);

        if(backpack_user()->hasRole('Admin')){

        }else{
            $district_id = backpack_user()->district_id;

            if(!empty($district_id)){
                $this->crud->addClause('where', 'project_district', '=', $district_id);
            }

        }

        $this->crud->addField([
          'label' => 'NGO',
          'type' => 'select2_from_array',
          'name' => 'ngo_id',
          'options'     => $this->GetNGO(),
          'wrapperAttributes' => ['class' => 'form-group col-md-6'],
          'allows_null'     => true,
          'default' => 'one',
      ]);

      $this->crud->addField([
        'label' => 'donor',
        'type' => 'hidden',
        'name' => 'donor',

    ]);
        $this->crud->addField([
          'label' => 'District',
          'type' => 'select2_from_array',
          'name' => 'project_district',
          'options'     => $this->Getdistrict(),
          'wrapperAttributes' => ['class' => 'form-group col-md-6'],
          'allows_null'     => true,
          'default' => 'one',
      ]);
      $this->crud->addField([
        'label' => 'Category',
        'name' => 'project_category',
        'type' => 'select2_from_array',
        'options'     => $this->ProjectCategoryTitle(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',

    ]);
    $this->crud->addField([
      'label' => 'Implementing Partner',
      'name' => 'implementing_partner',
      'type' => 'select2_from_array',
      'options'     => $this->ImplementingPartnerTitle(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',

  ]);
  $this->crud->addField([
    'label' => 'Implementing Org Type',
    'name' => 'implementing_org_type',
    'type' => 'select2_from_array',
    'options'     => $this->getImplementingOrgtypeTitle(),
    'wrapperAttributes' => ['class' => 'form-group col-md-6'],
    'allows_null'     => true,
    'default' => 'one',

]);
$this->crud->addField([
  'label' => 'Province',
  'name' => 'project_province',
  'type' => 'select2_from_array',
  'options'     => $this->getprovTitle(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',

]);  $this->crud->addField([
  'label' => 'Tehsil',
  'name' => 'project_tehsil',
  'type' => 'select2_from_array',
  'options'     => $this->GetTehsil(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
]);  $this->crud->addField([
  'label' => 'UC',
  'name' => 'project_uc',
  'type' => 'select2_from_array',
  'options'     => $this->GetUc(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',

]);
$this->crud->addField([
  'label' => 'Modality',
  'name' => 'project_modality',
  'type' => 'select2_from_array',
  'options'     => $this->getproject_modalityTitle(),
  'wrapperAttributes' => ['class' => 'form-group col-md-6'],
  'allows_null'     => true,
  'default' => 'one',

]);
$this->crud->addField([
  'label' => 'Thematic area',
  'name' => 'thematic_area',
  'type' => 'select2_from_array',
  'options'     => $this->getthematic_area(),
  'wrapperAttributes' => ['class' => 'form-group col-md-6'],
  'allows_null'     => true,
  'default' => 'one',

]);
$this->crud->addField([
  'label' => 'Project Sector',
  'name' => 'project_sector',
  'type' => 'select2_from_array',
  'options'     => $this->getproject_sector(),
  'wrapperAttributes' => ['class' => 'form-group col-md-6'],
  'allows_null'     => true,
  'default' => 'one',

]);
        $this->crud->addColumn([
            'label' => 'Category',
            'name' => 'project_category',
            'type' => 'model_function',
            'function_name' => 'getcategoryTitle',
            'attribute' => 'name',
            'model' => "App\Models\Project",

        ]);
        $this->crud->addColumn([
            'label' => 'Implementing Partner',
            'name' => 'implementing_partner',
            'type' => 'model_function',
            'function_name' => 'getImplementingPartnerTitle',
            'attribute' => 'name',
            'model' => "App\Models\Project",

        ]);
        $this->crud->addColumn([
            'label' => 'Implementing Org Type',
            'name' => 'implementing_org_type',
            'type' => 'model_function',
            'function_name' => 'getImplementingOrgtypeTitle',
            'attribute' => 'name',
            'model' => "App\Models\Project",

        ]);
        $this->crud->addColumn([
            'label' => 'Province',
            'name' => 'project_province',
            'type' => 'model_function',
            'function_name' => 'getprovTitle',
            'attribute' => 'name',
            'model' => "App\Models\Project",

        ]);
        $this->crud->addColumn([
        'label' => 'Tehsil',
        'name' => 'project_tehsil',
        'type' => 'model_function',
        'function_name' => 'getptehsilTitle',
        'attribute' => 'name',
        'model' => "App\Models\Project",

    ]);  $this->crud->addColumn([
        'label' => 'UC',
        'name' => 'project_uc',
        'type' => 'model_function',
        'function_name' => 'getucTitle',
        'attribute' => 'name',
        'model' => "App\Models\Project",

    ]);
        $this->crud->addColumn([
            'label' => 'Modality',
            'name' => 'project_modality',
            'type' => 'model_function',
            'function_name' => 'getproject_modalityTitle',
            'attribute' => 'name',
            'model' => "App\Models\Project",

        ]);
        $this->crud->addColumn([
            'name'     => 'prev_extension',
            'label'    => 'Previous Extension',
            'type'     => 'closure',
            'function' => function($entry) {
                if($entry->prev_extension == -1){
                    return 'Not Applicable';
                }
                if ($entry->prev_extension == 0){
                    return 'No';
                }
                return 'Yes';
            }
        ])->afterColumn('project_status');

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns
        if(backpack_user()->hasRole('Admin')){
            $this->crud->removeButton('create');


        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }
        $this->crud->addButtonFromView('line', 'show_summary', 'show_summary', 'end');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /*protected function setupSummaryRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/{id}/summary/modal', [
            'as'        => $routeName.'.summary-modal',
            'uses'      => $controller.'@getSummaryModal',
            'operation' => 'summary',
        ]);
    }

    public function getSummaryModal($id){
        if (! request()->has('entity')) {
            abort(400, 'No "entity" inside the request.');
        }
        $this->crud->hasAccessOrFail('update');
        $this->crud->setOperation('Summary');
//        return view('vendor.backpack.crud.summary', $this->data);
        return view(
            'crud::modals.report_summary_modal',
            [
                'crud' => $this->crud,
                'entity' => request()->get('entity'),
            ]
        );
    }*/

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProjectRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    protected function setupShowOperation()
    {
        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }
      $this->crud->addColumn([
        'label' => 'Category',
        'name' => 'project_category',
        'type' => 'model_function',
        'function_name' => 'getcategoryTitle',
        'attribute' => 'name',
        'model' => "App\Models\Project",

    ]);
    $this->crud->addColumn([
      'label' => 'Implementing Partner',
      'name' => 'implementing_partner',
      'type' => 'model_function',
      'function_name' => 'getImplementingPartnerTitle',
      'attribute' => 'name',
      'model' => "App\Models\Project",

  ]);
  $this->crud->addColumn([
    'label' => 'Implementing Org Type',
    'name' => 'implementing_org_type',
    'type' => 'model_function',
    'function_name' => 'getImplementingOrgtypeTitle',
    'attribute' => 'name',
    'model' => "App\Models\Project",

]);
$this->crud->addColumn([
  'label' => 'Province',
  'name' => 'project_province',
  'type' => 'model_function',
  'function_name' => 'getprovTitle',
  'attribute' => 'name',
  'model' => "App\Models\Project",

]);  $this->crud->addColumn([
  'label' => 'Tehsil',
  'name' => 'project_tehsil',
  'type' => 'model_function',
  'function_name' => 'getptehsilTitle',
  'attribute' => 'name',
  'model' => "App\Models\Project",

]);  $this->crud->addColumn([
  'label' => 'UC',
  'name' => 'project_uc',
  'type' => 'model_function',
  'function_name' => 'getucTitle',
  'attribute' => 'name',
  'model' => "App\Models\Project",

]);
$this->crud->addColumn([
  'label' => 'Modality',
  'name' => 'project_modality',
  'type' => 'model_function',
  'function_name' => 'getproject_modalityTitle',
  'attribute' => 'name',
  'model' => "App\Models\Project",

]);
        $this->crud->addColumn([
            'name'     => 'prev_extension',
            'label'    => 'Previous Extension',
            'type'     => 'closure',
            'function' => function($entry) {
                if($entry->prev_extension == -1){
                    return 'Not Applicable';
                }
                if ($entry->prev_extension == 0){
                    return 'No';
                }
                return 'Yes';
            }
        ])->afterColumn('project_status');

        $this->crud->addColumn([
            'label' => 'Source of Funds',
            'type' => 'model_function',
            'name' => 'source_of_funds',
            'function_name' => 'getFileUrl',
            'attribute' => 'name',
        ]);

    }

   /* protected function setupSummaryOperation(){
        $this->crud->addColumn([
            'label' => 'Project'
        ]);
    }*/

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function addCustomCrudFilters()
    {
        $this->crud->addFilter([
            'name' => 'ngo_id',
            'type' => 'select2',
            'label'=> 'NGO'
          ], function() {

            $user = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->where('model_has_roles.role_id',1)
            ->pluck('users.name', 'users.id','model_has_roles.role_id','model_has_roles.model_id')
            ->all();

            return $user;
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'ngo_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'project_district',
            'type' => 'select2',
            'label'=> 'District'
          ], function() {

              return District::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'project_district', $value);
          });

        $this->crud->addFilter([ // select2 filter
            'name' => 'project_uc',
            'type' => 'select2',
            'label'=> 'UC'
        ], function() {

            return Uc::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'project_uc', $value);
        });

          $this->crud->addFilter([ // select2 filter
            'name' => 'thematic_area',
            'type' => 'select2',
            'label'=> 'Thematic Area'
          ], function() {

              return ThematicArea::all()->keyBy('id')->pluck('area_name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'thematic_area', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'project_sector',
            'type' => 'select2',
            'label'=> 'Sector'
          ], function() {

              return Sector_Cluster::all()->keyBy('id')->pluck('title', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'project_sector', $value);
          });

        $this->crud->addFilter([ // dropdown filter
            'name' => 'project_status',
            'type' => 'dropdown',
            'label'=> 'Project Status',
        ], [ 'completed'=> 'Completed', 'ongoing' => 'On-Going'], function ($value) {
            // if the filter is active
            $this->crud->addClause('where', 'project_status', $value);
        });

        // daterange filter
        if(backpack_user()->hasRole('PDMA')){
            $this->crud->addFilter([
                'type'  => 'date_range',
                'name'  => 'start_date',
                'label' => 'Filter by Date'
            ],
                false,
                function ($value) { // if the filter is active, apply these constraints
                    $dates = json_decode($value);
                    $this->crud->addClause('where', 'start_date', '>=', $dates->from);
                    $this->crud->addClause('where', 'end_date', '<=', $dates->to);
                });
        }


    }

    public function Getdistrict()
    {
        $entries = District::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }
    public function getprovTitle()
    {
        $entries = Province::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }


    public function GetTehsil()
    {
        $entries = Tehsil::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }
    public function GetUc()
    {
        $entries = Uc::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }

    public function ProjectCategoryTitle()
    {
        $entries = ProjectCategory::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->category_name;
        }

        return $tab;
    }

    public function ImplementingPartnerTitle()
    {
        $entries = ImplementingPartner::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->title;
        }

        return $tab;
    }
    public function getImplementingOrgtypeTitle()
    {
        $entries = Implementing_organization_type::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->title;
        }

        return $tab;
    }
    public function getproject_modalityTitle()
    {
        $entries = Project_modality::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->title;
        }

        return $tab;
    }
    public function getthematic_area()
    {
        $entries = ThematicArea::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->area_name;
        }

        return $tab;
    }

    public function getproject_sector()
    {
        $entries = Sector_Cluster::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->title;
        }

        return $tab;
    }

    public function GetNGO()
    {
        $entries = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->where('model_has_roles.role_id',1)->where('status', 1)->get();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }


}
