<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\HazardProneAreasRequest;
use App\Models\District;
use App\Models\HazardType;
use App\Models\Tehsil;
use App\Models\Uc;
use App\Models\User;
use App\Notifications\HazardProneAreaAdded;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Notification;

/**
 * Class HazardProneAreasCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class HazardProneAreasCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation  { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\HazardProneAreas::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/hazard_prone_areas');
        CRUD::setEntityNameStrings('hazard prone areas', 'hazard prone areas');
        $this->addCustomCrudFilters();
        $this->crud->enableExportButtons();
        if(backpack_user()->hasRole('Admin')){

        }else{
            $district_id = backpack_user()->district_id;
            if(!empty($district_id)){
                $this->crud->addClause('where', 'district_id', '=', $district_id);
            }
        }

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeButton('delete');
        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('delete');

        }
        CRUD::setFromDb(); // columns
        //$this->crud->removeColumns(['district_id', 'uc_id','tehsil_id','hazard_type_id']);
        $this->crud->removeColumns(['longitude', 'latitude']);
        $this->crud->modifyColumn('village',[
            'label' => 'Town/Villages'
        ]);
        $this->crud->setColumnDetails('hazard_type_id',[
            'label' => 'Type of Hazard',
            'type'  => 'model_function',
            'function_name' => 'getHazardTypeName',
        ]);

        $this->crud->setColumnDetails('district_id',[
            'label' => 'District',
            'type'  => 'model_function',
            'function_name' => 'getDistrictTitle',
        ]);

        $this->crud->setColumnDetails('tehsil_id',[
            'label' => 'Tehsil',
            'visibleInTable'  => false,
            'visibleInExport' => true,
            'exportOnlyField' => true,
            'type'  => 'model_function',
            'function_name' => 'getTitleTehsil',
        ]);

        $this->crud->setColumnDetails('uc_id',[
            'visibleInTable'  => false,
            'visibleInExport' => true,
            'exportOnlyField' => true,
            'label' => 'UC',
            'type'  => 'model_function',
            'function_name' => 'getTitleUc',
        ]);


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(HazardProneAreasRequest::class);

        CRUD::setFromDb(); // fields

        $this->crud->removeFields(['hazard_type_id','district_id','uc_id','tehsil_id','expected_occurrence','happened_on']);

        $this->crud->addField([
            'type'        => 'select2_from_array',
            'label' => 'Type of Hazard',
            'name'      => 'hazard_type_id',
            'options'     => $this->getHazardTypes(),
            'allows_null'     => false,
            'default' => 'one',
        ])->beforeField('village');

        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'District',
            'name'      => 'district_id',
            'options'     => $this->getDistricts(),
            'allows_null'     => false,
            'default' => 'one',
        ])->afterField('hazard_type_id');

        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'Taluka',
            'name'      => 'tehsil_id',
            'options'     => $this->getTehsilList(),
            'allows_null'     => false,
            'default' => 'one',
        ])->afterField('district_id');

        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'UC',
            'name'      => 'uc_id',
            'options'     => $this->getUcList(),
            'allows_null'     => false,
            'default' => 'one',
        ])->afterField('tehsil_id');

        $this->crud->addField([
            'name'      => 'village',
            'label'     => 'Village',
            'type'      => 'text',
            'allows_null'     => false,
        ]);

        $this->crud->addField([
            'name'      => 'expected_occurrence',
            'label'             => 'Expected date of Occurrence',
            'type'  => 'date_picker',
            'allows_null'     => false,
        ]);
        $this->crud->addField([
            'name'      => 'happened_on',
            'label'             => 'When happened',
            'type'  => 'date_picker',
            'allows_null'     => false,
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->removeButton('delete');
    }

    public function getDistricts()
    {
        $entries = District::all();
        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {
            $tab[$entry->id] = $entry->name;
        }
        return $tab;
    }

    public function getUcList()
    {
        $entries = Uc::all();
        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {
            $tab[$entry->id] = $entry->name;
        }
        return $tab;
    }

    public function getTehsilList()
    {
        $entries = Tehsil::all();
        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {
            $tab[$entry->id] = $entry->name;
        }
        return $tab;
    }

    public function getHazardTypes()
    {
        $entries = HazardType::all();
        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {
            $tab[$entry->id] = $entry->name;
        }
        return $tab;
    }

    public function store()
    {
        $response = $this->traitStore();
        $data = $this->data['entry'];

        $users = User::select("users.*")
            ->join("model_has_roles","model_has_roles.model_id","=","users.id")->where('model_has_roles.role_id',1)
            ->where('users.status',1)
            ->get();

        Notification::send($users, new HazardProneAreaAdded($data));
        return $response;
    }


    public function addCustomCrudFilters()
    {

        $this->crud->addFilter([ // select2 filter
            'name' => 'hazard_type_id',
            'type' => 'select2',
            'label'=> 'Type of Hazard'
          ], function() {

              return HazardType::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'hazard_type_id', $value);
          });



        $this->crud->addFilter([ // select2 filter
            'name' => 'district_id',
            'type' => 'select2',
            'label'=> 'District'
          ], function() {

              return District::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'district_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'tehsil_id',
            'type' => 'select2',
            'label'=> 'Tehsil'
          ], function() {

              return Tehsil::all()->keyBy('id')->pluck('name', 'id')->toArray();
          },
          function($value) { // if the filter is active
            $this->crud->addClause('where', 'tehsil_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'uc_id',
            'type' => 'select2',
            'label'=> 'Uc'
          ], function() {

              return Uc::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'uc_id', $value);
          });

    }

}
