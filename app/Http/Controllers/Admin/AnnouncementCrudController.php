<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AnnouncementRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Forecastof;
use App\Models\District;
use Illuminate\Support\Facades\Notification;
use App\Notifications\AnnouncementCreated;
use App\User;

/**
 * Class AnnouncementCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AnnouncementCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Announcement::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/announcement');
        CRUD::setEntityNameStrings('announcement', 'Announcements & Alerts');
        $this->addCustomCrudFilters();
        $this->crud->enableExportButtons();
        $this->crud->setListView('vendor.backpack.crud.listAnnouncements');

        $this->crud->addColumn([
            'name' => 'forecast_of',
            'entity'    => 'Forecastof',
            'type'         => 'relationship',
            'attribute' => 'title',
        ]);

        $this->crud->addColumn([
            'name' => 'AnnouncementsDist',
            'label' => 'District',
            'type'         => 'relationship',
            'function_name' => 'AnnouncementsDist',

        ]);

        $this->crud->addField([
            'name' => 'forecast_of',
            'label' => 'Forecast Of',
            'type' => "relationship",
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'entity' => 'Forecastof',
            'attribute' => 'title',

        ]);
        // $this->crud->addField([
        //     'name' => 'district_id',
        //     'label' => 'District',
        //     'type' => "select2_multiple",
        //     'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        //     'entity' => 'district',
        //     'attribute' => 'name',
        //     'allows_multiple' => true,


        // ]);

        $this->crud->addField([
            'name' => 'AnnouncementsDist',
            'label' => 'District',
            'type'      => 'select2_multiple',

            'entity'    => 'AnnouncementsDist', // the method that defines the relationship in your Model
            'model'     => "App\Models\District", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            // 'pivot'     => true,
        ]);

        $this->crud->addField([
            'name' => 'from',
            'label' => 'From',
            'type' => "date_picker",
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],

        ]);

        $this->crud->addField([
            'name' => 'to',
            'label' => 'To',
            'type' => "date_picker",
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],

        ]);
        $this->crud->addField([
            'name' => 'subject',
            'label' => 'Subject',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([
            'name' => 'date_of_alert',
            'label' => 'Date of Alert',
            'type' => "date_picker",
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],

        ]);

        $this->crud->addField([
            'name' => 'message',
            'label' => 'Warning/Alert Message',
            'type'  => 'tinymce',
            'wrapperAttributes' => ['class' => 'form-group col-md-12'],

        ]);

        $this->crud->addField([
            'name' => 'signature',
            'label' => 'Signature',
            'type'  => 'textarea',
            'wrapperAttributes' => ['class' => 'form-group col-md-12'],
        ]);

        $this->crud->addField([
            'name' => 'date_of_alert',
            'label' => 'Date of Alert',
            'type' => "date_picker",
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],

        ]);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AnnouncementRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


    public function addCustomCrudFilters()
    {


        $this->crud->addFilter([
            'type'  => 'text',
            'name'  => 'subject',
            'label' => 'Subject'
          ],
          false,
          function($value) { // if the filter is active
             $this->crud->addClause('where', 'subject', 'LIKE', "%$value%");
          });

        // $this->crud->addFilter([ // select2 filter
        //     'name' => 'district_id',
        //     'type' => 'select2',
        //     'label'=> 'District'
        //   ], function() {

        //       return District::all()->keyBy('id')->pluck('name', 'id')->toArray();
        //   }, function($value) { // if the filter is active
        //     $this->crud->addClause('where', 'district_id', $value);
        //   });

          $this->crud->addFilter([ // select2 filter
            'name' => 'forecast_of',
            'type' => 'select2',
            'label'=> 'Forecast of'
          ], function() {

              return Forecastof::all()->keyBy('id')->pluck('title', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'forecast_of', $value);
          });






          $this->crud->addFilter([
            'type'  => 'date_range',
            'name'  => 'from_date',
            'label' => 'Filter by Date'
        ],
            false,
            function ($value) { // if the filter is active, apply these constraints
              if($value){
                  $dates = json_decode($value);
                  $this->crud->addClause('where', 'from', '>=', $dates->from);
                  $this->crud->addClause('where', 'to', '<=', $dates->to);
              }

            });


    }

    public function store()
    {
         $response = $this->traitStore();
        $date = $this->data['entry'];
//   dd( $date->AnnouncementsDist);
        $dist_list = '';
  foreach ($date->AnnouncementsDist as $key => $value) {
    $dist_list .= $value->name.',';
      # code...
  }
            $data_notification = array();
            $data_notification['title'] = $date->subject;
            $data_notification['forecast_of'] = $date->Forecastof->title;
            $data_notification['district_id'] = $dist_list;
            $data_notification['from'] = $date->from;
            $data_notification['to'] = $date->to;
            $data_notification['message'] = $date->message;
            $data_notification['created_at'] = $date->created_at;
            $users = User::select("users.*")
            ->where('status', 1)
            ->get();
            // dd( $users);
             Notification::send($users, new AnnouncementCreated($data_notification));
          return $response;
    }

    public function getSlugWithLink() {
        return '<a href="" target="_blank">Hello</a>';
    }

}

