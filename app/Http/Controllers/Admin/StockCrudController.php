<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StockRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\User;
use App\Models\District;
use App\Models\Tehsil;
use App\Models\Uc;
use App\Province;
use App\Models\Category_type;
use App\Models\Item_type;

/**
 * Class StockCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StockCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Stock::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/stock');
        CRUD::setEntityNameStrings('stock', 'stocks');
        $this->addCustomCrudFilters();


        $this->crud->addColumn([
            'label' => 'Name',
            'name' => 'name',

        ]);


        $this->crud->addColumn([
            'label' => 'NGO',
            'type' => 'model_function',
            'name' => 'ngo_id',
            'function_name' => 'getNgoName',
            'attribute' => 'name',
            'model' => "App\Models\Stock",
        ]);

        $this->crud->addColumn([
            'label' => 'Category',
            'type' => 'model_function',
            'name' => 'category_id',
            'function_name' => 'getCatName',
            'attribute' => 'name',
            'model' => "App\Models\Stock",
        ]);

        $this->crud->addColumn([
            'label' => 'Item Type',
            'type' => 'model_function',
            'name' => 'item_type_id',
            'function_name' => 'getItemName',
            'attribute' => 'name',
            'model' => "App\Models\Stock",
        ]);

        $this->crud->addColumn([
            'label' => 'District',
            'type' => 'model_function',
            'name' => 'district_id',
            'function_name' => 'getdistrictTitle',
            'attribute' => 'name',
        ]);
        $this->crud->addColumn([
            'label' => 'Tehsil',
            'name' => 'tehsil_id',
            'type' => 'model_function',
            'function_name' => 'getptehsilTitle',
            'attribute' => 'name',
        ]);
        $this->crud->addColumn([
        'label' => 'UC',
        'name' => 'uc_id',
        'type' => 'model_function',
        'function_name' => 'getucTitle',
        'attribute' => 'name',
        ]);


    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
//        $this->crud->removeButton('create');
        if(backpack_user()->hasRole('Admin')){
          $this->crud->removeButton('create');


        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }
        $this->crud->enableExportButtons();

        CRUD::setFromDb(); // columns
        $this->crud->removeColumn('location_id');
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StockRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }

    }

    public function addCustomCrudFilters()
    {
        $this->crud->addFilter([
            'name' => 'ngo_id',
            'type' => 'select2',
            'label'=> 'NGO'
          ], function() {

            $user = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->where('model_has_roles.role_id',1)
            ->pluck('users.name', 'users.id','model_has_roles.role_id','model_has_roles.model_id')
            ->all();

            return $user;
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'ngo_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'location_id',
            'type' => 'select2',
            'label'=> 'Location'
          ], function() {

              return Province::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'location_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'category_id',
            'type' => 'select2',
            'label'=> 'Category'
          ], function() {

              return Category_type::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'category_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'item_type_id',
            'type' => 'select2',
            'label'=> 'Item Type'
          ], function() {

              return Item_type::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'item_type_id', $value);
          });



    }
}
