<?php

namespace App\Http\Controllers\Admin;

use App\Affiliations;
use App\Http\Controllers\Controller;
use App\Models\Contact_pdma;
use App\Models\Emergency;
use App\Models\Needassesmentresponse;

use App\Models\Human_resource;
use App\Models\Ngo_contact;
use App\Models\NgoMou;
use App\Models\NgoProfile;
use App\Models\NgoProjects;
use App\Stocks;
use App\User;
use App\Volunteer;
use App\District;
use App\Models\Donor;
use App\Models\HazardProneAreas;
use App\Models\ReliefCamp;
use App\Models\Affiliation;



use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Notification;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $chart_user_district = '';
        $outputprojectfunds = '';
        $outputEmergency = '';

        $DonorCount = Donor::all()->count();
        $ReliefCampCount = ReliefCamp::all()->count();
        $AffiliationCount = Affiliation::all()->count();


        $HazardProneAreasCount = HazardProneAreas::all()->count();


        $ddmaTotal = User::select("users.*")
            ->join("model_has_roles","model_has_roles.model_id","=","users.id")->where('model_has_roles.role_id',2)
            ->count();
        $projectsCount = NgoProjects::where('project_status','=','ongoing')->count();
        $emergenciesCount = Emergency::all()->count();
        $emergencies = Emergency::all();

        $enlistedNgos = NgoProfile::all()->count();
        $enlistmentRequests = backpack_user()->notifications->filter(function ($item, $key) {
            return $item->type == 'App\Notifications\ProfileApproval';
        })->count();
        $nocRequests = backpack_user()->notifications->filter(function ($item, $key) {
            return $item->type == 'App\Notifications\RequestForNoc';
        })->count();

        $Volunteerall =Volunteer::get();
        $VolunteerTotle =count($Volunteerall);

        $Districts  = District::orderBy('name', 'asc')->get();
        foreach ($Districts as $key => $District) {
            $user_ngo = '';
            $Volunteer =  Volunteer::where('district_id', $District->id)->get();
            $volTotla =count($Volunteer);
                $chart_user_district .= "['$District->name', $volTotla],";
        }
        $pdmaTotal = User::select("users.*")
        ->join("model_has_roles","model_has_roles.model_id","=","users.id")->where('model_has_roles.role_id',2)
        ->count();
        $NgoTotal = User::select("users.*")
            ->join("model_has_roles","model_has_roles.model_id","=","users.id")->where('model_has_roles.role_id',1)
            ->count();

            // $projectsNgo = NgoProjects::where('project_status','=','ongoing')->get();
            $projectsNgo = NgoProjects::get();

            foreach ($projectsNgo as $key => $project) {

                $outputprojectfunds .= " [ '".addslashes($project->project_title)."', $project->funds_amount],";

            }

            $Emergencies = Emergency::get();

            foreach ($Emergencies as $key => $Emergency) {
                $Needassesmentresponse = Needassesmentresponse::where('emergency_id',$Emergency->id)->get();
                if(count($Needassesmentresponse) > 0){
                    $countresponce = count($Needassesmentresponse);
                }else{
                    $countresponce = '0';
                }

                $outputEmergency .= " [ '$Emergency->type_of_hazard', $countresponce],";

            }
// dd($outputEmergency);

        $outputdonut = " ['PDMA', $pdmaTotal],
        ['DDMA', $ddmaTotal],
        ['NGO', $NgoTotal], ['Volunteer', $VolunteerTotle]";

            $pk_orgCount = NgoProfile::where('legal_status','pk_org')->count();
            $corp_firmCount = NgoProfile::where('legal_status','corp_firm')->count();
            $intl_orgCount = NgoProfile::where('legal_status','intl_org')->count();
            $pk_charityCount = NgoProfile::where('legal_status','pk_charity')->count();
            $un_orgCount = NgoProfile::where('legal_status','un_org')->count();

            $allVolunteers = Volunteer::all()->count();
            $maleVolunteers = Volunteer::where('gender','male')->count();
            $femaleVolunteers = Volunteer::where('gender','female')->count();

            $projectsWithSectors = NgoProjects::with('projectSector')->where('project_status','ongoing')->where('status',1)->get()->groupBy('project_sector');
//dd($projectsWithSectors[1]->sum('funds_amount'));
            $totalOngoingProjects = array_sum($projectsWithSectors->map(function ($item, $key) {
                return $item->count();
            })->toArray());
            $totalOngoingProjectsFunds = array_sum($projectsWithSectors->map(function ($item, $key) {
                return $item->sum('funds_amount');
            })->toArray());
            $totalTargetFamilies = array_sum($projectsWithSectors->map(function ($item, $key) {
                return $item->sum('families_targeted');
            })->toArray());
            $totalAssistedFamilies = array_sum($projectsWithSectors->map(function ($item, $key) {
                return $item->sum('families_reached');
            })->toArray());

            $reliefCamps = ReliefCamp::where('latitude', '!=', 'NULL')->where('longitude', '!=','NULL')->get();
            $reliefCampsLatLang = $reliefCamps->map(function ($item, $key) {
                //dd(!$item->rel_district);
                $popData = '<br><b>District:</b> '.($item->rel_district == null ?  "--" : $item->rel_district->name).'<br>';
                $popData .= '<b>Taluka:</b> '.($item->rel_tehsil == null ?  "--" : $item->rel_tehsil->name).'<br>';
                $popData .= '<b>Site Name:</b> '.$item->site_name.'<br>';
                $popData .= '<b>Capacity:</b> '.$item->capacity.'<br>';
                return array($item->longitude, $item->latitude, $popData);
            });

            $hazardProneAreas = HazardProneAreas::all();
            $hazardProneAreasLatLang = $hazardProneAreas->map(function ($item, $key) {
                $popDataHPA = '<br><b>District:</b> '.($item->districtName == null ? "--" : $item->districtName->name) .'<br>';
                $popDataHPA .= '<b>Taluka:</b> '.($item->tehsilName == null ? "--" : $item->tehsilName->name).'<br>';
                $popDataHPA .= '<b>Happened On:</b> '.$item->happened_on.'<br>';
                $popDataHPA .= '<b>Expected Occurrence:</b> '.$item->expected_occurrence.'<br>';
                return array($item->longitude, $item->latitude, $popDataHPA);
            });

            $emergenciesAll = Emergency::where('latitude', '!=', 'NULL')->where('longitude', '!=','NULL')->get();
            $emergenciesLatLang = $emergenciesAll->map(function ($item, $key) {
                $popDataEmg = '<br><b>District:</b> '.($item->district == null ? "--" : $item->district->name).'<br>';
                $popDataEmg .= '<b>Taluka:</b> '.($item->tehsil == null ? "--" : $item->tehsil->name).'<br>';
                $popDataEmg .= '<b>Type of Emergency:</b> '.ucfirst($item->type_of_emergency).'<br>';
                $popDataEmg .= '<b>Hazard Type:</b> '.($item->hazardType == null ? "--" : $item->hazardType->name).'<br>';
                return array($item->longitude, $item->latitude, $popDataEmg);
            });
            /*dd($reliefCampsLatLang->map(function ($item, $key) {
                return array($item->latitude, $item->longitude);
            }));*/

        return view('vendor.backpack.base.dashboard',compact(
            'enlistedNgos','ddmaTotal','projectsCount','emergencies','enlistedNgos','enlistmentRequests','nocRequests','chart_user_district','outputdonut','outputprojectfunds','outputEmergency','VolunteerTotle','pdmaTotal',
            'NgoTotal','ddmaTotal','emergenciesCount','DonorCount','HazardProneAreasCount','ReliefCampCount','AffiliationCount','pk_charityCount','pk_orgCount','corp_firmCount','intl_orgCount','un_orgCount','allVolunteers','maleVolunteers','femaleVolunteers','projectsWithSectors','totalOngoingProjects','totalOngoingProjectsFunds','totalTargetFamilies','totalAssistedFamilies','reliefCampsLatLang','hazardProneAreasLatLang','emergenciesLatLang'
        ));
    }
}
