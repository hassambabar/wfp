<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UcRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Province;
use App\Models\District;
use App\Models\Tehsil;
/**
 * Class UcCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UcCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Uc::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/uc');
        CRUD::setEntityNameStrings('uc', 'ucs');


        $this->crud->addField([
			'name'  => 'name',
            'label' => 'Union council Name',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],

        ]);

        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'Province',
            'name'      => 'province_id',
            'options'     => $this->getprovince(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'District',
            'name'      => 'district_id',
            'options'     => $this->getDistrict(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        $this->crud->addField([ // select_from_array
            'type'        => 'select2_from_array',
            'label' => 'Tehsil',
            'name'      => 'tehsil_id',
            'options'     => $this->getTehsil(),
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
            'allows_null'     => true,
            'default' => 'one',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);


        
        $this->crud->addColumn([
			'name'  => 'name',
            'label' => 'Name',

        ]);

        $this->crud->addColumn([
            'label' => 'Province',
            'type' => 'model_function',
            'name' => 'province_id',
            'function_name' => 'getProvinceTitle',
            'attribute' => 'title',
            'model' => "App\Models\Union_council",
        ]);

        $this->crud->addColumn([
            'label' => 'District',
            'type' => 'model_function',
            'name' => 'district_id',
            'function_name' => 'getdistrictTitle',
            'attribute' => 'title',
            'model' => "App\Models\Union_council",
        ]);
        $this->crud->addColumn([
            'label' => 'Tehsil',
            'type' => 'model_function',
            'name' => 'tehsil_id',
            'function_name' => 'gettehsilTitle',
            'attribute' => 'title',
            'model' => "App\Models\Union_council",
        ]);

    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(UcRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


    public function getprovince()
	{
		$entries = Province::all();
		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {

          //  echo "<pre>"; print_r($entry->id); exit;

			$tab[$entry->id] = $entry->name;
			
			
		}
		
		return $tab;
    }
    public function getDistrict()
	{
		$entries = District::all();
		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {

          //  echo "<pre>"; print_r($entry->id); exit;

			$tab[$entry->id] = $entry->name;
			
			
		}
		
		return $tab;
    }
    public function getTehsil()
	{
		$entries = Tehsil::all();
		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {
			$tab[$entry->id] = $entry->name;	
		}
		return $tab;
    }


}
