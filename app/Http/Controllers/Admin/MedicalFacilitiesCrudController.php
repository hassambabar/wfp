<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MedicalFacilitiesRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MedicalFacilitiesCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MedicalFacilitiesCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\MedicalFacilities::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/medicalfacilities');
        CRUD::setEntityNameStrings('Medical Facilities', 'Medical Facilities Arranged/Provided');
        $this->crud->enableExportButtons();

        $this->crud->addColumn([
            'name' => 'relief_camp_id',
            'label'     => 'Relief Camp',
            'entity'    => 'reliefcamp',
            'type'         => 'relationship',
            'attribute' => 'title',

        ]);
        $this->crud->addField([
            'name'      => 'relief_camp_id',
            'label'     => 'Relief Camp',
            'type'      => 'relationship',
            'entity' => 'reliefcamp',
            'attribute' => 'title'
           
        ]);
        $this->crud->addField([
            'name'      => 'hospitals',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'dispensaries',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'label'     => 'BHUs',
            'name'      => 'bhus',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'medical_camps_established',
            'type'      => 'date_picker',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'focal_person_name',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'designation',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'email',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'phone',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(MedicalFacilitiesRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
