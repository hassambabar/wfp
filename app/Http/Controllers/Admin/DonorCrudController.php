<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DonorRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\User;
use App\Models\District;
/**
 * Class DonorCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DonorCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Donor::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/donor');
        CRUD::setEntityNameStrings('donor', 'donors');
        $this->addCustomCrudFilters();

        if(backpack_user()->hasRole('Admin')){

        }else{
            $district_id = backpack_user()->district_id;
            if(!empty($district_id)){
                $this->crud->addClause('where', 'project_district', '=', $district_id);
            }

        }


    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();

        $this->crud->removeButton('create');
        $this->crud->denyAccess('delete');
        $this->crud->denyAccess('update');
//        $this->crud->denyAccess('show');

        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }
        $this->crud->addColumn([
            'label' => 'NGO',
            'type' => 'model_function',
            'name' => 'ngo_id',
            'function_name' => 'getNgoName',
            'attribute' => 'name',
            'model' => "App\Models\Donor",
        ]);

        $this->crud->addColumn([
            'label' => 'Project Title',
            'name' => 'project_title',

        ]);
        $this->crud->addColumn([
            'label' => 'District',
            'type' => 'model_function',
            'name' => 'district_id',
            'function_name' => 'getdistrictTitle',
            'attribute' => 'name',
            'model' => "App\Models\Donor",
        ]);
        $this->crud->addColumn([
            'label' => 'Funds Donor Name',
            'name' => 'funds_donor_name',

        ]);
        $this->crud->addColumn([
            'label' => 'Funds Amount',
            'name' => 'funds_amount',

        ]);
        $this->crud->addColumn([
            'label' => 'Start Date',
            'name' => 'start_date',

        ]);
        $this->crud->addColumn([
            'label' => 'End Date',
            'name' => 'end_date',

        ]);
       // CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(DonorRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }

    }

    public function addCustomCrudFilters()
    {
        $this->crud->addFilter([
            'name' => 'ngo_id',
            'type' => 'select2',
            'label'=> 'NGO'
          ], function() {

            $user = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->where('model_has_roles.role_id',1)
            ->pluck('users.name', 'users.id','model_has_roles.role_id','model_has_roles.model_id')
            ->all();

            return $user;
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'ngo_id', $value);
          });

          $this->crud->addFilter([ // select2 filter
            'name' => 'district_id',
            'type' => 'select2',
            'label'=> 'District'
          ], function() {

              return District::all()->keyBy('id')->pluck('name', 'id')->toArray();
          }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'district_id', $value);
          });

        // daterange filter
        if(backpack_user()->hasRole('PDMA')){
            $this->crud->addFilter([
                'type'  => 'date_range',
                'name'  => 'start_date',
                'label' => 'Filter by Date'
            ],
                false,
                function ($value) { // if the filter is active, apply these constraints
                    $dates = json_decode($value);
                    $this->crud->addClause('where', 'start_date', '>=', $dates->from);
                    $this->crud->addClause('where', 'end_date', '<=', $dates->to);
                });
        }

    }

}
