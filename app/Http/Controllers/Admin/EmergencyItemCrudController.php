<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\EmergencyItemRequest;
use App\Models\Emergency;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;

/**
 * Class EmergencyItemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EmergencyItemCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        //$this->emergency_id = \Route::current()->parameter('id');

        CRUD::setModel(\App\Models\EmergencyItem::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/emergency-item');
        CRUD::setEntityNameStrings('emergency item', 'emergency items');
        $this->crud->enableExportButtons();
        /*if(request()->get('emergency_id') !== null){
            $this->crud->setRoute(url('admin/emergency-item/list?emergency_id='.request()->get('emergency_id')));
        }*/

        //if($this->emergency_id !== null){
            //$this->crud->addClause('where','emergency_id','=', (int) $this->emergency_id);
           /* $this->crud->addFilter([ // select2 filter
                'name' => 'emergency_id',
                'type' => 'select2',
                'label'=> 'Emergency'
            ], function() {

                return Emergency::all()->keyBy('id')->pluck('id', 'id')->toArray();
            }, function($value) { // if the filter is active
                $this->crud->addClause('where', 'emergency_id', 26);
            });*/
        //}
        $this->crud->addFilter([
            'name' => 'emergency_id',
            'type' => 'select2',
            'label' => 'Emergency ID'
        ], function() {

            return Emergency::all()->keyBy('id')->pluck('id', 'id')->toArray();
        });

        /*$this->crud->addColumn([
            'name' => 'emergency_id',
            'visibleInTable'  => false,
        ]);*/

        $this->crud->addField([
            'name'      => 'emergency_id',
            'type'  => 'hidden',
            'value' => request()->get('emergency_id') ?? request()->get('emergency_id')
        ]);

        $this->crud->addField([
            'name'      => 'rescue_items',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'quantity',
            'type'  => 'number',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'description',
            'type'  => 'textarea',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'name'      => 'when_needed',
            'type'  => 'date_picker',
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        ]);

        //$this->crud->setFromDb();
    }

    protected function setupListingRoutes($segment, $routeName, $controller) {
        Route::get($segment.'/list', [
            'as'        => $routeName.'.list',
            'uses'      => $controller.'@list',
            'operation' => 'list',
        ]);
        /*Route::post($segment.'/search', [
            'as'        => $routeName.'.search',
            'uses'      => $controller.'@search',
            'operation' => 'list',
        ]);*/
    }

    public function list(){

        $this->crud->hasAccessOrFail('list');
        $this->emergency_id = request()->segment(3);
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? mb_ucfirst($this->crud->entity_name_plural);

        $this->crud->operation('list', function () {
            $this->crud->addClause('where','emergency_id','=',26);
        });
        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getListView(), $this->data);
    }


    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns
        $eid = request()->get('emergency_id');
        //$this->crud->removeButton('create');
        if($eid != null){
            $this->crud->modifyButton('create', ['content' => 'crud::buttons.create_modify']);
            $this->crud->addButtonFromView('top', 'send_notification', 'send_notification', 'end');
        }
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(EmergencyItemRequest::class);

        CRUD::setFromDb(); // fields
        /*$this->crud->removeFields([
            'emergency_id','rescue_items','quantity',
            'description','when_needed'
        ]);*/
        $eid = request()->get('emergency_id');
        if($eid){
            $this->crud->removeSaveAction('save_and_new');
            $this->crud->addSaveAction([
                'name' => 'save_and_new_mod',
                'redirect' => function($crud, $request, $itemId) {
                    $eid = request()->get('emergency_id');
                    return $crud->route.'/create?emergency_id='.$eid;
                }, // what's the redirect URL, where the user will be taken after saving?

                // OPTIONAL:
                'button_text' => 'Save and new', // override text appearing on the button
                // You can also provide translatable texts, for example:
                // 'button_text' => trans('backpack::crud.save_action_one'),
                'visible' => function($crud) {
                    return true;
                }, // customize when this save action is visible for the current operation
                'referrer_url' => function($crud, $request, $itemId) {
                    return $crud->route;
                }, // override http_referrer_url
                'order' => 1, // change the order save actions are in
            ]);
        }

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function getEmergencies()
    {
        $entries = Emergency::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = 'Emergency #'.$entry->id;
        }

        return $tab;
    }
}
