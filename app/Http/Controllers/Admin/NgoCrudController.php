<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NgoRequest;
use App\Models\District;
use App\Models\FundingAgency;
use App\Models\Implementing_organization_type;
use App\Models\ImplementingPartner;
use App\Models\Project_modality;
use App\Models\ProjectCategory;
use App\Models\Province;
use App\Models\Sector_Cluster;
use App\Tehsil;
use App\Uc;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class NgoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class NgoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $value = 1;
        //$this->crud->setModel(config('backpack.permissionmanager.models.user'));
        // CRUD::setModel(\App\Models\User::class);
        CRUD::setModel(\App\Models\BackpackUser::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/ngo');
        CRUD::setEntityNameStrings('NGO', "NGO");
        $this->addCustomCrudFilters();

        if(backpack_user()->hasRole('Admin')){

        }else{
            $district_id = backpack_user()->district_id;
            if(!empty($district_id)){
                $this->crud->addClause('where', 'district_id', '=', $district_id);
            }

        }


        $record = $this->crud->getCurrentEntry();
        $focalperson = ($record)?($record->focalperson):null;
        $reporting = ($record)?($record->reporting):null;

            $this->crud->addClause('whereHas', 'roles', function ($query) use ($value) {
                $query->where('role_id', '=', $value);
            });

            $this->crud->addColumn([
                'name'  => 'name',
                'label' => 'Name',

            ]);
            $this->crud->addColumn([
                'name'  => 'email',
                'label' => 'Email',

            ]);
            // $this->crud->addColumn([
            //     'name'  => 'district_id ',
            //     'label' => 'District ', // Table column heading
            //     'entity'    => 'district',
            //     'type'         => 'relationship',
            //     'attribute' => 'name',

            // ]);
            $this->crud->addColumn([
                'name'  => 'Volunteers ',
                'label' => 'Volunteers ', // Table column heading
                'type'  => 'model_function',
                'function_name' => 'getSlugWithLink', // the method in your Model

            ]);
            $this->crud->addColumn([
                'name'  => 'Contact_PDMA ',
                'label' => 'Contact For PDMA ', // Table column heading
                'type'  => 'model_function',
                'function_name' => 'getcontactPDMALink', // the method in your Model

            ]);
            $this->crud->addColumn([
                'name'  => 'ngo_contact ',
                'label' => 'NGO Contacts ', // Table column heading
                'type'  => 'model_function',
                'function_name' => 'ngo_contact', // the method in your Model

            ]);
            $this->crud->addColumn([
                'name'  => 'ngo_affiliation ',
                'label' => 'NGO Affiliation ', // Table column heading
                'type'  => 'model_function',
                'function_name' => 'ngo_affiliation', // the method in your Model

            ]);
            $this->crud->addColumn([
                'name'  => 'Stocks ',
                'label' => 'Stocks ', // Table column heading
                'type'  => 'model_function',
                'function_name' => 'ngo_Stocks', // the method in your Model

            ]);
        $this->crud->addColumn([
            'name'  => 'hr',
            'label' => 'Human Resources ', // Table column heading
            'type'  => 'model_function',
            'function_name' => 'ngo_HR', // the method in your Model

        ]);
            $this->crud->addColumn([
                'name'  => 'status',
                'label' => 'Status',
                'type'  => 'boolean',
                'options' => [1 => 'Active', 0 => 'Inactive', 2 => 'Pending'],
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],

            ]);

            $this->crud->addField([
                'name'  => 'status',
                'label' => 'Status',
                'type'  => 'select_from_array',
                'options' => [1 => 'Active', 0 => 'Inactive', 2 => 'Pending'],
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'allows_null'     => false,
            ]);

            $this->crud->addField([ // select_from_array
                'name'            => 'district_id',
                'label'           => 'district_id',
                'type'            => 'hidden',

            ]);



            // =========> Tab Contact Person <=========
            // mailing_address
            $this->crud->addField([
                'name'              => 'focalperson_mailing_address',
                'label'              => 'Mailing Address',
                'tab'               => 'Contact Person',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => ($focalperson)?($focalperson->mailing_address):null,
            ]);
            // city
            $this->crud->addField([
                'name'              => 'focalperson_city',
                'label'              => 'City',
                'tab'               => 'Contact Person',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $focalperson?$focalperson->city_id:null,
            ]);
            // phone (landline)
            $this->crud->addField([
                'name'              => 'focalperson_phone_landline',
                'label'              => 'Phone (landline)',
                'tab'               => 'Contact Person',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $focalperson?$focalperson->landline:null,
            ]);
            // phone (cell)
            $this->crud->addField([
                'name'              => 'focalperson_phone_cell',
                'label'              => 'Phone (cell)',
                'tab'               => 'Contact Person',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $focalperson?$focalperson->cell:null,
            ]);
            // fax
            $this->crud->addField([
                'name'              => 'focalperson_fax',
                'label'              => 'Fax',
                'tab'               => 'Contact Person',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $focalperson?$focalperson->fax:null,
            ]);
            // email
            $this->crud->addField([
                'name'              => 'focalperson_email',
                'label'              => 'Email',
                'tab'               => 'Contact Person',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $focalperson?$focalperson->email:null,
            ]);
            // website
            $this->crud->addField([
                'name'              => 'focalperson_website',
                'label'              => 'Website',
                'tab'               => 'Contact Person',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $focalperson?$focalperson->website:null,
            ]);
            // Facebook
            $this->crud->addField([
                'name'              => 'focalperson_facebook',
                'label'              => 'Facebook',
                'tab'               => 'Contact Person',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $focalperson?$focalperson->facebook:null,
            ]);

             // dump( $focalperson  );
             // dump( $reporting   );

            // =========> Tab Reporting <=========
            // Context
            $this->crud->addField([
                'label'             => 'Context',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'value'             => $reporting?$reporting->project_category:null,
                'type' => 'select2_from_array',
                'name' => 'project_category',
                'options'     => $this->getProjectCategory(),
                'allows_null'     => false,
                'default' => '',
            ]);
            // Donor/Funding org.
            $this->crud->addField([
                'name'              => 'donor',
                'label'             => 'Donor/Funding org',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'select2_from_array',
                'value'             => $reporting?$reporting->donor:null,
                'options'     => $this->getFundingAgency(),
                'allows_null'     => false,
                'default' => '',
            ]);
            // Project code.
            $this->crud->addField([
                'name'              => 'project_code',
                'label'             => 'Project code',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->project_code:null,
            ]);
            // Project Modality.
            $this->crud->addField([
                'name'              => 'project_modality',
                'label'             => 'Project Modality',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'value'             => $reporting?$reporting->project_modality:null,
                'type'              => 'select2_from_array',
                'options'     => $this->getProjectModality(),
                'allows_null'     => false,
                'default' => '',
            ]);
            // Project Title.
            $this->crud->addField([
                'name'              => 'project_title',
                'label'             => 'Project Title',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->project_title:null,
            ]);
            // Project Owner
            $this->crud->addField([
                'name'              => 'project_owner_name',
                'label'             => 'Project Owner',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->project_owner_name:null,
            ]);
            // Project Owner Organization Type
            $this->crud->addField([
                'name'              => 'project_owner_org_type',
                'label'             => 'Project Owner Org. Type',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->project_owner_org_type:null,
            ]);
            // Implementing Partner
            $this->crud->addField([
                'name'              => 'implementing_partner',
                'label'             => 'Implementing Partner',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'value'             => $reporting?$reporting->implementing_partner:null,
                'type'              => 'select2_from_array',
                'options'     => $this->getImplementingPartner(),
                'allows_null'     => false,
                'default' => '',
            ]);
            // IP/Org type
            $this->crud->addField([
                'name'              => 'implementing_org_type',
                'label'             => 'IP/Org type',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'value'             => $reporting?$reporting->implementing_org_type:null,
                'type'              => 'select2_from_array',
                'options'     => $this->getImplementingOrgType(),
                'allows_null'     => false,
                'default' => '',
            ]);
           // Sector / Cluster
            $this->crud->addField([
                'name'              => 'project_sector',
                'label'             => 'Sector / Cluster',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'value'             => $reporting?$reporting->project_sector:null,
                'type'              => 'select2_from_array',
                'options'     => $this->getproject_sector(),
                'allows_null'     => false,
                'default' => '',
            ]);

            if( $reporting && $reporting->activities ){

                foreach ($reporting->activities as $akey => $activity) {

                // dump($activity);
                    // Activity
                    $this->crud->addField([
                        'name'              => 'activity'.$akey,
                        'label'             => 'Activity',
                        'tab'               => 'Reporting',
                        'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                        'type'              => 'text',
                        'value'             => $activity->activity,
                    ]);
                    // Activity Description
                    $this->crud->addField([
                        'name'              => 'description'.$akey,
                        'label'             => 'Activity Description',
                        'tab'               => 'Reporting',
                        'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                        'type'              => 'text',
                        'value'             => $activity->description,
                    ]);
                    // Activity Province
                    $this->crud->addField([
                        'name'              => 'reporting_activity_province_'.$akey,
                        'label'             => 'Province',
                        'tab'               => 'Reporting',
                        'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                        'value'             => $activity->activity_province,
                        'type'              => 'select2_from_array',
                        'options'     => $this->getprovTitle(),
                        'allows_null'     => false,
                        'default' => '',
                    ]);
                    // Activity District
                    $this->crud->addField([
                        'name'              => 'activity_district'.$akey,
                        'label'             => 'District',
                        'tab'               => 'Reporting',
                        'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                        'value'             => $activity->activity_district,
                        'type'              => 'select2_from_array',
                        'options'     => $this->Getdistrict(),
                        'allows_null'     => false,
                        'default' => '',
                    ]);
                    // Activity Tehsil
                    $this->crud->addField([
                        'name'              => 'activity_tehsil'.$akey,
                        'label'             => 'Taluka',
                        'tab'               => 'Reporting',
                        'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                        'value'             => $activity->activity_tehsil,
                        'type'              => 'select2_from_array',
                        'options'     => $this->GetTehsil(),
                        'allows_null'     => false,
                        'default' => '',
                    ]);
                    // Activity Union Council
                    $this->crud->addField([
                        'name'              => 'reporting_activity_uc_'.$akey,
                        'label'             => 'Union Council',
                        'tab'               => 'Reporting',
                        'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                        'value'             => $activity->activity_uc,
                        'type'              => 'select2_from_array',
                        'options'     => $this->GetUc(),
                        'allows_null'     => false,
                        'default' => '',
                    ]);
                    // Activity Village
                    $this->crud->addField([
                        'name'              => 'activity_village'.$akey,
                        'label'             => 'Village',
                        'tab'               => 'Reporting',
                        'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                        'type'              => 'text',
                        'value'             => $activity->activity_village,
                    ]);

                }

            }

            // Activity Target
            $this->crud->addField([
                'name'              => 'target_number',
                'label'             => 'Target (Number)',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->target_number:null,
            ]);
            $this->crud->addField([
                'name'              => 'achieved_number',
                'label'             => 'Achieved (Number)',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->achieved_number:null,
            ]);
            $this->crud->addField([
                'name'              => 'activity_unit',
                'label'             => 'Activity Unit',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->activity_unit:null,
            ]);
            $this->crud->addField([
                'name'              => 'women_targeted',
                'label'             => 'Women',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->women_targeted:null,
            ]);
            $this->crud->addField([
                'name'              => 'men_targeted',
                'label'             => 'Men',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->men_targeted:null,
            ]);
            $this->crud->addField([
                'name'              => 'girls_targeted',
                'label'             => 'Girls',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->girls_targeted:null,
            ]);
            $this->crud->addField([
                'name'              => 'boys_targeted',
                'label'             => 'Boys',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->boys_targeted:null,
            ]);
            $this->crud->addField([
                'name'              => 'female_headed',
                'label'             => 'Female Headed Household',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->female_headed:null,
            ]);
            $this->crud->addField([
                'name'              => 'women_reached',
                'label'             => 'Women',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->women_reached:null,
            ]);
            $this->crud->addField([
                'name'              => 'men_reached',
                'label'             => 'Men',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->men_reached:null,
            ]);
            $this->crud->addField([
                'name'              => 'girls_reached',
                'label'             => 'Girls',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->girls_reached:null,
            ]);
            $this->crud->addField([
                'name'              => 'boys_reached',
                'label'             => 'Boys',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->boys_reached:null,
            ]);

            $this->crud->addField([
                'name'              => 'contact_person',
                'label'             => 'Contact Person',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->contact_person:null,
            ]);
            $this->crud->addField([
                'name'              => 'contact_number',
                'label'             => 'Contact Number',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->contact_number:null,
            ]);
            $this->crud->addField([
                'name'              => 'contact_email',
                'label'             => 'Email',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->contact_email:null,
            ]);
            $this->crud->addField([
                'name'              => 'comments',
                'label'             => 'Comments',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->comments:null,
            ]);
            $this->crud->addField([
                'name'              => 'updated_date',
                'label'             => 'Date (date updated)',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'text',
                'value'             => $reporting?$reporting->updated_date:null,
            ]);
            $this->crud->addField([
                'name'              => 'reporting_narrative_reports',
                'label'             => 'Narrative Report',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => '<p>Narrative Report : <br/><a href="#">'.(($reporting)?($reporting->narrative_reports):('')).'</a></p>',
            ]);
            $this->crud->addField([
                'name'              => 'reporting_upload_image',
                'label'             => 'Upload Image',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => '<p>Upload Image : <br/><a href="#">'.(($reporting)?($reporting->upload_image):('')).'</a></p>',
            ]);




    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        //$this->crud->removeButton('create');

        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }


        $this->crud->enableExportButtons();
        CRUD::setFromDb(); // columns
 $this->crud->removeColumn('district_id');


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(NgoRequest::class);

        CRUD::setFromDb(); // fields
        $this->crud->removeField('password');
        $this->crud->removeField('name');
        $this->crud->removeField('email');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function addCustomCrudFilters()
    {


        $this->crud->addFilter([ // dropdown filter
          'name' => 'status',
          'type' => 'dropdown',
          'label'=> 'NGO Status',
        ], [1 => 'Active', 0 => 'Inactive', -1 => 'Pending'], function ($value) {
            // if the filter is active
            $this->crud->addClause('where', 'status', $value);
        });

        $this->crud->addFilter([ // select2 filter
            'name' => 'district_id',
            'type' => 'select2',
            'label'=> 'District'
        ], function() {

            return District::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'district_id', $value);
        });



    }

    protected function setupShowOperation()
    {

        if(backpack_user()->hasRole('Admin')){

        }else{
            $this->crud->removeButton('update');
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');

        }
        $record = $this->crud->getCurrentEntry();
        $focalperson = ($record)?($record->focalperson):null;
        $reporting = ($record)?($record->reporting):null;


         // =========> Tab Contact Person <=========
            // mailing_address
            $this->crud->addColumn([
                'name'              => 'focalperson_mailing_address1',
                'label'              => 'Mailing Address',
                'type'              => 'custom_html',
                'value'             =>  ($focalperson)?($focalperson->mailing_address):null,
            ]);
            // city
            $this->crud->addColumn([
                'name'              => 'focalperson_city',
                'label'              => 'City',
                'tab'               => 'Contact Person',
                'type'              => 'custom_html',
                'value'             => $focalperson?$focalperson->city_id:null,
            ]);
            // phone (landline)
            $this->crud->addColumn([
                'name'              => 'focalperson_phone_landline',
                'label'              => 'Phone (landline)',
                'tab'               => 'Contact Person',
                'type'              => 'custom_html',
                'value'             => $focalperson?$focalperson->landline:null,
            ]);
            // phone (cell)
            $this->crud->addColumn([
                'name'              => 'focalperson_phone_cell',
                'label'              => 'Phone (cell)',
                'tab'               => 'Contact Person',
                'type'              => 'custom_html',
                'value'             => $focalperson?$focalperson->cell:null,
            ]);
            // fax
            $this->crud->addColumn([
                'name'              => 'focalperson_fax',
                'label'              => 'Fax',
                'tab'               => 'Contact Person',
                'type'              => 'custom_html',
                'value'             => $focalperson?$focalperson->fax:null,
            ]);
            // email
            $this->crud->addColumn([
                'name'              => 'focalperson_email',
                'label'              => 'Email',
                'tab'               => 'Contact Person',
                'type'              => 'custom_html',
                'value'             => $focalperson?$focalperson->email:null,
            ]);
            // website
            $this->crud->addColumn([
                'name'              => 'focalperson_website',
                'label'              => 'Website',
                'tab'               => 'Contact Person',
                'type'              => 'custom_html',
                'value'             => $focalperson?$focalperson->website:null,
            ]);
            // Facebook
            $this->crud->addColumn([
                'name'              => 'focalperson_facebook',
                'label'              => 'Facebook',
                'tab'               => 'Contact Person',

                 'type'              => 'custom_html',
                'value'             => $focalperson?$focalperson->facebook:null,
            ]);



         // =========> Tab Reporting <=========
            // Context
            /*$this->crud->addColumn([
                'label' => 'Context',
                'name' => 'reporting_context',
                'type' => 'model_function',
                'function_name' => 'getprojectCategory',
                'attribute' => 'category_name',

            ]);
            // Donor/Funding org.
            $this->crud->addColumn([
                'label'             => 'Donor/Funding org',
                'type' => 'model_function',
                'function_name' => 'getprojectDonor',
                'attribute' => 'name',
            ]);
            // Project code.
            $this->crud->addColumn([
                'name'              => 'reporting_projectcode',
                'label'             => 'Project code',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->project_code:null,
            ]);*/
            // Project Modality.
           /* $this->crud->addColumn([
                'name'              => 'reporting_modality',
                'label'             => 'Project Modality',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->project_modality:null,
            ]);*/
            /*$this->crud->addColumn([
                'label' => 'Project Modality',
                'name' => 'project_modality',
                'type' => 'model_function',
                'function_name' => 'getproject_modalityTitle',
                'attribute' => 'title',

            ]);
            // Project Title.
            $this->crud->addColumn([
                'name'              => 'reporting_modality',
                'label'             => 'Project Title',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->project_title:null,
            ]);
            // Project Owner
            $this->crud->addColumn([
                'name'              => 'reporting_owner_name',
                'label'             => 'Project Owner',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->project_owner_name:null,
            ]);
            // Project Owner Organization Type
            $this->crud->addColumn([
                'name'              => 'reporting_owner_org_type',
                'label'             => 'Project Owner Org. Type',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->project_owner_org_type:null,
            ]);
            // Implementing Partner
            $this->crud->addColumn([
                'name'              => 'reporting_implementing_partner',
                'label'             => 'Implementing Partner',
                'type' => 'model_function',
                'function_name' => 'getprojectImplementingPartner',
                'attribute' => 'title',
            ]);
            // IP/Org type
            $this->crud->addColumn([
                'name'              => 'reporting_ip_org_type',
                'label'             => 'IP/Org type',
                'type' => 'model_function',
                'function_name' => 'getprojectImplementingOrgtype',
                'attribute' => 'title',
            ]);
           // Sector / Cluster
            $this->crud->addColumn([
                'name'              => 'reporting_implementing_partner',
                'label'             => 'Sector / Cluster',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->project_sector:null,
            ]);


             // Activity Target
            $this->crud->addColumn([
                'name'              => 'reporting_target_number',
                'label'             => 'Target (Number)',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->target_number:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_achieved_number',
                'label'             => 'Achieved (Number)',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->achieved_number:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_activity_unit',
                'label'             => 'Activity Unit',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->activity_unit:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_women',
                'label'             => 'Women',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->women_targeted:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_men',
                'label'             => 'Men',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->men_targeted:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_girl',
                'label'             => 'Girls',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->girls_targeted:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_boys',
                'label'             => 'Boys',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->boys_targeted:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_female_headed',
                'label'             => 'Female Headed Household',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->female_headed:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_women_reached',
                'label'             => 'Women',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->women_reached:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_men_reached',
                'label'             => 'Men',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->men_reached:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_girls_reached',
                'label'             => 'Girls',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->girls_reached:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_boys_reached',
                'label'             => 'Boys',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->boys_reached:null,
            ]);

            $this->crud->addColumn([
                'name'              => 'reporting_contact_person',
                'label'             => 'Contact Person',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->contact_person:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_contact_number',
                'label'             => 'Contact Number',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->contact_number:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_contact_email',
                'label'             => 'Email',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->contact_email:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_contact_comments',
                'label'             => 'Comments',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->comments:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_updated_date',
                'label'             => 'Date (date updated)',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => $reporting?$reporting->updated_date:null,
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_narrative_reports',
                'label'             => 'Narrative Report',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => '<p>Narrative Report : <br/><a href="#">'.(($reporting)?($reporting->narrative_reports):('')).'</a></p>',
            ]);
            $this->crud->addColumn([
                'name'              => 'reporting_upload_image',
                'label'             => 'Upload Image',
                'tab'               => 'Reporting',
                'wrapperAttributes' => ['class' => 'form-group col-md-6'],
                'type'              => 'custom_html',
                'value'             => '<p>Upload Image : <br/><a href="#">'.(($reporting)?($reporting->upload_image):('')).'</a></p>',
            ]);*/

            $this->crud->removeColumn('district_id');

    }

    public function getProjectCategory()
    {
        $entries = ProjectCategory::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->category_name;
        }

        return $tab;
    }

    public function getFundingAgency()
    {
        $entries = FundingAgency::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }

    public function getProjectModality()
    {
        $entries = Project_modality::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->title;
        }

        return $tab;
    }

    public function getImplementingPartner()
    {
        $entries = ImplementingPartner::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->title;
        }

        return $tab;
    }

    public function getImplementingOrgType()
    {
        $entries = Implementing_organization_type::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->title;
        }

        return $tab;
    }

    public function getproject_sector()
    {
        $entries = Sector_Cluster::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->title;
        }

        return $tab;
    }

    public function GetProvince()
    {
        $entries = Province::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }

    public function Getdistrict()
    {
        $entries = District::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }

    public function getprovTitle()
    {
        $entries = Province::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }

    public function GetTehsil()
    {
        $entries = Tehsil::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }

    public function GetUc()
    {
        $entries = Uc::all();

        if ($entries->count() <= 0) {
            return [];
        }
        $tab = [];
        foreach ($entries as $entry) {

            $tab[$entry->id] = $entry->name;
        }

        return $tab;
    }
}
