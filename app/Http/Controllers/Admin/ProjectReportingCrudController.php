<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProjectReportingRequest;
use App\Models\NgoProjects;
use App\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;

/**
 * Class ProjectReportingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProjectReportingCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ProjectReporting::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/project-reporting');
        CRUD::setEntityNameStrings('project reporting', 'project reporting');

        $this->addCustomCrudFilters();

        $this->crud->addColumn([
            'label' => 'NGO',
            'type' => 'model_function',
            'name' => 'ngo_id',
            'function_name' => 'getNgoName',
            'attribute' => 'name',
            'model' => "App\Models\Donor",
        ]);

        $this->crud->addColumn([
            'label' => 'Project Title',
            'name' => 'project_title',
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('project/'.$entry->report_project.'/show');
                },
                 'target' => '_blank',
            ],
        ]);
        $this->crud->addColumn([
            'label' => 'Start Date ',
            'name' => 'start_date',

        ]);
        $this->crud->addColumn([
            'label' => 'End Date',
            'name' => 'end_date',

        ]);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        //CRUD::setFromDb(); // columns
        $this->crud->removeButton('update');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProjectReportingRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function addCustomCrudFilters()
    {
        $this->crud->addFilter([
            'name' => 'ngo_id',
            'type' => 'select2',
            'label'=> 'NGO'
        ], function() {

            $user = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')->where('model_has_roles.role_id',1)
                ->pluck('users.name', 'users.id','model_has_roles.role_id','model_has_roles.model_id')
                ->all();

            return $user;
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'ngo_id', $value);
        });

        $this->crud->addFilter([ // select2 filter
            'name' => 'project_id',
            'type' => 'select2',
            'label'=> 'Project'
        ], function() {

            return NgoProjects::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'project_id', $value);
        });

        $this->crud->addFilter([ // select2 filter
            'name' => 'report_duration',
            'type' => 'select2',
            'label'=> 'Report Duration Type'
        ], function() {
            return array('monthly' => 'Monthly', 'quarterly' => 'Quarterly', 'final' => 'Final');
        }, function($value) { // if the filter is active
            $this->crud->addClause('where', 'report_duration', $value);
        });

        // daterange filter
        if(backpack_user()->hasRole('PDMA')){
            $this->crud->addFilter([
                'type'  => 'date_range',
                'name'  => 'start_date',
                'label' => 'Filter by Date'
            ],
                false,
                function ($value) { // if the filter is active, apply these constraints
                    $dates = json_decode($value);
                    $this->crud->addClause('where', 'start_date', '>=', $dates->from);
                    $this->crud->addClause('where', 'end_date', '<=', $dates->to);
                });
        }

    }
}
