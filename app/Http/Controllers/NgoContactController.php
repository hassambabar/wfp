<?php

namespace App\Http\Controllers;

use App\Models\NgoProfile;
use Illuminate\Http\Request;
use App\Affiliations;
use Auth;
use App\Models\Network;
use App\Models\Ngo_contact;
use App\District;
use App\Tehsil;
use App\Province;


class NgoContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {

        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding NGO Contacts.');
        }
        $Ngo_contact = Ngo_contact::where('ngo_id', $userId)->get();
        return view('NgoContact.index', compact('Ngo_contact','ngoProfile'));
    }


    // public function index()
    // {

    //     $userId = Auth::user()->id;

    //     $District = District::where('province_id',1)->get();
    //     $Tehsil = Tehsil::where('province_id',1)->get();
    //     $Province = Province::get();

    //     $Ngo_contact = Ngo_contact::where('ngo_id', $userId)->first();
    //     if($Ngo_contact){
    //         return view('NgoContact.Edit', compact('Ngo_contact','District','Tehsil','Province'));

    //     }else{
    //         return view('NgoContact.Add', compact('Ngo_contact','District','Tehsil','Province'));

    //     }
    // }

    public function create()
    {

        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding NGO Contacts.');
        }

        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();

        $Ngo_contact = Ngo_contact::where('ngo_id', $userId)->first();

            return view('NgoContact.add', compact('Ngo_contact','District','Tehsil','Province'));


    }

    public function edit($id)
    {

        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding NGO Contacts.');
        }

        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();

        $Ngo_contact = Ngo_contact::where('ngo_id', $userId)->where('id',$id)->first();

        return view('NgoContact.Edit', compact('Ngo_contact','District','Tehsil','Province'));
    }

    public function show($id)
    {

        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding NGO Contacts.');
        }

        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();

        $Ngo_contact = Ngo_contact::where('ngo_id', $userId)->where('id',$id)->first();

        return view('NgoContact.show', compact('Ngo_contact','District','Tehsil','Province','ngoProfile'));
    }

    public function store(Request $request)
    {
         $request->validate([
            'contact_person_name' => 'required',
            'office' => 'required',
            'cell' => 'required',
            'email' => 'required',
            'can_pdma_contact' => 'required',
         ]);
        $userId = Auth::id();

        $Ngo_contact = new Ngo_contact([

            'contact_person_name' => $request->get('contact_person_name'),
            'designation' => $request->get('designation'),
            'can_pdma_contact' => $request->get('can_pdma_contact'),
            'office' => $request->get('office'),
            'province_id' => $request->get('province_id'),

            'mailing_address' => $request->get('mailing_address'),
            'district_id' => $request->get('district_id'),
            'landline' => $request->get('landline'),
            'cell' => $request->get('cell'),
            'fax' => $request->get('fax'),
            'email' => $request->get('email'),
            'website' => $request->get('website'),
            'facebook' => $request->get('facebook'),
            'ngo_id' => $userId,
        ]);
       $Ngo_contact->save();

        if($request->submit_type && $request->submit_type == 'save_new'){
            return redirect('/ngocontact/create')->with('message', 'Saved!');
        }

       return redirect('/ngocontact')->with('message', 'Saved!');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'contact_person_name' => 'required',
            'office' => 'required',
            'cell' => 'required',
            'email' => 'required',
            'can_pdma_contact' => 'required',
        ]);
        $Ngo_contact = Ngo_contact::find($id);

        $Ngo_contact->contact_person_name =  $request->get('contact_person_name');
        $Ngo_contact->designation =  $request->get('designation');
        $Ngo_contact->can_pdma_contact =  $request->get('can_pdma_contact');
        $Ngo_contact->office =  $request->get('office');

        $Ngo_contact->province_id =  $request->get('province_id');
        $Ngo_contact->mailing_address =  $request->get('mailing_address');
        $Ngo_contact->district_id =  $request->get('district_id');
        $Ngo_contact->landline =  $request->get('landline');
        $Ngo_contact->cell =  $request->get('cell');
        $Ngo_contact->fax =  $request->get('fax');
        $Ngo_contact->email =  $request->get('email');
        $Ngo_contact->website =  $request->get('website');
        $Ngo_contact->facebook =  $request->get('facebook');

        $Ngo_contact->save();
        return redirect('/ngocontact')->with('message', 'Update successfully!');
    }

    public function delete($id){

        $userId = Auth::id();
        $Ngo_contact = Ngo_contact::where('id', $id)->where('ngo_id',$userId)->first();
        if($Ngo_contact){
            $Ngo_contactDelete = Ngo_contact::find($Ngo_contact->id);
            $Ngo_contactDelete->delete();

        }

        return redirect('/ngocontact')->with('message', 'Deleted successfully!');
    }

}
