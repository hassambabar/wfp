<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\Models\Comment;
   
class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $request = request();
        // dd($request);
      if(backpack_user()){
          $user_id = backpack_user()->id;
      }else{
        $user_id = auth()->user()->id;
      }
        $request->validate([
            'body'=>'required',
        ]);
        $input = $request->all();
        $input['user_id'] =  $user_id;

        Comment::create($input);
   

        return back();
    }
}