<?php

namespace App\Http\Controllers\Ngo;

use App\Http\Controllers\Controller;
use App\Models\FundingAgency;
use App\Models\NgoMou;
use App\Models\OrganizationNatures;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\NgoProfile;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class NgoMouController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding MOU.');
        }
        $mouData = $ngoProfile->mous;
        return view('ngo.mou.index', compact('ngoProfile','mouData'));
    }

    public function create(){
        $userId = Auth::user()->id;
        /*$fundingAgency = FundingAgency::all();
        $orgNatures = OrganizationNatures::all();*/
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding MOU.');
        }
        return view('ngo.mou.create', compact('userId','ngoProfile'));
    }

    public function edit($id){
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'warning|Profile doesn\'t exists.');
        }
        $mou = NgoMou::findOrFail($id);
        return view('ngo.mou.edit', compact('ngoProfile','mou','userId'));
    }

    public function store(Request $request)
    {
        // $validator = Validator::make($request->all(),[
        //     'mou_status' => 'required_if:have_mou,1',
        //     'mou_expiry_date' => 'required_if:have_mou,1',
        //     'mou_file' => 'mimes:doc,pdf,docx,jpg,jpeg'
        // ]);
        $validator = Validator::make($request->all(),[
            'mou_status' => 'required',
            'mou_expiry_date' => 'required_if:mou_status,valid,invalid',
            'mou_file' => 'mimes:doc,pdf,docx,jpg,jpeg'
        ]);

        if ($validator->fails()) {
            return redirect('mous/create')
                ->withErrors($validator)
                ->withInput();
        }

        $userId = Auth::user()->id;
        DB::beginTransaction();
        try{
            $datMou = $request->all();
            $datMou['mou_signed_date'] = Carbon::parse($request->mou_signed_date);
            if($request->mou_expiry_date){
                $datMou['mou_expiry_date'] = Carbon::parse($request->mou_expiry_date);
            }
            $mouData = new NgoMou($datMou);
            $mouData->ngo()->associate($userId);
            $mouData->save();
            //if($mouData){
                $disks = 'uploads_dir';
                $fileName = '';
                if ($request->hasFile('mou_file') && $request->file('mou_file')->isValid()) {
                    /*$name = $request->file('mou_file');
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();

                    $fileSystem = \Storage::disk($disks);
                    $filePath = 'mou/' . $request->ngo_id . '/contents/' . $fileName;
                    $fileSystem->put($filePath, file_get_contents($request->file('mou_file')), 'public');
                    $mouData->mou_file = $filePath;
                    $mouData->update();*/

                    $name = $request->file('mou_file');
                    $destinationPath = 'uploads/mous/' . $request->ngo_id . '/contents/';
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                    $request->file('mou_file')->move($destinationPath, $fileName);
                    $mouData->mou_file = $destinationPath.'/'.$fileName;
                    $mouData->update();
                }
            DB::commit();
            //}
        }catch (\Exception $e){
            // do task when error
            DB::rollBack();
            // do task when error
            return redirect('mous/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }
        return redirect('/mous')->with('message', 'success|MOU data saved.');
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'mou_status' => 'required',
            'mou_expiry_date' => 'required_if:mou_status,valid,invalid',
            'mou_file' => 'mimes:doc,pdf,docx,jpg,jpeg'
        ]);

        if ($validator->fails()) {
            return redirect(route('mous.edit',$id))
                ->withErrors($validator)
                ->withInput();
        }

        $datMou = $request->all();
        $datMou['mou_signed_date'] = Carbon::parse($request->mou_signed_date);
        if($request->mou_expiry_date){
            $datMou['mou_expiry_date'] = Carbon::parse($request->mou_expiry_date);
        }

        try{
            $mouData =   NgoMou::find($id);
            if(!$mouData)
                return redirect(route('mous.index'))->with('message', 'error|MOU with id '.$id.' not exist.');

            $mouData->update($datMou);

            $disks = !empty(\Config::get('filesystems.default'))?\Config::get('filesystems.default'):'local';

            if ($request->hasFile('mou_file') && $request->file('mou_file')->isValid()) {
                /*$name = $request->file('mou_file');
                $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();

                $fileSystem = \Storage::disk($disks);
                $filePath = 'mou/' . $mouData->ngo_id . '/contents/' . $fileName;
                $fileSystem->put($filePath, file_get_contents($request->file('mou_file')), 'public');
                $mouData->mou_file = $filePath;
                $mouData->update();*/

                $name = $request->file('mou_file');
                $destinationPath = 'uploads/mous/' . $mouData->ngo_id . '/contents/';
                $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                $request->file('mou_file')->move($destinationPath, $fileName);
                $mouData->mou_file = $destinationPath.'/'.$fileName;
                $mouData->update();
            }
        }catch (\Exception $e){
            // do task when error
            return redirect(route('mous.edit',$id))
                ->withErrors($e->getMessage())
                ->withInput();
        }

        return redirect('/mous')->with('message', 'success|MOU data updated.');
    }

    public function delete($id){

        $userId = Auth::id();
        $mou = NgoMou::where('id', $id)->where('ngo_id',$userId)->first();
        if($mou){
            $mouDelete = NgoMou::find($mou->id);
            $mouDelete->delete();

        }

        return redirect('/mous')->with('message', 'success|Deleted successfully!');
    }

}
