<?php

namespace App\Http\Controllers\Ngo;

use App\District;
use App\Http\Controllers\Controller;
use App\Models\Implementing_organization_type;
use App\Models\ImplementingPartner;
use App\Models\NgoOrgNatures;
use App\Models\NgoProjects;
use App\Models\Project_modality;
use App\Models\ProjectActivity;
use App\Models\ProjectAreas;
use App\Models\ProjectsNoc;
use App\Models\ProjectThematicAreas;
use App\Models\Sector_Cluster;
use App\Models\ThematicArea;
use App\Models\ProjectCategory;
use App\Models\FundingAgency;
use App\Models\Uc;
use App\Notifications\NocUpdateApproval;
use App\Notifications\RequestForNoc;
use App\Province;
use App\Tehsil;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\NgoProfile;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class NgoProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Projects.');
        }
        $projects = $ngoProfile->projects;
        $thematicAreas = ThematicArea::all();
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $projectSector = Sector_Cluster::all();


        return view('ngo.projects.index', compact('ngoProfile','projects','thematicAreas','District','Tehsil','projectSector'));
    }

    public function create(){
        $userId = Auth::user()->id;
        $thematicAreas = ThematicArea::all();
        $projectCategories = ProjectCategory::all();
        $fundingAgency = FundingAgency::all();
        $modalities = Project_modality::all();
        $implementingPartner = ImplementingPartner::all();
        $implementingOrgType = Implementing_organization_type::all();
        $projectSector = Sector_Cluster::all();
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();
        $ucs = Uc::where('province_id',1)->get();
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Projects.');
        }
        return view('ngo.projects.add', compact(
            'userId','thematicAreas','projectCategories','ngoProfile','fundingAgency','modalities','implementingPartner',
            'implementingOrgType','projectSector','Province','District','Tehsil','ucs'
        ));
    }

    public function store(Request $request)
    {
        $userId = Auth::id();
        $data = $request->all();

        if($data['submit_type'] == 'draft'){
            $data['start_date'] = Carbon::parse($request->start_date);
            $data['end_date'] = Carbon::parse($request->end_date);
            DB::beginTransaction();
            try{
                $data['status'] = -1; // save as draft
                $projectData = new NgoProjects($data);

                $projectData->ngo()->associate($userId);
                $projectData->save();

                //update ProjectAreas
                if(isset($data['project_district']) && $data['project_district'][0] !== null){
                    $deleteProjectAreas = ProjectAreas::where('project_id',$projectData->id)->delete();
                    foreach($data['project_district'] as $index => $value){
                        $projectAreas = new ProjectAreas([
                            'project_id' => $projectData->id,
                            'district_id' => $data['project_district'][$index],
                            'tehsil_id' => $data['project_tehsil'][$index],
                            'uc_id' => $data['project_uc'][$index],
                        ]);
                        $projectAreas->save();
                    }
                }

                //update Project Thematic Areas
                if(isset($data['thematic_area']) && $data['thematic_area'][0] !== null){
                    $deleteProjectThematicAreas = ProjectThematicAreas::where('project_id',$projectData->id)->delete();
                    foreach($data['thematic_area'] as $index => $value){
                        $projectThematicAreas = new ProjectThematicAreas([
                            'project_id' => $projectData->id,
                            'thematic_area' => $data['thematic_area'][$index],
                        ]);
                        $projectThematicAreas->save();
                    }
                }

                DB::commit();
            }catch (\Exception $e){
                DB::rollBack();
                // do task when error
                return redirect('projects/create')
                    ->withErrors($e->getMessage())
                    ->withInput();
            }


            try{
                if($projectData){
                    // $disks = !empty(\Config::get('filesystems.default'))?\Config::get('filesystems.default'):'local';
                    $disks = 'uploads_dir';
                    $fileName = '';
                    if ($request->hasFile('source_of_funds') && $request->file('source_of_funds')->isValid()) {
                        /*$name = $request->file('source_of_funds');
                        $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                        $fileSystem = \Storage::disk($disks);
                        $filePath = 'projects/' . $projectData->id . '/contents/' . $fileName;
                        $fileSystem->put($filePath, file_get_contents($request->file('source_of_funds')), 'public');
                        $projectData->source_of_funds = $filePath;
                        $projectData->update();*/

                        $name = $request->file('source_of_funds');
                        $destinationPath = 'uploads/projects/' . $projectData->id . '/contents/';
                        $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                        $request->file('source_of_funds')->move($destinationPath, $fileName);
                        $projectData->source_of_funds = $destinationPath.'/'.$fileName;
                        $projectData->update();
                    }
                }
            }catch (\Exception $e){
                DB::rollBack();
                return redirect('projects/create')
                    ->withErrors($e->getMessage())
                    ->withInput();
            }

            try{
                //insert activities
                // check if activity / StartDate / EndDate / Province are not empty.
                if( isset($data['activity']['project_activity']) && $data['activity']['project_activity'][0] !== null &&
                    isset($data['activity']['activity_start_date']) && $data['activity']['activity_start_date'][0] !== null &&
                    isset($data['activity']['project_description']) && $data['activity']['project_description'][0] !== null &&
                    isset($data['activity']['activity_province']) && $data['activity']['activity_province'][0] !== null &&
                    isset($data['activity']['activity_district']) && $data['activity']['activity_district'][0] !== null
                ){
                    foreach($data['activity']['project_activity'] as $index => $value){
                        $activity_start_date = Carbon::parse($data['activity']['activity_start_date'][$index]);
                        $activity_end_date = Carbon::parse($data['activity']['activity_end_date'][$index]);
                        $activity = new ProjectActivity([
                            'project_activity' => $data['activity']['project_activity'][$index],
                            'project_description' => $data['activity']['project_description'][$index],
                            'activity_start_date' => $activity_start_date,
                            'activity_end_date' => $activity_end_date,
                            'month_assistance' =>  $data['activity']['month_assistance'][$index],
                            'activity_province' => $data['activity']['activity_province'][$index],
                            'activity_district' => $data['activity']['activity_district'][$index],
                            'activity_tehsil' => $data['activity']['activity_tehsil'][$index],
                            'activity_uc' => $data['activity']['activity_uc'][$index],
                            'activity_village' => $data['activity']['activity_village'][$index],
                            'latitude' => $data['activity']['latitude'][$index],
                            'longitude' => $data['activity']['longitude'][$index],
                        ]);

                        $projectData->activities()->save($activity);
                    }
                }


            }catch (\Exception $e){
                DB::rollBack();
                return redirect('projects/create')
                    ->withErrors($e->getMessage())
                    ->withInput();
            }

            return redirect('/projects')->with('message', 'success|Project draft saved successfully.');
        }

        $validator = Validator::make($request->all(),[
            'project_category' => 'required',
            'project_title' => 'required',
            'donor' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('projects/create')
                ->withErrors($validator)
                ->withInput();
        }

        $data['start_date'] = Carbon::parse($request->start_date);
        $data['end_date'] = Carbon::parse($request->end_date);
        DB::beginTransaction();
        try{
            $projectData = new NgoProjects($data);

            $projectData->ngo()->associate($userId);
            $projectData->save();

            //update ProjectAreas
            if(isset($data['project_district']) && $data['project_district'][0] !== null){
                $deleteProjectAreas = ProjectAreas::where('project_id',$projectData->id)->delete();
                foreach($data['project_district'] as $index => $value){
                    $projectAreas = new ProjectAreas([
                        'project_id' => $projectData->id,
                        'district_id' => $data['project_district'][$index],
                        'tehsil_id' => $data['project_tehsil'][$index],
                        'uc_id' => $data['project_uc'][$index],
                    ]);
                    $projectAreas->save();
                }
            }

            //update Project Thematic Areas
            if(isset($data['thematic_area']) && $data['thematic_area'][0] !== null){
                $deleteProjectThematicAreas = ProjectThematicAreas::where('project_id',$projectData->id)->delete();
                foreach($data['thematic_area'] as $index => $value){
                    $projectThematicAreas = new ProjectThematicAreas([
                        'project_id' => $projectData->id,
                        'thematic_area' => $data['thematic_area'][$index],
                    ]);
                    $projectThematicAreas->save();
                }
            }

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            // do task when error
            return redirect('projects/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }

        try{
            if($projectData){
                // $disks = !empty(\Config::get('filesystems.default'))?\Config::get('filesystems.default'):'local';
                $disks = 'uploads_dir';
                $fileName = '';
                if ($request->hasFile('source_of_funds') && $request->file('source_of_funds')->isValid()) {
                    /*$name = $request->file('source_of_funds');
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                    $fileSystem = \Storage::disk($disks);
                    $filePath = 'projects/' . $projectData->id . '/contents/' . $fileName;
                    $fileSystem->put($filePath, file_get_contents($request->file('source_of_funds')), 'public');
                    $projectData->source_of_funds = $filePath;
                    $projectData->update();*/

                    $name = $request->file('source_of_funds');
                    $destinationPath = 'uploads/projects/' . $projectData->id . '/contents/';
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                    $request->file('source_of_funds')->move($destinationPath, $fileName);
                    $projectData->source_of_funds = $destinationPath.'/'.$fileName;
                    $projectData->update();
                }
            }
        }catch (\Exception $e){
            DB::rollBack();
            return redirect('projects/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }

        try{
            //insert activities
            // check if activity / StartDate / EndDate / Province are not empty.
            if( isset($data['activity']['project_activity']) && $data['activity']['project_activity'][0] !== null &&
                isset($data['activity']['activity_start_date']) && $data['activity']['activity_start_date'][0] !== null &&
                isset($data['activity']['project_description']) && $data['activity']['project_description'][0] !== null &&
                isset($data['activity']['activity_province']) && $data['activity']['activity_province'][0] !== null &&
                isset($data['activity']['activity_district']) && $data['activity']['activity_district'][0] !== null
            ){
                foreach($data['activity']['project_activity'] as $index => $value){
                    $activity_start_date = Carbon::parse($data['activity']['activity_start_date'][$index]);
                    $activity_end_date = Carbon::parse($data['activity']['activity_end_date'][$index]);
                    $activity = new ProjectActivity([
                        'project_activity' => $data['activity']['project_activity'][$index],
                        'project_description' => $data['activity']['project_description'][$index],
                        'activity_start_date' => $activity_start_date,
                        'activity_end_date' => $activity_end_date,
                        'month_assistance' =>  $data['activity']['month_assistance'][$index],
                        'activity_province' => $data['activity']['activity_province'][$index],
                        'activity_district' => $data['activity']['activity_district'][$index],
                        'activity_tehsil' => $data['activity']['activity_tehsil'][$index],
                        'activity_uc' => $data['activity']['activity_uc'][$index],
                        'activity_village' => $data['activity']['activity_village'][$index],
                        'latitude' => $data['activity']['latitude'][$index],
                        'longitude' => $data['activity']['longitude'][$index],
                    ]);

                    $projectData->activities()->save($activity);
                }
            }


        }catch (\Exception $e){
            DB::rollBack();
            return redirect('projects/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }

        try{
            if($request->noc_apply == 1){
                $projectNoc = new ProjectsNoc([
                    'project_id' => $projectData->id,
                    'comments' => '...',
                    'status' => 'pending',
                ]);
                $projectNoc->save();
            }
        }catch (\Exception $e){
            DB::rollBack();
            return redirect('projects/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }

        if($request->noc_apply == 1 && isset($projectNoc->id)) {
            $users = User::select("users.*")
                ->join("model_has_roles", "model_has_roles.model_id", "=", "users.id")->where('model_has_roles.role_id', 3)
                ->where('users.status',1)
                ->get();
            Notification::send($users, new RequestForNoc($projectData));
        }

        if($request->apply_for_noc == 1){
            if($request->submit_type && $request->submit_type == 'save_new'){
                return redirect('/noc-apply/create')->with('message', 'Saved!');
            }

            return redirect('/noc-requests')->with('message', 'success|NOC applied successfully.');
        }

        if($request->submit_type && $request->submit_type == 'save_new'){
            return redirect('/projects/create')->with('message', 'Saved!');
        }

        return redirect('/projects')->with('message', 'success|Project created successfully.');
    }

    public function edit($id){

        $userId = Auth::user()->id;
        $thematicAreas = ThematicArea::all();
        $projectCategories = ProjectCategory::all();
        $fundingAgency = FundingAgency::all();
        $modalities = Project_modality::all();
        $implementingPartner = ImplementingPartner::all();
        $implementingOrgType = Implementing_organization_type::all();
        $projectSector = Sector_Cluster::all();
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();
        $ucs = Uc::where('province_id',1)->get();
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        $project = NgoProjects::where('id', $id)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Projects.');
        }

        $areaDistricts = $project->projectAreas ? $project->projectAreas->pluck('district_id')->toArray() : [];
        $areaTehsils = $project->projectAreas ? $project->projectAreas->pluck('tehsil_id')->toArray() : [];
        $areaUcs = $project->projectAreas ? $project->projectAreas->pluck('uc_id')->toArray() : [];

        $projectThematicAreas = $project->projectThematicAreas ? $project->projectThematicAreas->pluck('thematic_area')->toArray() : [];

        // ngo/projects/edit
        return view('ngo.projects.edit', compact(
            'userId','thematicAreas','projectCategories','ngoProfile','fundingAgency','modalities','implementingPartner',
            'implementingOrgType','projectSector','Province','District','Tehsil','ucs','project', 'areaDistricts', 'areaTehsils','areaUcs','projectThematicAreas'
        ));
    }

    public function show($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Stocks.');
        }
        $project = NgoProjects::where('id', $id)->first();

        $areaDistricts = $project->projectAreas->map(function ($pareas){
            return $pareas->district->name;
        });
        $areaTehsils = $project->projectAreas->map(function ($pareas){
            return $pareas->tehsil->name;
        });
        $areaUcs = $project->projectAreas->map(function ($pareas){
            return $pareas->uc->name;
        });

        $thematicAreas = $project->projectThematicAreas->map(function ($thareas){
            return $thareas->thematicArea->area_name;
        });

        $areaDistricts = $areaDistricts->toArray();
        $areaTehsils = $areaTehsils->toArray();
        $areaUcs = $areaUcs->toArray();
        $thematicAreas = $thematicAreas->toArray();
        return view('ngo.projects.show', compact('project','ngoProfile','areaDistricts','areaTehsils','areaUcs','thematicAreas'));
    }

    public function update_approval(){
        $user = Auth::user();
        $ngoProfile = NgoProfile::where('user_id', $user->id)->first();
        $nocNotify = auth()->user()->notifications->filter(function ($notification){
            return $notification->read_at == NULL && $notification->type == 'App\Notifications\ProjectNocReviewByPdma';
        })->map(function ($nt){
            return $nt->data['noc']['project_id'];
        })->toArray();

        if($user->noc_enable_edit == 0 && $user->pdma_noc_review_decision == 'deferred'){
            return redirect()->back()->with("error","You have already applied for NOC update approval.");
        }

        $users = User::select("users.*")
            ->join("model_has_roles", "model_has_roles.model_id", "=", "users.id")->whereIn('model_has_roles.role_id',[3,4])
            ->where('users.status',1)
            ->get();

        foreach ($nocNotify as $project){
            Notification::send($users, new NocUpdateApproval(array('id' => $project)));
        }

        $user->noc_enable_edit = 0;
        $user->save();

        return redirect()->back()->with("success","Request for NOC update approval successfully submitted.");
    }

    public function update(Request $request, $id){

        // dd($request->toArray() );

        $validator = Validator::make($request->all(),[
            'thematic_area' => 'required',
            'project_category' => 'required',
            'project_title' => 'required',
            'donor' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('projects.edit',$id))
                ->withErrors($validator)
                ->withInput();
        }

        $userId = Auth::id();

        $data = $request->all();
        $data['start_date'] = Carbon::parse($request->start_date);
        $data['end_date'] = Carbon::parse($request->end_date);

        try{
            $projectData =   NgoProjects::find($id);
            if(!$projectData)
             return redirect(route('projects.index'))->with('message', 'error|Project with id '.$id.' not exist.');

            $projectData->update($data);

            //update ProjectAreas
            if(isset($data['project_district']) && $data['project_district'][0] !== null){
                $deleteProjectAreas = ProjectAreas::where('project_id',$projectData->id)->delete();
                foreach($data['project_district'] as $index => $value){
                    $projectAreas = new ProjectAreas([
                        'project_id' => $projectData->id,
                        'district_id' => $data['project_district'][$index],
                        'tehsil_id' => $data['project_tehsil'][$index],
                        'uc_id' => $data['project_uc'][$index],
                    ]);
                    $projectAreas->save();
                }
            }

            //update Project Thematic Areas
            if(isset($data['thematic_area']) && $data['thematic_area'][0] !== null){
                $deleteProjectThematicAreas = ProjectThematicAreas::where('project_id',$projectData->id)->delete();
                foreach($data['thematic_area'] as $index => $value){
                    $projectThematicAreas = new ProjectThematicAreas([
                        'project_id' => $projectData->id,
                        'thematic_area' => $data['thematic_area'][$index],
                    ]);
                    $projectThematicAreas->save();
                }
            }

        }catch (\Exception $e){
            // do task when error
            return redirect(route('projects.edit',$id))
                ->withErrors($e->getMessage())
                ->withInput();
        }

        try{
            if($projectData){
                $disks = !empty(\Config::get('filesystems.default'))?\Config::get('filesystems.default'):'local';
                $fileName = '';
                if ($request->hasFile('source_of_funds') && $request->file('source_of_funds')->isValid()) {
                    /*$name = $request->file('source_of_funds');
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();

                    $fileSystem = \Storage::disk($disks);
                    $filePath = 'projects/' . $projectData->id . '/contents/' . $fileName;
                    $fileSystem->put($filePath, file_get_contents($request->file('source_of_funds')), 'public');
                    $projectData->source_of_funds = $filePath;
                    $projectData->update();*/

                    $name = $request->file('source_of_funds');
                    $destinationPath = 'uploads/projects/' . $projectData->id . '/contents/';
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                    $request->file('source_of_funds')->move($destinationPath, $fileName);
                    $projectData->source_of_funds = $destinationPath.'/'.$fileName;
                    $projectData->update();
                }
            }
        }catch (\Exception $e){
            return redirect(route('projects.edit', $id))
                ->withErrors($e->getMessage())
                ->withInput();
        }

        try{
            //insert activities
            if( isset($data['activity']['project_activity']) && $data['activity']['project_activity'][0] !== null &&
                isset($data['activity']['activity_start_date']) && $data['activity']['activity_start_date'][0] !== null &&
                isset($data['activity']['project_description']) && $data['activity']['project_description'][0] !== null &&
                isset($data['activity']['activity_province']) && $data['activity']['activity_province'][0] !== null &&
                isset($data['activity']['activity_district']) && $data['activity']['activity_district'][0] !== null
            ) {
                foreach ($data['activity']['project_activity'] as $index => $value) {
                    $activity_start_date = Carbon::parse($data['activity']['activity_start_date'][$index]);
                    $activity_end_date = Carbon::parse($data['activity']['activity_end_date'][$index]);
                    // update existing activity
                    if (isset($data['activity']['id'][$index]) && !empty($data['activity']['id'][$index])) {
                        $activity = ProjectActivity::find($data['activity']['id'][$index]);
                        if (!empty($activity)) {
                            $activity->project_activity = $data['activity']['project_activity'][$index];
                            $activity->project_description = $data['activity']['project_description'][$index];
                            $activity->activity_start_date = $activity_start_date;
                            $activity->activity_end_date = $activity_end_date;
                            $activity->month_assistance =  $data['activity']['month_assistance'][$index];
                            $activity->activity_province = $data['activity']['activity_province'][$index];
                            $activity->activity_district = $data['activity']['activity_district'][$index];
                            $activity->activity_tehsil = $data['activity']['activity_tehsil'][$index];
                            $activity->activity_uc = $data['activity']['activity_uc'][$index];
                            $activity->activity_village = $data['activity']['activity_village'][$index];
                            $activity->latitude = $data['activity']['latitude'][$index];
                            $activity->longitude = $data['activity']['longitude'][$index];
                            $activity->update();
                        }

                    } else {

                        $activity = new  ProjectActivity();
                        $activity->project_id = $id;
                        $activity->project_activity = $data['activity']['project_activity'][$index];
                        $activity->project_description = $data['activity']['project_description'][$index];
                        $activity->activity_start_date = $activity_start_date;
                        $activity->activity_end_date = $activity_end_date;
                        $activity->month_assistance =  $data['activity']['month_assistance'][$index];
                        $activity->activity_province = $data['activity']['activity_province'][$index];
                        $activity->activity_district = $data['activity']['activity_district'][$index];
                        $activity->activity_tehsil = $data['activity']['activity_tehsil'][$index];
                        $activity->activity_uc = $data['activity']['activity_uc'][$index];
                        $activity->activity_village = $data['activity']['activity_village'][$index];
                        $activity->latitude = $data['activity']['latitude'][$index];
                        $activity->longitude = $data['activity']['longitude'][$index];
                        $activity->save();
                    }
                }
            }
        }catch (\Exception $e){
            return redirect(route('projects.edit', $id))
                ->withErrors($e->getMessage())
                ->withInput();
        }

        return redirect('/projects')->with('message', 'success|Project updated successfully.');
    }

    public function delete($id){

        $userId = Auth::id();
        $project = NgoProjects::where('id', $id)->where('ngo_id',$userId)->first();
        if($project){
            $deleteProjectActivity = ProjectActivity::where('project_id',$id)->delete();
            $projectDelete = NgoProjects::find($project->id);
            $projectDelete->delete();

        }
        return redirect(route('projects.index'))->with('message', 'success|Deleted successfully!');
    }

    public function nocApply(){
        $userId = Auth::user()->id;
        $thematicAreas = ThematicArea::all();
        $projectCategories = ProjectCategory::all();
        $fundingAgency = FundingAgency::all();
        $modalities = Project_modality::all();
        $implementingPartner = ImplementingPartner::all();
        $implementingOrgType = Implementing_organization_type::all();
        $projectSector = Sector_Cluster::all();
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();
        $ucs = Uc::where('province_id',1)->get();
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Projects.');
        }
        return view('ngo.noc.add', compact(
            'userId','thematicAreas','projectCategories','ngoProfile','fundingAgency','modalities','implementingPartner',
            'implementingOrgType','projectSector','Province','District','Tehsil','ucs'
        ));
    }

    public function saveDraft($request){
        $userId = Auth::id();
        $data = $request->all();


    }
}
