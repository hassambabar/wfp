<?php

namespace App\Http\Controllers\Ngo;

use App\District;
use App\Http\Controllers\Controller;
use App\Models\Disaster;
use App\Models\Implementing_organization_type;
use App\Models\ImplementingPartner;
use App\Models\NgoPastEmergencies;
use App\Models\NgoProjects;
use App\Models\Project_modality;
use App\Models\ProjectActivity;
use App\Models\Sector_Cluster;
use App\Models\ThematicArea;
use App\Models\ProjectCategory;
use App\Models\FundingAgency;
use App\Models\Uc;
use App\Province;
use App\Tehsil;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\NgoProfile;
use Auth;
use Illuminate\Support\Facades\Validator;

class NgoPastEmergenciesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Projects.');
        }
        $pastEmergencies = $ngoProfile->pastEmergencies;
        return view('ngo.emergencies.index', compact('ngoProfile','pastEmergencies'));
    }

    public function create(){
        $userId = Auth::user()->id;
        $disasters = Disaster::all();
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();
        $ucs = Uc::where('province_id',1)->get();
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        $projects = $ngoProfile->projects->where('status',1);
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Projects.');
        }
        return view('ngo.emergencies.add', compact(
            'userId','Province','District','Tehsil','ucs','disasters','projects'
        ));
    }

    public function edit($id){
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if($ngoProfile->count() < 0){
            return redirect('/profile')->with('message', 'warning|Profile doesn\'t exists.');
        }
        $disasters = Disaster::all();
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();
        $ucs = Uc::where('province_id',1)->get();
        $pastEmergency = NgoPastEmergencies::findOrFail($id);
        $projects = $ngoProfile->projects->where('status',1);
        return view('ngo.emergencies.edit', compact(
            'Province','District','Tehsil','ucs','disasters','pastEmergency','projects'
        ));
    }

    public function show($id){
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if($ngoProfile->count() < 0){
            return redirect('/profile')->with('message', 'warning|Profile doesn\'t exists.');
        }
        $disasters = Disaster::all();
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();
        $ucs = Uc::where('province_id',1)->get();
        $pastEmergency = NgoPastEmergencies::findOrFail($id);
        $projects = $ngoProfile->projects->where('status',1);
        return view('ngo.emergencies.show', compact(
            'Province','District','Tehsil','ucs','disasters','pastEmergency','projects','ngoProfile'
        ));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'project_id' => 'required',
            'year' => 'required',
            'type_of_emergency' => 'required',
            'province' => 'required',
            'district' => 'required',
            'tehsil' => 'required',
            'uc' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('past-emergencies/create')
                ->withErrors($validator)
                ->withInput();
        }

        $userId = Auth::id();
        $data = $request->all();

        try{
            $pastEmergencies = new NgoPastEmergencies($data);

            $pastEmergencies->ngo()->associate($userId);
            $pastEmergencies->project()->associate($data['project_id']);
            $pastEmergencies->save();
        }catch (\Exception $e){
            // do task when error
            return redirect('past-emergencies/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }
        if($request->submit_type && $request->submit_type == 'save_new'){
            return redirect('/past-emergencies/create')->with('message', 'Saved!');
        }

        return redirect('/past-emergencies')->with('message', 'success|Record created successfully.');
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'project_id' => 'required',
            'year' => 'required',
            'type_of_emergency' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('past-emergencies.edit',$id))
                ->withErrors($validator)
                ->withInput();
        }
        $data = $request->all();

        try{
            $pastEmergencies = NgoPastEmergencies::findOrFail($id);
            if(!$pastEmergencies)
                return redirect(route('past-emergencies.index'))->with('message', 'error|Past Emergency with id '.$id.' not exist.');

            $pastEmergencies->update($data);
        }catch (\Exception $e){
            // do task when error
            return redirect(route('past-emergencies.edit',$id))
                ->withErrors($e->getMessage())
                ->withInput();
        }
        return redirect('/past-emergencies')->with('message', 'success|Past Emergency updated successfully.');
    }

    public function delete($id){

        $userId = Auth::id();
        $emergency = NgoPastEmergencies::where('id', $id)->where('ngo_id',$userId)->first();
        if($emergency){
            $emergencyDelete = NgoPastEmergencies::find($emergency->id);
            $emergencyDelete->delete();
        }

        return redirect('/past-emergencies')->with('message', 'success|Deleted successfully!');
    }

}
