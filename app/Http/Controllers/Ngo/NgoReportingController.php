<?php

namespace App\Http\Controllers\Ngo;

use App\District;
use App\Http\Controllers\Controller;
use App\Models\Implementing_organization_type;
use App\Models\ImplementingPartner;
use App\Models\NgoProjects;
use App\Models\NgoReporting;
use App\Models\Project_modality;
use App\Models\NgoReportingActivity;
use App\Models\Sector_Cluster;
use App\Models\ThematicArea;
use App\Models\ProjectCategory;
use App\Models\FundingAgency;
use App\Models\Uc;
use App\Province;
use App\Tehsil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\NgoProfile;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class NgoReportingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Volunteers.');
        }
        $reporting = $ngoProfile->reporting;
        $ngoProjects = $ngoProfile->projects->where('project_status','!=','completed')->where('status','1');
        return view('ngo.reporting.index', compact('ngoProfile','reporting','ngoProjects'));
        //ngo/reporting/index
    }

    public function create(){
        $userId = Auth::user()->id;
        $thematicAreas = ThematicArea::all();
        $projectCategories = ProjectCategory::all();
        $fundingAgency = FundingAgency::all();
        $modalities = Project_modality::all();
        $implementingPartner = ImplementingPartner::all();
        $implementingOrgType = Implementing_organization_type::all();
        $projectSector = Sector_Cluster::all();
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();
        $ucs = Uc::where('province_id',1)->get();
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Projects.');
        }
        $ngoProjects = $ngoProfile->projects->where('project_status','!=','completed')->where('status','1');
        // ngo/reporting/add
        return view('ngo.reporting.add', compact(
            'userId','thematicAreas','projectCategories','ngoProfile','fundingAgency','modalities','implementingPartner',
            'implementingOrgType','projectSector','Province','District','Tehsil','ucs','ngoProjects'
        ));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'project_category' => 'required',
            'project_title' => 'required',
            'donor' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('reporting/create')
                ->withErrors($validator)
                ->withInput();
        }

        $userId = Auth::id();
        $data = $request->all();
        $data['start_date'] = Carbon::parse($request->start_date);
        $data['end_date'] = Carbon::parse($request->end_date);
        $data['updated_date'] = Carbon::parse($request->updated_date);
        DB::beginTransaction();
        try{
            $reportingData = new NgoReporting($data);
            $reportingData->ngo()->associate($userId);
            $reportingData->save();
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            // do task when error
            return redirect('reporting/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }

        try{
            if($reportingData){
                $disks = 'uploads_dir'; // !empty(\Config::get('filesystems.default'))?\Config::get('filesystems.default'):'local';
                $fileName = '';
                $updateReporting = false;
                if ($request->hasFile('narrative_reports') && $request->file('narrative_reports')->isValid()) {
                    /*$name = $request->file('narrative_reports');
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();

                    $fileSystem = \Storage::disk($disks);
                    $filePath = 'reportings/' . $reportingData->id . '/contents/' . $fileName;
                    $fileSystem->put($filePath, file_get_contents($request->file('narrative_reports')), 'public');
                    $reportingData->narrative_reports = $filePath;
                    $updateReporting = true;*/

                    $name = $request->file('narrative_reports');
                    $destinationPath = 'uploads/reportings/' . $reportingData->id . '/contents/';
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                    $request->file('narrative_reports')->move($destinationPath, $fileName);
                    $reportingData->narrative_reports = $destinationPath.'/'.$fileName;
                    $updateReporting = true;

                }

                 if ($request->hasFile('upload_image') && $request->file('upload_image')->isValid()) {
                    /*$name = $request->file('upload_image');
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();

                    $fileSystem = \Storage::disk($disks);
                    $filePath = 'reportings/' . $reportingData->id . '/contents/' . $fileName;
                    $fileSystem->put($filePath, file_get_contents($request->file('upload_image')), 'public');
                    $reportingData->upload_image = $filePath;
                    $updateReporting = true;*/

                     $name = $request->file('upload_image');
                     $destinationPath = 'uploads/reportings/' . $reportingData->id . '/contents/';
                     $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                     $request->file('upload_image')->move($destinationPath, $fileName);
                     $reportingData->upload_image = $destinationPath.'/'.$fileName;
                     $updateReporting = true;
                }

                if($updateReporting)
                     $reportingData->update();


            }
        }catch (\Exception $e){
            DB::rollBack();
            return redirect('reporting/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }

        try{
            // insert activities
            // check if activity / StartDate / EndDate / Province are not empty.
            /*if( !empty($data['activity']['activity']) ||
                !empty($data['activity']['activity_start_date']) ||
                !empty($data['activity']['activity_end_date']) ||
                !empty($data['activity']['activity_province'])
            ){*/
            if( isset($data['activity']['activity']) && $data['activity']['activity'][0] !== null &&
                isset($data['activity']['activity_start_date']) && $data['activity']['activity_start_date'][0] !== null &&
                isset($data['activity']['description']) && $data['activity']['description'][0] !== null &&
                isset($data['activity']['activity_province']) && $data['activity']['activity_province'][0] !== null &&
                isset($data['activity']['activity_district']) && $data['activity']['activity_district'][0] !== null
            ){
                foreach($data['activity']['activity'] as $index => $value){
                    $activity_start_date = Carbon::parse($data['activity']['activity_start_date'][$index]);
                    $activity_end_date = Carbon::parse($data['activity']['activity_end_date'][$index]);
                    $activity = new NgoReportingActivity([
                        'activity' => $data['activity']['activity'][$index],
                        'description' => $data['activity']['description'][$index],
                        'activity_start_date' => $activity_start_date,
                        'activity_end_date' => $activity_end_date,
                        'activity_province' => $data['activity']['activity_province'][$index],
                        'activity_district' => $data['activity']['activity_district'][$index],
                        'activity_tehsil' => $data['activity']['activity_tehsil'][$index],
                        'activity_uc' => $data['activity']['activity_uc'][$index],
                        'activity_village' => $data['activity']['activity_village'][$index],
                    ]);
                    $reportingData->activities()->save($activity);
                }
            }

        }catch (\Exception $e){
            DB::rollBack();
            return redirect('reporting/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }

        return redirect('/reporting')->with('message', 'success|Report created successfully.');
    }

    public function edit($id){

        $userId = Auth::user()->id;
        $thematicAreas = ThematicArea::all();
        $projectCategories = ProjectCategory::all();
        $fundingAgency = FundingAgency::all();
        $modalities = Project_modality::all();
        $implementingPartner = ImplementingPartner::all();
        $implementingOrgType = Implementing_organization_type::all();
        $projectSector = Sector_Cluster::all();
        $District = District::where('province_id',1)->get();
        $Tehsil = Tehsil::where('province_id',1)->get();
        $Province = Province::get();
        $ucs = Uc::where('province_id',1)->get();
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        $reporting = NgoReporting::where('id', $id)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Projects.');
        }
        $ngoProjects = $ngoProfile->projects->where('project_status','!=','completed')->where('status','1');
        // ngo/reporting/add
        return view('ngo.reporting.edit', compact(
            'userId','thematicAreas','projectCategories','ngoProfile','fundingAgency','modalities','implementingPartner',
            'implementingOrgType','projectSector','Province','District','Tehsil','ucs','reporting','ngoProjects'
        ));

    }

    public function update(Request $request, $id){
        // dd($request);
        $validator = Validator::make($request->all(),[
            'project_category' => 'required',
            'project_title' => 'required',
            'donor' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('reporting.edit', $id))
                ->withErrors($validator)
                ->withInput();
        }

        $userId = Auth::id();
        $data = $request->all();
        $data['start_date'] = Carbon::parse($request->start_date);
        $data['end_date'] = Carbon::parse($request->end_date);
        $data['updated_date'] = Carbon::parse($request->updated_date);

        $reportingData = NgoReporting::find($id);
        if(!$reportingData)
             return redirect('/reporting')->with('message', 'error|Report with id '.$id.' not exist.');

        $reportingData->update($data);
        // $reportingData->ngo()->sync($userId);


        try{
            if($reportingData){
                $disks = 'public';
                $fileName = '';
                $updateReporting = false;
                if ($request->hasFile('narrative_reports') && $request->file('narrative_reports')->isValid()) {
                    /*$name = $request->file('narrative_reports');
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                    $fileSystem = \Storage::disk($disks);
                    $filePath = 'reportings/' . $reportingData->id . '/contents/' . $fileName;
                    $fileSystem->put($filePath, file_get_contents($request->file('narrative_reports')), 'public');
                    $reportingData->narrative_reports = $filePath;
                    $updateReporting = true;
                    dd( $filePath );*/

                    $name = $request->file('narrative_reports');
                    $destinationPath = 'uploads/reportings/' . $reportingData->id . '/contents/';
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                    $request->file('narrative_reports')->move($destinationPath, $fileName);
                    $reportingData->narrative_reports = $destinationPath.'/'.$fileName;
                    $updateReporting = true;
                }

                 if ($request->hasFile('upload_image') && $request->file('upload_image')->isValid()) {
                    /*$name = $request->file('upload_image');
                    $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                    $fileSystem = \Storage::disk($disks);
                    $filePath = 'reportings/' . $reportingData->id . '/contents/' . $fileName;
                    $fileSystem->put($filePath, file_get_contents($request->file('upload_image')), 'public');
                    $reportingData->upload_image = $filePath;
                    $updateReporting = true;*/

                     $name = $request->file('upload_image');
                     $destinationPath = 'uploads/reportings/' . $reportingData->id . '/contents/';
                     $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                     $request->file('upload_image')->move($destinationPath, $fileName);
                     $reportingData->upload_image = $destinationPath.'/'.$fileName;
                     $updateReporting = true;
                }

                if($updateReporting)
                     $reportingData->update();

            }
        }catch (\Exception $e){
            return redirect('reporting/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }

        try{
            //insert activities
            if( isset($data['activity']['activity']) && $data['activity']['activity'][0] !== null &&
                isset($data['activity']['activity_start_date']) && $data['activity']['activity_start_date'][0] !== null &&
                isset($data['activity']['description']) && $data['activity']['description'][0] !== null &&
                isset($data['activity']['activity_province']) && $data['activity']['activity_province'][0] !== null &&
                isset($data['activity']['activity_district']) && $data['activity']['activity_district'][0] !== null
            ) {
                foreach ($data['activity']['activity'] as $index => $value) {
                    $activity_start_date = Carbon::parse($data['activity']['activity_start_date'][$index]);
                    $activity_end_date = Carbon::parse($data['activity']['activity_end_date'][$index]);
                    // update existing activity
                    if (isset($data['activity']['id'][$index]) && !empty($data['activity']['id'][$index])) {
                        $activity = NgoReportingActivity::find($data['activity']['id'][$index]);
                        if (!empty($activity)) {
                            $activity->activity = $data['activity']['activity'][$index];
                            $activity->description = $data['activity']['description'][$index];
                            $activity->activity_start_date = $activity_start_date;
                            $activity->activity_end_date = $activity_end_date;
                            $activity->activity_province = $data['activity']['activity_province'][$index];
                            $activity->activity_district = $data['activity']['activity_district'][$index];
                            $activity->activity_tehsil = $data['activity']['activity_tehsil'][$index];
                            $activity->activity_uc = $data['activity']['activity_uc'][$index];
                            $activity->activity_village = $data['activity']['activity_village'][$index];
                            $activity->update();
                        }
                    } else {
                        // add as a new activity.
                        $activity = new NgoReportingActivity();
                        $activity->report_id = $id;
                        $activity->activity = $data['activity']['activity'][$index];
                        $activity->description = $data['activity']['description'][$index];
                        $activity->activity_start_date = $activity_start_date;
                        $activity->activity_end_date = $activity_end_date;
                        $activity->activity_province = $data['activity']['activity_province'][$index];
                        $activity->activity_district = $data['activity']['activity_district'][$index];
                        $activity->activity_tehsil = $data['activity']['activity_tehsil'][$index];
                        $activity->activity_uc = $data['activity']['activity_uc'][$index];
                        $activity->activity_village = $data['activity']['activity_village'][$index];
                        $activity->save();
                    }

                }
            }
        }catch (\Exception $e){
            return redirect(route('reporting.edit',$id))
                ->withErrors($e->getMessage())
                ->withInput();
        }

        return redirect('/reporting')->with('message', 'success|Report updated successfully.');

    }

    public function delete($id){

        $userId = Auth::id();
        $reporting = NgoReporting::where('id', $id)->where('ngo_id',$userId)->first();
        if($reporting){
            $deleteRepActivity = NgoReportingActivity::where('report_id',$id)->delete();
            $reportingDelete = NgoReporting::find($reporting->id);
            $reportingDelete->delete();

        }
        return redirect('/reporting')->with('message', 'success|Deleted successfully!');
    }

    public function show($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Stocks.');
        }
        $reporting = NgoReporting::where('id', $id)->first();

        return view('ngo.reporting.show', compact('reporting','ngoProfile'));
    }
}
