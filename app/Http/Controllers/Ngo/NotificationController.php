<?php

namespace App\Http\Controllers\Ngo;

use App\Http\Controllers\Controller;
// use Carbon\Carbon;
use App\Models\Emergency;
use App\Models\EmergencyItem;
use App\Models\Needassesmentresponse;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('ngo.notification.index');
        //ngo/notification/index
    }

    public function emergencies(){
        $userId = Auth::user()->id;
//        $emergencyNotifications = auth()->user()->unreadNotifications->where('type','App\Notifications\EmergenciesCreated');
        $EmergencyItems = EmergencyItem::orderBy("id", "desc")->get();

        foreach ($EmergencyItems as $key => $value) {
            $Needassesmentresponse = Needassesmentresponse::where('emergency_item_id',$value->id)->where('ngo_id',$userId)->first();
            if($Needassesmentresponse){
                $EmergencyItems[$key]->Needassesmentresponse = 1;
            }else{
                $EmergencyItems[$key]->Needassesmentresponse = 0;
            }
        }
        return view('ngo.notification.emergencies', compact('EmergencyItems'));
    }




}
