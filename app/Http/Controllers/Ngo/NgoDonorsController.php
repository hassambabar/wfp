<?php

namespace App\Http\Controllers\Ngo;

use App\Http\Controllers\Controller;

use App\Models\NgoProjects;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\NgoProfile;
use Auth;
use Illuminate\Support\Facades\Validator;

class NgoDonorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Donors.');
        }
        $projects = $ngoProfile->projects->where('status',1);
        return view('ngo.donors.index', compact('ngoProfile','projects'));
    }

    public function edit($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Donors.');
        }
        $project = NgoProjects::findOrFail($id);
        return view('ngo.donors.edit', compact('ngoProfile','project'));
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(),[
            'funds_donor_name' => 'required',
            'funds_amount' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('donors.edit',$id))
                ->withErrors($validator)
                ->withInput();
        }

        $data = $request->all();

        try{
            $projectData =   NgoProjects::find($id);
            if(!$projectData)
                return redirect(route('donors.index'))->with('message', 'error|Project with id '.$id.' not exist.');

            $projectData->update($data);
        }catch (\Exception $e){
            // do task when error
            return redirect(route('donors.edit',$id))
                ->withErrors($e->getMessage())
                ->withInput();
        }
        return redirect('/donors')->with('message', 'success|Donor updated successfully.');
    }
}
