<?php

namespace App\Http\Controllers\Ngo;

use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\NgoProfile;
use Auth;
use Illuminate\Support\Facades\Validator;

class NgoNocRequestsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Projects.');
        }
        $projects = $ngoProfile->projects->where('status',1);
        return view('ngo.noc.index', compact('ngoProfile','projects'));
    }
}
