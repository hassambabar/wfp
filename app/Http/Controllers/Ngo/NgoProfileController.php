<?php

namespace App\Http\Controllers\Ngo;

use App\Http\Controllers\Controller;
use App\Models\FundingAgency;
use App\Models\NgoMou;
use App\Models\NgoOrgNatures;
use App\Models\NgoRegistrationLaw;
use App\Models\OrganizationNatures;
use App\Notifications\ProfileApproval;
use App\Notifications\ProfileUpdateApproval;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\NgoProfile;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class NgoProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        $mouData = $ngoProfile ? $ngoProfile->mous : '';
        $ngoOrgNatures = [];
        if(isset($ngoProfile->ngoOrgNatures) && !$ngoProfile->ngoOrgNatures->isEmpty()){
            $orNatures = $ngoProfile->ngoOrgNatures->map(function ($item, $key){
                return $item->natureOfOrg;
            });
            $ngoOrgNatures = $orNatures->pluck('nature_of_org')->toArray();
        }

        return view('ngo.profile.index', compact('ngoProfile','mouData','ngoOrgNatures'));
    }

    public function create(){
        $userId = Auth::user()->id;
        /*$fundingAgency = FundingAgency::all();*/
        $orgNatures = OrganizationNatures::all();
        $regLaws = NgoRegistrationLaw::all();
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(isset($ngoProfile) && $ngoProfile !== null){
            return redirect('/profile')->with('message', 'warning|Profile already exists.');
        }
        return view('ngo.profile.add', compact('userId','orgNatures', 'regLaws'));
    }

    public function edit(){
        $userId = Auth::user()->id;
        $orgNatures = OrganizationNatures::all();
        $regLaws = NgoRegistrationLaw::all();
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if($ngoProfile->count() < 0){
            return redirect('/profile')->with('message', 'warning|Profile doesn\'t exists.');
        }
        $ngoOrgNatures = $ngoProfile->ngoOrgNatures ? $ngoProfile->ngoOrgNatures->pluck('org_nature_id')->toArray() : [];
//        dd($ngoOrgNatures);
        return view('ngo.profile.edit', compact('ngoProfile','orgNatures','ngoOrgNatures', 'regLaws'));
    }

    public function update_approval(){
        $user = Auth::user();

        if($user->enable_edit == 0 && $user->pdma_profile_review_decision == 'deferred'){
            return redirect()->back()->with("error","You have already applied for profile update approval.");
        }

        $users = User::select("users.*")
            ->join("model_has_roles", "model_has_roles.model_id", "=", "users.id")->whereIn('model_has_roles.role_id',[3,4])
            ->where('users.status',1)
            ->get();

        Notification::send($users, new ProfileUpdateApproval($user));

        $user->enable_edit = 0;
        $user->save();

        return redirect()->back()->with("success","Request for profile update approval successfully submitted.");
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'legal_status' => 'required',
            'reg_law' => 'required_if:legal_status,intl_org,pk_charity,corp_firm,pk_org',
            'reg_date' => 'required_if:legal_status,intl_org,pk_charity,corp_firm,pk_org',
            /*'reg_no' => 'bail|required',*/
            'hq_origin_country' => 'required_if:legal_status,intl_org,un_org',
            'hq_reg_law' => 'required_if:legal_status,intl_org',
            'hq_reg_date' => 'required_if:legal_status,intl_org',
            'hq_reg_no' => 'required_if:legal_status,intl_org',
            'mou_status' => 'required_if:have_mou,1',
            'mou_expiry_date' => 'required_if:mou_status,valid,invalid',
            'mou_file' => 'mimes:doc,pdf,docx,jpg,jpeg',
            'registration_certificate' => 'mimes:doc,pdf,docx,jpg,jpeg',
        ]);

        if ($validator->fails()) {
            return redirect('profile/create')
                ->withErrors($validator)
                ->withInput();
        }

        $userId = Auth::id();

        $data = $request->all();
        if($request->reg_date){
            $data['reg_date'] = Carbon::parse($request->reg_date);
        }
        $data['hq_reg_date'] = Carbon::parse($request->hq_reg_date);
        DB::beginTransaction();
        try{
            $ngoData = new NgoProfile($data);

            $ngoData->user()->associate($userId);

            $ngoData->save();

            if ($request->hasFile('registration_certificate') && $request->file('registration_certificate')->isValid()) {
                $name = $request->file('registration_certificate');
                $destinationPath = 'uploads/ngo/' . $ngoData->id . '/contents/';
                $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                $request->file('registration_certificate')->move($destinationPath, $fileName);
                $ngoData->registration_certificate = $destinationPath.'/'.$fileName;
                $ngoData->update();
            }

            //update nature of org
            if(isset($data['nature_of_org']) && $data['nature_of_org'][0] !== null){
                $deleteNgoNature = NgoOrgNatures::where('ngo_id',$ngoData->id)->delete();
                foreach($data['nature_of_org'] as $index => $value){
                    $ngoOrgNature = new NgoOrgNatures([
                        'ngo_id' => $ngoData->id,
                        'org_nature_id' => $data['nature_of_org'][$index],
                    ]);
                    $ngoOrgNature->save();
                }
            }

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            // do task when error
            return redirect('profile/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }

        try{
            if(isset($ngoData)){
                $datMou = $request->all();
                if($datMou['have_mou'] == 1){
                    $datMou['mou_signed_date'] = Carbon::parse($request->mou_signed_date);
                    if($request->mou_expiry_date){
                        $datMou['mou_expiry_date'] = Carbon::parse($request->mou_expiry_date);
                    }
                    $mouData = new NgoMou($datMou);
                    $mouData->ngo()->associate($userId);
                    $mouData->save();
                    if($mouData){
                        $disks = !empty(\Config::get('filesystems.default'))?\Config::get('filesystems.default'):'local';
                        $fileName = '';
                        if ($request->hasFile('mou_file') && $request->file('mou_file')->isValid()) {
                            /*$fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                            $fileSystem = \Storage::disk($disks);
                            $filePath = 'mou/' . $ngoData->id . '/contents/' . $fileName;
                            $fileSystem->put($filePath, file_get_contents($request->file('mou_file')), 'public');
                            $mouData->mou_file = $filePath;
                            $mouData->update();*/

                            $name = $request->file('mou_file');
                            $destinationPath = 'uploads/mous/' . $ngoData->id . '/contents/';
                            $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                            $request->file('mou_file')->move($destinationPath, $fileName);
                            $mouData->mou_file = $destinationPath.'/'.$fileName;
                            $mouData->update();
                        }
                    }
                }
            }
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            // do task when error
            return redirect('profile/create')
                ->withErrors($e->getMessage())
                ->withInput();
        }
        return redirect('/profile')->with('message', 'success|Profile data saved.');
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(),[
            'legal_status' => 'required',
            'reg_law' => 'required_if:legal_status,intl_org,pk_charity,corp_firm,pk_org',
            'reg_date' => 'required_if:legal_status,intl_org,pk_charity,corp_firm,pk_org',
            /*'reg_no' => 'bail|required',*/
            'hq_origin_country' => 'required_if:legal_status,intl_org,un_org',
            'hq_reg_law' => 'required_if:legal_status,intl_org',
            'hq_reg_date' => 'required_if:legal_status,intl_org',
            'hq_reg_no' => 'required_if:legal_status,intl_org',
            'registration_certificate' => 'mimes:doc,pdf,docx,jpg,jpeg',
        ]);
        if ($validator->fails()) {
            return redirect('profile/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        }

        $data = $request->all();
        if($request->reg_date){
            $data['reg_date'] = Carbon::parse($request->reg_date);
        }
        $data['hq_reg_date'] = Carbon::parse($request->hq_reg_date);
        try{
            $profileData =   NgoProfile::find($id);
            if(!$profileData)
                return redirect(route('profile.index'))->with('message', 'error|Profile with id '.$id.' not exist.');

            if ($request->hasFile('registration_certificate') && $request->file('registration_certificate')->isValid()) {
                $name = $request->file('registration_certificate');
                $destinationPath = 'uploads/ngo/' . $id . '/contents/';
                $fileName = str_replace(' ', '_', pathinfo($name->getClientOriginalName())['filename']).'_'.time() . '.' . $name->getClientOriginalExtension();
                $request->file('registration_certificate')->move($destinationPath, $fileName);
                $profileData->registration_certificate = $destinationPath.'/'.$fileName;
            }
            $profileData->update($data);

            //update nature of org
            if(isset($data['nature_of_org']) && $data['nature_of_org'][0] !== null){
                $deleteNgoNature = NgoOrgNatures::where('ngo_id',$profileData->id)->delete();
                foreach($data['nature_of_org'] as $index => $value){
                    $ngoOrgNature = new NgoOrgNatures([
                        'ngo_id' => $profileData->id,
                        'org_nature_id' => $data['nature_of_org'][$index],
                    ]);
                    $ngoOrgNature->save();
                }
            }
        }catch (\Exception $e){
            // do task when error
            return redirect(route('profile.edit',$id))
                ->withErrors($e->getMessage())
                ->withInput();
        }
        return redirect('/profile')->with('message', 'success|Profile updated successfully.');
    }

    public function approval(){
        $user = Auth::user();
        if($user->status == -1){
            return redirect()->back()->with("error","You have already applied for profile approval.");
        }
        $users = User::select("users.*")
            ->join("model_has_roles", "model_has_roles.model_id", "=", "users.id")->where('model_has_roles.role_id', 3)
            ->where('users.status',1)
            ->get();
        Notification::send($users, new ProfileApproval($user));

        $user->status = -1;
        $user->save();

        return redirect()->back()->with("success","Request for profile approval successfully submitted.");
    }
}
