<?php

namespace App\Http\Controllers;

use App\Models\NgoProfile;
use Illuminate\Http\Request;
use App\Affiliations;
use Auth;
use App\Models\Network;

class AffiliationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Affiliations.');
        }
        $Affiliations = Affiliations::where('ngo_id', $userId)->get();
        return view('Affiliations.index', compact('Affiliations','ngoProfile'));
    }

    public function create()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Affiliations.');
        }
        $Network = Network::get();
        return view('Affiliations.add', compact('Network'));
    }

    public function edit($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Affiliations.');
        }
        $Network = Network::get();
        $Affiliations = Affiliations::where('id', $id)->first();

        return view('Affiliations.Edit', compact('Affiliations','Network'));
    }

    public function show($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Affiliations.');
        }
        $Network = Network::get();
        $Affiliations = Affiliations::where('id', $id)->first();

        return view('Affiliations.show', compact('Affiliations','Network','ngoProfile'));
    }


    public function store(Request $request)
    {
         $request->validate([
            'network_id' => 'required',
            'joining_date' => 'required',

            ]);
        $userId = Auth::id();

        $Affiliations = new Affiliations([
            'network_id' => $request->get('network_id'),
            'joining_date' => $request->get('joining_date'),
            'status' => $request->get('status'),
            'ngo_id' => $userId,
        ]);
       $Affiliations->save();

        if($request->submit_type && $request->submit_type == 'save_new'){
            return redirect('/affiliations/create')->with('message', 'Saved!');
        }

       return redirect('/affiliations')->with('message', 'Saved!');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'network_id' => 'required',
            'joining_date' => 'required',

            ]);
        $Affiliations = Affiliations::find($id);
        $Affiliations->network_id =  $request->get('network_id');
        $Affiliations->joining_date =  $request->get('joining_date');
        $Affiliations->status =  $request->get('status');
        $Affiliations->save();
        return redirect('/affiliations')->with('message', 'Update successfully!');
    }

    public function delete($id){

        $userId = Auth::id();
        $Affiliations = Affiliations::where('id', $id)->where('ngo_id',$userId)->first();
        if($Affiliations){
            $AffiliationsDelete = Affiliations::find($Affiliations->id);
            $AffiliationsDelete->delete();

        }

        return redirect('/affiliations')->with('message', 'Deleted successfully!');    }
}
