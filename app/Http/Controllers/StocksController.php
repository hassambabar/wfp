<?php

namespace App\Http\Controllers;

use App\Models\NgoProfile;
use Illuminate\Http\Request;
use App\Volunteer;
use Auth;

use App\District;
use App\Province;

use App\Tehsil;
use App\Uc;
use App\Stocks;
use App\Models\Category_type;

use App\Models\Item_type;

class StocksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Stocks.');
        }
        $Stocks = Stocks::where('ngo_id', $userId)->get();
        return view('Stocks.index', compact('Stocks','ngoProfile'));
    }

    public function create()
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Stocks.');
        }
        $Province = Province::get();
        $District = District::get();
        $Tehsil = Tehsil::get();
        $Uc = Uc::get();
        $Category_type = Category_type::get();
        $Item_type = Item_type::get();
        return view('Stocks.add', compact('District','Tehsil','Uc','Category_type','Item_type','Province'));
    }

    public function edit($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Stocks.');
        }
        $Province = Province::get();
        $District = District::get();
        $Tehsil = Tehsil::get();
        $Uc = Uc::get();
        $Stocks = Stocks::where('id', $id)->first();
        $Category_type = Category_type::get();
        $Item_type = Item_type::get();
        return view('Stocks.Edit', compact('Stocks','District','Tehsil','Uc','Category_type','Item_type','Province'));
    }

    public function show($id)
    {
        $userId = Auth::user()->id;
        $ngoProfile = NgoProfile::where('user_id', $userId)->first();
        if(!isset($ngoProfile) && $ngoProfile === null){
            return redirect('/profile/create')->with('message', 'error|NGO Profile should exist before adding Stocks.');
        }
        $Stocks = Stocks::where('id', $id)->first();
        return view('Stocks.show', compact('Stocks','ngoProfile'));
    }


    public function store(Request $request)
    {
         $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'quantity' => 'required',
            'item_type_id' => 'required',
            'location_id' => 'required',
            ]);
        $userId = Auth::id();

        $Stocks = new Stocks([
            'name' => $request->get('name'),
            'category_id' => $request->get('category_id'),
            'quantity' => $request->get('quantity'),
            'item_type_id' => $request->get('item_type_id'),
            'availability' => $request->get('availability'),
            'location_id' => $request->get('location_id'),
            'remarks' => $request->get('remarks'),
            'district_id' => $request->get('district_id'),
            'tehsil_id' => $request->get('tehsil_id'),
            'uc_id' => $request->get('uc_id'),
            'ngo_id' => $userId,
        ]);
       $Stocks->save();

        if($request->submit_type && $request->submit_type == 'save_new'){
            return redirect('/stocks/create')->with('message', 'Saved!');
        }

       return redirect('/stocks')->with('message', 'Saved!');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'quantity' => 'required',
            'item_type_id' => 'required',
            'location_id' => 'required',
            ]);

        $Stocks = Stocks::find($id);
        $Stocks->name =  $request->get('name');
        $Stocks->category_id =  $request->get('category_id');
        $Stocks->quantity =  $request->get('quantity');
        $Stocks->item_type_id =  $request->get('item_type_id');
        $Stocks->availability =  $request->get('availability');
        $Stocks->location_id =  $request->get('location_id');
        $Stocks->remarks =  $request->get('remarks');
        $Stocks->district_id =  $request->get('district_id');
        $Stocks->tehsil_id =  $request->get('tehsil_id');
        $Stocks->uc_id =  $request->get('uc_id');
        $Stocks->save();
        return redirect('/stocks')->with('message', 'Update successfully!');
    }

    public function delete($id){

        $userId = Auth::id();
        $Stocks = Stocks::where('id', $id)->where('ngo_id',$userId)->first();
        if($Stocks){
            $StocksDelete = Stocks::find($Stocks->id);
            $StocksDelete->delete();

        }

        return redirect('/stocks')->with('message', 'Deleted successfully!');    }
}
