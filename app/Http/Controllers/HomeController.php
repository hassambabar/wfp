<?php

namespace App\Http\Controllers;

use App\Affiliations;
use App\Models\Contact_pdma;
use App\Models\Human_resource;
use App\Models\Ngo_contact;
use App\Models\NgoProfile;
use App\Stocks;
use App\Volunteer;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(checkUserRole('NGO')){
            $userId = Auth::user()->id;
            $ngoProfile = NgoProfile::where('user_id', $userId);
            $ngoMous = $projects = $pastEmergencies = null;
            if($ngoProfile->count() > 0){
                $ngoMous = $ngoProfile->first()->mous->count();
                $projects = $ngoProfile->first()->projects->count();
                $pastEmergencies = $ngoProfile->first()->pastEmergencies->count();
            }

            $Human_resource = Human_resource::where('ngo_id', $userId)->count();
            $Volunteers = Volunteer::where('ngo_id', $userId)->count();
            $Stocks = Stocks::where('ngo_id', $userId)->count();
            $Affiliations = Affiliations::where('ngo_id', $userId)->count();
            $Ngo_contact = Ngo_contact::where('ngo_id', $userId)->count();
            $Contact_pdma = Contact_pdma::where('ngo_id', $userId)->count();
            return view('home',compact(
                'ngoProfile','ngoMous','projects','pastEmergencies','Human_resource','Volunteers','Stocks','Affiliations',
                'Ngo_contact','Contact_pdma'
            ));
        }else{
            return view('home');
        }

    }

    public function readremainder($id)
    {
        auth()->user()->unreadNotifications->where('id', $id)->markAsRead();
    }
}
