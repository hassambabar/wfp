<?php
if (!function_exists('getLegalStatusName')) {
    function getLegalStatusName($legal_status_key)
    {
        $legal_status_name = '';
        switch ($legal_status_key) {
            case 'pk_org':
                $legal_status_name = 'Pakistani Organization';
                break;
            case 'corp_firm':
                $legal_status_name = 'Corporate Firm';
                break;
            case 'pk_charity':
                $legal_status_name = 'Pakistani Charity Organization';
                break;
            case 'intl_org':
                $legal_status_name = 'International Organization';
                break;
            case 'un_org':
                $legal_status_name = 'UN Organization Agency';
                break;
            default:
                break;
        }

        return $legal_status_name;
    }
}

if (!function_exists('displayMessage')) {
    function displayMessage()
    {
        if (!empty(session('message'))) {
            if (is_array(session('message'))) {
                $type = 'info';
                if (isset(session('message')['level'])) {
                    $type = session('message')['level'];
                }
                $message = session('message')['content'];
            } else if (session('message')) {
                list($type, $message) = explode('|', session('message'));
            }
            if ($type == 'error')
                $type = 'danger';
            if ($type == 'message')
                $type = 'info';
            return '<div class="alert alert-' . $type . '"><a href="#" class="close" data-dismiss="alert"
                        aria-label="close">&times;</a>' . $message . '</div>';
        } else {
            return '';
        }
    }
}
if (!function_exists('activeMenu')) {
    function activeMenu($uri = '')
    {
        $active = '';
        /*if (Request::is(Request::segment(1) . '/' . $uri . '/*') || Request::is(Request::segment(1) . '/' . $uri) || Request::is($uri)) {

        }*/
        if(Route::is(''.$uri.'s*') || Route::is(''.$uri.'.*') || Route::is(''.$uri.'')){
            $active = 'active-menu';
        }
        return $active;
    }
}

if (!function_exists('checkUserStatus')){
    function checkUserStatus(){
        $user = auth()->user();
        if($user->status == 1){
            return true;
        }else
            return false;
    }
}

if (!function_exists('checkUserRole')){
    function checkUserRole($role){
        if(Auth::user()->hasRoleName($role)){
            return true;
        }
        return false;
    }
}

if (!function_exists('checkProfileApproval')){
    function checkProfileApproval(){
        if(Auth::user()->pdma_profile_review_decision == 'deferred'){
            return true;
        }
        return false;
    }
}

if (!function_exists('checkNocApproval')){
    function checkNocApproval(){
        if(Auth::user()->pdma_noc_review_decision == 'deferred'){
            return true;
        }
        return false;
    }
}

if (!function_exists('checkEnableEdit')){
    function checkEnableEdit(){
        if(Auth::user()->enable_edit == 1){
            return true;
        }
        return false;
    }
}

if (!function_exists('checkNocEnableEdit')){
    function checkNocEnableEdit($projectId = null){
        $nocNotify = auth()->user()->notifications->filter(function ($notification){
            return $notification->type == 'App\Notifications\ProjectNocReviewByPdma';
        })->map(function ($nt){
            return $nt->data['noc']['project_id'];
        })->toArray();

        if($projectId !== null){
            if(Auth::user()->noc_enable_edit == 1 && in_array($projectId, $nocNotify)){
                return true;
            }else{
                return false;
            }
        }elseif(Auth::user()->noc_enable_edit == 1){
            return true;
        }

        return false;
    }
}

if (!function_exists('checkTCAccepted')){
    function checkTCAccepted(){
        if(Auth::user()->terms_accepted == 1){
            return true;
        }
        return false;
    }
}

if (!function_exists('getTermsHtml')){
    function getTermsHtml(){
        $terms = \App\Models\TermsContent::where('status',1)->get()->first();
        return $terms;
    }
}
