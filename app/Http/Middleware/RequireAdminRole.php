<?php

namespace App\Http\Middleware;

use Closure;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\DB;

class RequireAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


      //  dd($user_role->role_id);

        if (backpack_user() && backpack_user()->hasRole('NGO'))
        {
            abort(403, 'Access denied');
        }

        return $next($request);
    }
}
