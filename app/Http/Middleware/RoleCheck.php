<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class RoleCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, String $role)
    {
        // /echo $role; exit;
        $backpack_user = backpack_user();
        if($backpack_user){
            $user =  backpack_user();
        }
        if(Auth::user()){
            $user =  Auth::user();
        }
        // /dd($user->role($role));
        if($user->role($role))
            return $next($request);
        else 
        abort(403, 'Access denied');
      //      return redirect((strtoupper($role) == 'PDMA')?'/admin':'/');
            // return redirect('login');

    }

    
}
