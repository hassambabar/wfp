<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AnnouncementCreated extends Notification
{
    use Queueable;
    protected $date;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($date)
    {
        //
      //  dd($Emergencies);
        $this->date=$date;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
      //  return ['mail'];
      return ['database','mail'];

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $date = $this->date;
        // dd($date);
        return (new MailMessage)
                    ->subject($date['title'])
                    ->greeting('Announcements & Alerts!')
                    ->line($date['title'])
  
                    ->line('Forecast Of: '.$date['forecast_of'])
                    ->line('District: '.$date['district_id'])
                    ->line('From Date: '.$date['from'])
                    ->line('To Date: '.$date['to'])
                    ->line('Message: '.$date['message'])

                    ->line('Created At: '.$date['created_at']);
                    // ->action('View', url($date['link']));

    }

    public function toDatabase($notifiable)
    {

       // dd($notifiable);

        return [
            'user' => $notifiable,
            'alerts' => $this->date,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
