<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmergenciesRespond extends Notification
{
    use Queueable;
    protected $Emergencies;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($Emergencies)
    {
        //
      //  dd($Emergencies);
        $this->Emergencies=$Emergencies;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
      //  return ['mail'];
      return ['database','mail'];

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $Emergencies = $this->Emergencies;
        return (new MailMessage)
                    ->greeting('Hello!')
                    ->subject('Emergency Response')
                    ->line('<strong>Emergency Response.</strong>')
                    ->line('Emergency Response: '.$Emergencies->response)
                    ->line('Quantity: '.$Emergencies->response_quantity)

                    ->action('Emergencies Action', url('/admin/emergency',$Emergencies->emergency_id.'/show'));
    }

    public function toDatabase($notifiable)
    {

       // dd($notifiable);

        return [
            'user' => $notifiable,
            'emergency' => $this->Emergencies,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
