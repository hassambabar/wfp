<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class HazardProneAreaAdded extends Notification
{
    use Queueable;
    protected $hazardProneArea;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($hazardProneArea)
    {
        $this->hazardProneArea = $hazardProneArea;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $hazardProneArea = $this->hazardProneArea;
        return (new MailMessage)
                    ->greeting('Hello!')
                    ->line('New Hazard Prone Area is added.')
                    ->line('Type Of Hazard: '.$hazardProneArea->hazardType->name)
                    ->line('District: '.$hazardProneArea->districtName->name)
                    ->line('Taluka: '.$hazardProneArea->tehsilName->name)
                    ->line('UC: '.$hazardProneArea->UcName->name)
                    ->line('Expected date of Occurrence: '.$hazardProneArea->expected_occurrence)
                    ->line('When happened: '.$hazardProneArea->happened_on);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {

        // dd($notifiable);

        return [
            'user' => $notifiable,
            'hazardProneArea' => $this->hazardProneArea,
        ];
    }
}
