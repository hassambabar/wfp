<?php

namespace App;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\DB;
use Backpack\PermissionManager\app\Models\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Auth;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use App\Models\Ngo_contact;


class User extends Authenticatable implements MustVerifyEmail
{
    use HasRoles;
    use CrudTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function assignRole(Role $role)
    {
        return $this->roles()->save($role);
    }

    public function role()
    {
        $user = auth()->user();

        $backpack_user = backpack_user();
        if($backpack_user){
            $user =  backpack_user();
        }
        if(Auth::user()){
       
            $user =  Auth::user();
        }

      
        $user_role = DB::table('model_has_roles')->where('model_id',$user->id)->first();
       
        $role = Role::find($user_role->role_id);

       // echo "<pre>"; print_r($role); exit;
        return $role;
    }

    function hasRoleName($roleName){
        return ($this->role() && $this->role()->name == strtoupper($roleName))?true:false;
    }


    protected $fillable = [
        'name', 'email', 'password','status','district_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function focalperson()
    {

      //  $Ngo_contact = Ngo_contact::where('ngo_id',$this->id)->where('can_pdma_contact',1)->get();

       // return $Ngo_contact;
    //   $aa = $this->belongsTo('App\Models\Ngo_contact', 'ngo_id','id');
    //   dd($aa);
         return $this->belongsTo('App\Models\Ngo_contact', 'ngo_id','id');

    }

//     public function focalperson(){
//         return $this->belongsTo('App\Models\Ngo_contact', 'id','ngo_id');
//    }

   public function reporting(){
        return $this->belongsTo('App\Models\NgoReporting', 'id','ngo_id');
   }


}
