<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Tehsil extends Model
{
    //
    use CrudTrait;

    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id', 'id');

    }

}
