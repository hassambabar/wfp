@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="panel panel-default">
            <div class="panel-heading">
                Human Resource
            </div>
            <div class="panel-body">
                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif


                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Nationality</th>
                                <th>Contact number</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($Human_resource as $key => $item)
                            <tr class="odd gradeX">
                                <td>{{$item->name}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->nationality}}</td>
                                <td>{{$item->phone_no}}</td>
                                <td>
                                    <a href="{{ url("/human-resource/detail/{$item->id}") }}">View</a>
                                    @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                                    <a href="{{ url("/human-resource/{$item->id}/edit") }}"> - Edit - </a>
                                    @endif
                                    @if($ngoProfile->user->status === 0 )
                                        - <a href="{{ url("/human-resource/{$item->id}/delete") }}">Delete</a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr class="gradeU">
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                            @endforelse



                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>

@endsection

@section('pagespecificscripts')
<script>
       $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
@endsection
