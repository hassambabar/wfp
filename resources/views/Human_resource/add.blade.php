@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{-- <div class="card-title">
                        <div class="title">Basic example</div>
                    </div> --}}
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                        @if (session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    <form action="{{route('human-resource.store')}}" id="human_resource" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="submit_type" value="" />
                        {{-- <div class="form-group col-md-6">
                            <label for="total_national_staff">Number of total National Staff</label>
                            <input type="text" class="form-control" id="total_national_staff" placeholder="Number of total National Staff" value="{{old('total_national_staff')}}" name="total_national_staff">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="total_frn_national_staff">Number of total Foreign National Staff</label>
                            <input type="text" class="form-control" id="total_frn_national_staff" placeholder="Number of total Foreign National Staff" value="{{old('total_frn_national_staff')}}" name="total_frn_national_staff">
                        </div>
                        <div class="col-md-12">
                            <hr class="style1" />
                        </div> --}}
                        <div class="form-group col-md-6">
                                <label for="name">Name *</label>
                                <input required type="text" class="form-control" id="name" placeholder="Name" value="{{old('name')}}" name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="nationality">Nationality</label>
                            <select name="nationality" id="nationality" class="form-control">
                                <option value="">Select</option>
                                <option value="pakistani">Pakistani</option>
                                <option value="other">Foreign National</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email address *</label>
                            <input required type="email" class="form-control" id="email" placeholder="example@gmail.com" value="{{old('email')}}"  name="email">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="phone_no">Phone No *</label>
                            <input required type="text" class="form-control is_numeric" id="phone_no" placeholder="03330000000" value="{{old('phone_no')}}"  name="phone_no">
                        </div>

                        <div class="boxpakistan">
                            <div class="form-group col-md-6">
                                <label for="father_name">Father Name</label>
                                <input type="text" class="form-control" id="father_name" placeholder="Father Name" value="{{old('father_name')}}"  name="father_name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="cnic">CNIC *</label>
                                <input type="text" class="form-control is_numeric is_req" id="cnic" placeholder="CNIC" value="{{old('cnic')}}"  name="cnic">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="address">Complete Address (Permanent)</label>
                                <input type="text" class="form-control" id="address" placeholder="Complete Address (Permanent)" value="{{old('address')}}"  name="address">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="temp_address">Complete Address (Temporary)</label>
                                <input type="text" class="form-control" id="temp_address" placeholder="Complete Address (Temporary)" value="{{old('temp_address')}}"  name="temp_address">
                            </div>
                        </div>

                        <div class="boxother">
                            <div class="form-group col-md-6">
                                <label for="passport">Passport</label>
                                <input type="file" class="form-control is_numeric" id="passport" name="passport">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="country_home_add">Country Home address</label>
                                <input type="text" class="form-control" id="country_home_add" placeholder="" value="{{old('country_home_add')}}"  name="country_home_add">
                            </div><div class="form-group col-md-6">
                                <label for="address_in_pakistan">Address In Pakistan</label>
                                <input type="text" class="form-control" id="address_in_pakistan" placeholder="" value="{{old('address_in_pakistan')}}"  name="address_in_pakistan">
                            </div>
                        </div>


                        <div class="form-group col-md-12">
                            <button id="form_submit" type="submit" class="btn btn-default">Save</button>
                            <a id="save_new" href="javascript:void(0)" class="btn btn-primary">Save & New</a>
                            <a href="{{url('human-resource')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
<script>
    $('#save_new').on('click', function (){
        $('input[name="submit_type"]').val('save_new');
        $('#form_submit').click();
    });
    $(".boxother").hide();
    $(".boxpakistan").hide();

    $('#nationality').on('change', function() {
      if(this.value=='pakistani'){
        $(".boxpakistan").show();
        $(".boxother").hide();
        $('.is_req').attr('required', 'true');

      }else{
        $(".boxother").show();
        $(".boxpakistan").hide();
        $('.is_req').removeAttr('required');

      }
    });

</script>
<script>
    // Disable form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {

            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        Swal.fire({
                            title: 'Error!',
                            text: 'Please fill all required fields and then submit.',
                            icon: 'error',
                            confirmButtonText: 'OK'
                        })
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
@endsection
