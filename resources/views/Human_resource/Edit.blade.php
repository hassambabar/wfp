@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br/>
                    @endif
                    <form action="{{route('human-resource.update',$Human_resource->id)}}" class="needs-validation"
                          novalidate method="POST" enctype="multipart/form-data">
                        @method('PATCH')

                        @csrf
                        {{--
                                                <div class="form-group col-md-6">
                                                    <label for="total_national_staff">Number of total National Staff</label>
                                                    <input type="text" class="form-control" id="total_national_staff" placeholder="Number of total National Staff" value="{{$Human_resource->total_national_staff}}" name="total_national_staff">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="total_frn_national_staff">Number of total Foreign National Staff</label>
                                                    <input type="text" class="form-control" id="total_frn_national_staff" placeholder="Number of total Foreign National Staff" value="{{$Human_resource->total_frn_national_staff}}" name="total_frn_national_staff">
                                                </div> --}}

                        <div class="form-group col-md-6">
                            <label for="name">Name *</label>
                            <input required type="text" class="form-control" id="name" placeholder="Name"
                                   value="{{$Human_resource->name}}" name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="nationality">Nationality</label>
                            <select name="nationality" id="nationality" class="form-control">
                                <option value="">Select</option>

                                <option value="pakistani"
                                        @if($Human_resource->nationality == 'pakistani') selected @endif >Pakistani
                                </option>
                                <option value="other" @if($Human_resource->nationality == 'other') selected @endif >
                                    Foreign National
                                </option>

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email address *</label>
                            <input required type="email" class="form-control" id="email" placeholder="example@gmail.com"
                                   value="{{$Human_resource->email}}" name="email">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="phone_no">Phone No *</label>
                            <input required type="text" class="form-control is_numeric" id="phone_no"
                                   placeholder="03330000000" value="{{$Human_resource->phone_no}}" name="phone_no">
                        </div>

                        <div class="boxpakistan">
                            <div class="form-group col-md-6">
                                <label for="father_name">Father Name</label>
                                <input type="text" class="form-control" id="father_name" placeholder="Father Name"
                                       value="{{$Human_resource->father_name}}" name="father_name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="cnic">CNIC *</label>
                                <input type="text" class="form-control is_numeric is_req" id="cnic" placeholder="CNIC"
                                       value="{{$Human_resource->cnic}}" name="cnic">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="address">Complete Address (Permanent)</label>
                                <input type="text" class="form-control" id="address" placeholder="address"
                                       value="{{$Human_resource->address}}" name="address">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="temp_address">Complete Address (Temporary)</label>
                                <input type="text" class="form-control" id="temp_address"
                                       placeholder="Complete Address (Temporary)"
                                       value="{{$Human_resource->temp_address}}" name="temp_address">
                            </div>

                        </div>

                        <div class="boxother">
                            <div class="form-group col-md-6">
                                <label for="passport">Passport</label>
                                <input type="file" class="form-control is_numeric" id="passport"
                                       value="{{$Human_resource->passport}}" name="passport">
                                @if($Human_resource->passport)
                                    <a target="_blank" href="{{ url($Human_resource->passport) }}">View File</a>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="country_home_add">Country Home address</label>
                                <input type="text" class="form-control" id="country_home_add" placeholder=""
                                       value="{{$Human_resource->country_home_add}}" name="country_home_add">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="address_in_pakistan">Address In Pakistan</label>
                                <input type="text" class="form-control" id="address_in_pakistan" placeholder=""
                                       value="{{$Human_resource->address_in_pakistan}}" name="address_in_pakistan">
                            </div>


                        </div>


                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('pagespecificscripts')
    @if($Human_resource->nationality == 'pakistani')
        <script>
            $(".boxother").hide();
            $(".boxpakistan").show();
        </script>
    @else
        <script>
            $(".boxother").show();
            $(".boxpakistan").hide();
        </script>
    @endif

    <script>

        $('#nationality').on('change', function () {
            if (this.value == 'pakistani') {
                $(".boxpakistan").show();
                $(".boxother").hide();
                $('.is_req').attr('required', 'true');

            } else {
                $(".boxother").show();
                $(".boxpakistan").hide();
                $('.is_req').removeAttr('required');

            }
        });
    </script>
    <script>
        // Disable form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {

                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                            Swal.fire({
                                title: 'Error!',
                                text: 'Please fill all required fields and then submit.',
                                icon: 'error',
                                confirmButtonText: 'OK'
                            })
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
@endsection
