@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Human Resource</h3>
                        </div>
                        @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                            <div class="col-md-6 text-right">
                                <a href="{{url("/human-resource/{$Human_resource->id}/edit")}}">Edit Details</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <p><b>Name:</b> {{$Human_resource->name}}</p>
                            </div>
                            <div class="col-md-6">
                                <p><b>Nationality:</b> {{ucfirst($Human_resource->nationality)}}</p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <p><b>Email address:</b> {{$Human_resource->email}}</p>
                            </div>
                            <div class="col-md-6">
                                <p><b>Phone No:</b> {{ucfirst($Human_resource->phone_no)}}</p>
                            </div>
                        </div>

                        <div class="col-md-12 {{$Human_resource->nationality == 'pakistani' ? 'show' : 'hide'}}">
                            <div class="col-md-6">
                                <p><b>Father Name:</b> {{$Human_resource->father_name}}</p>
                            </div>
                            <div class="col-md-6">
                                <p><b>CNIC:</b> {{$Human_resource->cnic}}</p>
                            </div>
                        </div>

                        <div class="col-md-12 {{$Human_resource->nationality == 'pakistani' ? 'show' : 'hide'}}">
                            <div class="col-md-6">
                                <p><b>Complete Address (Permanent):</b> {{$Human_resource->address}}</p>
                            </div>
                            <div class="col-md-6">
                                <p><b>Complete Address (Temporary):</b> {{$Human_resource->temp_address}}</p>
                            </div>
                        </div>

                        <div class="col-md-12 {{$Human_resource->nationality == 'Foreign National' ? 'show' : 'hide'}}">
                            <div class="col-md-6">
                                <p><b>Passport:</b>
                                    @if($Human_resource->passport !== null)
                                        <a target="_blank" href="{{ url($Human_resource->passport) }}">View File</a>
                                    @else
                                        {{'---'}}
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p><b>Country Home address:</b> {{$Human_resource->country_home_add}}</p>
                            </div>
                        </div>

                        <div class="col-md-12 {{$Human_resource->nationality == 'Foreign National' ? 'show' : 'hide'}}">
                            <div class="col-md-6">
                                <p><b>Address In Pakistan:</b> {{$Human_resource->address_in_pakistan}}</p>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <a href="{{url('/human-resource')}}" class="btn btn-default">Back</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection

