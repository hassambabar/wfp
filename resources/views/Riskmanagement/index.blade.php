@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="panel panel-default">
            <div class="panel-heading">
                Risk Management
            </div>
            <div class="panel-body">
                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
                @endif

                @if(isset($ngoProfile) && $ngoProfile == null)
                    <div>
                        You need to complete your profile first. Click <a href="{{url('/profile/create')}}">here</a> to fill in your details.
                    </div>
                @else
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Subject</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($Riskmanagement as $key => $item)
                            <tr class="odd gradeX">
                                <td>{{$item->title}}</td>
                                <td>{{$item->subject}}</td>
                                {{--<td><a href="{{ url("/risk-management/{$item->id}") }}">View</a> - <a href="{{ url("/human-resource/{$item->id}/delete") }}">Delete</a></td>--}}
                                <td>
                                    @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                                        <a href="{{ url("/risk-management/{$item->id}")}}">View</a>
                                    @endif
                                    @if($ngoProfile->user->status === 0 )
                                        - <a href="{{ url("/risk-management/{$item->id}/delete") }}">Delete</a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr class="gradeU">
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>

@endsection

@section('pagespecificscripts')
<script>
       $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
@endsection
