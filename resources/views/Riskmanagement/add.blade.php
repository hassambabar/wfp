@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{-- <div class="card-title">
                        <div class="title">Basic example</div>
                    </div> --}}
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form action="{{ route('risk-management.store') }}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group col-md-6">
                            <label for="title">Title *</label>
                            <input type="text" required class="form-control" id="title" placeholder="Title"
                                value="{{ old('title') }}" name="title">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="subject">Subject *</label>
                            <input type="text" required class="form-control" id="subject" placeholder="Subject"
                                value="{{ old('subject') }}" name="subject">
                        </div>
                            <div class="form-group col-md-6">
                                <label for="passport">Image</label>
                                <input type="file" id="image" class="form-control uploadFileInput" name="image[]" multiple>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="country_home_add">Document</label>
                                <input type="file" id="document" class="form-control uploadFileInput" name="document[]" multiple>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="description">Description *</label>
                                <textarea id="description"  rows="8" cols="50" required name="description" class="form-control">{{ old('description') }}</textarea>

                            </div>

                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-default">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
 <script>
    $( function() {
        var MAX_FILE_SIZE = 5 * 1024 * 1024; // 5MB
        $('.uploadFileInput').change(function() {
            console.log(' uploadFileInput change ', this.files);
            var fileSizeExceede = false;
            var fileExceedName = '';
            for (var i = 0; i <= this.files.length - 1; i++) {
                if(this.files[i].size > MAX_FILE_SIZE){
                    fileSizeExceede = true;
                    fileExceedName = this.files[i].name;
                }
            }
            if(fileSizeExceede){
                this.setCustomValidity("File "+fileExceedName+" must not exceed 5 MB!");
                this.reportValidity();
            }else{
                this.setCustomValidity("");
            }

        });

    });
</script>
 <script>
     // Disable form submissions if there are invalid fields
     (function() {
         'use strict';
         window.addEventListener('load', function() {

             // Get the forms we want to add validation styles to
             var forms = document.getElementsByClassName('needs-validation');
             // Loop over them and prevent submission
             var validation = Array.prototype.filter.call(forms, function(form) {
                 form.addEventListener('submit', function(event) {
                     if (form.checkValidity() === false) {
                         event.preventDefault();
                         event.stopPropagation();
                     }
                     form.classList.add('was-validated');
                 }, false);
             });
         }, false);
     })();
 </script>
@endsection
