@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                   
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                      
                      
                        <div class="col-md-12 needassesmentrow">
                            <strong>Title :</strong> {{$Riskmanagement->title}}  
                        </div>
                        <div class="col-md-12 needassesmentrow">
                            <strong>Subject :</strong> {{$Riskmanagement->subject}}  
                        </div>
                         <div class="col-md-12 needassesmentrow">
                            <strong>Images :</strong> 
                            @foreach ($image as $item)
                            <br><a href="{{ url('uploads/risk-management/' . $item) }}">{{$item}}</a>
                            @endforeach
                          
                        </div>

                        <div class="col-md-12 needassesmentrow">
                            <strong>Document :</strong> 
                            @foreach ($document as $item)
                            <br><a href="{{ url('uploads/risk-management/document/' . $item) }}">{{$item}}</a>
                            @endforeach
                          
                        </div>


                        {{-- <div class="col-md-12 needassesmentrow">
                            <strong>Quantity :</strong> {{$Riskmanagement->quantity}}  
                        </div>  --}}
                        <div class="col-md-12 needassesmentrow">
                            <strong>Description :</strong> {{$Riskmanagement->description}}  
                        </div>
                        

                      
                </div>
            </div>
        </div>
    </div>

@endsection


@section('pagespecificscripts')

@endsection