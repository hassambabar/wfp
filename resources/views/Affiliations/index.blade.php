@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="panel panel-default">
            <div class="panel-heading">
                Details of Memberships & Affiliations with Networks / Forums
            </div>
            <div class="panel-body">
                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif


                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name of Network or Forum</th>
                                <th>Joining Date</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($Affiliations as $key => $item)
                            <tr class="odd gradeX">
                                <td>{{$item->network->name}}</td>
                                <td>{{$item->joining_date}}</td>
                                <td>@if($item->status == 1) Active @else Inactive @endif </td>
                                <td>
                                    <a href="{{ url("/affiliations/detail/{$item->id}") }}">View</a>
                                    @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                                    <a href="{{ url("/affiliations/{$item->id}/edit") }}"> - Edit - </a>
                                    @endif

                                    @if($ngoProfile->user->status === 0 )
                                            - <a href="{{ url("/affiliations/{$item->id}/delete") }}">Delete</a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr class="gradeU">
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>

@endsection

@section('pagespecificscripts')
<script>
       $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
@endsection
