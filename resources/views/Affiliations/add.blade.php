@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="card-title">
                        <div class="title">Add Memberships & Affiliations</div>
                    </div>
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                        @if (session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    <form action="{{route('affiliations.store')}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="submit_type" value="" />
                        <div class="form-group col-md-6">
                            <label for="network_id">Name of Network or Forum *</label>
                            <select required id="network_id" name="network_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Network as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="joining_date">Joining Date *</label>
                            <input required type="text" class="form-control" id="joining_date" placeholder="" value="{{old('joining_date')}}" name="joining_date">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="joining_date">Membership status *</label>
                            <div class="form-check form-check-inline">
                                <input required class="form-check-input custom-control-input" type="radio" name="status" id="inlineRadio1" value="1">
                                <label class="form-check-label custom-control-label" for="inlineRadio1"><small>Active</small></label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input required class="form-check-input custom-control-input" type="radio" name="status" id="inlineRadio2" value="0">
                                <label class="form-check-label custom-control-label" for="inlineRadio2"><small>Inactive</small></label>
                              </div>

                        </div>

                        <div class="form-group col-md-12">
                            <button id="form_submit" type="submit" class="btn btn-default">Save</button>
                            <a id="save_new" href="javascript:void(0)" class="btn btn-primary">Save & New</a>
                            <a href="{{url('affiliations')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
<script>
    $( function() {
        $('#save_new').on('click', function (){
            $('input[name="submit_type"]').val('save_new');
            $('#form_submit').click();
        });
        $( "#joining_date" ).datepicker();
    });
</script>
<script>
    // Disable form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {

            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        Swal.fire({
                            title: 'Error!',
                            text: 'Please fill all required fields and then submit.',
                            icon: 'error',
                            confirmButtonText: 'OK',
                            // confirmButtonColor: '#a5dc86'
                        })
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
@endsection
