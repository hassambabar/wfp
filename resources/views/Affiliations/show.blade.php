@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-6">
                        <h3>Affiliation</h3>
                    </div>
                    @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                        <div class="col-md-6 text-right">
                            <a href="{{route('affiliations.edit', $Affiliations->id)}}">Edit Details</a>
                        </div>
                    @endif
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <p><b>Name of Network or Forum:</b> {{$Affiliations->network->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Joining Date:</b> {{$Affiliations->joining_date}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Membership status:</b> {{$Affiliations->status == '1' ? 'Active' : 'Inactive'}}</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <a href="{{url('/affiliations')}}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
