@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                    <form action="{{route('affiliations.update',$Affiliations->id)}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @method('PATCH')

                        @csrf


                        <div class="form-group col-md-6">
                            <label for="network_id">Name of Network or Forum *</label>
                            <select required name="network_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Network as $item)
                                <option value="{{$item->id}}" @if($Affiliations->network_id == $item->id)  selected  @endif>{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="joining_date">Joining Date *</label>
                            <input required type="text" class="form-control" id="joining_date" placeholder="" value="{{$Affiliations->joining_date}}" name="joining_date">
                        </div>


                        <div class="form-group col-md-12">
                            <label for="joining_date">Membership status *</label>
                            <div class="form-check form-check-inline">
                                <input required class="form-check-input custom-control-input" type="radio" name="status" id="inlineRadio1" value="1" @if($Affiliations->status == '1')  checked  @endif>
                                <label class="form-check-label custom-control-label" for="inlineRadio1"><small>Active</small></label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input required class="form-check-input custom-control-input" type="radio" name="status" id="inlineRadio2" value="0" @if($Affiliations->status == '0')  checked  @endif>
                                <label class="form-check-label custom-control-label" for="inlineRadio2"><small>Inactive</small></label>
                              </div>

                        </div>

                        <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
<script>


            $( function() {
    $( "#joining_date" ).datepicker();
  } );
    </script>
@endsection
