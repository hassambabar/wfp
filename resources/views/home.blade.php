@extends('layouts.app')

@section('content')
<div class="container auto-wisth">
    <div class="row justify-content-center">
        <div class="col-md-12 nopadding">
            @if(checkUserRole('NGO'))
            <div class="panel panel-default">
                {{-- <div class="panel-heading">
                    {{ __('Dashboard') }}
                </div> --}}
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                    @if(
                        (isset($ngoProfile) && $ngoProfile->count() > 0) && (isset($ngoMous) && $ngoMous > 0) && (isset($projects) && $projects > 0) &&
                        (isset($pastEmergencies) && $pastEmergencies > 0) && (isset($Human_resource) && $Human_resource > 0) && (isset($Human_resource) && $Human_resource > 0) &&
                        (isset($Volunteers) && $Volunteers > 0) && (isset($Stocks) && $Stocks > 0) && (isset($Affiliations) && $Affiliations > 0) &&
                        (isset($Ngo_contact) && $Ngo_contact > 0)
                    )
                        @if(\Auth::user()->status != -1 && \Auth::user()->status != 1)
                            <div class="alert alert-success">
                                <strong>Note: </strong>You've successfully completed all your enlisting details. Click on the button below to apply for profile approval.
                            </div>
                            <div class="col-md-12">
                                <a href="{{url('/profile/request-approval')}}" class="btn btn-primary">Request for Profile Approval</a>
                            </div>
                        @endif
                        @if(\Auth::user()->status == -1)
                            <div class="alert alert-warning">
                                <strong>Note: </strong>Your request for profile approval is pending.
                            </div>
                        @endif
                    @else
                        <div class="alert alert-info profile-notice">
                            <strong>Note: </strong> You can apply for enlistment by filling all required fields.
                        </div>
                    @endif
                    <div class="col-md-12 profile-steps">
                        <div class="col-md-6 col-xs-12">
                        @if(isset($ngoProfile) && $ngoProfile->count() > 0)
                            <div class="col-md-2">
                                <svg class="svg-success" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                    <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                    <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                                </svg>
                            </div>
                            <div class="col-md-8">
                                <p class="success">1. Organization Profile & Legal status</p>
                            </div>
                        @else
                            <div class="col-md-2">
                            <svg class="svg-error" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                <circle class="path circle" fill="none" stroke="#D06079" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                                <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                            </svg>
                            </div>
                            <div class="col-md-8">
                                <p class="error"><a href="{{url('/profile/create')}}">1. Organization Profile & Legal status</a></p>
                            </div>
                        @endif
                        </div>
                        <div class="col-md-6 col-xs-12">
                            @if(isset($Human_resource) && $Human_resource > 0)
                                <div class="col-md-2">
                                    <svg class="svg-success" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="success">2. Human Resources</p>
                                </div>
                            @else
                                <div class="col-md-2">
                                    <svg class="svg-error" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#D06079" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="error"><a href="{{url('/human-resource/create')}}">2. Human Resources</a></p>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12">
                            @if(isset($Volunteers) && $Volunteers > 0)
                                <div class="col-md-2">
                                    <svg class="svg-success" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="success">3. Volunteers</p>
                                </div>
                            @else
                                <div class="col-md-2">
                                    <svg class="svg-error" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#D06079" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="error"><a href="{{url('/volunteers/create')}}">3. Volunteers</a></p>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12">
                            @if(isset($projects) && $projects > 0)
                                <div class="col-md-2">
                                    <svg class="svg-success" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="success">4. Thematic Areas & Projects</p>
                                </div>
                            @else
                                <div class="col-md-2">
                                    <svg class="svg-error" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#D06079" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="error"><a href="{{url('/projects/create')}}">4. Thematic Areas & Projects</a></p>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12">
                            @if(isset($pastEmergencies) && $pastEmergencies > 0)
                                <div class="col-md-2">
                                    <svg class="svg-success" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="success">5. Emergencies Responded</p>
                                </div>
                            @else
                                <div class="col-md-2">
                                    <svg class="svg-error" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#D06079" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="error"><a href="{{url('/past-emergencies/create')}}">5. Emergencies Responded</a></p>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12">
                            @if(isset($Stocks) && $Stocks > 0)
                                <div class="col-md-2">
                                    <svg class="svg-success" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="success">6. Stocks</p>
                                </div>
                            @else
                                <div class="col-md-2">
                                    <svg class="svg-error" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#D06079" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="error"><a href="{{url('/stocks/create')}}">6. Stocks</a></p>
                                </div>
                            @endif
                        </div>

                        <div class="col-md-6 col-xs-12">
                        @if(isset($Affiliations) && $Affiliations > 0)
                            <div class="col-md-2">
                                <svg class="svg-success" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                    <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                    <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                                </svg>
                            </div>
                            <div class="col-md-8">
                                <p class="success">7. Memberships / Affiliations</p>
                            </div>
                        @else
                            <div class="col-md-2">
                                <svg class="svg-error" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                    <circle class="path circle" fill="none" stroke="#D06079" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                    <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                                    <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                                </svg>
                            </div>
                            <div class="col-md-8">
                                <p class="error"><a href="{{url('/affiliations/create')}}">7. Memberships / Affiliations</a></p>
                            </div>
                        @endif
                        </div>
                        <div class="col-md-6 col-xs-12">
                        @if(isset($Ngo_contact) && $Ngo_contact > 0)
                            <div class="col-md-2">
                                <svg class="svg-success" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                    <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                    <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                                </svg>
                            </div>
                            <div class="col-md-8">
                                <p class="success">8. NGO/Organization Contacts</p>
                            </div>
                        @else
                            <div class="col-md-2">
                                <svg class="svg-error" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                    <circle class="path circle" fill="none" stroke="#D06079" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                    <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                                    <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                                </svg>
                            </div>
                            <div class="col-md-8">
                                <p class="error"><a href="{{url('/ngocontact/create')}}">8. NGO/Organization Contacts</a></p>
                            </div>
                        @endif
                        </div>
                        <div class="col-md-6 col-xs-12">
                            @if(isset($ngoMous) && $ngoMous > 0)
                                <div class="col-md-2">
                                    <svg class="svg-success" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="success">9. Other MoUs</p>
                                </div>
                            @else
                                <div class="col-md-2">
                                    <svg class="svg-error" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                        <circle class="path circle" fill="none" stroke="#D06079" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                                        <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                                    </svg>
                                </div>
                                <div class="col-md-8">
                                    <p class="error"><a href="{{url('/mous/create')}}">9. Other MoUs</a></p>
                                </div>
                            @endif
                        </div>
                        {{--<div class="col-md-6 col-xs-12">
                        @if(isset($Contact_pdma) && $Contact_pdma > 0)
                            <div class="col-md-2">
                                <svg class="svg-success" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                    <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                    <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
                                </svg>
                            </div>
                            <div class="col-md-8">
                                <p class="success">Contacts from PDMA</p>
                            </div>
                        @else
                            <div class="col-md-2">
                                <svg class="svg-error" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                                    <circle class="path circle" fill="none" stroke="#D06079" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
                                    <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3"/>
                                    <line class="path line" fill="none" stroke="#D06079" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2"/>
                                </svg>
                            </div>
                            <div class="col-md-8">
                                <p class="error"><a href="{{url('/contactpdma/create')}}">Contacts from PDMA</a></p>
                            </div>
                        @endif
                        </div>--}}
                    </div>
                </div>

            </div>
            @endif
        </div>
    </div>
</div>
@endsection
