@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form action="{{route('needassesmentresponses')}}" method="POST" enctype="multipart/form-data">

                        @csrf

                        <div class="col-md-12 needassesmentrow">
                            <strong>Type of emergency :</strong> {{$EmergencyItem->emergency->type_of_emergency}}
                        </div>
                        <div class="col-md-12 needassesmentrow">
                            <strong>Type of hazard :</strong> {{$EmergencyItem->emergency->type_of_hazard}}
                        </div>
                        <div class="col-md-12 needassesmentrow">
                            <strong>District :</strong> {{$EmergencyItem->emergency->district->name}}
                        </div>
                        <div class="col-md-12 needassesmentrow">
                            <strong>Rescue items :</strong> {{$EmergencyItem->rescue_items}}
                        </div>
                        <div class="col-md-12 needassesmentrow">
                            <strong>Quantity :</strong> {{$EmergencyItem->quantity}}
                        </div>
                        <div class="col-md-12 needassesmentrow">
                            <strong>When needed :</strong> {{$EmergencyItem->when_needed}}
                        </div>
                        <div class="col-md-12 needassesmentrow">
                            <strong>Description :</strong> {!!$EmergencyItem->description !!}
                        </div>
                        @if($Needassesmentresponse)
                            <div class="col-md-12 needassesmentrow">
                                <strong>Response Quantity :</strong> {{$Needassesmentresponse->response_quantity}}
                            </div>
                            <div class="col-md-12 needassesmentrow">
                                <strong>Response :</strong> {!!$Needassesmentresponse->response !!}
                            </div>
                        @endif

                        @if(!$Needassesmentresponse)
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Response Quantity</label>
                                    <input class="form-control" name="response_quantity" type="number">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Response</label>
                                    <textarea class="form-control" name="response"></textarea>
                                </div>
                            </div>
                            <input type="hidden" name="emergency_id" value="{{$EmergencyItem->emergency_id}}" readonly >
                            <input type="hidden" name="emergency_item_id" value="{{$EmergencyItem->id}}" readonly >

                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-default">Send To PDMA/DDMA </button>
                            </div>

                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('pagespecificscripts')

@endsection
