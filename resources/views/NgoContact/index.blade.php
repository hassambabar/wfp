@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="panel panel-default">
            <div class="panel-heading">
                Addresses/Contacts & Contact Persons Detail
            </div>
            <div class="panel-body">
                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

                @if(isset($ngoProfile) && $ngoProfile == null)
                    <div>
                        You need to complete your profile first. Click <a href="{{url('/profile/create')}}">here</a> to fill in your details.
                    </div>
                @else

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Office</th>
                                <th>Contact number</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($Ngo_contact as $key => $item)
                            <tr class="odd gradeX">
                                <td>{{$item->contact_person_name}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->office}}</td>
                                <td>{{$item->cell}} - {{$item->landline}}</td>
                                <td>
                                    <a href="{{ url("/ngocontact/detail/{$item->id}") }}">View</a>
                                    @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                                    <a href="{{ url("/ngocontact/{$item->id}/edit") }}"> - Edit - </a>
                                    @endif
                                    @if($ngoProfile->user->status === 0 )
                                        - <a href="{{ url("/ngocontact/{$item->id}/delete") }}">Delete</a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr class="gradeU">
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                            @endforelse



                        </tbody>
                    </table>
                </div>

                @endif

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>

@endsection

@section('pagespecificscripts')
<script>
       $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
@endsection
