@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Contact Details</h3>
                        </div>
                        @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                            <div class="col-md-6 text-right">
                                <a href="{{route('ngocontact.edit', $Ngo_contact->id)}}">Edit Details</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <p><b>Contact Person Office Location:</b> {{$Ngo_contact->office ? $Ngo_contact->office : ' -- '}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Contact Person Name:</b> {{$Ngo_contact->contact_person_name ? $Ngo_contact->contact_person_name : ' -- '}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Designation/Title:</b> {{$Ngo_contact->designation ? $Ngo_contact->designation : ' -- '}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Province:</b> {{$Ngo_contact->getprovinceTitle() ? $Ngo_contact->getprovinceTitle() : ' -- '}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>District:</b> {{$Ngo_contact->getdistrictTitle() ? $Ngo_contact->getdistrictTitle() : ' -- '}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Mailing Address:</b> {{$Ngo_contact->mailing_address ? $Ngo_contact->mailing_address : ' -- '}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Phone - Landline:</b> {{$Ngo_contact->landline ? $Ngo_contact->landline : ' --'}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Phone - Cell:</b> {{$Ngo_contact->cell ? $Ngo_contact->cell : ' -- '}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Fax:</b> {{$Ngo_contact->fax ? $Ngo_contact->fax : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Email:</b> {{$Ngo_contact->email ? $Ngo_contact->email : ' -- '}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Website:</b> {{$Ngo_contact->website ? $Ngo_contact->website : ' -- '}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Facebook:</b> {{$Ngo_contact->facebook ? $Ngo_contact->facebook : ' -- '}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Can PDMA contact this person:</b> {{$Ngo_contact->can_pdma_contact == '1' ? 'Yes' : 'No'}}</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <a href="{{url('/ngocontact')}}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
