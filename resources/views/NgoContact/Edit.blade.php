@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                    <form action="{{route('ngocontact.update',$Ngo_contact->id)}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @method('PATCH')

                        @csrf


                        <div class="form-group col-md-6">
                            <label for="office">Contact Person Office Location *</label>
                            <select required name="office" class="form-control">
                                <option value="">Select</option>


                                <option value="Head Office"  @if($Ngo_contact->office == 'Head Office')  selected  @endif >Main Office / Head Office</option>
                                <option value="Sindh Office"  @if($Ngo_contact->office == 'Sindh Office')  selected  @endif >Sindh Province Main Office</option>


                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="contact_person_name">Contact Person Name *</label>
                            <input required type="text" class="form-control" id="contact_person_name" placeholder="" value="{{$Ngo_contact->contact_person_name}}" name="contact_person_name">

                        </div>

                        <div class="form-group col-md-6">
                            <label for="designation">Designation/Title</label>
                            <input type="text" class="form-control" id="designation" placeholder="" value="{{$Ngo_contact->designation}}" name="designation">

                        </div>

                        <div class="form-group col-md-6">
                            <label for="province_id">Province</label>
                            <select name="province_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Province as $item)
                                <option value="{{$item->id}}"  @if($Ngo_contact->province_id == $item->id)  selected  @endif >{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="district_id">District</label>
                            <select name="district_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($District as $item)
                                <option value="{{$item->id}}" @if($Ngo_contact->district_id == $item->id)  selected  @endif >{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>



                        <div class="form-group col-md-6">
                            <label for="mailing_address">Mailing Address</label>
                            <input type="text" class="form-control" id="mailing_address" placeholder="" value="{{$Ngo_contact->mailing_address}}" name="mailing_address">

                        </div>

                        <div class="form-group col-md-6">
                            <label for="landline">Phone - Landline</label>
                            <input type="text" class="form-control is_numeric" id="landline" placeholder="" value="{{$Ngo_contact->landline}}" name="landline">

                        </div>

                        <div class="form-group col-md-6">
                            <label for="cell">Phone - Cell *</label>
                            <input required type="text" class="form-control is_numeric" id="cell" placeholder="" value="{{$Ngo_contact->cell}}" name="cell">

                        </div>

                        <div class="form-group col-md-6">
                            <label for="fax">Fax</label>
                            <input type="text" class="form-control is_numeric" id="fax" placeholder="" value="{{$Ngo_contact->fax}}" name="fax">

                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email *</label>
                            <input required type="email" class="form-control" id="email" placeholder="" value="{{$Ngo_contact->email}}" name="email">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="website">Website</label>
                            <input type="text" class="form-control" id="website" placeholder="" value="{{$Ngo_contact->website}}" name="website">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="facebook">Facebook</label>
                            <input type="text" class="form-control" id="facebook" placeholder="" value="{{$Ngo_contact->facebook}}" name="facebook">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="joining_date">Can PDMA contact this person *</label>
                            <div class="form-check form-check-inline">
                                <input required class="form-check-input custom-control-input" type="radio" name="can_pdma_contact" id="inlineRadio1" value="1" @if($Ngo_contact->can_pdma_contact == '1')  checked  @endif>
                                <label class="form-check-label custom-control-label" for="inlineRadio1">YES</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input required class="form-check-input custom-control-input" type="radio" name="can_pdma_contact" id="inlineRadio2" value="0" @if($Ngo_contact->can_pdma_contact == '0')  checked  @endif>
                                <label class="form-check-label custom-control-label" for="inlineRadio2">NO</label>
                              </div>

                        </div>


                        <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
<script>


            $( function() {
    $( "#joining_date" ).datepicker();
  } );
    </script>
<script>
    // Disable form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {

            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
@endsection
