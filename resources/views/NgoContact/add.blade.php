@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="card-title">
                        <div class="title">Contact Persons</div>
                    </div>
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                        @if (session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    <form action="{{route('ngocontact.store')}}" class="needs-validation" novalidate  method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="submit_type" value="" />

                        <div class="form-group col-md-6">
                            <label for="office">Contact Person Office Location *</label>
                            <select required name="office" class="form-control">
                                <option value="">Select</option>
                                <option value="Head Office">Main Office / Head Office</option>
                                <option value="Sindh Office">Sindh Province Main Office</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="contact_person_name">Contact Person Name *</label>
                            <input required type="text" class="form-control" id="contact_person_name" placeholder="" value="{{old('contact_person_name')}}" name="contact_person_name">

                        </div>

                        <div class="form-group col-md-6">
                            <label for="designation">Designation/Title</label>
                            <input type="text" class="form-control" id="designation" placeholder="" value="{{old('designation')}}" name="designation">

                        </div>


                        <div class="form-group col-md-6">
                            <label for="province_id">Province</label>
                            <select name="province_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Province as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="district_id">District</label>
                            <select name="district_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($District as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>

                        {{-- <div class="form-group col-md-6">
                            <label for="tehsil_id">Tehsil</label>
                            <select name="tehsil_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Tehsil as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div> --}}


                        <div class="form-group col-md-6">
                            <label for="mailing_address">Mailing Address</label>
                            <input type="text" class="form-control" id="mailing_address" placeholder="" value="{{old('mailing_address')}}" name="mailing_address">

                        </div>

                        <div class="form-group col-md-6">
                            <label for="landline">Phone - Landline</label>
                            <input type="text" class="form-control is_numeric" id="landline" placeholder="" value="{{old('landline')}}" name="landline">

                        </div>

                        <div class="form-group col-md-6">
                            <label for="cell">Phone - Cell *</label>
                            <input required type="text" class="form-control is_numeric" id="cell" placeholder="" value="{{old('cell')}}" name="cell">

                        </div>

                        <div class="form-group col-md-6">
                            <label for="fax">Fax</label>
                            <input type="text" class="form-control is_numeric" id="fax" placeholder="" value="{{old('fax')}}" name="fax">

                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email *</label>
                            <input required type="email" class="form-control" id="email" placeholder="" value="{{old('email')}}" name="email">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="website">Website</label>
                            <input type="text" class="form-control" id="website" placeholder="" value="{{old('website')}}" name="website">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="facebook">Facebook</label>
                            <input type="text" class="form-control" id="facebook" placeholder="" value="{{old('facebook')}}" name="facebook">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="">Can PDMA contact this person *</label>
                            <div class="form-check form-check-inline">
                                <input required class="form-check-input custom-control-input" type="radio" name="can_pdma_contact" id="inlineRadio1" value="1">
                                <label class="form-check-label custom-control-label" for="inlineRadio1">YES</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input required class="form-check-input custom-control-input" type="radio" name="can_pdma_contact" id="inlineRadio2" value="0">
                                <label class="form-check-label custom-control-label" for="inlineRadio2">NO</label>
                              </div>

                        </div>

                         <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                        <div class="form-group col-md-12">
                            <button id="form_submit" type="submit" class="btn btn-default">Save</button>
                            <a id="save_new" href="javascript:void(0)" class="btn btn-primary">Save & New</a>
                            <a href="{{url('ngocontact')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
<script>
    $( function() {
        $('#save_new').on('click', function (){
            $('input[name="submit_type"]').val('save_new');
            $('#form_submit').click();
        });
        ( "#joining_date" ).datepicker();
    });
</script>
<script>
    // Disable form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {

            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        Swal.fire({
                            title: 'Error!',
                            text: 'Please fill all required fields and then submit.',
                            icon: 'error',
                            confirmButtonText: 'OK'
                        })
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
@endsection
