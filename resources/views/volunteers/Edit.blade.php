@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br/>
                    @endif
                    <form action="{{route('volunteers.update',$volunteers->id)}}" method="POST" class="needs-validation"
                          novalidate enctype="multipart/form-data">
                        @method('PATCH')

                        @csrf
                        <div class="form-group col-md-6">
                            <label for="f_name">First Name *</label>
                            <input type="text" class="form-control" required id="f_name" placeholder="First Name"
                                   value="{{$volunteers->f_name}}" name="f_name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="l_name">Last Name *</label>
                            <input type="text" class="form-control" required id="l_name" placeholder="Last Name"
                                   value="{{$volunteers->l_name}}" name="l_name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">CNIC *</label>
                            <input type="text" class="form-control is_numeric" required id="cnic" placeholder="CNIC"
                                   value="{{$volunteers->cnic}}" name="cnic">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="age">Age</label>
                            <input type="text" class="form-control is_numeric" id="age" placeholder="Age"
                                   value="{{$volunteers->age}}" name="age">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email address *</label>
                            <input type="email" class="form-control" id="email" required placeholder="Enter email"
                                   value="{{$volunteers->email}}" name="email">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="gender">Gender</label>
                            <select name="gender" class="form-control">
                                <option value="male" @if($volunteers->gender == 'male')  selected @endif>Male</option>
                                <option value="female" @if($volunteers->gender == 'female' )  selected @endif >Female
                                </option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="status">Status *</label>
                            <select name="status" class="form-control" required>
                                <option value="0" @if($volunteers->status == 0)  selected @endif>Inactive</option>
                                <option value="1" @if($volunteers->status == 1)  selected @endif>Active</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="district_id">District *</label>
                            <select required name="district_id" id="district_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($District as $item)
                                    <option value="{{$item->id}}"
                                            @if($volunteers->district_id == $item->id)  selected @endif>{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="tehsil_id">Tehsil</label>
                            <select name="tehsil_id" id="tehsil_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Tehsil as $item)
                                    <option value="{{$item->id}}"
                                            @if($volunteers->tehsil_id == $item->id)  selected @endif >{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="uc_id">UC</label>
                            <select name="uc_id" id="uc_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Uc as $item)
                                    <option value="{{$item->id}}"
                                            @if($volunteers->uc_id == $item->id)  selected @endif >{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="address">Education</label>
                            <input type="text" class="form-control" id="education" placeholder="education"
                                   value="{{$volunteers->education}}" name="education">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="occupation">Occupation</label>
                            <input type="text" class="form-control" id="occupation" placeholder="occupation"
                                   value="{{$volunteers->occupation}}" name="occupation">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="contact_no">Contact number *</label>
                            <input type="text" class="form-control is_numeric" required id="contact_no"
                                   placeholder="Contact number" value="{{$volunteers->contact_no}}" name="contact_no">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="whats_app">WhatsApp number</label>
                            <input type="text" class="form-control is_numeric" id="whats_app" placeholder="Whatsapp"
                                   value="{{$volunteers->whats_app}}" name="whats_app">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address" placeholder="Address"
                                   value="{{$volunteers->address}}" name="address">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="name_of_ngo">Name of <small>NGO/INGO/UN/Govt Dept for whom you have been
                                    enrolled as volunteer</small></label>
                            <input type="text" class="form-control" id="name_of_ngo" placeholder="name of ngo"
                                   value="{{$volunteers->name_of_ngo}}" name="name_of_ngo">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="address_org">Organization Address</label>
                            <input type="text" class="form-control" id="address_org" placeholder="Organization Address"
                                   value="{{$volunteers->address_org}}" name="address_org">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="email_of_org">Email of org</label>
                            <input type="text" class="form-control" id="email_of_org" placeholder="Email of org"
                                   value="{{$volunteers->email_of_org}}" name="email_of_org">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="contact_of_org_focal_person">Contact # of Org. Focal person</label>
                            <input type="text" class="form-control is_numeric" id="contact_of_org_focal_person"
                                   placeholder="Contact # of Org. Focal person"
                                   value="{{$volunteers->contact_of_org_focal_person}}"
                                   name="contact_of_org_focal_person">
                        </div>


                        <div class="form-group col-md-6">
                            <label for="availability">Availability</label>
                            <input type="text" class="form-control datepicker" placeholder=""
                                   value="{{$volunteers->availability}}" name="availability">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="availability_end_date">Availability End Date</label>
                            <input type="text" class="form-control datepicker" placeholder="" value="{{old('availability_end_date',$volunteers->availability_end_date)}}"  name="availability_end_date">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="area_of_expertise">Area Of Expertise</label>
                            <input type="text" class="form-control" id="area_of_expertise"
                                   placeholder="Area Of Expertise" value="{{$volunteers->area_of_expertise}}"
                                   name="area_of_expertise">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="physical_limitation">Physical Limitation</label>
                            <input type="text" class="form-control" id="physical_limitation"
                                   placeholder="Physical Limitation" value="{{$volunteers->physical_limitation}}"
                                   name="physical_limitation">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="image">Image</label>
                            <input type="file" id="image" class="form-control" name="image">
                            @if($volunteers->image)
                                <a target="_blank" href="{{ url($volunteers->image) }}">View File</a>
                            @endif
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
            <?php
            if($volunteers->district_id){
            ?>
                // district id
                var id = $('#district_id').val();

                // AJAX request
                $.ajax({
                    url: '/getTehsils/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response != null){
                            len = response.length;
                        }
                        if(len > 0){
                            // Empty the dropdown
                            $('#tehsil_id').find('option').not(':first').remove();
                            $('#uc_id').find('option').not(':first').remove();
                            // Read data and create <option >
                            let current_tehsil = <?php echo $volunteers->tehsil_id; ?>;
                            for(var i=0; i<len; i++){
                                var id = response[i].id;
                                var name = response[i].name;
                                if(id == current_tehsil){
                                    var option = "<option selected value='"+id+"'>"+name+"</option>";
                                }else{
                                    var option = "<option value='"+id+"'>"+name+"</option>";
                                }
                                $("#tehsil_id").append(option);
                            }
                        }
                    }
                });

            <?php } ?>

            <?php
            if($volunteers->tehsil_id){
            ?>
            // district id
            var tehsil_id = $('#tehsil_id').val();
            // Empty the dropdown
            $('#uc_id').find('option').not(':first').remove();
            // AJAX request
            $.ajax({
                url: '/getUcs/'+tehsil_id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    var len = 0;
                    if(response != null){
                        len = response.length;
                    }
                    if(len > 0){
                        let current_uc = <?php echo $volunteers->uc_id; ?>;
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response[i].id;
                            var name = response[i].name;
                            if(id == current_uc){
                                var option = "<option selected value='"+id+"'>"+name+"</option>";
                            }else {
                                var option = "<option value='"+id+"'>"+name+"</option>";
                            }

                            $("#uc_id").append(option);
                        }
                    }
                }
            });

            <?php } ?>

            $('#district_id').change(function(){
                // district id
                var id = $(this).val();
                // Empty the dropdown
                $('#tehsil_id').find('option').not(':first').remove();
                $('#uc_id').find('option').not(':first').remove();
                // AJAX request
                $.ajax({
                    url: '/getTehsils/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response != null){
                            len = response.length;
                        }
                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++){
                                var id = response[i].id;
                                var name = response[i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#tehsil_id").append(option);
                            }
                        }
                    }
                });
            });

            $('#tehsil_id').change(function(){
                // tehsil id
                var id = $(this).val();
                // Empty the dropdown
                $('#uc_id').find('option').not(':first').remove();
                // AJAX request
                $.ajax({
                    url: '/getUcs/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response != null){
                            len = response.length;
                        }
                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++){
                                var id = response[i].id;
                                var name = response[i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#uc_id").append(option);
                            }
                        }
                    }
                });
            });
        });

        $(function () {
            $(".datepicker").datepicker();
        });
    </script>
    <script>
        // Disable form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {

                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                            Swal.fire({
                                title: 'Error!',
                                text: 'Please fill all required fields and then submit.',
                                icon: 'error',
                                confirmButtonText: 'OK'
                            })
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
@endsection
