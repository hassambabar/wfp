@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-6">
                        <h3>Volunteer</h3>
                    </div>
                    @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                        <div class="col-md-6 text-right">
                            <a href="{{route('volunteers.edit', $volunteers->id)}}">Edit Details</a>
                        </div>
                    @endif
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <p><b>First Name:</b> {{$volunteers->f_name}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Last Name:</b> {{$volunteers->l_name}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>CNIC:</b> {{$volunteers->cnic}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Age:</b> {{$volunteers->age}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Email address:</b> {{$volunteers->email}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Gender:</b> {{ucfirst($volunteers->gender)}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Status:</b> {{($volunteers->status == 0) ? 'Inactive' : 'Active'}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>District:</b> {{$volunteers->district->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Tehsil:</b> {{$volunteers->tehsil->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>UC:</b> {{$volunteers->Uc->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Education:</b> {{$volunteers->education}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Occupation:</b> {{$volunteers->occupation}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Contact number:</b> {{$volunteers->contact_no}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>WhatsApp number:</b> {{$volunteers->whats_app}}</p>
                        </div>

                        <div class="col-md-6">
                            <p><b>Address:</b> {{$volunteers->address}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Name of <small>NGO/INGO/UN/Govt Dept for whom you have been
                                        enrolled as volunteer:</b> {{$volunteers->name_of_ngo}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Organization Address:</b> {{$volunteers->address_org}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Email of org:</b> {{$volunteers->email_of_org}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Contact # of Org. Focal person:</b> {{$volunteers->contact_of_org_focal_person}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Availability:</b> {{$volunteers->availability}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Availability End Date:</b> {{$volunteers->availability_end_date}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Area Of Expertise:</b> {{$volunteers->area_of_expertise}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Physical Limitation:</b> {{$volunteers->physical_limitation}}</p>
                        </div>
                        @if($volunteers->image)
                            <div class="col-md-6">
                                <p><b>Image:</b>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#volunteersimage">View File</button>
                                </p>
                            </div>
                            <div class="modal fade" id="volunteersimage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <img style="width: 100%;" src="{{ url($volunteers->image) }}" />
                                    </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <a href="{{url('/volunteers')}}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
