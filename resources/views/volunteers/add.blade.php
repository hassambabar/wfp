@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{-- <div class="card-title">
                        <div class="title">Basic example</div>
                    </div> --}}
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                    <form action="{{route('volunteers.store')}}" method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
                        @csrf
                            <input type="hidden" name="submit_type" value="" />
                            <div class="form-group col-md-6">
                            <label for="f_name">First Name *</label>
                            <input type="text" class="form-control" id="f_name" required placeholder="First Name" value="{{old('f_name')}}" name="f_name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="l_name">Last Name *</label>
                            <input type="text" class="form-control" id="l_name" required placeholder="Last Name" value="{{old('l_name')}}"  name="l_name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">CNIC *</label>
                            <input type="text" class="form-control is_numeric" id="cnic" required placeholder="CNIC" value="{{old('cnic')}}"  name="cnic">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="age">Age</label>
                            <input type="text" class="form-control is_numeric" id="age" placeholder="Age" value="{{old('age')}}"  name="age">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email address *</label>
                            <input type="email" class="form-control" id="email" required placeholder="Enter email" value="{{old('email')}}"  name="email">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="gender">Gender</label>
                            <select name="gender" class="form-control">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="status">Status*</label>
                            <select required name="status" class="form-control">
                                <option value="0">Inactive</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="district_id">District *</label>
                            <select name="district_id" id="district_id" class="form-control" required>
                                <option value="">Select</option>
                                @foreach ($District as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="tehsil_id">Tehsil</label>
                            <select name="tehsil_id" id="tehsil_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Tehsil as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="uc_id">UC</label>
                            <select name="uc_id" id="uc_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Uc as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="address">Education</label>
                            <input type="text" class="form-control" id="education" placeholder="education" value="{{old('education')}}"  name="education">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="occupation">Occupation</label>
                            <input type="text" class="form-control" id="occupation" placeholder="occupation" value="{{old('occupation')}}"  name="occupation">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="contact_no">Contact number *</label>
                            <input type="text" class="form-control is_numeric" required id="contact_no" placeholder="Contact number" value="{{old('contact_no')}}"  name="contact_no" >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="whats_app">WhatsApp number</label>
                            <input type="text" class="form-control is_numeric" id="whats_app" placeholder="Whatsapp" value="{{old('whats_app')}}"  name="whats_app">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address" placeholder="Address" value="{{old('address')}}"  name="address">
                        </div>



                        <div class="form-group col-md-6">
                            <label for="name_of_ngo">Name of <small>NGO/INGO/UN/Govt Dept for whom you have been enrolled as volunteer</small></label>
                            <input type="text" class="form-control" id="name_of_ngo" placeholder="name of ngo" value="{{old('name_of_ngo')}}"  name="name_of_ngo">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="address_org">Organization Address</label>
                            <input type="text" class="form-control" id="address_org" placeholder="Organization Address" value="{{old('address_org')}}"  name="address_org">
                        </div>


                        <div class="form-group col-md-6">
                            <label for="email_of_org">Email of org</label>
                            <input type="text" class="form-control" id="email_of_org" placeholder="Email of org" value="{{old('email_of_org')}}"  name="email_of_org">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="contact_of_org_focal_person">Contact # of Org. Focal person</label>
                            <input type="text" class="form-control is_numeric" id="contact_of_org_focal_person" placeholder="Contact # of Org. Focal person" value="{{old('contact_of_org_focal_person')}}"  name="contact_of_org_focal_person">
                        </div>



                        <div class="form-group col-md-6">
                            <label for="availability">Availability</label>
                            <input type="text" class="form-control datepicker" placeholder="" value="{{old('availability')}}"  name="availability">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="availability">Availability End Date</label>
                            <input type="text" class="form-control datepicker" placeholder="" value="{{old('availability_end_date')}}"  name="availability_end_date">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="area_of_expertise">Area Of Expertise</label>
                            <input type="text" class="form-control" id="area_of_expertise" placeholder="Area Of Expertise" value="{{old('area_of_expertise')}}"  name="area_of_expertise">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="physical_limitation">Physical Limitation</label>
                            <input type="text" class="form-control" id="physical_limitation" placeholder="Physical Limitation" value="{{old('physical_limitation')}}"  name="physical_limitation">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="image">Image</label>
                            <input type="file" id="image" class="form-control uploadFileInput" name="image">
                        </div>
                        <div class="form-group col-md-12">
                        <button id="form_submit" type="submit" class="btn btn-default">Submit</button>
                            <a id="save_new" href="javascript:void(0)" class="btn btn-primary">Save & New</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
<script>
       $(document).ready(function () {
           $('#dataTables-example').dataTable();

            var MAX_FILE_SIZE = 5 * 1024 * 1024; // 5MB
            $('.uploadFileInput').change(function() {
                console.log(' uploadFileInput change ', this.files);
                if(this.files.length && this.files[0].size > MAX_FILE_SIZE){
                    this.setCustomValidity("File must not exceed 5 MB!");
                    this.reportValidity();
                    // this.value = this.defaultValue;
                }else {
                    this.setCustomValidity("");
                }
            });

           $('#district_id').change(function(){
               // district id
               var id = $(this).val();
               // Empty the dropdown
               $('#tehsil_id').find('option').not(':first').remove();
               $('#uc_id').find('option').not(':first').remove();
               // AJAX request
               $.ajax({
                   url: '/getTehsils/'+id,
                   type: 'get',
                   dataType: 'json',
                   success: function(response){
                       var len = 0;
                       if(response != null){
                           len = response.length;
                       }
                       if(len > 0){
                           // Read data and create <option >
                           for(var i=0; i<len; i++){
                               var id = response[i].id;
                               var name = response[i].name;
                               var option = "<option value='"+id+"'>"+name+"</option>";
                               $("#tehsil_id").append(option);
                           }
                       }
                   }
               });
           });

           $('#tehsil_id').change(function(){
               // tehsil id
               var id = $(this).val();
               // Empty the dropdown
               $('#uc_id').find('option').not(':first').remove();
               // AJAX request
               $.ajax({
                   url: '/getUcs/'+id,
                   type: 'get',
                   dataType: 'json',
                   success: function(response){
                       var len = 0;
                       if(response != null){
                           len = response.length;
                       }
                       if(len > 0){
                           // Read data and create <option >
                           for(var i=0; i<len; i++){
                               var id = response[i].id;
                               var name = response[i].name;
                               var option = "<option value='"+id+"'>"+name+"</option>";
                               $("#uc_id").append(option);
                           }
                       }
                   }
               });
           });

       });
       $('#save_new').on('click', function (){
            $('input[name="submit_type"]').val('save_new');
            $('#form_submit').click();
        });
       $( function() {
        $( ".datepicker" ).datepicker();
       });
    </script>
<script>
    // Disable form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {

            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        Swal.fire({
                            title: 'Error!',
                            text: 'Please fill all required fields and then submit.',
                            icon: 'error',
                            confirmButtonText: 'OK'
                        })
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
@endsection
