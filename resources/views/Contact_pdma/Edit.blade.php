@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                    <form action="{{route('contacpdma.update',$Contact_pdma->id)}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @method('PATCH')

                        @csrf

                    <div class="form-group col-md-6">
                        <label for="cell_1">Cell 1 *</label>
                        <input required type="text" class="form-control is_numeric" id="cell_1" placeholder="Cell 1" value="{{$Contact_pdma->cell_1}}" name="cell_1">
                </div>
                <div class="form-group col-md-6">
                    <label for="cell_2">Cell 2</label>
                    <input type="text" class="form-control is_numeric" id="cell_2" placeholder="Cell 2" value="{{$Contact_pdma->cell_2}}" name="cell_2">
                </div>

                    <div class="form-group col-md-6">
                        <label for="email_1">Email 1 *</label>
                        <input required type="email" class="form-control" id="email_1" placeholder="example@gmail.com" value="{{$Contact_pdma->email_1}}"  name="email_1">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email_2">Email 2</label>
                        <input type="email" class="form-control" id="email_2" placeholder="example@gmail.com" value="{{$Contact_pdma->email_2}}"  name="email_2">
                    </div>



                        <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('pagespecificscripts')
    <script>
        // Disable form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {

                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
@endsection
