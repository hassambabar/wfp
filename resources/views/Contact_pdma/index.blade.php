@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="panel panel-default">
            <div class="panel-heading">
                Contact For PDMA
            </div>
            <div class="panel-body">
                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif


                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Cell 1</th>
                                <th>Cell 2</th>
                                <th>Email 1</th>
                                <th>Email 2</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($Contact_pdma as $key => $item)
                            <tr class="odd gradeX">
                                <td>{{$item->cell_1}}</td>
                                <td>{{$item->cell_2}}</td>
                                <td>{{$item->email_1}}</td>
                                <td>{{$item->email_2}}</td>
                                <td>
                                    @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                                    <a href="{{ url("/contacpdma/{$item->id}/edit") }}">Edit</a>
                                    @endif

                                    @if($ngoProfile->user->status === 0 )
                                        - <a href="{{ url("/contacpdma/{$item->id}/delete") }}">Delete</a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr class="gradeU">
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>

@endsection

@section('pagespecificscripts')
<script>
       $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
@endsection
