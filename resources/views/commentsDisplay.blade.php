@if($comments)
    @foreach($comments as $comment)
        <div class="display-comment" @if($comment->parent_id != null) style="margin-left:40px;" @endif>
            <div class="commentDetailBox">
            <strong>{{ $comment->user->name }}</strong>
            <p>{!! $comment->body !!} <br>   {{ $comment->created_at }}</p>
            <a href="javascript:void(0)" class="replyform noprint"  data-id="reply_{{$comment->id}}"  id="reply_{{$comment->id}}">Reply</a>
            <form method="post" action="{{ route('comments.store') }}"  class="noprint formReply reply_{{$comment->id}}" style="display: none" >
                @csrf

                <div class="form-group ">
                    <input type="text" name="body" class="form-control textareasummernote " />

                    <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                    <input type="hidden" name="type" value="{{ $comment->type }}" />
                    <input type="hidden" name="parent_id" value="{{ $comment->id }}" />

                </div>
                <div class="form-group noprint">
                    <input type="submit" class="btn btn-warning" value="Reply" />
                </div>
            </form>
            @include('commentsDisplay', ['comments' => $comment->replies])
        </div>
    </div>

    @endforeach
@endif
