@extends('layouts.auth')

@section('content')
<div class="ngo_login">
<div class="mb-4">
    <h3 style="text-align: center;"><strong>Web-based System for Coordination, Progress Sharing, Monitoring and mapping for PDMA </strong></h3>
  </div>
               
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group">
            <label for="email" >Email <span class="required">*</span></label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="password" >Password <span class="required">*</span></label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="d-flex mb-5 align-items-center">
            <label class="control control--checkbox mb-0"><span class="caption">Remember me</span>
                <input type="checkbox" checked="checked" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                <div class="control__indicator"></div>
            </label>
            <span class="ml-auto"><a href="{{ route('password.request') }}" class="forgot-pass">Forgot Password</a></span> 
            </div>
            <input type="submit" value="Log In" class="btn btn-pill text-white btn-block btn-primary">
    </form>
    <div class="row">
        <div class="col-md-12" style="padding-top:20px;text-align: center;">
            <a href="{{ url('register') }}" >Click Here to Register </a>
        </div>
    </div>
@endsection
</div>