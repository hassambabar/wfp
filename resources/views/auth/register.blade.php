@extends('layouts.auth')

@section('content')
<div class="ngo_login">
<div class="mb-4">
    <h3 style="text-align: center;"><strong>Register To Web-based System for Coordination, Progress Sharing, Monitoring and mapping for PDMA </strong></h3>
  </div>
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="form-group">
            <label for="name" >{{ __('Name') }}<span class="required">*</span></label>
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="email" >{{ __('E-Mail Address') }}<span class="required">*</span></label>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="password" >{{ __('Password') }}<span class="required">*</span></label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="password-confirm" >{{ __('Confirm Password') }}<span class="required">*</span></label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-12 submit-registerbutton">
                <button type="submit" class="btn btn-primary">
                    {{ __('Register') }}
                </button>
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-md-12" style="padding-top:20px;text-align: center;">
            <a href="{{ url('login') }}" >Click Here to Login </a>
        </div>
    </div>
</div>    
@endsection