@if ($crud->hasAccess('create') && request()->get('emergency_id') != null)
    <a href="javascript:void(0)" onclick="sendEmgNotification(this)" data-route="{{ url($crud->route.'/send-emg-notification') }}" class="btn btn-primary" data-button-type="send-notification" data-emg-id="{{request()->get('emergency_id')}}">
        <span class="ladda-label"><i class="la la-send"></i> Send Notification</span>
    </a>
@endif

@push('after_scripts')
    <script>
        if (typeof sendEmgNotification != 'function') {
            $("[data-button-type=send-notification]").unbind('click');

            function sendEmgNotification(button) {
                // ask for confirmation before deleting an item
                // e.preventDefault();
                var button = $(button);
                var route = button.attr('data-route');
                var emgID = button.attr('data-emg-id');

                $.ajax({
                    url: route,
                    type: 'POST',
                    data: { emgID: emgID },
                    success: function(result) {
                        // Show an alert with the result
                        console.log(result,route);
                        new Noty({
                            text: "Notification sent successfully.",
                            type: "success"
                        }).show();

                        // Hide the modal, if any
                        $('.modal').modal('hide');

                        //crud.table.ajax.reload();
                    },
                    error: function(result) {
                        // Show an alert with the result
                        new Noty({
                            text: "Notification not sent. Please try again.",
                            type: "warning"
                        }).show();
                    }
                });
            }
        }
    </script>
@endpush
