@php
$entity = request()->get('entity');
@endphp
    <a href="javascript:void(0)" onclick="showSummaryReport(this)" data-route="{{ url($crud->route.'/show-report-summary/'.$entry->getKey()) }}" class="btn btn-sm btn-link" data-button-type="show-report-summary">
        <span class="ladda-label"><i class="las la-book-open"></i> Show Report Summary</span>
    </a>

    <script>
        if (typeof showSummaryReport != 'function') {
            $("[data-button-type=show-report-summary]").unbind('click');

            function showSummaryReport(button) {
                // ask for confirmation before deleting an item
                // e.preventDefault();
                var button = $(button);
                var route = button.attr('data-route');
                var $modal = $('#report-summary-dialog');
                $('body').append($modal);
                var $modalCancelButton = $modal.find('#cancelButton');
                $.ajax({
                    url: route,
                    type: 'GET',
                    success: function(result) {
                        $($modal).find('.modal-body').html(result);
                        // Hide the modal, if any
                        $modal.modal();

                        $modalCancelButton.on('click', function () {
                            $($modal).modal('hide');
                        });

                        //crud.table.ajax.reload();
                    },
                    error: function(result) {
                        // Show an alert with the result
                        new Noty({
                            text: "Notification not sent. Please try again.",
                            type: "warning"
                        }).show();
                    }
                });
            }
        }
    </script>

