@if ($crud->hasAccess('create'))
	<a href="{{ url($crud->route.'/create?emergency_id='.request()->get('emergency_id').'') }}" class="btn btn-primary" data-style="zoom-in"><span class="ladda-label"><i class="la la-plus"></i> {{ trans('backpack::crud.add') }} {{ $crud->entity_name }}</span></a>
@endif
