
<div class="modal fade" id="report-summary-dialog" tabindex="0" role="dialog" aria-labelledby="report-summary-dialog-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="report-summary-dialog-label">
                    Report Summary
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-light">
                <h4>Summary Content!!!!</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="cancelButton">{{trans('backpack::crud.cancel')}}</button>
            </div>
        </div>
    </div>
</div>




