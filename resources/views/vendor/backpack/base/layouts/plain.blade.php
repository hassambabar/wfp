<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ config('backpack.base.html_direction') }}">
<head>
    @include(backpack_view('inc.head'))
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/backpack/login.css')}}">

</head>
<body class="app flex-row align-items-center plainbody" style="background: url({{ asset('/assets/img/PDMA-BANNER.jpg') }}) no-repeat center center fixed;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;background-position: 100% 80%;">

  @yield('header')

  <div class="container">
  @yield('content')
  </div>

  <footer class="app-footer sticky-footer">
    {{-- @include('backpack::inc.footer') --}}
  </footer>

  @yield('before_scripts')
  @stack('before_scripts')

  @include(backpack_view('inc.scripts'))

  @yield('after_scripts')
  @stack('after_scripts')

</body>
</html>
