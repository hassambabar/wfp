<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

@if (backpack_auth()->user()->hasRole('Admin'))

<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
	<ul class="nav-dropdown-items">
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
	  {{-- <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li> --}}
	</ul>
</li>

@endif

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('ngo') }}'>Organizations/NGOs</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('human_resource') }}'>Human Resources</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('volunteer') }}'>Volunteers</a></li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">Projects & Reports</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('project') }}'>Projects</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('project-reporting') }}'>Project Reports</a></li>
    </ul>
</li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('donor') }}'>Donors</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('stock') }}'>Stocks</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('emergency') }}'> Hazards & Emergencies</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('affiliation') }}'>Memberships / Affiliations</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('ngo_contact') }}'>NGO/Organization Contacts</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('hazard_prone_areas') }}'>Hazard Prone Areas</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('announcement') }}'>Announcements & Alerts</a></li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">Relief Camps</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('relief_camps') }}'>Relief Camps List</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('medicalfacilities') }}'>Medical Facilities</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('personstreated') }}'>Persons Treated</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('foodpackage') }}'>Food Packages</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('livestockcamp') }}'>Livestock Camps</a></li>

    </ul>
</li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('affectedarea') }}'> Losses/Damages Reports</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('needassesmentresponse') }}'> Emergency Support/Response</a></li>


{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('hazard_areas') }}'>Hazard Prone Areas</a></li>--}}

{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('contact_pdma') }}'> Contact PDMA</a></li> --}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('emergency-item') }}'>Emergency Items</a></li>--}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('riskmanagement') }}'> Risk Managements</a></li> --}}

@if (backpack_auth()->user()->hasRole('Admin'))
<li class="nav-item nav-dropdown">
  <a class="nav-link nav-dropdown-toggle" href="#">Dropdown CRUDs</a>
  <ul class="nav-dropdown-items">
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('district') }}'> Districts</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('province') }}'> Provinces</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tehsil') }}'> Tehsils</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('uc') }}'> Ucs</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ngo-registration-law') }}'>NGO Registration Laws</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('area_expertise') }}'> Area expertises</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('disaster') }}'> Disasters</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('hazard_type') }}'>Hazard Types</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('hazard_areas') }}'> Hazard Areas</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('funding_agency') }}'>Funding Agencies</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('organization_natures') }}'>Organization Natures</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('thematic_areas') }}'>Thematic Areas</a></li>
      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('project_category') }}'>Project Categories</a></li>
  </ul>
</li>
<li class="nav-item nav-dropdown">
  <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-magic"></i> Settings</a>
  <ul class="nav-dropdown-items">
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('item_type') }}'>Stock Item types</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category_type') }}'> Category types</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('network') }}'>Networks</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('projecttype') }}'> Project Types</a></i>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('project_modality') }}'>Project Modalities</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('implementing_organization_type') }}'>Implementing Organization Types</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('sector_cluster') }}'>Sector Clusters</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('implementingpartner') }}'>Implementing Partners</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('forecastof') }}'>Forecast of</a></li>

      <li class='nav-item'><a class='nav-link' href='{{ backpack_url('terms-content') }}'>Terms Content</a></li>
  </ul>
</li>
@endif


<li class='nav-item'><a class='nav-link' href='{{ backpack_url('notifications') }}'> Notifications </a></li>



{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('reliefcampcontact') }}'><i class='nav-icon la la-question'></i> Reliefcampcontacts</a></li> --}}


