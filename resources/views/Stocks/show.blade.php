@extends('layouts.app')
{{--@dd($Stocks->category_id)--}}
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-6">
                        <h3>Stocks</h3>
                    </div>
                    @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                        <div class="col-md-6 text-right">
                            <a href="{{route('stocks.edit', $Stocks->id)}}">Edit Details</a>
                        </div>
                    @endif
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <p><b>Category:</b> {{$Stocks->category->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Name:</b> {{$Stocks->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Quantity:</b> {{$Stocks->quantity}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Item Type:</b> {{$Stocks->type->name}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Availability:</b> {{$Stocks->availability}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Location:</b> {{$Stocks->location->name}}</p>
                        </div>
                        @if($Stocks->district)
                        <div class="col-md-6">
                            <p><b>District:</b> {{$Stocks->district->name}}</p>
                        </div>
                        @endif
                        @if($Stocks->tehsil)
                        <div class="col-md-6">
                            <p><b>Tehsil:</b> {{$Stocks->tehsil->name}}</p>
                        </div>
                        @endif
                        @if($Stocks->uc)
                        <div class="col-md-6">
                            <p><b>UC:</b> {{$Stocks->uc->name}}</p>
                        </div>
                        @endif
                        @if($Stocks->office_warehouse_address)
                        <div class="col-md-6">
                            <p><b>Office/Warehouse Address:</b> {{$Stocks->office_warehouse_address}}</p>
                        </div>
                        @endif
                        <div class="col-md-6">
                            <p><b>Remarks:</b> {{$Stocks->remarks}}</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <a href="{{url('/stocks')}}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
