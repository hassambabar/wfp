@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="card-title">
                        <div class="title">Add Stock</div>
                    </div>
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                        @if (session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    <form action="{{route('stocks.store')}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="submit_type" value="" />

                        <div class="form-group col-md-6">
                            <label for="category_id">Category *</label>
                            <select required id="category_id" name="category_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Category_type as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Name" value="{{old('name')}}" name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="quantity">Quantity *</label>
                            <input required type="number" class="form-control is_numeric" id="quantity" placeholder="Quantity" value="{{old('quantity')}}" name="quantity">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="item_type_id">Item Type *</label>
                            <select required id="item_type_id" name="item_type_id" class="form-control">
                                <option value="">Select</option>
                                @foreach ($Item_type as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="availability">Availability</label>
                            <input type="text" class="form-control" id="availability" placeholder="availability" value="{{old('availability')}}"  name="availability">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="location_id">Location *</label>
                            <select required id="location_id" name="location_id" class="form-control">
                                <option value="">Select</option>
                                @foreach ($Province as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="district_id">District *</label>
                            <select required id="district_id" name="district_id" class="form-control">
                                <option value="">Select</option>
                                @foreach ($District as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tehsil_id">Tehsil *</label>
                            <select required id="tehsil_id" name="tehsil_id" class="form-control">
                                <option value="">Select</option>
                                @foreach ($Tehsil as $item)
                                    <option title="{{$item->district->name}}" value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="uc_id">UC *</label>
                            <select required id="uc_id" name="uc_id" class="form-control">
                                <option value="">Select</option>
                                @foreach ($Uc as $item)
                                    <option title="{{$item->tehsil->name}}" value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="office_warehouse_address">Office/Warehouse Address</label>
                            <input type="text" class="form-control" id="office_warehouse_address" placeholder="Office/Warehouse Address" value="{{old('office_warehouse_address')}}"  name="remarks">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="remarks">Remarks</label>
                            <input type="text" class="form-control" id="remarks" placeholder="remarks" value="{{old('remarks')}}"  name="remarks">
                        </div>
                        <div class="form-group col-md-12">
                            <button id="form_submit" type="submit" class="btn btn-default">Save</button>
                            <a id="save_new" href="javascript:void(0)" class="btn btn-primary">Save & New</a>
                            <a href="{{url('stocks')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
<script>
       $(document).ready(function () {
           $('#save_new').on('click', function (){
               $('input[name="submit_type"]').val('save_new');
               $('#form_submit').click();
           });
           $('#dataTables-example').dataTable();

           var $district_id = $('#district_id'),
               $tehsil_id = $('#tehsil_id'),
               $uc_id = $('#uc_id'),
               $ucs = $uc_id.find('option');
           $tehsils = $tehsil_id.find('option');

           $district_id.on('change', function() {
               $tehsil_id.html($tehsils.filter('[title="' + $('#district_id').find(":selected").text() + '"]'));
               $tehsil_id.trigger('change');
           }).trigger('change');
           $tehsil_id.on('change', function() {
               $uc_id.html($ucs.filter('[title="' + $('#tehsil_id').find(":selected").text() + '"]'));
               $uc_id.trigger('change');
           }).trigger('change');

       });
       $(function() {
           $( "#availability" ).datepicker();
       });
</script>
<script>
    // Disable form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {

            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        Swal.fire({
                            title: 'Error!',
                            text: 'Please fill all required fields and then submit.',
                            icon: 'error',
                            confirmButtonText: 'OK'
                        })
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
@endsection
