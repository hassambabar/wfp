@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                    <form action="{{route('stocks.update',$Stocks->id)}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @method('PATCH')

                        @csrf

                        <div class="form-group col-md-6">
                            <label for="category_id">Category *</label>
                            <select required id="category_id" name="category_id" class="form-control">
                                <option value="">Select</option>

                                @foreach ($Category_type as $item)
                                <option value="{{$item->id}}" @if($Stocks->category_id == $item->id)  selected  @endif >{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Name" value="{{$Stocks->name}}" name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="quantity">Quantity *</label>
                            <input type="number" class="form-control is_numeric" id="quantity" placeholder="Quantity" value="{{$Stocks->quantity}}" name="quantity">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="item_type_id">Item Type *</label>
                            <select id="item_type_id" required name="item_type_id" class="form-control">
                                <option value="">Select</option>
                                @foreach ($Item_type as $item)
                                <option value="{{$item->id}}" @if($Stocks->item_type_id == $item->id)  selected  @endif >{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="availability">Availability</label>
                            <input type="text" class="form-control" id="availability" placeholder="availability" value="{{$Stocks->availability}}"  name="availability">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="location_id">Location *</label>
                            <select required id="location_id" name="location_id" class="form-control">
                                <option value="">Select</option>
                                @foreach ($Province as $item)
                                <option value="{{$item->id}}" @if($Stocks->location_id == $item->id)  selected  @endif >{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="district_id">District *</label>
                            <select required id="district_id" name="district_id" class="form-control">
                                <option value="">Select</option>
                                @foreach ($District as $item)
                                    <option
                                        @if($Stocks->district_id == $item->id)  selected  @endif
                                        value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tehsil_id">Tehsil *</label>
                            <select required id="tehsil_id" name="tehsil_id" class="form-control">
                                <option value="">Select</option>
                                @foreach ($Tehsil as $item)
                                    <option
                                        @if($Stocks->tehsil_id == $item->id)  selected  @endif
                                        title="{{$item->district->name}}" value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="uc_id">UC *</label>
                            <select required id="uc_id" name="uc_id" class="form-control">
                                <option value="">Select</option>
                                @foreach ($Uc as $item)
                                    <option
                                        @if($Stocks->uc_id == $item->id)  selected  @endif
                                        title="{{$item->tehsil->name}}" value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="office_warehouse_address">Office/Warehouse Address</label>
                            <input type="text" class="form-control" id="office_warehouse_address" placeholder="Office/Warehouse Address" value="{{old('office_warehouse_address',$Stocks->remarks)}}"  name="remarks">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="remarks">Remarks</label>
                            <input type="text" class="form-control" id="remarks" placeholder="remarks" value="{{$Stocks->remarks}}"  name="remarks">
                        </div>

                        <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
<script>

    $( function() {
        var $district_id = $('#district_id'),
            $tehsil_id = $('#tehsil_id'),
            $uc_id = $('#uc_id'),
            $ucs = $uc_id.find('option');
        $tehsils = $tehsil_id.find('option');

        $district_id.on('change', function() {
            let selectedTh = $tehsil_id.find('option:selected');
            $tehsil_id.html($tehsils.filter('[title="' + $('#district_id').find(":selected").text() + '"]'));
            $tehsil_id.trigger('change');
            $tehsil_id.val($(selectedTh).val());
        }).trigger('change');
        $tehsil_id.on('change', function() {
            let selectedUc = $uc_id.find('option:selected');
            $uc_id.html($ucs.filter('[title="' + $('#tehsil_id').find(":selected").text() + '"]'));
            $uc_id.trigger('change');
            $uc_id.val($(selectedUc).val());
        }).trigger('change');
        $( "#availability" ).datepicker();
    } );
    </script>
<script>
    // Disable form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {

            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
@endsection
