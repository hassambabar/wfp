@extends(backpack_view('blank'))

@section('header')
  <div class="container-fluid">
    <h2>
      <span class="text-capitalize">Notifications</span>
      <small id="datatable_info_stack"></small>
    </h2>
  </div>
@endsection

@section('content')
 <nav aria-label="breadcrumb" class="d-none d-lg-block">
  <ol class="breadcrumb bg-transparent p-0 justify-content-end">
    <li class="breadcrumb-item text-capitalize"><a href="{{url('/admin/dashboard')}}">Admin</a></li>
    <li class="breadcrumb-item text-capitalize"><a href="{{url('/admin/notifications')}}">Notifications</a></li>
    <li class="breadcrumb-item text-capitalize active" aria-current="page">List</li>
  </ol>
</nav>


            <table id="crudTable" class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2" cellspacing="0">
            <thead>
              <tr>

                  <th>Type</th>
                  <th>Created at</th>
                  <th>Read at</th>

                  <th>Action</th>

              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>


              @foreach(\App\User::find(backpack_auth()->user()->id)->notifications as $notification)
                <tr class="odd gradeX">
                    <td>
                      {{-- {{$notification->type}} --}}
                      @if($notification->type == 'App\Notifications\EmergenciesCreated')
                      Emergencies Created    
                      @elseif($notification->type == 'App\Notifications\ProfileApproval')

                      Profile Approval

                      @elseif($notification->type == 'App\Notifications\RequestForNoc')
                      Request ForNoc

                      @elseif($notification->type == 'App\Notifications\EmergenciesRespond')
                      Emergencies Respond
                          

                      @elseif($notification->type == 'App\Notifications\ProfileUpdateApproval')
                      Profile Update Approval

                      @elseif($notification->type == 'App\Notifications\PdmaEmpAssignmentNoc')
                      PDMA Employee Assignment Noc

                      @elseif($notification->type == 'App\Notifications\PdmaEmpAssignmentProfile')
                      PDMA Employee Assignment Profile

                      @else 
                        {{basename($notification->type)}}
                    
                          
                      @endif
                    </td>
                    <td><em>{{
                        \Carbon\Carbon::parse($notification->created_at)->diffForhumans()
                    }}</em></td>
                    <td><em>{{
                        ($notification->read_at != NULL)?
                        (\Carbon\Carbon::parse($notification->read_at)->diffForhumans()):
                        'Un Read'
                    }}</em></td>

                    <td>
                         @if($notification->type == 'App\Notifications\EmergenciesCreated')
                         <?php $id_em = $notification->data['emergency']['id']; ?>
                         <a href="{{url("need-assesment/$id_em")}}"  class="readRemander" data-id="{{$notification->id}}">
                         View</a>
                         @endif

                         @if($notification->type == 'App\Notifications\ProfileApproval' || $notification->type == 'App\Notifications\ProfileUpdateApproval')
                             <a class="readRemander" href="{{url('admin/notifications/profileApproval/'.$notification->data['profile']['id'])}}">
                             View</a>
                         @endif

                         @if($notification->type == 'App\Notifications\RequestForNoc' || $notification->type == 'App\Notifications\NocUpdateApproval')
                             <a class="readRemander" href="{{url('admin/notifications/nocApproval/'.$notification->data['project']['id'])}}">
                                 View</a>
                         @endif

                        @if(backpack_user()->hasRole('Pdma Emp') && $notification->type == 'App\Notifications\PdmaEmpAssignmentProfile')
                         <a class="readRemander" href="{{url('admin/notifications/profileApproval/'.$notification->data['ngoProfile']['user_id'])}}">
                             View
                         </a>
                        @endif

                        @if(backpack_user()->hasRole('Pdma Emp') && $notification->type == 'App\Notifications\PdmaEmpAssignmentNoc')
                         <a class="readRemander" href="{{url('admin/notifications/nocApproval/'.$notification->data['ngoProject']['id'])}}">
                             View
                         </a>
                        @endif


                        @if ($notification->type == 'App\Notifications\EmergenciesRespond' )
                            <a class="readRemander" href="{{ url('admin/emergency',$notification->data['emergency']['emergency_id'].'/show')  }}">
                             View</a>
                        @endif


                    </td>
                </tr>
              @endforeach


            </tfoot>
          </table>




@endsection




@section('after_styles')
  <!-- DATA TABLES -->
  <link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css').'?v='.config('backpack.base.cachebusting_string') }}">
  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/form.css').'?v='.config('backpack.base.cachebusting_string') }}">
  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/list.css').'?v='.config('backpack.base.cachebusting_string') }}">

  <!-- CRUD LIST CONTENT - crud_list_styles stack -->
  @stack('crud_list_styles')
@endsection

@section('after_scripts')
  {{-- @include('crud::inc.datatables_logic') --}}
  <script src="{{ asset('packages/backpack/crud/js/crud.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
  <script src="{{ asset('packages/backpack/crud/js/form.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
  <script src="{{ asset('packages/backpack/crud/js/list.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>

  <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
  @stack('crud_list_scripts')
@endsection
