@extends(backpack_view('blank'))

@section('header')
  <div class="container-fluid">
    <h2>
      <span class="text-capitalize">NOC Approval </span>
      <small id="datatable_info_stack"></small>
    </h2>
  </div>
@endsection

@section('content')
 <nav aria-label="breadcrumb" class="d-none d-lg-block">
  <ol class="breadcrumb bg-transparent p-0 justify-content-end">
    <li class="breadcrumb-item text-capitalize"><a href="{{url('/admin/dashboard')}}">Admin</a></li>
    <li class="breadcrumb-item text-capitalize"><a href="{{url('/admin/notifications')}}">Notifications</a></li>
    <li class="breadcrumb-item text-capitalize active" aria-current="page">List</li>
  </ol>
</nav>

  <div class="row">
    <div class="col-md-8">

      <div class="card no-padding no-border">
        <table class="table table-striped mb-0">
         <tbody>

          {{-- @dump( $ngoUserInfo ) --}}

          <tr><td colspan="2"> <h4>Details of Organization Thematic Areas (select appropriate)</h4> </td></tr>
          <tr>
            <td><strong>Thematic Areas:</strong></td>
            <td>{{$project->thematicArea->area_name}}</td>
          </tr>
          <tr><td colspan="2"> <h4>Details of Complete/Ongoing Project</h4> </td></tr>
          <tr>
            <td><strong>Context:</strong></td>
            <td>{{$project->category->category_name}}</td>
          </tr>

          <tr>
            <td><strong>Donor/Funding Organization:</strong></td>
            <td>{{$project->fundingAgency->name}}</td>
          </tr>

          <tr>
            <td><strong>Project Code:</strong></td>
            <td>{{$project->project_code}}</td>
          </tr>

          <tr>
            <td><strong>Project Modality:</strong></td>
            <td>
              {{$project->modality->title}}
            </td>
          </tr>


          <tr>
              <td> <strong>Project Title</strong> </td>
            <td>{{$project->project_title}}</td>
          </tr>

          <tr>
            <td><strong>Project Owner:</strong></td>
            <td>{{$project->project_owner_name}}</td>
          </tr>

          <tr>
            <td><strong>Project Start Date:</strong></td>
            <td>{{$project->start_date}}</td>
          </tr>

          <tr>
            <td><strong>Project End Date:</strong></td>
            <td>{{$project->end_date}}</td>
          </tr>

          <tr>
            <td><strong>Project Owner Organization Type:</strong></td>
            <td>{{$project->project_owner_org_type}}</td>
          </tr>

          <tr>
            <td><strong>Implementing Partner:</strong></td>
            <td>{{$project->implementingPartner->title}}</td>
          </tr>

          <tr>
            <td><strong>Implementing Organization Type:</strong></td>
            <td>{{$project->implementingOrgType->title}}</td>
          </tr>

          <tr>
            <td><strong>Sector / Cluster:</strong></td>
            <td>{{ $project->projectSector->title}}</td>
          </tr>

          <tr>
              <td> <strong> Project Province</strong></td>
            <td>{{  $project->province->name }}</td>
          </tr>

          <tr>
            <td><strong> Project District:</strong></td>
            <td>{{  $project->district->name }}</td>
          </tr>

          <tr>
            <td><strong> Project Tehsil:</strong></td>
            <td>{{  $project->tehsil->name }}</td>
          </tr>

          <tr>
            <td><strong>Project Union Council:</strong></td>
            <td>{{  $project->uc->name }}</td>
          </tr>

          <tr>
              <td><strong>Project Village: </strong></td>
              <td>{{  $project->project_village }}</td>
          </tr>

          <tr><td colspan="2"> <h4>Activity</h4> </td></tr>
          <tr>
              <td><strong>Target (Number):</strong></td>
              <td>{{  $project->target_number }}</td>
          </tr>
          @if(isset($project->achieved_number))
          <tr>
              <td><strong>Achieved (Number):</strong></td>
              <td>{{  $project->achieved_number }}</td>
          </tr>
          @endif
          <tr>
              <td><strong>Activity Unit:</strong></td>
              <td>{{  $project->activity_unit }}</td>
          </tr>
          <tr>
              <td><strong>Assistance Type and Description:</strong></td>
              <td>{{  $project->assist_type }}</td>
          </tr>
          <tr><td colspan="2"> <h4>Targeted</h4> </td></tr>
          <tr>
              <td><strong>Women:</strong></td>
              <td>{{  $project->women_targeted }}</td>
          </tr>
          <tr>
              <td><strong>Men:</strong></td>
              <td>{{  $project->men_targeted }}</td>
          </tr>
          <tr>
              <td><strong>Girls:</strong></td>
              <td>{{  $project->girls_targeted }}</td>
          </tr>
          <tr>
              <td><strong>Boys:</strong></td>
              <td>{{  $project->boys_targeted }}</td>
          </tr>
          <tr><td colspan="2"> <h4>Contacts</h4> </td></tr>
          <tr>
              <td><strong>Contact Person:</strong></td>
              <td>{{  $project->contact_person }}</td>
          </tr>
          <tr>
              <td><strong>Contact Number:</strong></td>
              <td>{{  $project->contact_number }}</td>
          </tr>
          <tr>
              <td><strong>Email:</strong></td>
              <td>{{  $project->contact_email }}</td>
          </tr>
          <tr>
              <td><strong>Comments:</strong></td>
              <td>{{  $project->comments }}</td>
          </tr>
          <tr><td colspan="2"> <h4>Source of Funds</h4> </td></tr>
          {{--<tr>
              <td><strong>Contact Person:</strong></td>
              <td>{{  $project->contact_person }}</td>
          </tr>--}}
          <tr>
              <td><strong>Donor's Name:</strong></td>
              <td>{{  $project->funds_donor_name }}</td>
          </tr>
          <tr>
              <td><strong>Project Amount:</strong></td>
              <td>{{  $project->funds_amount }}</td>
          </tr>
          <tr>
              <td><strong>Extension for Previous Projects:</strong></td>
              <td>{{  ($project->prev_extension) == 1 ? 'Yes' : 'No' }}</td>
          </tr>

         </tbody>
        </table>
      </div>
        @if(backpack_user()->hasRole('PDMA') || backpack_user()->hasRole('Admin'))
       <div class="card p-4  ">
          <form method="post" action="{{route('assignNocApproval')}}">
            @csrf
            <input type="hidden" name="ngo_id" value="{{$ngoProfile->id}}">
            <input type="hidden" name="project_id" value="{{$project->id}}">

            <h3>Approval Process</h3>

            <div class="row">
              <div class="col-md-3">Status:</div>
              <div class="col-md-9">

                  <div class="radio3 radio-check radio-inline">
                      <input type="radio" id="status_active" name="status" checked value="accepted" {{($project->noc()->first() && $project->noc()->first()->status == 'accepted') ?'checked':'' }}>
                      <label for="status_active">Active</label>
                  </div>
                  <div class="radio3 radio-check radio-inline">
                      <input type="radio" id="status_pending" name="status" value="pending" {{($project->noc()->first() && $project->noc()->first()->status == 'pending')?'checked':''}}>
                      <label for="status_pending">Pending</label>
                  </div>

              </div>
            </div>

            <div class="row">
              <div class="col-md-3"><strong>Assign to PDMA Employee: </strong></div>
              <div class="col-md-9">
                <select class="form-control select2_from_array" name="pdma_employee_assign">
                   <option value=""> -- Select PDMA Employee -- </option>
                  @foreach ($pdma_employees as $employee)
                    <option value="{{$employee->id}}">{{ $employee->name }}</option>
                  @endforeach

                </select>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                 <button type="submit" class="btn btn-default">Submit</button>
              </div>
            </div>

        </form>

       </div>

        <div class="card p-4  ">
            <form method="post" action="{{route('pdmaNocReview')}}">
                @csrf
                <input type="hidden" name="ngo_id" value="{{$ngoProfile->id}}">
                <input type="hidden" name="ngo_user_id" value="{{$project->ngo_id}}">
                <input type="hidden" name="project_id" value="{{$project->id}}">

                <h3>PDMA Review</h3>

                <div class="row">
                    <div class="col-md-3"><strong>Decision:</strong></div>
                    <div class="col-md-9">

                        <div class="radio3 radio-check radio-inline">
                            <input type="radio" id="profile_accept" name="pdma_noc_review_decision" checked value="accept" {{($ngoProfile->user->pdma_noc_review_decision == 'accept')?'checked':'' }}>
                            <label for="profile_accept">Accept</label>
                        </div>
                        <div class="radio3 radio-check radio-inline">
                            <input type="radio" id="profile_pending" name="pdma_noc_review_decision" value="reject" {{($ngoProfile->user->pdma_noc_review_decision == 'reject')?'checked':''}}>
                            <label for="profile_pending">Reject</label>
                        </div>
                        <div class="radio3 radio-check radio-inline">
                            <input type="radio" id="profile_deferred" name="pdma_noc_review_decision" value="deferred" {{($ngoProfile->user->pdma_noc_review_decision == 'deferred')?'checked':''}}>
                            <label for="profile_deferred">Deferred</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3"><strong>Comment: </strong></div>
                    <div class="col-md-9">
                        <textarea class="form-control" cols="5" rows="5" name="pdma_noc_review_comment">{{$ngoProfile->user->pdma_noc_review_comment}}</textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>

            </form>

        </div>
            @endif

        <div class="row">

            <div class="col-md-12 col-xl-12 commentbox">


                <h4>COMMENTS</h4>
                @include('commentsDisplay', ['comments' => $ngoProfile->user->commentsNoc, 'post_id' => $project->id])

                <form method="post" action="{{ route('comments.store') }}" class="noprint">
                    @csrf
                    <div class="form-group">
                        <textarea class="form-control bodyMain" name="body"></textarea>
                        <input type="hidden" name="post_id" value="{{ $project->id }}" />
                        <input type="hidden" name="type" value="noc" />

                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Add Comment" />
                    </div>
                </form>
            </div>

        </div>

    </div>
  </div>





@endsection


@section('after_styles')
  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css').'?v='.config('backpack.base.cachebusting_string') }}">
  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css').'?v='.config('backpack.base.cachebusting_string') }}">
@endsection

@section('after_scripts')
  <script src="{{ asset('packages/backpack/crud/js/crud.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
  <script src="{{ asset('packages/backpack/crud/js/show.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
    <script>
        $('a.replyform').on('click',function (){
            let reply_id = $(this).attr('data-id');
            $('.formReply.'+reply_id+'').css('display','block');
        });
    </script>
@endsection
