@extends(backpack_view('blank'))

@section('header')
  <div class="container-fluid">
    <h2>
      <span class="text-capitalize">Profile Approval </span>
      <small id="datatable_info_stack"></small>
    </h2>
  </div>
@endsection

@section('content')
 <nav aria-label="breadcrumb" class="d-none d-lg-block">
  <ol class="breadcrumb bg-transparent p-0 justify-content-end">
    <li class="breadcrumb-item text-capitalize"><a href="{{url('/admin/dashboard')}}">Admin</a></li>
    <li class="breadcrumb-item text-capitalize"><a href="{{url('/admin/notifications')}}">Notifications</a></li>
    <li class="breadcrumb-item text-capitalize active" aria-current="page">List</li>
  </ol>
</nav>

  <div class="row">
    <div class="col-md-8">

      <div class="card no-padding no-border">
        <table class="table table-striped mb-0">
         <tbody>

          {{-- @dump( $ngoUserInfo ) --}}

          <tr><td> <strong>User Info</strong></h4> </td></tr>
          <tr>
            <td><strong>Name:</strong></td>
            <td>{{ucfirst($ngoUserInfo->name)}}</td>
          </tr>

          <tr>
            <td><strong>Email:</strong></td>
            <td>{{$ngoUserInfo->email}}</td>
          </tr>

          <tr>
            <td><strong>Orginization Full Name:</strong></td>
            <td>{{$ngoProfile->org_full_name}}</td>
          </tr>

          <tr>
            <td><strong>Acronym:</strong></td>
            <td>{{$ngoProfile->acronym}}</td>
          </tr>

          <tr>
            <td><strong>Legal Status:</strong></td>
            <td>
              @if ($ngoProfile->legal_status == 'pk_org')
                For Pakistani Organizations
              @elseif( $ngoProfile->legal_status == 'corp_firm' )
                Corporate Firm
              @elseif( $ngoProfile->legal_status == 'pk_charity' )
                Pakistani Charity Org
              @elseif( $ngoProfile->legal_status == 'intl_org' )
                International Organizations Only
              @elseif( $ngoProfile->legal_status == 'un_org' )
                UN Organization Agency
              @endif
            </td>
          </tr>

          <tr><td> <strong>Legal Status</strong>></h4> </td></tr>
          <tr>
            <td><strong>Registration Law:</strong></td>
            <td>{{$ngoProfile->reg_law}}</td>
          </tr>

          <tr>
            <td><strong>Date of Reg:</strong></td>
            <td>{{$ngoProfile->reg_date}}</td>
          </tr>

          <tr>
            <td><strong>Reg No:</strong></td>
            <td>{{$ngoProfile->reg_no}}</td>
          </tr>

          <tr>
            <td><strong>Nature of Organizations:</strong></td>
            <td>{{$ngoProfile->reg_law}}</td>
          </tr>


          <tr><td><strong>Registration details at HQ</strong></h4> </td></tr>
          <tr>
            <td><strong>Country of Origin:</strong></td>
            <td>{{$ngoProfile->hq_origin_country}}</td>
          </tr>

          <tr>
            <td><strong>Registration Law:</strong></td>
            <td>{{$ngoProfile->hq_reg_law}}</td>
          </tr>

          <tr>
            <td><strong>Date of Reg:</strong></td>
            <td>{{$ngoProfile->hq_reg_date}}</td>
          </tr>

          <tr>
            <td><strong>Reg. No. </strong></td>
            <td>{{$ngoProfile->hq_reg_no}}</td>
          </tr>

          <tr>
            <td><strong>Do you have MOU with Department of Economic Affairs or Ministry of Interior?</strong></td>
            <td>{{ ($ngoProfile->have_mou)?'Yes':'No'}}</td>
          </tr>

          <tr><td> <strong> Goal/Vision/Mission</strong></h4> </td></tr>
          <tr>
            <td><strong> Goal / Vision Statement <i>(if applicable)</i></strong></td>
            <td>{{  $ngoProfile->ngo_goal }}</td>
          </tr>

          <tr>
            <td><strong> Mission Statement <i>(if applicable)</i></strong></td>
            <td>{{  $ngoProfile->mission_statement }}</td>
          </tr>

          <tr>
            <td><strong> Objectives <i>(if applicable)</i></strong></td>
            <td>{{  $ngoProfile->objectives }}</td>
          </tr>

          <tr>
            <td><strong>Profile Info</strong></td>
            <td>{{  $ngoProfile->profile }}</td>
          </tr>



         </tbody>
        </table>
      </div>
        @if(backpack_user()->hasRole('PDMA'))
       <div class="card p-4  ">
          <form method="post" action="{{route('assingProfileApproval')}}">
            @csrf
            <input type="hidden" name="ngo_id" value="{{$ngoProfile->id}}">
            <input type="hidden" name="ngo_user_id" value="{{$ngoUserInfo->id}}">

            <h3>Approval Process</h3>

            <div class="row">
              <div class="col-md-3">Status:</div>
              <div class="col-md-9">

                  <div class="radio3 radio-check radio-inline">
                      <input type="radio" id="status_active" name="status" checked value="1" {{($ngoUserInfo->status == '1')?'checked':'' }}>
                      <label for="status_active">Active</label>
                  </div>
                  <div class="radio3 radio-check radio-inline">
                      <input type="radio" id="status_pending" name="status" value="-1" {{($ngoUserInfo->status == '-1')?'checked':''}}>
                      <label for="status_pending">Pending</label>
                  </div>

              </div>
            </div>

            <div class="row">
              <div class="col-md-3"><strong>Assign to PDMA Employee: </strong></div>
              <div class="col-md-9">
                <select class="form-control select2_from_array" name="pdma_employee_assign">
                   <option value=""> -- Select PDMA Employee -- </option>
                  @foreach ($pdma_employees as $employee)
                    <option value="{{$employee->id}}">{{ $employee->name }}</option>
                  @endforeach

                </select>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                 <button type="submit" class="btn btn-default">Submit</button>
              </div>
            </div>

        </form>

       </div>

        <div class="card p-4  ">
            <form method="post" action="{{route('pdmaProfileReview')}}">
                @csrf
                <input type="hidden" name="ngo_id" value="{{$ngoProfile->id}}">
                <input type="hidden" name="ngo_user_id" value="{{$ngoUserInfo->id}}">

                <h3>PDMA Review</h3>

                <div class="row">
                    <div class="col-md-3"><strong>Decision:</strong></div>
                    <div class="col-md-9">

                        <div class="radio3 radio-check radio-inline">
                            <input type="radio" id="profile_accept" name="pdma_profile_review_decision" checked value="accept" {{($ngoUserInfo->pdma_profile_review_decision == 'accept')?'checked':'' }}>
                            <label for="profile_accept">Accept</label>
                        </div>
                        <div class="radio3 radio-check radio-inline">
                            <input type="radio" id="profile_pending" name="pdma_profile_review_decision" value="reject" {{($ngoUserInfo->pdma_profile_review_decision == 'reject')?'checked':''}}>
                            <label for="profile_pending">Reject</label>
                        </div>
                        <div class="radio3 radio-check radio-inline">
                            <input type="radio" id="profile_deferred" name="pdma_profile_review_decision" value="deferred" {{($ngoUserInfo->pdma_profile_review_decision == 'deferred')?'checked':''}}>
                            <label for="profile_deferred">Deferred</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3"><strong>Comment: </strong></div>
                    <div class="col-md-9">
                        <textarea class="form-control" cols="5" rows="5" name="pdma_profile_review_comment">{{$ngoUserInfo->pdma_profile_review_comment}}</textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>

            </form>

        </div>
            @endif

        <div class="row">

            <div class="col-md-12 col-xl-12 commentbox">


                <h4>COMMENTS</h4>
                @include('commentsDisplay', ['comments' => $ngoUserInfo->comment, 'post_id' => $ngoUserInfo->id])

                <form method="post" action="{{ route('comments.store') }}" class="noprint">
                    @csrf
                    <div class="form-group">
                        <textarea class="form-control bodyMain" name="body"></textarea>
                        <input type="hidden" name="post_id" value="{{ $ngoUserInfo->id }}" />
                        <input type="hidden" name="type" value="profile" />

                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Add Comment" />
                    </div>
                </form>
            </div>

        </div>

    </div>
  </div>





@endsection


@section('after_styles')
  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css').'?v='.config('backpack.base.cachebusting_string') }}">
  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css').'?v='.config('backpack.base.cachebusting_string') }}">
@endsection

@section('after_scripts')
  <script src="{{ asset('packages/backpack/crud/js/crud.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
  <script src="{{ asset('packages/backpack/crud/js/show.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
    <script>
        $('a.replyform').on('click',function (){
            let reply_id = $(this).attr('data-id');
            $('.formReply.'+reply_id+'').css('display','block');
        });
    </script>
@endsection
