<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
{{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}


<!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

    <meta charset="utf-8"/>
    <meta content="" name="description"/>
    <meta content="webthemez" name="author"/>
    <!-- Bootstrap Styles-->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet"/>
    <!-- FontAwesome Styles-->
    <link href="{{ asset('assets/font-awesome-4.7.0/font-awesome.css') }}" rel="stylesheet"/>
    <!-- Morris Chart Styles-->
    <link href="{{ asset('assets/js/morris/morris-0.4.3.min.css') }}" rel="stylesheet"/>
    <!-- Custom Styles-->
    <link href="{{ asset('assets/css/custom-styles.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>
    <link rel="stylesheet" href="{{ asset('assets/js/Lightweight-Chart/cssCharts.css') }}">
    <link href="{{ asset('assets/js/dataTables-1.10.15/dataTables.bootstrap.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{--    <link rel="stylesheet" href="{{ asset('assets/js/sweetalert2/dist/sweetalert2.min.css') }}" />--}}
    <link rel="stylesheet" href="{{ asset('assets/sweetalert2-themes/default/default.scss') }}"/>
    <style>
        .swal2-popup {
            font-size: initial !important;
        }
    </style>

</head>

<body>
<div id="wrapper">

    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><strong><i class="icon fa fa-sitemap"></i>
                    NGO</strong></a>

            <div id="sideNav" href="">
                <i class="fa fa-bars icon"></i>
            </div>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            @if((checkProfileApproval() && checkEnableEdit()) || (checkNocApproval() && checkNocEnableEdit()) )
                <li>
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-info dropdown-toggle" aria-expanded="false">
                            Request
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            @if(checkProfileApproval() && checkEnableEdit())
                                <li>
                                    <a href="{{url('/profile/approval/request-update')}}">Profile Update Approval</a>
                                </li>
                            @endif
                            @if(checkNocApproval() && checkNocEnableEdit())
                                <li>
                                    <a href="{{url('/noc/approval/request-update')}}">NOC Update Approval</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </li>
            @endif


            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"
                   aria-expanded="false"> @if(count(auth()->user()->unreadNotifications) > 0)
                        <span
                            class="badge bg-purple pull-right">{{count(auth()->user()->unreadNotifications->where('type', '!=','App\Notifications\EmergenciesCreated'))}}</span> @endif
                    <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>

                <ul class="dropdown-menu dropdown-messages">

                    {{-- @if(count(auth()->user()->unreadNotifications) == 0)
                    <li>
                    <a href="">
                        <div>
                            <strong>No Alters</strong>

                        </div>
                        <div>Nothing New
                        </div>
                    </a>
                    </li>
                    <li class="divider"></li>
                    @endif --}}
                    @foreach(auth()->user()->unreadNotifications as $notification)
                        {{--@if($notification->read_at == NULL && $notification->type == 'App\Notifications\EmergenciesCreated')
                            <?php $id_em = $notification->data['emergency']['id']; ?>
                            <li class="<?php if ($notification->read_at != NULL) {
                                echo "read-message";
                            }?>">
                                <a href="{{url("need-assesment/$id_em")}}" class="readRemander"
                                   data-id="{{$notification->id}}">
                                    <div>
                                        <strong>Emergency</strong>
                                        <span class="pull-right text-muted">
        <em> {{ \Carbon\Carbon::parse($notification->data['emergency']['created_at'])->diffForhumans() }}
             </em>
    </span>
                                    </div>
                                    <div>Emergency Type
                                        <strong>{{$notification->data['emergency']['type_of_emergency']}}</strong>
                                        Hazard <strong>{{$notification->data['emergency']['type_of_hazard']}}</strong>
                                        Rescue items
                                        <strong>{{$notification->data['emergency']['rescue_items']}}</strong><br>
                                        Quantity <strong>{{$notification->data['emergency']['quantity']}}</strong>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                        @endif--}}

                        @if($notification->read_at == NULL && $notification->type == 'App\Notifications\NeedAssesmentResponseUpdated')
                            <?php $id_em = $notification->data['emergency']['emergency_id']; ?>
                            <li class="<?php if ($notification->read_at != NULL) {
                                echo "read-message";
                            }?>">
                                <a href="{{url("need-assesment/$id_em")}}" class="readRemander"
                                   data-id="{{$notification->id}}">
                                    <div>
                                        <strong>Emergency Responded</strong>
                                        <span class="pull-right text-muted">
        <em> {{ \Carbon\Carbon::parse($notification->data['emergency']['updated_at'])->diffForhumans() }}
             </em>
    </span>
                                    </div>
                                    <div>Emergency Responded
                                        <strong> @if($notification->data['emergency']['status'] == 0) Rejected @else
                                                Accepted @endif</strong>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                        @endif


                        @if($notification->read_at == NULL && $notification->type == 'App\Notifications\NgoProfileReviewByPdma' )
                            <li>
                                <a data-notify-decision="{{$notification->data['user']['pdma_profile_review_decision']}}"
                                   data-notify-comment="{{$notification->data['user']['pdma_profile_review_comment']}}"
                                   class="quickRead readRemander" href="javascript:void(0)">
                                    <div>
                                        <strong>Ngo Profile Review</strong>
                                        <span class="pull-right text-muted">
    <em> {{\Carbon\Carbon::parse($notification->created_at)->diffForhumans()}}</em>
    </span>
                                    </div>
                                    <div>
                                        Name: <strong>{{$notification->data['user']['name']}}</strong>
                                        Email: <strong>{{$notification->data['user']['email']}}</strong>
                                        Review Decision:
                                        <strong>{{$notification->data['user']['pdma_profile_review_decision']}}</strong>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                        @endif

                        @if($notification->read_at == NULL && $notification->type == 'App\Notifications\AnnouncementCreated' )
                        <li>
                            <a class="quickRead readRemander" href="javascript:void(0)" data-id="{{$notification->id}}">
                                <div>
                                    <strong>Announcements & Alerts</strong>
                                    <span class="pull-right text-muted">
<em> {{\Carbon\Carbon::parse($notification->created_at)->diffForhumans()}}</em>
</span>
                                </div>
                                <div>
                                    <strong>{{$notification->data['alerts']['title']}}</strong><br>
                                    Forecast of: <strong>{{$notification->data['alerts']['forecast_of']}}</strong>  -  District: <strong>{{$notification->data['alerts']['district_id']}}</strong><br>

                                    From Date: <strong>{{$notification->data['alerts']['from']}}</strong>
                                    To Date: <strong>{{$notification->data['alerts']['to']}}</strong>

                                    {{-- Review Decision:
                                    <strong>{{$notification->data['user']['pdma_profile_review_decision']}}</strong> --}}
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                    @endif
                    @endforeach
                </ul>


            {{-- <ul class="dropdown-menu dropdown-messages">



            <li>
            <a class="text-center" href="#">
            <strong>Read All Messages</strong>
            <i class="fa fa-angle-right"></i>
            </a>
            </li>
            </ul> --}}
            <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="{{url('user-profile')}}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    {{--<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>--}}
                    <li class="divider"></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
    </nav>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">

                <li>
                    <a class="{{ activeMenu('home') }}" href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>
                        Dashboard</a>
                </li>
                @if(checkUserRole('NGO'))
                    <li>
                        <a class="{{ activeMenu('profile') }}" href="{{ url('/profile') }}"><i
                                class="fa fa-sitemap"></i> Organization Profile & Legal status
                        </a>
                    </li>
                    <li>
                        <a class="{{ activeMenu('human-resource') }}" href="#"><i class="fa fa-users"></i> Human Resources
                            <span class="fa arrow"></span> </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ url('human-resource') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('human-resource.create') }}">Add New</a>
                            </li>
                        </ul>

                    </li>
                    <li>
                        <a class="{{ activeMenu('volunteer') }}" href="#"><i class="fa fa-male"></i> Volunteers<span
                                class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ url('volunteers') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('volunteers.create') }}">Add New</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="{{ activeMenu('projects') }}" href="#"><i class="fa fa-folder-o"></i> Thematic Areas & Projects<span
                                class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ url('projects') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('projects.create') }}">Add New</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="{{ activeMenu('past-emergencies') }}" href="#"><i class="fa fa-ambulance"></i>
                            Emergencies Responded<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ url('past-emergencies') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('past-emergencies.create') }}">Add New</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="{{ activeMenu('stock') }}" href="#"><i class="fa fa-cubes"></i> Stocks <span
                                class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ url('stocks') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('stocks.create') }}">Add New</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="{{ activeMenu('affiliation') }}" href="#"><i class="fa fa-black-tie"></i> Membership & Affiliations <span
                                class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ url('affiliations') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('affiliations.create') }}">Add New</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="{{ activeMenu('ngocontact') }}" href="{{ url('ngocontact') }}"><i
                                class="fa fa-vcard"></i> NGO Contacts <span class="fa arrow"></span> </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ url('ngocontact') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('ngocontact.create') }}">Add New</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="{{ activeMenu('mou') }}" href="#"><i class="fa fa-list-alt"></i> Other MOUs<span
                                class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ url('mous') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('mous.create') }}">Add New</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a class="{{ activeMenu('donors') }}" href="{{url('donors')}}"><i class="fa fa-dollar"></i>Donors</a>
                    </li>
                @endif

                @if(checkUserStatus() == true && checkUserRole('NGO'))
                    <li>
                        <a href="{{url('noc-requests')}}"><i class="fa fa-qrcode"></i> Existing Requests</a>
                    </li>
                @endif

                {{--<li><a class="{{ activeMenu('notification') }}" href="{{route('notification.index')}}"><i class="fa fa-bell-o"></i> Notifications </a></li>--}}
                <li>
                    <a class="{{ activeMenu('notification') }}" href="{{ url('reporting') }}"><i
                            class="fa fa-bell-o"></i> Notifications <span
                            class="fa arrow"></span></a>
                    <ul class="nav nav-second-level {{ ((request()->is('notification*')))?'in':'' }} ">
                        <li><a href="{{ route('notification.index') }}">Others</a></li>
                        <li><a href="{{ route('notification.emergencies') }}">Emergencies</a></li>
                    </ul>
                </li>

                @if(checkUserStatus() == true && checkUserRole('NGO'))
                    <li>
                        <a class="{{ activeMenu('reporting') }}" href="{{ url('reporting') }}"><i
                                class="fa fa-line-chart"></i> Reporting <span
                                class="fa arrow"></span></a>
                        <ul class="nav nav-second-level {{ ((request()->is('reporting*')))?'in':'' }} ">
                            <li><a href="{{ route('reporting.index') }}">List</a></li>
                            <li><a href="{{ route('reporting.new') }}">Add New</a></li>
                        </ul>
                    </li>
                @endif




                @if(checkUserStatus() == true && checkUserRole('NGO'))
                    <li>
                        <a href="{{url('risk-management')}}" class="{{ activeMenu('risk-management') }}"><i
                                class="fa fa-pie-chart"></i> Risk Management <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ url('risk-management') }}">List</a></li>
                            <li><a href="{{ route('risk-management.create') }}">Add New</a></li>
                        </ul>
                    </li>
                @endif

                @if(checkUserStatus() == true && checkUserRole('NGO'))
                    <li>
                        <a href="{{url('need-assesment')}}" class="{{ activeMenu('need-assesment') }}">
                            <i class="fa fa-life-ring"></i> Need Assessment </a>
                    </li>

                    <li>
                        <a href="{{url('noc-apply/create')}}" class="{{ activeMenu('noc-apply') }}">
                            <i class="fa fa-briefcase"></i> Apply for NOC </a>
                    </li>
                @endif



                {{--@if(checkUserRole('NGO'))


                    <li>
                        <a class="{{ activeMenu('contacpdma') }}" href="{{ url('contacpdma') }}"><i
                                class="fa fa-edit"></i> Contact For PDMA <span class="fa arrow"></span> </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ url('contacpdma') }}">List</a>
                            </li>
                            <li>
                                <a href="{{ route('contacpdma.create') }}">Add New</a>
                            </li>
                        </ul>
                    </li>

                @endif--}}



                {{-- <li>
                <a href="#"><i class="fa fa-edit"></i> Target Achieved </a>
                </li> --}}

                {{--
                <li>
                <a href="#"><i class="fa fa-fw fa-file"></i> Logout</a>
                </li> --}}
            </ul>

        </div>

    </nav>
    <!-- /. NAV SIDE  -->

    <div id="page-wrapper">
        <div class="header">
            <h1 class="page-header">
                Dashboard <small>Welcome {{\Auth::user()->name}}</small>
            </h1>
            {{-- <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Data</li>
            </ol> --}}

        </div>
        <div id="page-inner">

            <!-- /. ROW  -->
        @yield('content')
        <!-- /. ROW  -->
            <!-- /. ROW  -->
            <footer>
                <p>All right reserved.: <a href="http://cybervision.com">CyberVision</a></p>


            </footer>

        </div>
        <!-- /. PAGE INNER  -->
    </div>

</div>

{{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}

 <script src="{{ asset('assets/js/jquery/jquery-1.12.4.js') }}"></script>
 <script src="{{ asset('assets/js/jquery-ui/jquery-ui.js') }}"></script>
<!-- Bootstrap Js -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

<!-- Metis Menu Js -->
<script src="{{ asset('assets/js/jquery.metisMenu.js') }}"></script>
<!-- Morris Chart Js -->
<script src="{{ asset('assets/js/morris/raphael-2.1.0.min.js') }}"></script>


<script src="{{ asset('assets/js/easypiechart.js') }}"></script>
<script src="{{ asset('assets/js/easypiechart-data.js') }}"></script>

<script src="{{ asset('assets/js/Lightweight-Chart/jquery.chart.js') }}"></script>

<script src="{{ asset('assets/js/dataTables-1.10.15/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/js/dataTables-1.10.15/dataTables.bootstrap.min.js') }}"></script>
<!-- Custom Js -->
<script src="{{ asset('assets/js/custom-scripts.js') }}"></script>
<script src="{{ asset('assets/js/select2.full.min.js') }}"></script>

<script src="{{ asset('assets/js/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>


<!-- Chart Js -->
@yield('pagespecificscripts')

<script>
    $(".readRemander").click(function (e) {
        var locId = $(this).data("id") // will return the number 123

        $.ajax({
            type: 'GET',
            url: '/readremainder/' + locId,
            cache: false,
            dataType: 'html',
            data: {
                id: locId,
                _token: '{{csrf_token()}}'
            },
            success: function (res) {

            }
        });
    });

    $(".is_numeric").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            Swal.fire({
                title: 'Error!',
                text: 'Only numbers allowed.',
                icon: 'error',
                confirmButtonText: 'OK'
            })
            return false;
        }
    });

    @if(!checkTCAccepted())
    (async () => {
        const { value: accept } = await Swal.fire({
            title: 'Terms and conditions',
            input: 'checkbox',
            inputValue: 1,
            html: '<?php echo str_replace(array("\r", "\n"), '', addslashes(getTermsHtml()->content)); ?>',
            width: '50em',
            allowOutsideClick: false,
            allowEscapeKey: false,
            customClass: "terms-popup",
            inputPlaceholder:
                'Yes (I make this declaration and freely consent on behalf of the staff of organization (National & International Staff and Volunteers of this organization) by providing their personal detail on the web-application and I am the authorized persons/entity acting on behalf of my organization',
            confirmButtonText:
                'Continue <i class="fa fa-arrow-right"></i>',
            inputValidator: (result) => {
                return !result && 'You need to agree with T&C'
            }
        })

        if (accept) {
            $.ajax({
                url: 'accept-terms/<?php echo \Auth::user()->id ?>',
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": <?php echo \Auth::user()->id ?>
                }
            });
        }
    })()

    @endif

</script>

</body>

</html>
