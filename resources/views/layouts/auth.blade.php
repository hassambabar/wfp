<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->


    <!-- Styles -->


    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/login/fonts/icomoon/style.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/login/css/owl.carousel.min.css') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/login/css/bootstrap.min.css') }}">
    
    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('assets/login/css/style.css') }}">
  


</head>
<body  style="background: url({{ asset('/assets/img/PDMA-BANNER.jpg') }}) no-repeat center center fixed;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;background-position: 100% 80%;">

    <div class="content">
        <div class="container">
          <div class="row justify-content-center">
            <!-- <div class="col-md-6 order-md-2">
              <img src="images/undraw_file_sync_ot38.svg" alt="Image" class="img-fluid">
            </div> -->
            <div class="col-md-6 contents">
              <div class="row justify-content-center">
                <div class="col-md-12">
                  <div class="form-block">
                     


                    <main class="py-4">
                        
                        @yield('content')
                    </main>


                   
                  </div>
                </div>
              </div>
              
            </div>
            
          </div>
        </div>
      </div>

      
      <script src="{{ asset('assets/login/js/jquery-3.3.1.min.js') }}"></script>
      <script src="{{ asset('assets/login/js/popper.min.js') }}"></script>
      <script src="{{ asset('assets/login/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('assets/login/js/main.js') }}"></script>

    
</body>
</html>
