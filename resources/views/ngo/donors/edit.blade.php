@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br/>
                    @endif
                    <form action="{{route('donors.update',$project->id)}}" method="POST">
                        @method('PATCH')
                        @csrf

                        <div class="form-group col-md-6">
                            <label for="funds_donor_name">Donor's Name</label>
                            <input type="text" class="form-control" id="funds_donor_name" placeholder="Donor's Name" value="{{old('funds_donor_name',$project->funds_donor_name)}}" name="funds_donor_name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="funds_amount">Donation/Project Amount</label>
                            <input type="text" class="form-control is_numeric" id="funds_amount" placeholder="Donation/Project Amount" value="{{old('funds_amount',$project->funds_amount)}}" name="funds_amount">
                        </div>

                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
