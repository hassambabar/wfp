@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
                {!! displayMessage() !!}
            @endif
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Donors Information
                </div>
                <div class="panel-body">
                    @if(isset($ngoProfile) && $ngoProfile == null)
                        <div>
                            You need to complete your profile first. Click <a href="{{url('/profile/create')}}">here</a> to fill in your details.
                        </div>
                    @else
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="donors">
                                <thead>
                                <tr>
                                    <th>Donor's Name</th>
                                    <th>Project Title</th>
                                    <th>Donation/Project Amount</th>
                                    <th>Project Start Date</th>
                                    <th>Project End Date</th>
                                    <th>Province</th>
                                    <th>District</th>
                                    {{--<th>Actions</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($projects as $key => $item)
                                    <tr class="odd gradeX">
                                        <td>{{$item->funds_donor_name}}</td>
                                        <td>{{$item->project_title}}</td>
                                        <td>{{$item->funds_amount}}</td>
                                        <td>{{$item->start_date}}</td>
                                        <td>{{$item->end_date}}</td>
                                        <td>{{$item->province ? $item->province->name : ' -- '}}</td>
                                        <td>
                                            <?php
                                            $projectDistricts = [];
                                            if(isset($projectAreas) && !$projectAreas->isEmpty()){
                                                $districts = $projectAreas->map(function ($item, $key){
                                                    return $item->district;
                                                });
                                                $projectDistricts = $districts->pluck('name')->toArray();
                                            }
                                            ?>
                                            {{isset($projectDistricts) && count($projectDistricts) > 0 ? implode(', ',$projectDistricts) : ' -- '}}
                                        </td>
                                        {{--<td>
                                            @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                                                <a href="{{ route('donors.edit', $item->id) }}">Edit</a>
                                            @endif
                                        </td>--}}
                                    </tr>
                                @empty
                                    <tr class="gradeU">
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        {{--<td>-</td>--}}
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>

@endsection
@section('pagespecificscripts')
    <script src="{{ asset('assets/js/filterDropDown.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#donors').dataTable({
                // Definition of filter to display
                filterDropDown: {
                    columns: [
                        {
                            idx: 5
                        },
                    ],
                    bootstrap: true
                },
                "columnDefs": [
                    {
                        "targets": [ 5 ],
                        "visible": false,
                    },
                    {
                        "targets": [ 6 ],
                        "visible": false
                    }
                ]
            } );
        });
    </script>
@endsection
