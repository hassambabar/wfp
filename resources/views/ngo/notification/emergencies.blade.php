@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="panel panel-default">
            <div class="panel-heading">
                Emergencies Notifications
            </div>
            <div class="panel-body">
                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif


                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Type Emergency</th>
                                <th>Type Of Hazard</th>
                                <th>District</th>
                                <th>Tehsil - UC</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($EmergencyItems as $key => $item)
                            <tr class="odd gradeX">
                                <td>{{$item->emergency->type_of_emergency}}</td>
                                <td>{{$item->emergency->type_of_hazard}}</td>
                                <td>{{$item->emergency->district->name}}</td>
                                <td>{{$item->emergency->tehsil->name}} - {{$item->emergency->Uc->name}}</td>
                                {{-- <td>{{$item->Needassesmentresponse}}</td> --}}
                                <td>@if($item->Needassesmentresponse) Completed @else Pending @endif</td>
                                <td>@if($item->Needassesmentresponse) <a href="{{ url("/need-assesment/{$item->id}") }}">View Response</a> @else <a href="{{ url("/need-assesment/{$item->id}") }}">Respond Now</a> @endif </td>
                            </tr>
                            @empty
                            <tr class="gradeU">
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                            @endforelse



                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>

@endsection

@section('pagespecificscripts')
<script>
       $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
@endsection
