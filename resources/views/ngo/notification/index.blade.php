@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
                {!! displayMessage() !!}
            @endif
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">Notifications</div>
                <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="notification-table">
                                <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Created at</th>
                                    <th>Read at</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(auth()->user()->notifications as $notification)
                                    @if(checkUserStatus() && $notification->type == 'App\Notifications\EmergenciesCreated')
                                    <tr class="odd gradeX">
                                        <td>Emergency</td>
                                        <td><em>{{\Carbon\Carbon::parse($notification->created_at)->diffForhumans()}}</em></td>
                                        <td>
                                            <em>
                                                {{($notification->read_at != NULL)?
                                                    (\Carbon\Carbon::parse($notification->read_at)->diffForhumans()):
                                                    'Un Read'
                                                }}
                                            </em>
                                        </td>

                                        <td>
                                        @if ($notification->read_at == NULL)
                                            @if($notification->type == 'App\Notifications\EmergenciesCreated')
                                                <?php $id_em = $notification->data['emergency']['id']; ?>
                                                <a href="{{url("need-assesment/$id_em")}}"  class="readRemander" data-id="{{$notification->id}}">
                                                    View</a>
                                            @endif
                                         @endif
                                        </td>
                                    </tr>
                                    @elseif($notification->type !== 'App\Notifications\EmergenciesCreated')
                                        <tr class="odd gradeX">
                                            <td>
                                                @if ( $notification->type == 'App\Notifications\NgoProfileReviewByPdma')
                                                    NGO Profile Review
                                                @elseif ( $notification->type == 'App\Notifications\PdmaEmpAssignmentProfile')
                                                    Profile review Assigned by PDMA
                                                    @elseif ( $notification->type == 'App\Notifications\AnnouncementCreated')
                                                    Announcement 
                                                    @elseif ( $notification->type == 'App\Notifications\NeedAssesmentResponseUpdated')
                                                Need Assesment Response Updated
                                                @elseif ( $notification->type == 'App\Notifications\ProfileApproved')
                                                Profile Approved
                                                    @else
                                                    {{basename($notification->type)}}
                                                @endif
                                            </td>
                                            <td><em>{{\Carbon\Carbon::parse($notification->created_at)->diffForhumans()}}</em></td>
                                            <td>
                                                <em>
                                                    {{($notification->read_at != NULL)?
                                                        (\Carbon\Carbon::parse($notification->read_at)->diffForhumans()):
                                                        'Un Read'
                                                    }}
                                                </em>
                                            </td>
                                            <td>
                                                @if($notification->type == 'App\Notifications\ProfileApproval')
                                                    <a class="readRemander" href="{{url('admin/notifications/profileApproval/'.$notification->data['profile']['id'])}}">
                                                        View</a>
                                                @endif
                                                @if($notification->type == 'App\Notifications\PdmaEmpAssignmentProfile')
                                                    <a class="readRemander" href="{{url('admin/notifications/profileApproval/'.$notification->data['ngoProfile']['user_id'])}}">
                                                        View
                                                    </a>
                                                @endif
                                                @if($notification->read_at == NULL && $notification->type == 'App\Notifications\NgoProfileReviewByPdma')
                                                    <a data-notify-decision="{{$notification->data['user']['pdma_profile_review_decision']}}"
                                                       data-notify-comment="{{$notification->data['user']['pdma_profile_review_comment']}}"
                                                       class="quickRead" href="javascript:void(0)">View</a>
                                                @endif

                                                    @if($notification->read_at == NULL && $notification->type == 'App\Notifications\ProjectNocReviewByPdma')

                                                        <?php
//                                                        dd($notification->data['noc']['project_id']);
                                                            $pid = $notification->data['noc']['project_id'];
//                                                            dd($pid);
                                                            $project = \App\Models\NgoProjects::where('id',$pid)->first();
                                                        ?>
                                                        <a data-notify-decision="{{$notification->data['user']['pdma_noc_review_decision']}}"
                                                           data-notify-comment="{{$notification->data['user']['pdma_noc_review_comment']}}"
                                                           data-notify-item="{{$project ? $project->project_title.' - ID: '.$project->id : ''}}"
                                                           class="quickRead" href="javascript:void(0)">View</a>
                                                    @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>

@endsection
@section('pagespecificscripts')
    <script src="{{ asset('assets/js/filterDropDown.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#notification-table').dataTable();
            $('a.quickRead').on('click',function (){
                let decisionText = $(this).attr('data-notify-decision');
                let commentText = $(this).attr('data-notify-comment');
                let itemtext = $(this).attr('data-notify-item');
                Swal.fire({
                    title: decisionText.toUpperCase(),
                    text: itemtext ? 'For '+itemtext+' '+commentText : commentText,
                    icon: 'info',
                    confirmButtonText: 'OK'
                })
            });
        });
    </script>
@endsection
