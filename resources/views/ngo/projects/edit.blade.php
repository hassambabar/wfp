@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{-- <div class="card-title">
                        <div class="title">Basic example</div>
                    </div> --}}
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form action="{{route('projects.update', $project->id)}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf
                        @if(checkUserStatus() == true)
                            <input type="hidden" name="noc_apply" value="0" />
                        @endif
                        <div class="form-group col-md-12">
                            <h4>i. Details of Organization Thematic Areas (select appropriate)</h4>
                            <div class="form-group col-md-6">
                                <label for="thematic_area">Thematic Areas *</label>
                                <select id="thematic_area" name="thematic_area[]" class="form-control" multiple required>
                                    @foreach ($thematicAreas as $tarea)
                                        <option @if (in_array($tarea->id,$projectThematicAreas)) selected="selected" @endif value="{{$tarea->id}}">{{$tarea->area_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <h4>ii. Details of Complete/Ongoing Project</h4>

                            <div class="form-group col-md-6">
                                <label for="project_category">Context *</label>
                                <select id="project_category" name="project_category" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($projectCategories as $projCat)
                                        <option @if ($project->project_category == $projCat->id) selected="selected" @endif value="{{$projCat->id}}">{{$projCat->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="donor">Donor/Funding Organization *</label>
                                <select id="donor" name="donor" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($fundingAgency as $agency)
                                        <option @if ($project->donor == $agency->id) selected="selected" @endif value="{{$agency->id}}">{{$agency->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_code">Project Code *</label>
                                <input type="text" class="form-control" required id="project_code" placeholder="Project Code" value="{{$project->project_code}}"  name="project_code">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_modality">Project Modality *</label>
                                <select id="project_modality" name="project_modality" required class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($modalities as $modality)
                                        <option @if ($project->project_modality == $modality->id) selected="selected" @endif value="{{$modality->id}}">{{$modality->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_title">Project Title *</label>
                                <input type="text" class="form-control" required id="project_title" placeholder="Project Title" value="{{$project->project_title}}"  name="project_title">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_owner_name">Project Owner *</label>
                                <input type="text" class="form-control" required id="project_owner_name" placeholder="Project Owner" value="{{$project->project_owner_name}}"  name="project_owner_name">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_start_date">Project Start Date *</label>
                                <input type="text" class="form-control datepicker" required id="project_start_date" autocomplete="off" placeholder="Project Start Date" value="{{$project->start_date}}"  name="start_date">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_end_date">Project End Date *</label>
                                <input type="text" class="form-control datepicker" required id="project_end_date" autocomplete="off" placeholder="Project End Date" value="{{$project->end_date}}"  name="end_date">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_owner_org_type">Project Owner Organization Type *</label>
                                <input type="text" class="form-control" required id="project_owner_org_type" placeholder="Project Owner Organization Type" value="{{$project->project_owner_org_type}}"  name="project_owner_org_type">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="implementing_partner">Implementing Partner *</label>
                                <select id="implementing_partner" required name="implementing_partner" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($implementingPartner as $imPartner)
                                        <option @if ($project->implementing_partner == $imPartner->id) selected="selected" @endif value="{{$imPartner->id}}">{{$imPartner->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="implementing_org_type">Implementing Organization Type *</label>
                                <select id="implementing_org_type" name="implementing_org_type" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($implementingOrgType as $imOrgType)
                                        <option @if ($project->implementing_org_type == $imOrgType->id) selected="selected" @endif value="{{$imOrgType->id}}">{{$imOrgType->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_sector">Sector / Cluster *</label>
                                <select id="project_sector" required name="project_sector" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($projectSector as $sector)
                                        <option @if ($project->project_sector == $sector->id) selected="selected" @endif value="{{$sector->id}}">{{$sector->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_province">Project Province *</label>
                                <select id="project_province" required name="project_province" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($Province as $item)
                                        <option @if ($project->project_province == $item->id) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="project_district">Project District *</label>
                                <select id="project_district" required name="project_district[]" multiple class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($District as $item)
                                        <option @if (in_array($item->id,$areaDistricts)) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_tehsil">Project Tehsil *</label>
                                <select id="project_tehsil" required name="project_tehsil[]" multiple class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($Tehsil as $item)
                                        <option data-district="{{$item->district_id}}" @if (in_array($item->id,$areaTehsils)) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="project_uc">Project Union Council *</label>
                                <select id="project_uc" required name="project_uc[]" multiple class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($ucs as $item)
                                        <option data-tehsil="{{$item->tehsil_id}}" @if (in_array($item->id,$areaUcs)) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_village">Project Village</label>
                                <input type="text" class="form-control" id="project_village" placeholder="Project Village" value="{{$project->project_village}}"  name="project_village">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <hr class="style1">
                        </div>
                        <div class="form-group col-md-12 activities">
                             @if ($project->activities->count() > 0)
                                @foreach ($project->activities as $akey => $activity)

                                    <div class="activity">
                                        <div class="col-md-12 row-head">
                                            <h5>Activity : {{$akey+1}}</h5>
                                            <input type="hidden" class="form-control activity_id_1" id="activity_id_1" placeholder="Activity" value="{{$activity->id}}"  name="activity[id][]">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="project_activity_1">Activity *</label>
                                            <input required type="text" class="form-control" id="project_activity_1" placeholder="Activity" value="{{$activity->project_activity}}"  name="activity[project_activity][]">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="project_description_1">Activity Description *</label>
                                            <input required type="text" class="form-control" id="project_description_1" placeholder="Activity Description" value="{{$activity->project_description}}"  name="activity[project_description][]">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="activity_start_date_1">Activity Start Date *</label>
                                            <input required type="text" class="form-control datepicker" autocomplete="off" id="" placeholder="Activity Start Date" value="{{$activity->activity_start_date}}"  name="activity[activity_start_date][]">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="activity_end_date_1">Activity End Date *</label>
                                            <input required type="text" class="form-control datepicker" autocomplete="off" id="" placeholder="Activity End Date" value="{{$activity->activity_end_date}}"  name="activity[activity_end_date][]">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="month_assistance_1">Month of Assistance *</label>
                                            <input type="text" required class="form-control" autocomplete="off" id="month_assistance_1" placeholder="Month of Assistance" value="{{$activity->month_assistance}}"  name="activity[month_assistance][]">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="activity_province_1">Activity Province *</label>
                                            <select required id="activity_province_1" name="activity[activity_province][]" class="form-control">
                                                <option value="">Select</option>
                                                @foreach ($Province as $item)
                                                    <option {{($activity->activity_province == $item->id)?'selected="selected"':''}} value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="activity_district_1">Activity District *</label>
                                            <select required id="activity_district_1" name="activity[activity_district][]" class="form-control">
                                                <option value="">Select</option>
                                                @foreach ($District as $item)
                                                    <option {{($activity->activity_district == $item->id)?'selected="selected"':''}} value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="activity_tehsil_1">Activity Tehsil *</label>
                                            <select required id="activity_tehsil_1" name="activity[activity_tehsil][]" class="form-control">
                                                <option value="">Select</option>
                                                @foreach ($Tehsil as $item)
                                                    <option {{($activity->activity_tehsil == $item->id)?'selected="selected"':''}} value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="activity_uc_1">Activity Union Council *</label>
                                            <select required id="activity_uc_1" name="activity[activity_uc][]" class="form-control">
                                                <option value="">Select</option>
                                                @foreach ($ucs as $item)
                                                    <option {{($activity->activity_uc == $item->id)?'selected="selected"':''}} value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="activity_village_1">Activity Village *</label>
                                            <input required type="text" class="form-control" id="activity_village_1" placeholder="Activity Village" value="{{$activity->activity_village}}"  name="activity[activity_village][]">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="latitude">Latitude</label>
                                            <input type="text" class="form-control" autocomplete="off" id="latitude" placeholder="Latitude" value="{{$activity->latitude}}"  name="activity[latitude][]">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="longitude">Longitude</label>
                                            <input type="text" class="form-control" autocomplete="off" id="longitude" placeholder="Longitude" value="{{$activity->longitude}}"  name="activity[longitude][]">
                                        </div>
                                    </div>

                             @endforeach
                             @else
                                <div class="activity">
                                <div class="col-md-12 row-head">
                                    <h5>Activity 1:</h5>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="project_activity_1">Activity *</label>
                                    <input required type="text" class="form-control" id="project_activity_1" placeholder="Activity" value=""  name="activity[project_activity][]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="project_description_1">Activity Description *</label>
                                    <input required type="text" class="form-control" id="project_description_1" placeholder="Activity Description" value=""  name="activity[project_description][]">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_start_date_1">Activity Start Date *</label>
                                    <input required type="text" class="form-control datepicker" autocomplete="off" id="" placeholder="Activity Start Date" value=""  name="activity[activity_start_date][]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="activity_end_date_1">Activity End Date *</label>
                                    <input required type="text" class="form-control datepicker" autocomplete="off" id="" placeholder="Activity End Date" value=""  name="activity[activity_end_date][]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="month_assistance_1">Month of Assistance *</label>
                                    <input type="text" required class="form-control" autocomplete="off" id="month_assistance_1" placeholder="Month of Assistance" value=""  name="activity[month_assistance][]">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_province_1">Activity Province *</label>
                                    <select required id="activity_province_1" name="activity[activity_province][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($Province as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="activity_district_1">Activity District *</label>
                                    <select required id="activity_district_1" name="activity[activity_district][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($District as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_tehsil_1">Activity Tehsil *</label>
                                    <select required id="activity_tehsil_1" name="activity[activity_tehsil][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($Tehsil as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="activity_uc_1">Activity Union Council *</label>
                                    <select required id="activity_uc_1" name="activity[activity_uc][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($ucs as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_village_1">Activity Village *</label>
                                    <input required type="text" class="form-control" id="activity_village_1" placeholder="Activity Village" value=""  name="activity[activity_village][]">
                                </div>

                                    <div class="form-group col-md-6">
                                        <label for="latitude">Latitude</label>
                                        <input type="text" class="form-control" autocomplete="off" id="latitude" placeholder="Latitude" value=""  name="activity[latitude][]">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="longitude">Longitude</label>
                                        <input type="text" class="form-control" autocomplete="off" id="longitude" placeholder="Longitude" value=""  name="activity[longitude][]">
                                    </div>
                            </div>

                             @endif

                            <div class="add-more-activity">
                                <a href="javascript:void(0)" class="btn btn-primary">Add Activity</a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <hr class="style1">
                        </div>

                        <div class="form-group col-md-12">
                            <h5><b><u>Activity</u></b></h5>
                            <div class="form-group col-md-6">
                                <label for="target_number">Target (Number)</label>
                                <input type="number" class="form-control is_numeric" id="target_number" placeholder="Target (Number)" value="{{$project->target_number}}" name="target_number">
                            </div>
                            @if(checkUserStatus() == false)
                                <div class="form-group col-md-6">
                                    <label for="achieved_number">Achieved (Number)</label>
                                    <input type="number" class="form-control is_numeric" id="achieved_number" placeholder="Achieved (Number)" value="{{$project->achieved_number}}" name="achieved_number">
                                </div>
                            @endif

                            <div class="form-group col-md-6">
                                <label for="activity_unit">Activity Unit</label>
                                <input type="text" class="form-control" id="activity_unit" placeholder="Activity Unit" value="{{$project->activity_unit}}" name="activity_unit">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="assist_type">Assistance Type and Description</label>
                                <input type="text" class="form-control" id="assist_type" placeholder="Assistance Type and Description" value="{{$project->assist_type}}" name="assist_type">
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <h5><b><u>Beneficiary Targeted</u></b></h5>
                            <div class="form-group col-md-6">
                                <label for="families_targeted">Families</label>
                                <input type="number" class="form-control is_numeric" id="families_targeted" placeholder="Families" value="{{$project->families_targeted}}" name="families_targeted">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="women_targeted">Women</label>
                                <input type="number" class="form-control is_numeric" id="women_targeted" placeholder="Women" value="{{$project->women_targeted}}" name="women_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="men_targeted">Men</label>
                                <input type="number" class="form-control is_numeric" id="men_targeted" placeholder="Men" value="{{$project->men_targeted}}" name="men_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="girls_targeted">Girls</label>
                                <input type="number" class="form-control is_numeric" id="girls_targeted" placeholder="Girls" value="{{$project->girls_targeted}}" name="girls_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="boys_targeted">Boys</label>
                                <input type="number" class="form-control is_numeric" id="boys_targeted" placeholder="Boys" value="{{$project->boys_targeted}}" name="boys_targeted">
                            </div>
                        </div>
                        @if(checkUserStatus() == false)
                            <div class="form-group col-md-12">
                                <h5><b><u>Beneficiary Reached</u></b></h5>
                                <div class="form-group col-md-6">
                                    <label for="female_headed">Female Headed Household</label>
                                    <input type="number" class="form-control is_numeric" id="female_headed" placeholder="Female Headed Household" value="{{$project->female_headed}}" name="female_headed">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="families_reached">Families</label>
                                    <input type="number" class="form-control is_numeric" id="families_reached" placeholder="Families" value="{{$project->families_reached}}" name="families_reached">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="women_reached">Women</label>
                                    <input type="number" class="form-control is_numeric" id="women_reached" placeholder="Women" value="{{$project->women_reached}}" name="women_reached">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="men_reached">Men</label>
                                    <input type="number" class="form-control is_numeric" id="men_reached" placeholder="Men" value="{{$project->men_reached}}" name="men_reached">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="girls_reached">Girls</label>
                                    <input type="number" class="form-control is_numeric" id="girls_reached" placeholder="Girls" value="{{$project->girls_reached}}" name="girls_reached">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="boys_reached">Boys</label>
                                    <input type="number" class="form-control is_numeric" id="boys_reached" placeholder="Boys" value="{{$project->boys_reached}}" name="boys_reached">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="with_disabilities">Persons/Families with disabilities</label>
                                    <input type="text" class="form-control" id="with_disabilities" placeholder="Persons/Families with disabilities" value="{{$project->with_disabilities}}" name="with_disabilities">
                                </div>
                            </div>
                        @endif

                        <div class="form-group col-md-12">
                            <h5><b><u>Contacts</u></b></h5>
                            <div class="form-group col-md-6">
                                <label for="contact_person">Contact Person</label>
                                <input type="text" class="form-control" id="contact_person" placeholder="Contact Person" value="{{$project->contact_person}}" name="contact_person">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="contact_number">Contact Number</label>
                                <input type="text" class="form-control is_numeric" id="contact_number" placeholder="Contact Number" value="{{$project->contact_number}}" name="contact_number">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="contact_email">Email</label>
                                <input type="email" class="form-control" id="contact_email" placeholder="Email" value="{{$project->contact_email}}" name="contact_email">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="comments">Comments</label>
                                <input type="text" class="form-control" id="comments" placeholder="Comments" value="{{$project->comments}}" name="comments">
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <h4>Source of Funds</h4>
                            <div class="form-group col-md-6">
                                <label for="source_of_funds">Upload Donor Agreement/MoU/Acknowledgement Letter</label>
                                <input type="file" class="form-control" id="source_of_funds" name="source_of_funds">
                                @if($project->source_of_funds)
                                    <a target="_blank" href="{{ url($project->source_of_funds) }}">View File</a>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="funds_donor_name">Donor's Name *</label>
                                <input required type="text" class="form-control" id="funds_donor_name" placeholder="Donor's Name" value="{{$project->funds_donor_name}}" name="funds_donor_name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="funds_amount">Donation/Project Amount *</label>
                                <input required type="text" class="form-control is_numeric" id="funds_amount" placeholder="Donation/Project Amount" value="{{$project->funds_amount}}" name="funds_amount">
                            </div>
                        </div>
                        @if(checkUserStatus() == false)
                        <div class="form-group col-md-12">
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="project_complete" name="project_status" value="completed" {{ $project->project_status == 'completed' ? 'checked' : '' }}>
                                <label for="project_complete">
                                    Complete
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="project_ongoing" name="project_status" value="ongoing" {{ $project->project_status == 'ongoing' ? 'checked' : '' }}>
                                <label for="project_ongoing">
                                    Ongoing
                                </label>
                            </div>
                        </div>
                        @endif
                        @if(checkUserStatus() == true)
                        <div class="form-group col-md-12">
                            <label>Extension for Previous Projects *</label>
                            <div class="radio3 radio-check radio-inline">
                                <input required type="radio" id="ext_yes" name="prev_extension" value="1" {{ $project->prev_extension == 1 ? 'checked' : '' }}>
                                <label for="ext_yes">
                                    Yes
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input required type="radio" id="ext_no" name="prev_extension" value="0" {{ $project->prev_extension == 0 ? 'checked' : '' }}>
                                <label for="ext_no">
                                    No
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input required type="radio" id="not_applicable" name="prev_extension" value="-1" {{ $project->prev_extension == -1 ? 'checked' : '' }}>
                                <label for="not_applicable">
                                    Not Applicable
                                </label>
                            </div>
                        </div>
                        @endif

                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-default triggerSubmit">Update</button>
                            @if(checkUserStatus() == true)
                                <a href="javascript:void(0)" class="btn btn-default noc-apply">Apply for NOC</a>
                            @endif
                            <a href="{{url('projects')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
    <script>
        $( function() {
            function hideTehsil(value) {
                let distArr = [];
                $.each($('#project_district').select2('data'), function (idx, item){
                    distArr.push(item.id);
                })
                if($.inArray( String($(value.element).data('district')), distArr ) !== -1){
                    return $('<span>' + value.text + '</span>');
                }
            }
            function hideUC(value) {
                let tehsilArr = [];
                $.each($('#project_tehsil').select2('data'), function (idx, item){
                    tehsilArr.push(item.id);
                })
                if($.inArray( String($(value.element).data('tehsil')), tehsilArr ) !== -1){
                    return $('<span>' + value.text + '</span>');
                }
            }
            $('#thematic_area').select2();
            var project_district = $('#project_district').select2();
            var project_tehsil = $('#project_tehsil').select2({
                templateResult: hideTehsil,
            });

            $('#project_uc').select2({
                templateResult: hideUC,
            });
            $(document).on('focus', '.datepicker', function() {
                $(this).datepicker();
            });
            // $( ".datepicker" ).datepicker();
            $('.add-more-activity a').on('click', function () {
                let cloneActivity = $('.activities .activity:first-child').clone();
                let totalActivity = $('.activities .activity').length;
                $(cloneActivity).find('.row-head h5').html('Activity '+( totalActivity+ 1)+':');
                $(cloneActivity).find('.activity_id_1').remove();
                $(cloneActivity).find('input.datepicker').removeClass('hasDatepicker');
                $(cloneActivity).find('input.datepicker').removeAttr('id');

                $(cloneActivity).insertBefore('.activities .add-more-activity');
            });
            $('.noc-apply').on('click', function () {
                $('input[name="noc_apply"]').val(1);
                $('button.triggerSubmit').click();
            });

            var MAX_FILE_SIZE = 5 * 1024 * 1024; // 5MB
            $('.uploadFileInput').change(function() {
                console.log(' uploadFileInput change ', this.files);
                if(this.files.length && this.files[0].size > MAX_FILE_SIZE){
                    this.setCustomValidity("File must not exceed 5 MB!");
                    this.reportValidity();
                    // this.value = this.defaultValue;
                }else {
                    this.setCustomValidity("");
                }

            });

            <?php
            if($project->project_district){
            ?>
            // district id
            /*var id = $('#project_district').val();

            // AJAX request
            $.ajax({
                url: '/getTehsils/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    var len = 0;
                    if(response != null){
                        len = response.length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        let current_tehsil = <?php echo $project->project_tehsil; ?>;
                        // Empty the dropdown
                        $('#project_tehsil').find('option').not(':first').remove();
                        $('#project_uc').find('option').not(':first').remove();
                        for(var i=0; i<len; i++){
                            var id = response[i].id;
                            var name = response[i].name;
                            if(id == current_tehsil){
                                var option = "<option selected value='"+id+"'>"+name+"</option>";
                            }else{
                                var option = "<option value='"+id+"'>"+name+"</option>";
                            }
                            $("#project_tehsil").append(option);
                        }
                    }
                }
            });*/

            <?php } ?>

            <?php
            if($project->project_tehsil){
            ?>
            // district id
            /*var tehsil_id = $('#project_tehsil').val();
            // Empty the dropdown
            $('#project_uc').find('option').not(':first').remove();
            // AJAX request
            $.ajax({
                url: '/getUcs/'+tehsil_id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    var len = 0;
                    if(response != null){
                        len = response.length;
                    }
                    if(len > 0){
                        let current_uc = <?php echo $project->project_uc; ?>;
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response[i].id;
                            var name = response[i].name;
                            if(id == current_uc){
                                var option = "<option selected value='"+id+"'>"+name+"</option>";
                            }else {
                                var option = "<option value='"+id+"'>"+name+"</option>";
                            }

                            $("#project_uc").append(option);
                        }
                    }
                }
            });*/

            <?php } ?>

            /*$('#project_district').change(function(){
                // district id
                var id = $(this).val();
                // Empty the dropdown
                $('#project_tehsil').find('option').not(':first').remove();
                $('#project_uc').find('option').not(':first').remove();
                // AJAX request
                $.ajax({
                    url: '/getTehsils/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response != null){
                            len = response.length;
                        }
                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++){
                                var id = response[i].id;
                                var name = response[i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#project_tehsil").append(option);
                            }
                        }
                    }
                });
            });

            $('#project_tehsil').change(function(){
                // tehsil id
                var id = $(this).val();
                // Empty the dropdown
                $('#project_uc').find('option').not(':first').remove();
                // AJAX request
                $.ajax({
                    url: '/getUcs/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        var len = 0;
                        if(response != null){
                            len = response.length;
                        }
                        if(len > 0){
                            // Read data and create <option >
                            for(var i=0; i<len; i++){
                                var id = response[i].id;
                                var name = response[i].name;
                                var option = "<option value='"+id+"'>"+name+"</option>";
                                $("#project_uc").append(option);
                            }
                        }
                    }
                });
            });*/
        });
    </script>
    <script>
        // Disable form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {

                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                            Swal.fire({
                                title: 'Error!',
                                text: 'Please fill all required fields and then submit.',
                                icon: 'error',
                                confirmButtonText: 'OK'
                            })
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    <link href="{{ asset('assets/css/checkbox3.min.css') }}" rel="stylesheet" >
    <style>
        .select2-container{
            height: 34px;
        }
    </style>
@endsection
