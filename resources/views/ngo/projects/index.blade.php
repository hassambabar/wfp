@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
        @if (session()->has('message'))
            {!! displayMessage() !!}
        @endif
        <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Projects
                </div>
                <div class="panel-body">
                    @if(isset($ngoProfile) && $ngoProfile == null)
                        <div>
                            You need to complete your profile first. Click <a href="{{url('/profile/create')}}">here</a>
                            to fill in your details.
                        </div>
                    @else

                        <div class="row">
                            <div class="col-md-3">
                                <select name="cars" id="thematicAreas" class="form-control">
                                    <option value="">--Select Thematic Areas--</option>

                                    @foreach ($thematicAreas  as $key => $item)
                                        <option value="{!!$item->area_name!!}">{!!$item->area_name!!}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="cars" id="District" class="form-control">
                                    <option value="">--Select District--</option>

                                    @foreach ($District  as $key => $item)
                                        <option value="{!!$item->name!!}">{!!$item->name!!}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="cars" id="Tehsil" class="form-control">
                                    <option value="">--Select Tehsil--</option>

                                    @foreach ($Tehsil  as $key => $item)
                                        <option value="{!!$item->name!!}">{!!$item->name!!}</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="col-md-3">
                                <select name="cars" id="projectSector" class="form-control">
                                    <option value="">--Select Sector--</option>

                                    @foreach ($projectSector  as $key => $item)
                                        <option value="{!!$item->title!!}">{!!$item->title!!}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    <br>
                        <div class="row">
                            <div class="col-md-3">
                                <select name="project_status" id="projectStatus" class="form-control">
                                    <option value="">--Select Project Status--</option>
                                    <option value="ongoing">On-Going</option>
                                    <option value="completed">Completed</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <select name="status" id="status" class="form-control">
                                    <option value="">--Select Status--</option>
                                    <option value="Published">Published</option>
                                    <option value="Draft">Draft</option>

                                </select>
                            </div>
                        </div>
                        <br>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="projects-table">
                                <thead>
                                <tr>
                                    <th>Project ID</th>
                                    <th>Project Title</th>
                                    <th>Thematic Areas</th>
                                    <th>District</th>
                                    <th>Tehsil</th>
                                    <th>Sector</th>

                                    <th>Extension for Previous NOC</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Location</th>
                                    <th>NOC Status</th>
                                    <th>Project Status</th>
                                    <th>Actions</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($projects as $key => $item)
                                    @php $projectAreas = $item->projectAreas; @endphp
                                    @php $projectThematicAreas = $item->projectThematicAreas; @endphp
                                    <tr class="odd gradeX">
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->project_title ? $item->project_title : ' -- '}}</td>
                                        <td>
                                            <?php
                                            $projectThematicAreasList = [];
                                            if(isset($projectThematicAreas) && !$projectThematicAreas->isEmpty()){
                                                $thematicArea = $projectThematicAreas->map(function ($item, $key){
                                                    return $item->thematicArea;
                                                });
                                                $projectThematicAreasList = $thematicArea->pluck('area_name')->toArray();
                                            }
                                            ?>
                                            {{isset($projectThematicAreasList) && count($projectThematicAreasList) > 0 ? implode(', ',$projectThematicAreasList) : ' -- '}}

                                        </td>
                                        <td>
                                            <?php
                                                $projectDistricts = [];
                                                if(isset($projectAreas) && !$projectAreas->isEmpty()){
                                                    $districts = $projectAreas->map(function ($item, $key){
                                                        return $item->district;
                                                    });
                                                    $projectDistricts = $districts->pluck('name')->toArray();
                                                }
                                            ?>
                                            {{isset($projectDistricts) && count($projectDistricts) > 0 ? implode(', ',$projectDistricts) : ' -- '}}
                                        </td>
                                        <td>
                                            <?php
                                            $projectTehsils = [];
                                            if(isset($projectAreas) && !$projectAreas->isEmpty()){
                                                $tehsils = $projectAreas->map(function ($item, $key){
                                                    return $item->tehsil;
                                                });
                                                $projectTehsils = $tehsils->pluck('name')->toArray();
                                            }
                                            ?>
                                            {{isset($projectTehsils) && count($projectTehsils) > 0 ? implode(', ',$projectTehsils) : ' -- '}}
                                        </td>
                                        <td>{{$item->projectSector ? $item->projectSector->title : ' -- '}}</td>
                                        <td>{{$item->prev_extension == 1 ? 'Yes' : ($item->prev_extension == 0 ? 'No' : ($item->prev_extension == -1 ? 'Not Applicable' : ''))}}</td>
                                        <td>{{$item->start_date}}</td>
                                        <td>{{$item->end_date}}</td>
                                        <td>{{$item->province ? $item->province->name : ' -- '}}</td>
                                        <td>{{$item->noc()->first() ? $item->noc()->first()->status : ''}}</td>
                                        <td>{{ucfirst($item->project_status)}}</td>
                                        <td>
                                            <a href="{{ route('projects.show', $item->id) }}">View</a>
                                            @if((checkProfileApproval() && checkEnableEdit())
                                                    || (checkNocApproval() && checkNocEnableEdit($item->id))
                                                    || $ngoProfile->user->status === 0
                                            )
                                                <a href="{{ route('projects.edit', $item->id) }}">- Edit -</a>
                                            @endif
                                            @if($ngoProfile->user->status === 0 )
                                                - <a href="{{ url("/project/{$item->id}/delete") }}">Delete</a>
                                            @endif
                                        </td>
                                        <td>
                                            {{$item->status == '1' ? 'Published' : 'Draft'}}
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="gradeU">
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
@endsection
@section('pagespecificscripts')
    <script src="{{ asset('assets/js/filterDropDown.js') }}"></script>
    <script>

        oTableContact = $('#projects-table').DataTable({
            "columnDefs": [
                {
                    "targets": [12],
                    "visible": false
                }
            ]
        });

        // $('.name_search').keyup(function(){
        //     oTableContact.search($(this).val()).draw() ;
        // })


        // $(document).ready(function () {
        //     $('#projects-table').dataTable({
        //         // Definition of filter to display
        //         filterDropDown: {
        //             columns: [
        //                 {
        //                     idx: 2
        //                 },
        //                 {
        //                     idx: 6
        //                 },
        //                 {
        //                     idx: 7
        //                 }
        //             ],
        //             bootstrap: true
        //         }
        //     } );
        // });

        $('#thematicAreas').change(function () {
            oTableContact.column('2').search($(this).val()).draw();
        });
        $('#District').change(function () {
            oTableContact.column('3').search($(this).val()).draw();
        });
        $('#Tehsil').change(function () {
            oTableContact.column('4').search($(this).val()).draw();
        });
        $('#projectSector').change(function () {
            oTableContact.column('5').search($(this).val()).draw();
        });
        $('#projectStatus').change(function () {
            oTableContact.column('11').search($(this).val()).draw();
        });
        $('#status').change(function () {
            oTableContact.column('13').search($(this).val()).draw();
        });


    </script>
@endsection
