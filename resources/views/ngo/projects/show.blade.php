@extends('layouts.app')
{{--@dd($project->category_id)--}}
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-6">
                        <h3>Details</h3>
                    </div>
                    @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                        <div class="col-md-6 text-right">
                            <a href="{{route('projects.edit', $project->id)}}">Edit Details</a>
                        </div>
                    @endif
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <p><b>Thematic Areas:</b>
                                @if(is_array($thematicAreas) && count($thematicAreas) > 0)
                                    {{implode(',',$thematicAreas)}}
                                @else
                                {{' -- '}}
                                @endif
                               {{-- {{$project->thematicArea ? $project->thematicArea->area_name : ' -- '}}--}}
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Context:</b> {{$project->category ? $project->category->category_name : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Donor/Funding Organization:</b> {{$project->fundingAgency ? $project->fundingAgency->name : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Code:</b> {{$project->project_code ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Modality:</b> {{$project->modality ? $project->modality->title : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Title:</b> {{$project->project_title ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Owner:</b> {{$project->project_owner_name ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Start Date:</b> {{$project->start_date ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project End Date:</b> {{$project->end_date ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Owner Organization Type:</b> {{$project->project_owner_org_type ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Implementing Partner:</b> {{$project->implementingPartner ? $project->implementingPartner->title : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Implementing Organization Type:</b> {{$project->implementingOrgType ? $project->implementingOrgType->title : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Sector / Cluster:</b> {{$project->projectSector ? $project->projectSector->title : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Province:</b> {{$project->province ? $project->province->name : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Districts:</b>
                                @if(is_array($areaDistricts) && count($areaDistricts) > 0)
                                    {{implode(',',$areaDistricts)}}
                                @else
                                    {{' -- '}}
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Tehsil:</b>
                                @if(is_array($areaTehsils) && count($areaTehsils) > 0)
                                    {{implode(',',$areaTehsils)}}</p>
                            @else
                                {{' -- '}}
                            @endif
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Union Council:</b>
                                @if(is_array($areaUcs) && count($areaUcs) > 0)
                                    {{implode(',',$areaUcs)}}</p>
                            @else
                                {{' -- '}}
                            @endif
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Village:</b> {{$project->project_village ? : ' -- '}}</p>
                        </div>

                        @if ($project->activities->count() > 0)
                            @foreach ($project->activities as $akey => $activity)
                                <div class="activity">
                                    <div class="col-md-12 row-head">
                                        <hr>
                                        <h4>Activity : {{$akey+1}}</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity:</b> {{$activity->project_activity ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Description:</b> {{$activity->project_description ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Start Date:</b> {{$activity->activity_start_date ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity End Date:</b> {{$activity->activity_end_date ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Month of Assistance:</b> {{$activity->month_assistance ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Province:</b> {{$activity->province ? $activity->province->name : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity District:</b> {{$activity->district ? $activity->district->name : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Tehsil:</b> {{$activity->tehsil ? $activity->tehsil->name : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Union Council:</b> {{$activity->uc ? $activity->uc->name : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Latitude:</b> {{$activity->latitude ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Longitude:</b> {{$activity->longitude ?  : ' -- '}}</p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-12 row-head">
                            <h4>Activity</h4>
                        </div>
                        <div class="col-md-4">
                            <p><b>Target (Number):</b> {{$project->target_number ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Achieved (Number):</b> {{$project->achieved_number ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Activity Unit:</b> {{$project->activity_unit ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Assistance Type and Description:</b> {{$project->assist_type ?  : ' -- '}}</p>
                        </div>

                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-12 row-head">
                            <h4>Beneficiary Targeted</h4>
                        </div>
                        <div class="col-md-4">
                            <p><b>Families:</b> {{$project->families_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Women:</b> {{$project->women_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Men:</b> {{$project->men_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Girls:</b> {{$project->girls_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Boys:</b> {{$project->boys_targeted ?  : ' -- '}}</p>
                        </div>

                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-12 row-head">
                            <h4>Beneficiary Reached</h4>
                        </div>
                        <div class="col-md-4">
                            <p><b>Female Headed Household:</b> {{$project->female_headed ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Families:</b> {{$project->families_reached ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Women:</b> {{$project->women_reached ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Men:</b> {{$project->men_reached ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Girls:</b> {{$project->girls_reached ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Boys:</b> {{$project->boys_reached ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Persons/Families with disabilities:</b> {{$project->with_disabilities ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-12 row-head">
                            <h4>Contacts</h4>
                        </div>
                        <div class="col-md-4">
                            <p><b>Contact Person:</b> {{$project->contact_person ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Contact Number:</b> {{$project->contact_number ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Email:</b> {{$project->contact_email ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Comments:</b> {{$project->comments ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-12 row-head">
                            <h4>Source of Funds</h4>
                        </div>
                        <div class="col-md-4">
                            <p>
                                <b>Donor Agreement/MoU/Acknowledgement Letter:</b>
                                @if($project->source_of_funds)
                                    <a target="_blank" href="{{ url($project->source_of_funds) }}">View File</a>
                                @else
                                    {{' -- '}}
                                @endif
                            </p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Donor's Name:</b> {{$project->funds_donor_name ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Donation/Project Amount:</b> {{$project->funds_amount ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-12 row-head">
                            <hr>
                        </div>


                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <a href="{{url('/projects')}}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
