@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
                {!! displayMessage() !!}
            @endif
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    NOC Requests
                </div>
                <div class="panel-body">
                    @if(isset($ngoProfile) && $ngoProfile == null)
                        <div>
                            You need to complete your profile first. Click <a href="{{url('/profile/create')}}">here</a> to fill in your details.
                        </div>
                    @else
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="requests-table">
                                <thead>
                                <tr>
                                    <th>Project Title</th>
                                    <th>Previous availability NOC status</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Location</th>
                                    <th>NOC Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($projects as $key => $item)
                                    <tr class="odd gradeX">
                                        <td>{{$item->project_title}}</td>
                                        <td>{{$item->prev_extension == 1 ? 'Yes' : ($item->prev_extension == 0 ? 'No' : ($item->prev_extension == -1 ? 'Not Applicable' : ''))}}</td>
                                        <td>{{$item->start_date}}</td>
                                        <td>{{$item->end_date}}</td>
                                        <td>{{$item->province->name}}</td>
                                        <td>{{$item->noc()->first() ? $item->noc()->first()->status : ''}}</td>
                                    </tr>
                                @empty
                                    <tr class="gradeU">
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>

@endsection
@section('pagespecificscripts')
    <script src="{{ asset('assets/js/filterDropDown.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#requests-table').dataTable();
        });
    </script>
@endsection
