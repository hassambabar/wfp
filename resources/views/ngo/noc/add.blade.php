@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{-- <div class="card-title">
                        <div class="title">Basic example</div>
                    </div> --}}
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif

                        @if (session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    <form action="{{route('projects.store')}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="submit_type" value="" />
                        <input type="hidden" name="apply_for_noc" value="1" />
                        @if(checkUserStatus() == true)
                            <input type="hidden" name="noc_apply" value="1" />
                        @endif
                        <div class="form-group col-md-12">
                            <h4>i. Details of Organization Thematic Areas (select appropriate)</h4>
                            <div class="form-group col-md-6">
                                <label for="thematic_area">Thematic Areas *</label>
                                <select id="thematic_area" name="thematic_area" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($thematicAreas as $tarea)
                                        <option @if (old('thematic_area') == $tarea->id) selected="selected" @endif value="{{$tarea->id}}">{{$tarea->area_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <h4>ii. Details of Current/Ongoing Project</h4>

                            <div class="form-group col-md-6">
                                <label for="project_category">Context *</label>
                                <select id="project_category" name="project_category" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($projectCategories as $projCat)
                                        <option @if (old('project_category') == $projCat->id) selected="selected" @endif value="{{$projCat->id}}">{{$projCat->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="donor">Donor/Funding Organization *</label>
                                <select id="donor" name="donor" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($fundingAgency as $agency)
                                        <option @if (old('donor') == $agency->id) selected="selected" @endif value="{{$agency->id}}">{{$agency->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_code">Project Code *</label>
                                <input type="text" required class="form-control" id="project_code" placeholder="Project Code" value="{{old('project_code')}}"  name="project_code">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_modality">Project Modality *</label>
                                <select id="project_modality" name="project_modality" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($modalities as $modality)
                                        <option @if (old('project_modality') == $modality->id) selected="selected" @endif value="{{$modality->id}}">{{$modality->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_title">Project Title *</label>
                                <input type="text" required class="form-control" id="project_title" placeholder="Project Title" value="{{old('project_title')}}"  name="project_title">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_owner_name">Project Owner</label>
                                <input type="text" class="form-control" id="project_owner_name" placeholder="Project Owner" value="{{old('project_owner_name')}}"  name="project_owner_name">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_start_date">Project Start Date *</label>
                                <input type="text" required class="form-control datepicker" id="project_start_date" autocomplete="off" placeholder="Project Start Date" value="{{old('start_date')}}"  name="start_date">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_end_date">Project End Date *</label>
                                <input type="text" required class="form-control datepicker" id="project_end_date" autocomplete="off" placeholder="Project End Date" value="{{old('end_date')}}"  name="end_date">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_owner_org_type">Project Owner Organization Type *</label>
                                <input type="text" class="form-control" required id="project_owner_org_type" placeholder="Project Owner Organization Type" value="{{old('project_owner_org_type')}}"  name="project_owner_org_type">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="implementing_partner">Implementing Partner *</label>
                                <select id="implementing_partner" name="implementing_partner" required class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($implementingPartner as $imPartner)
                                        <option @if (old('implementing_partner') == $imPartner->id) selected="selected" @endif value="{{$imPartner->id}}">{{$imPartner->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="implementing_org_type">Implementing Organization Type *</label>
                                <select id="implementing_org_type" name="implementing_org_type" required class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($implementingOrgType as $imOrgType)
                                        <option @if (old('implementing_org_type') == $imOrgType->id) selected="selected" @endif value="{{$imOrgType->id}}">{{$imOrgType->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_sector">Sector / Cluster *</label>
                                <select id="project_sector" name="project_sector" required class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($projectSector as $sector)
                                        <option @if (old('project_sector') == $sector->id) selected="selected" @endif value="{{$sector->id}}">{{$sector->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_province">Project Province *</label>
                                <select id="project_province" name="project_province" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($Province as $item)
                                        <option @if (old('project_province') == $item->id) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="project_district">Project District *</label>
                                <select id="project_district" name="project_district" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($District as $item)
                                        <option @if (old('project_district') == $item->id) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_tehsil">Project Taluka</label>
                                <select id="project_tehsil" required name="project_tehsil" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($Tehsil as $item)
                                        <option @if (old('project_tehsil') == $item->id) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="project_uc">Project Union Council</label>
                                <select id="project_uc" required name="project_uc" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($ucs as $item)
                                        <option @if (old('project_uc') == $item->id) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_village">Project Village</label>
                                <input type="text" class="form-control" id="project_village" placeholder="Project Village" value="{{old('project_village')}}"  name="project_village">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <hr class="style1">
                        </div>
                        <div class="form-group col-md-12 activities">
                            <div class="activity">
                                <div class="col-md-12 row-head">
                                    <h5>Activity 1:</h5>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="project_activity_1">Activity *</label>
                                    <input type="text" required class="form-control" id="project_activity_1" placeholder="Activity" value=""  name="activity[project_activity][]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="project_description_1">Activity Description *</label>
                                    <input type="text" required class="form-control" id="project_description_1" placeholder="Activity Description" value=""  name="activity[project_description][]">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_start_date_1">Activity Start Date *</label>
                                    <input type="text" required class="form-control datepicker" autocomplete="off" id="" placeholder="Activity Start Date" value=""  name="activity[activity_start_date][]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="activity_end_date_1">Activity End Date *</label>
                                    <input type="text" required class="form-control datepicker" autocomplete="off" id="" placeholder="Activity End Date" value=""  name="activity[activity_end_date][]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="month_assistance_1">Month of Assistance *</label>
                                    <input type="text" required class="form-control" autocomplete="off" id="month_assistance_1" placeholder="Month of Assistance" value=""  name="activity[month_assistance][]">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_province_1">Activity Province *</label>
                                    <select id="activity_province_1" required name="activity[activity_province][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($Province as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="activity_district_1">Activity District *</label>
                                    <select id="activity_district_1" required name="activity[activity_district][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($District as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_tehsil_1">Activity Taluka *</label>
                                    <select id="activity_tehsil_1" required name="activity[activity_tehsil][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($Tehsil as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="activity_uc_1">Activity Union Council *</label>
                                    <select id="activity_uc_1" name="activity[activity_uc][]" required class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($ucs as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_village_1">Activity Village *</label>
                                    <input type="text" required class="form-control" id="activity_village_1" placeholder="Activity Village" value=""  name="activity[activity_village][]">
                                </div>
                            </div>

                            <div class="add-more-activity">
                                <a href="javascript:void(0)" class="btn btn-primary">Add Activity</a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <hr class="style1">
                        </div>

                        <div class="form-group col-md-12">
                            <h5><b><u>Activity</u></b></h5>
                            <div class="form-group col-md-6">
                                <label for="target_number">Target (Number)</label>
                                <input type="number" class="form-control is_numeric" id="target_number" placeholder="Target (Number)" value="{{old('target_number')}}" name="target_number">
                            </div>
                            @if(checkUserStatus() == false)
                                <div class="form-group col-md-6">
                                    <label for="achieved_number">Achieved (Number)</label>
                                    <input type="number" class="form-control is_numeric" id="achieved_number" placeholder="Achieved (Number)" value="{{old('achieved_number')}}" name="achieved_number">
                                </div>
                            @endif

                            <div class="form-group col-md-6">
                                <label for="activity_unit">Activity Unit</label>
                                <input type="text" class="form-control" id="activity_unit" placeholder="Activity Unit" value="{{old('activity_unit')}}" name="activity_unit">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="assist_type">Assistance Type and Description</label>
                                <input type="text" class="form-control" id="assist_type" placeholder="Assistance Type and Description" value="{{old('assist_type')}}" name="assist_type">
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <h5><b><u>Beneficiary Targeted</u></b></h5>
                            <div class="form-group col-md-6">
                                <label for="families_targeted">Families</label>
                                <input type="number" class="form-control is_numeric" id="families_targeted" placeholder="Families" value="{{old('families_targeted')}}" name="families_targeted">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="women_targeted">Women</label>
                                <input type="number" class="form-control is_numeric" id="women_targeted" placeholder="Women" value="{{old('women_targeted')}}" name="women_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="men_targeted">Men</label>
                                <input type="number" class="form-control is_numeric" id="men_targeted" placeholder="Men" value="{{old('men_targeted')}}" name="men_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="girls_targeted">Girls</label>
                                <input type="number" class="form-control is_numeric" id="girls_targeted" placeholder="Girls" value="{{old('girls_targeted')}}" name="girls_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="boys_targeted">Boys</label>
                                <input type="number" class="form-control is_numeric" id="boys_targeted" placeholder="Boys" value="{{old('boys_targeted')}}" name="boys_targeted">
                            </div>
                        </div>
                        @if(checkUserStatus() == false)
                            <div class="form-group col-md-12">
                                <h5><b><u>Beneficiary Reached</u></b></h5>
                                <div class="form-group col-md-6">
                                    <label for="female_headed">Female Headed Household</label>
                                    <input type="number" class="form-control is_numeric" id="female_headed" placeholder="Female Headed Household" value="{{old('female_headed')}}" name="female_headed">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="families_reached">Families</label>
                                    <input type="number" class="form-control is_numeric" id="families_reached" placeholder="Families" value="{{old('families_reached')}}" name="families_reached">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="women_reached">Women</label>
                                    <input type="number" class="form-control is_numeric" id="women_reached" placeholder="Women" value="{{old('women_reached')}}" name="women_reached">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="men_reached">Men</label>
                                    <input type="number" class="form-control is_numeric" id="men_reached" placeholder="Men" value="{{old('men_reached')}}" name="men_reached">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="girls_reached">Girls</label>
                                    <input type="number" class="form-control is_numeric" id="girls_reached" placeholder="Girls" value="{{old('girls_reached')}}" name="girls_reached">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="boys_reached">Boys</label>
                                    <input type="number" class="form-control is_numeric" id="boys_reached" placeholder="Boys" value="{{old('boys_reached')}}" name="boys_reached">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="with_disabilities">Persons/Families with disabilities</label>
                                    <input type="text" class="form-control" id="with_disabilities" placeholder="Persons/Families with disabilities" value="{{old('with_disabilities')}}" name="with_disabilities">
                                </div>
                            </div>
                        @endif

                        <div class="form-group col-md-12">
                            <h5><b><u>Contacts</u></b></h5>
                            <div class="form-group col-md-6">
                                <label for="contact_person">Contact Person</label>
                                <input type="text" class="form-control" id="contact_person" placeholder="Contact Person" value="{{old('contact_person')}}" name="contact_person">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="contact_number">Contact Number</label>
                                <input type="text" class="form-control is_numeric" id="contact_number" placeholder="Contact Number" value="{{old('contact_number')}}" name="contact_number">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="contact_email">Email</label>
                                <input type="email" class="form-control" id="contact_email" placeholder="Email" value="{{old('contact_email')}}" name="contact_email">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="comments">Comments</label>
                                <input type="text" class="form-control" id="comments" placeholder="Comments" value="{{old('comments')}}" name="comments">
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <h4>Source of Funds</h4>
                            <div class="form-group col-md-6">
                                <label for="source_of_funds">Upload Donor Agreement/MoU/Acknowledgement Letter</label>
                                <input type="file" class="form-control uploadFileInput" id="source_of_funds" name="source_of_funds">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="funds_donor_name">Donor's Name *</label>
                                <input type="text" required class="form-control" id="funds_donor_name" placeholder="Donor's Name" value="{{old('funds_donor_name')}}" name="funds_donor_name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="funds_amount">Donation/Project Amount *</label>
                                <input type="text" required class="form-control is_numeric" id="funds_amount" placeholder="Donation/Project Amount" value="{{old('funds_amount')}}" name="funds_amount">
                            </div>
                        </div>
                        @if(checkUserStatus() == false)
                        <div class="form-group col-md-12">
                            <h4>Project Status</h4>
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="project_complete" name="project_status" value="completed" {{ old('project_status') == 'completed' ? 'checked' : '' }}>
                                <label for="project_complete">
                                    Complete
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="project_ongoing" name="project_status" value="ongoing" {{ old('project_status') == 'ongoing' ? 'checked' : '' }}>
                                <label for="project_ongoing">
                                    Ongoing
                                </label>
                            </div>
                        </div>
                        @endif
                        @if(checkUserStatus() == true)
                        <div class="form-group col-md-12">
                            <label>Extension for Previous Projects *</label>
                            <div class="radio3 radio-check radio-inline">
                                <input required type="radio" id="ext_yes" name="prev_extension" value="1" {{ old('prev_extension') == 1 ? 'checked' : '' }}>
                                <label for="ext_yes">
                                    Yes
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input required type="radio" id="ext_no" name="prev_extension" value="0" {{ old('prev_extension') == 0 ? 'checked' : '' }}>
                                <label for="ext_no">
                                    No
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input required type="radio" id="not_applicable" name="prev_extension" value="-1" {{ old('prev_extension') == -1 ? 'checked' : '' }}>
                                <label for="not_applicable">
                                    Not Applicable
                                </label>
                            </div>
                        </div>
                        @endif

                        {{--<div class="form-group col-md-12">
                            <button type="submit" class="btn btn-default triggerSubmit">Submit</button>
                            @if(checkUserStatus() == true)
                                <a href="javascript:void(0)" class="btn btn-default noc-apply">Apply for NOC</a>
                            @endif
                            <a href="{{url('projects')}}" class="btn btn-default">Cancel</a>
                        </div>--}}

                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-default triggerSubmit">Save</button>
                            {{--@if(checkUserStatus() == true)
                                <a href="javascript:void(0)" class="btn btn-default noc-apply">Apply for NOC</a>
                            @endif--}}
                            <a id="save_new" href="javascript:void(0)" class="btn btn-primary">Save & New</a>
                            <a href="{{url('noc-requests')}}" class="btn btn-danger">Cancel</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
    <script>
        $( function() {
            $(document).on('click', '#save_new', function (){
                $('input[name="submit_type"]').val('save_new');
                $('button.triggerSubmit').click();
            });
            $(document).on('focus', '.datepicker', function() {
                $(this).datepicker();
            });
            // $( ".datepicker" ).datepicker();
            $('.add-more-activity a').on('click', function () {
                let cloneActivity = $('.activities .activity:first-child').clone();
                let totalActivity = $('.activities .activity').length;
                $(cloneActivity).find('.row-head h5').html('Activity '+( totalActivity+ 1)+':');
                $(cloneActivity).find('input.datepicker').removeClass('hasDatepicker');
                $(cloneActivity).find('input.datepicker').removeAttr('id');

                $(cloneActivity).insertBefore('.activities .add-more-activity');
                // $( ".datepicker" ).datepicker();
            });
            $('.noc-apply').on('click', function () {
                $('input[name="noc_apply"]').val(1);
                $('button.triggerSubmit').click();
            });

            var MAX_FILE_SIZE = 5 * 1024 * 1024; // 5MB
            $('.uploadFileInput').change(function() {
                console.log(' uploadFileInput change ', this.files);
                if(this.files.length && this.files[0].size > MAX_FILE_SIZE){
                    this.setCustomValidity("File must not exceed 5 MB!");
                    this.reportValidity();
                    // this.value = this.defaultValue;
                }else {
                    this.setCustomValidity("");
                }

            });


        });
    </script>
    <script>
        // Disable form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {

                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                            Swal.fire({
                                title: 'Error!',
                                text: 'Please fill all required fields and then submit.',
                                icon: 'error',
                                confirmButtonText: 'OK'
                            })
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    <link href="{{ asset('assets/css/checkbox3.min.css') }}" rel="stylesheet" >
@endsection
