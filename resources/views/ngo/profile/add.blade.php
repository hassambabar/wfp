@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            @if (session()->has('message'))
                {!! displayMessage() !!}
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{-- <div class="card-title">
                        <div class="title">Basic example</div>
                    </div> --}}
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form action="{{route('profile.store')}}" method="POST" class="needs-validation" enctype="multipart/form-data" novalidate>
                        @csrf

                        <div class="form-group col-md-12">
                            <h4>Basic Details of Organization</h4>
                            <div class="form-group col-md-6">
                                <label for="acronym">Abbreviation/Acronym *</label>
                                <input type="text" class="form-control" id="acronym" placeholder="Abbreviation/Acronym" value="{{old('acronym')}}" required name="acronym">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="org_full_name">Full Name of Organization *</label>
                                <input type="text" class="form-control" autocomplete="off" id="org_full_name" required placeholder="Full Name of Organization" value="{{old('org_full_name')}}"  name="org_full_name">
                            </div>
                        </div>

                        <div class="form-group col-md-12 legal_status">
                            <h4>Type of Organization *</h4>
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="pk_org" name="legal_status" value="pk_org" {{ old('legal_status') == 'pk_org' ? 'checked' : 'checked' }}>
                                <label for="pk_org">
                                    For Pakistani Organizations
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="corp_firm" name="legal_status" value="corp_firm" {{ old('legal_status') == 'corp_firm' ? 'checked' : '' }}>
                                <label for="corp_firm">
                                    Corporate Firm
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="pk_charity" name="legal_status" value="pk_charity" {{ old('legal_status') == 'pk_charity' ? 'checked' : '' }}>
                                <label for="pk_charity">
                                    Pakistani Charity Org
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="intl_org" name="legal_status" value="intl_org" {{ old('legal_status') == 'intl_org' ? 'checked' : '' }}>
                                <label for="intl_org">
                                    International Organizations Only
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="un_org" name="legal_status" value="un_org" {{ old('legal_status') == 'un_org' ? 'checked' : '' }}>
                                <label for="un_org">
                                    UN Organization Agency
                                </label>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <h4>Nature of Organization Work/Services</h4>
                            <div class="form-group col-md-6">
                                <label for="nature_of_org">Nature of Organization</label>
                                <select id="nature_of_org" name="nature_of_org[]" multiple class="form-control multiple-select">
                                    <option value="">Select</option>
                                    @foreach ($orgNatures as $nature)
                                        <option value="{{$nature->id}}">{{$nature->nature_of_org}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-12 legal_status_local">
                            <h4>Legal Status</h4>
                            <p class="hidden org_hq_inputs">i. Registration details in Pakistan</p>
                            <div class="form-group col-md-3">
                                <label for="reg_law">Registration Authority/Law *</label>
                                {{--<input type="text" required class="form-control" id="reg_law" placeholder="Registration Authority/Law" value="{{old('reg_law')}}"  name="reg_law">--}}
                                <select required id="reg_law" name="reg_law" class="form-control">
                                    <option value="">Select</option>
                                    @if($regLaws)
                                        @foreach($regLaws as $regLaw)
                                            <option
                                                value="{{old('reg_law',$regLaw->id)}}"
                                            >
                                                {{$regLaw->title}}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="reg_date">Date of Registration. *</label>
                                <input type="text" required class="form-control" autocomplete="off" id="reg_date" placeholder="Date of Registration" value="{{old('reg_date')}}"  name="reg_date">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="reg_no">Registration No</label>
                                <input type="text" class="form-control" id="reg_no" placeholder="Registration No" value="{{old('reg_no')}}"  name="reg_no">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="registration_certificate">Registration Certificate</label>
                                <input type="file" id="registration_certificate" class="form-control" name="registration_certificate">
                            </div>
                        </div>
                        <div class="form-group col-md-12 hidden org_hq_inputs">
                            <p>ii. Registration details at Head Quarters</p>
                            <div class="form-group col-md-3">
                                <label for="hq_origin_country">Country of Origin/Head Quarters</label>
                                <input type="text" class="form-control" id="hq_origin_country" placeholder="Country of Origin/Head Quarters" value="{{old('hq_origin_country')}}"  name="hq_origin_country">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="hq_reg_law">Registration Authority/Law</label>
                                <input type="text" class="form-control" id="hq_reg_law" placeholder="Registration Authority/Law" value="{{old('hq_reg_law')}}"  name="hq_reg_law">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="hq_reg_date">Date of Registration</label>
                                <input type="text" class="form-control" id="hq_reg_date" autocomplete="off" placeholder="Date of Registration" value="{{old('hq_reg_date')}}"  name="hq_reg_date">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="hq_reg_no">Registration No</label>
                                <input type="text" class="form-control" id="hq_reg_no" placeholder="Registration No" value="{{old('hq_reg_no')}}"  name="hq_reg_no">
                            </div>
                        </div>

                        <div class="form-group col-md-12 mou_radio">
                            <label>Do you have MOU with Department of Economic Affairs or Ministry of Interior? *</label>
                            <div class="radio3 radio-check radio-inline">
                                <input required type="radio" id="mou_yes" name="have_mou" value="1" {{ old('have_mou') == '1' ? 'checked' : '' }}>
                                <label for="mou_yes">
                                    Yes
                                </label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input required type="radio" id="mou_no" name="have_mou" value="0" {{ old('have_mou') == '0' ? 'checked' : 'checked' }}>
                                <label for="mou_no">
                                    No
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12 mou_fields hidden">
                            <div class="form-group col-md-6">
                                <label for="mou_status">MOU Status *</label>
                                <select name="mou_status" id="mou_status" class="form-control is-req">
                                    <option value="">Select Status</option>
                                    <option value="valid">Valid</option>
                                    <option value="invalid">Invalid</option>
                                    <option value="ack_letter">Acknowledgement Letter</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mou_file">MOU</label>
                                <input type="file" id="mou_file" class="form-control" name="mou_file">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mou_sign_date">MOU Sign Date *</label>
                                <input type="text" id="mou_sign_date" autocomplete="off" class="form-control is-req mou_signed_date" name="mou_signed_date" value="{{old('mou_signed_date')}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mou_exp_date">MOU Expiry Date *</label>
                                <input type="text" id="mou_exp_date" autocomplete="off" class="form-control is-req mou_expiry_date" name="mou_expiry_date" value="{{old('mou_expiry_date')}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="num_of_years">No of Years</label>
                                <input type="text" id="num_of_years" class="form-control is_numeric" name="num_of_years" value="{{old('num_of_years')}}">
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <h4>Goal/Vision/Mission</h4>
                            <div class="form-group">
                                <label for="ngo_goal">Goal / Vision Statement <i>(if applicable)</i></label>
                                <textarea class="form-control" cols="10" rows="6" name="ngo_goal" id="ngo_goal">{{old('ngo_goal')}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="mission_statement">Mission Statement <i>(if applicable)</i></label>
                                <textarea class="form-control" cols="10" rows="6" name="mission_statement" id="mission_statement">{{old('mission_statement')}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="objectives">Objectives <i>(if applicable)</i></label>
                                <textarea class="form-control" cols="10" rows="6" name="objectives" id="objectives">{{old('objectives')}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="profile">Add Organization Profile Information *</label>
                                <textarea required class="form-control" id="profile" cols="10" rows="6" name="profile">{{old('profile')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
    <script>
        $( function() {
            $('.multiple-select').select2();
            <?php
            if(old('legal_status') === 'un_org'){ ?>
            $('.legal_status_local').addClass('hidden');
            $('#reg_law, #reg_date').removeAttr('required');
            $('#hq_origin_country, #hq_reg_law, #hq_reg_date, #hq_reg_no').attr('required',true);
            <?php } ?>
            $( "#reg_date, #hq_reg_date, .mou_signed_date, .mou_expiry_date" ).datepicker();
            $('.legal_status input[type=radio]').change(function () {
                if($(this).val() === 'intl_org' || $(this).val() === 'un_org'){
                    $('.org_hq_inputs').removeClass('hidden');
                    if($(this).val() === 'un_org'){
                        $('.legal_status_local').addClass('hidden');
                        $('#reg_law, #reg_date').removeAttr('required');
                        $('#hq_origin_country, #hq_reg_law, #hq_reg_date, #hq_reg_no').attr('required',true);
                    }else{
                        $('.legal_status_local').removeClass('hidden');
                        $('#reg_law, #reg_date').attr('required',true);
                    }
                }else{
                    $('.org_hq_inputs').addClass('hidden');
                    $('.legal_status_local').removeClass('hidden');
                    $('#reg_law, #reg_date').attr('required',true);
                    $('#hq_origin_country, #hq_reg_law, #hq_reg_date, #hq_reg_no').removeAttr('required');
                }

            });

            $('.mou_radio input[type=radio]').change(function () {
                if($(this).val() === '1'){
                    $('.mou_fields').removeClass('hidden');
                    $('.is-req').attr("required", "true");
                }else{
                    $('.mou_fields').addClass('hidden');
                    $('.is-req').removeAttr("required");
                }
            });
            $('#mou_status').on('change', function (){
                if($(this).val() === 'ack_letter'){
                    $('#mou_exp_date').removeAttr('required');
                    // $('#mou_exp_date').parent().addClass('d-none');
                }else{
                    $('#mou_exp_date').attr('required');
                    // $('#mou_exp_date').parent().removeClass('d-none');
                }
            });
        });
    </script>
    <script>
        // Disable form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {

                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                            Swal.fire({
                                title: 'Error!',
                                text: 'Please fill all required fields and then submit.',
                                icon: 'error',
                                confirmButtonText: 'OK'
                            })
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    <link href="{{ asset('assets/css/checkbox3.min.css') }}" rel="stylesheet" >
@endsection
