@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
                {!! displayMessage() !!}
            @endif
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Profile</h3>
                        </div>
                        @if((checkProfileApproval() && checkEnableEdit()) || (isset($ngoProfile) && $ngoProfile->user->status === 0 ) )
                            <div class="col-md-6 text-right">
                                <a href="{{route('profile.edit',$ngoProfile->id)}}">Edit Details</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="panel-body">
                    @if(!isset($ngoProfile) && $ngoProfile === null)
                        <div>
                            You need to complete your profile first. Click <a href="{{url('/profile/create')}}">here</a> to fill in your details.
                        </div>
                    @else
                        <div class="col-md-12">
                            <h4>Basic Details of Organization</h4>
                            <div class="col-md-6">
                                <p><b>Organization Full Name:</b> {{$ngoProfile->org_full_name}}</p>
                            </div>
                            <div class="col-md-6">
                                <p><b>Abbreviation/Acronym:</b> {{$ngoProfile->acronym}}</p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4>Type of Organization</h4>
                            <p><b>Legal Status:</b> @php echo getLegalStatusName($ngoProfile->legal_status); @endphp</p>
                            <div class="row row-no-gutters">
                                @if($ngoProfile->legal_status !== 'pk_org' &&
                                    $ngoProfile->legal_status !== 'pk_charity' &&
                                    $ngoProfile->legal_status !== 'corp_firm'
                                )
                                <div class="col-md-12">
                                    <h4><b><u>Details in Pakistan</u></b></h4>
                                </div>
                                @endif
                                <div class="col-md-4">
                                    <p><b>Registration Law:</b> @php echo $ngoProfile->reg_law; @endphp</p>
                                </div>
                                <div class="col-md-4">
                                    <p><b>Date of Reg:</b> @php echo $ngoProfile->reg_date; @endphp</p>
                                </div>
                                <div class="col-md-4">
                                    <p><b>Reg No:</b> @php echo $ngoProfile->reg_no; @endphp</p>
                                </div>
                                @if($ngoProfile->legal_status !== 'pk_org' &&
                                    $ngoProfile->legal_status !== 'pk_charity' &&
                                    $ngoProfile->legal_status !== 'corp_firm'
                                )
                                    <div class="col-md-12">
                                        <h4><b><u>Details at Headquarters</u></b></h4>
                                    </div>
                                    <div class="col-md-4">
                                        <p><b>Country of Origin:</b> @php echo $ngoProfile->hq_origin_country; @endphp</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p><b>Registration Law:</b> @php echo $ngoProfile->hq_reg_law; @endphp</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p><b>Date of Reg:</b> @php echo $ngoProfile->hq_reg_date; @endphp</p>
                                    </div>
                                    <div class="col-md-4">
                                        <p><b>Reg No:</b> @php echo $ngoProfile->hq_reg_no; @endphp</p>
                                    </div>
                                @endif
                            </div>
                            <div class="row row-no-gutters">
                                <div class="col-md-4">
                                    <p><b>Registration Certificate:</b>
                                        @if($ngoProfile->registration_certificate)
                                            <a target="_blank" href="{{ url($ngoProfile->registration_certificate) }}">View File</a>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <p><b>Nature of Organizations:</b> @php echo isset($ngoOrgNatures) && count($ngoOrgNatures) > 0 ? implode(',',$ngoOrgNatures) : ''; @endphp</p>
                        </div>

                        @if(isset($mouData) && $mouData->count() > 0)
                            @php
                            $mouCount = 1;
                            @endphp
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <p>MOUs</p>
                                    </div>
                                    <div class="panel-body">
                                        <div class="panel-group" id="mou-accordion">
                                            @foreach($mouData as $mou)
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#mou-accordion" href="#mou_{{$mouCount}}">Mou {{$mouCount}}</a>
                                                        </h4>
                                                    </div>
                                                    <div id="mou_{{$mouCount}}" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="col-md-4">
                                                                <p><b>MOU Status:</b> @php echo $mou->mou_status; @endphp</p>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p><b>MOU Signed Date:</b> @php echo $mou->mou_signed_date; @endphp</p>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p><b>MOU Expiry Date:</b> @php echo $mou->mou_expiry_date; @endphp</p>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p><b>No of Years:</b> @php echo $mou->num_of_years; @endphp</p>
                                                            </div>
                                                            @if($mou->mou_file)
                                                            <div class="col-md-6">
                                                                {{-- Storage::disk('uploads_dir')->url($mou->mou_file) --}}
                                                                <p><b>MOU:</b>
                                                                        <a target="_blank" href="{{ url($mou->mou_file) }}">View File</a>
                                                                </p>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @php $mouCount++; @endphp
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="col-md-12">
                            <h4><b>Goal/Vision Statement</b></h4>
                            <p>{{$ngoProfile->ngo_goal}}</p>
                        </div>

                        <div class="col-md-12">
                            <h4><b>Mission Statement</b></h4>
                            <p>{{$ngoProfile->mission_statement}}</p>
                        </div>

                        <div class="col-md-12">
                            <h4><b>Objectives</b></h4>
                            <p>{{$ngoProfile->objectives}}</p>
                        </div>
                        <div class="col-md-12">
                            <h4><b>Organization Profile</b></h4>
                            <p>{{$ngoProfile->profile}}</p>
                        </div>
                    @endif
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>

@endsection
