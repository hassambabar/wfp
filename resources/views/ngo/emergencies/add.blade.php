@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{-- <div class="card-title">
                        <div class="title">Basic example</div>
                    </div> --}}
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                        @if (session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    <form action="{{route('past-emergencies.store')}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="submit_type" value="" />
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-6">
                                <label for="project_id">Project *</label>
                                <select required id="project_id" name="project_id" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($projects as $project)
                                        <option @if (old('project_id') == $project->id) selected="selected" @endif value="{{$project->id}}">{{$project->project_title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="year">Year *</label>
                                <select required id="year" name="year" class="form-control">
                                    <option value="">Select</option>
                                    @for ($i = date("Y", strtotime('-1 year')); $i >= date("Y", strtotime('-10 year')); $i-- )
                                        <option @if (old('year') == $i) selected="selected" @endif value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="type_of_emergency">Type of Emergency/Disaster *</label>
                                <select required id="type_of_emergency" name="type_of_emergency" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($disasters as $disaster)
                                        <option @if (old('type_of_emergency') == $disaster->id) selected="selected" @endif value="{{$disaster->id}}">{{$disaster->type}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="province">Province *</label>
                                <select required id="province" name="province" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($Province as $item)
                                        <option @if (old('province') == $item->id) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="district">District *</label>
                                <select required id="district" name="district" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($District as $item)
                                        <option @if (old('district') == $item->id) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="tehsil">Tehsil *</label>
                                <select required id="tehsil" name="tehsil" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($Tehsil as $item)
                                        <option @if (old('tehsil') == $item->id) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="uc">Union Council *</label>
                                <select required id="uc" name="uc" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($ucs as $item)
                                        <option @if (old('uc') == $item->id) selected="selected" @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="response_type">Type of Response</label>
                                <input type="text" class="form-control" id="response_type" placeholder="Type of Response" value="{{old('response_type')}}"  name="response_type">
                            </div>

                            <div class="form-group col-md-12">
                                <label for="village_details">Details of Village</label>
                                <textarea class="form-control" id="village_details" name="village_details" rows="6" cols=5>{{old('village_details')}}</textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="desc_deliverables">Description of Deliverables</label>
                                <input type="text" class="form-control" id="desc_deliverables" placeholder="Description of Deliverables" value="{{old('desc_deliverables')}}"  name="desc_deliverables">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="status">Status *</label>
                                <select required id="status" name="status" class="form-control">
                                    <option value="">Select</option>
                                    <option value="completed">Completed</option>
                                    <option value="in-progress">In-progress</option>
                                </select>
                                {{--<input required type="text" class="form-control" id="status" placeholder="Responded or Pending" value="{{old('status')}}"  name="status">--}}
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <button id="form_submit" type="submit" class="btn btn-default">Save</button>
                            <a id="save_new" href="javascript:void(0)" class="btn btn-primary">Save & New</a>
                            <a href="{{url('past-emergencies')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
    <script>
        $('#district').change(function(){
            // district id
            var id = $(this).val();
            // Empty the dropdown
            $('#tehsil').find('option').not(':first').remove();
            $('#uc').find('option').not(':first').remove();
            // AJAX request
            $.ajax({
                url: '/getTehsils/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    var len = 0;
                    if(response != null){
                        len = response.length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response[i].id;
                            var name = response[i].name;
                            var option = "<option value='"+id+"'>"+name+"</option>";
                            $("#tehsil").append(option);
                        }
                    }
                }
            });
        });

        $('#tehsil').change(function(){
            // tehsil id
            var id = $(this).val();
            // Empty the dropdown
            $('#uc').find('option').not(':first').remove();
            // AJAX request
            $.ajax({
                url: '/getUcs/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    var len = 0;
                    if(response != null){
                        len = response.length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response[i].id;
                            var name = response[i].name;
                            var option = "<option value='"+id+"'>"+name+"</option>";
                            $("#uc").append(option);
                        }
                    }
                }
            });
        });
        $('#save_new').on('click', function (){
            $('input[name="submit_type"]').val('save_new');
            $('#form_submit').click();
        });
        // Disable form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {

                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                            Swal.fire({
                                title: 'Error!',
                                text: 'Please fill all required fields and then submit.',
                                icon: 'error',
                                confirmButtonText: 'OK'
                            })
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
@endsection
