@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
                {!! displayMessage() !!}
            @endif
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Emergencies Responded (during past 10 years)
                </div>
                <div class="panel-body">
                    @if(isset($ngoProfile) && $ngoProfile == null)
                        <div>
                            You need to complete your profile first. Click <a href="{{url('/profile/create')}}">here</a> to fill in your details.
                        </div>
                    @else
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="emergencies-responded">
                                <thead>
                                <tr>
                                    <th>Project Title</th>
                                    <th>Year</th>
                                    <th>Type of Emergency</th>
                                    <th>Province</th>
                                    <th>District</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($pastEmergencies as $key => $item)
                                    <tr class="odd gradeX">
                                        <td>{{$item->project->project_title}}</td>
                                        <td>{{$item->year}}</td>
                                        <td>{{$item->disaster->type}}</td>
                                        <td>{{$item->provinces->name}}</td>
                                        <td>{{$item->districts->name}}</td>
                                        <td>{{$item->status}}</td>
                                        <td>
                                            <a href="{{ url("/past-emergency/detail/{$item->id}") }}">View</a>
                                            @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                                                <a href="{{ route('past-emergencies.edit', $item->id)}}"> - Edit - </a>
                                            @endif
                                            @if($ngoProfile->user->status === 0 )
                                                - <a href="{{ url("/past-emergencies/{$item->id}/delete") }}">Delete</a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="gradeU">
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>

@endsection
@section('pagespecificscripts')
    <script src="{{ asset('assets/js/filterDropDown.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#emergencies-responded').dataTable({
                // Definition of filter to display
                filterDropDown: {
                    columns: [
                        {
                            idx: 3
                        },
                        {
                            idx: 4
                        }
                    ],
                    bootstrap: true
                }
            } );
        });
    </script>
@endsection
