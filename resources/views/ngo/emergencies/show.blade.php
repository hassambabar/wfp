@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Emergency Responded</h3>
                        </div>
                        @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                            <div class="col-md-6 text-right">
                                <a href="{{route('past-emergencies.edit', $pastEmergency->id)}}">Edit Details</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <p><b>Project:</b> {{$pastEmergency->project->project_title}}</p>
                            </div>

                            <div class="col-md-6">
                                <p><b>Year:</b> {{$pastEmergency->year}}</p>
                            </div>

                            <div class="col-md-6">
                                <p><b>Type of Emergency/Disaster:</b> {{$pastEmergency->disaster->type}}</p>
                            </div>

                            <div class="col-md-6">
                                <p><b>Type of Emergency/Disaster:</b> {{$pastEmergency->provinces->name}}</p>
                            </div>
                            <div class="col-md-6">
                                <p><b>District:</b> {{$pastEmergency->districts->name}}</p>
                            </div>

                            <div class="col-md-6">
                                <p><b>Tehsil:</b> {{$pastEmergency->tehsils->name}}</p>
                            </div>
                            <div class="col-md-6">
                                <p><b>Union Council:</b> {{$pastEmergency->ucs->name}}</p>
                            </div>

                            <div class="col-md-6">
                                <p><b>Type of Response:</b> {{$pastEmergency->response_type}}</p>
                            </div>

                            <div class="col-md-6">
                                <p><b>Details of Village:</b> {{$pastEmergency->village_details}}</p>
                            </div>

                            <div class="col-md-6">
                                <p><b>Description of Deliverables:</b> {{$pastEmergency->desc_deliverables}}</p>
                            </div>

                            <div class="col-md-6">
                                <p><b>Status:</b> {{$pastEmergency->status}}</p>
                            </div>
                        </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <a href="{{url('/past-emergencies')}}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
