@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{-- <div class="card-title">
                        <div class="title">Basic example</div>
                    </div> --}}
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form action="{{route('mous.store')}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="ngo_id" value="{{$userId}}">
                        <div class="col-md-12 mou_fields">
                            <div class="form-group col-md-6">
                                <label for="mou_status">MOU Status *</label>
                                <select name="mou_status" id="mou_status" class="form-control" required>
                                    <option value="">Select Status</option>
                                    <option value="valid">Valid</option>
                                    <option value="invalid">Invalid</option>
                                    <option value="ack_letter">Acknowledgement Letter</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mou_file">MOU</label>
                                <input type="file" id="mou_file" class="form-control uploadFileInput" name="mou_file">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mou_sign_date">MOU Sign Date *</label>
                                <input required type="text" id="mou_sign_date" autocomplete="off" class="form-control mou_signed_date" name="mou_signed_date">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mou_exp_date">MOU Expiry Date *</label>
                                <input required type="text" id="mou_exp_date" autocomplete="off" class="form-control mou_expiry_date" name="mou_expiry_date">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="num_of_years">No of Years</label>
                                <input type="text" id="num_of_years" class="form-control is_numeric" name="num_of_years">
                            </div>

                        </div>

                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
    <script>
        $( function() {
            $( "#reg_date, #hq_reg_date, .mou_signed_date, .mou_expiry_date" ).datepicker();
            $('.legal_status input[type=radio]').change(function () {
                if($(this).val() === 'intl_org' || $(this).val() === 'un_org'){
                    $('.org_hq_inputs').removeClass('hidden');
                }else{
                    $('.org_hq_inputs').addClass('hidden');
                }

            });

            $('.mou_radio input[type=radio]').change(function () {
                if($(this).val() === '1'){
                    $('.mou_fields').removeClass('hidden');
                }else{
                    $('.mou_fields').addClass('hidden');
                }
            });

            $('#mou_status').on('change', function (){
                if($(this).val() === 'ack_letter'){
                    $('#mou_exp_date').removeAttr('required');
                    // $('#mou_exp_date').parent().addClass('d-none');
                }else{
                    $('#mou_exp_date').attr('required');
                    // $('#mou_exp_date').parent().removeClass('d-none');
                }
            });


            var MAX_FILE_SIZE = 5 * 1024 * 1024; // 5MB
            $('.uploadFileInput').change(function() {
                if(this.files.length && this.files[0].size > MAX_FILE_SIZE){
                    this.setCustomValidity("File must not exceed 5 MB!");
                    this.reportValidity();
                }else {
                    this.setCustomValidity("");
                }
            });

        });
    </script>
    <script>
        // Disable form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {

                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                            Swal.fire({
                                title: 'Error!',
                                text: 'Please fill all required fields and then submit.',
                                icon: 'error',
                                confirmButtonText: 'OK'
                            })
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    <link href="{{ asset('assets/css/checkbox3.min.css') }}" rel="stylesheet" >
@endsection
