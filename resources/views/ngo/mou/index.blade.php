@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
        @if (session()->has('message'))
            {!! displayMessage() !!}
        @endif
        <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    MOU for {{$ngoProfile->user->name}}
                </div>
                <div class="panel-body">
                    @if(isset($ngoProfile) && $ngoProfile == null)
                        <div>
                            You need to complete/create your profile first. Click <a href="{{url('/profile/create')}}">here</a> to fill in your details.
                        </div>
                    @else
                        @if(isset($mouData) && $mouData->count() > 0)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    MOUs
                                </div>
                                <div class="panel-body">
                                    <div class="panel-group" id="mou-accordion">
                                        @foreach($mouData as $key=>$mou)
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#mou-accordion" href="#mou_{{$key+1}}">Mou {{$key+1}}</a>
                                                        @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                                                            <a class="pull-right" href="{{ route('mous.edit', $mou->id)}}">Edit</a>
                                                        @endif
                                                        @if($ngoProfile->user->status === 0 )
                                                            <a class="pull-right" href="{{ url("/mou/{$mou->id}/delete")}}">Delete | </a>
                                                        @endif
                                                    </h4>
                                                </div>
                                                <div id="mou_{{$key+1}}" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="col-md-4">
                                                            <p><b>MOU Status:</b> @php echo $mou->mou_status; @endphp</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <p><b>MOU Signed Date:</b> @php echo $mou->mou_signed_date; @endphp</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <p><b>MOU Expiry Date:</b> @php echo $mou->mou_expiry_date; @endphp</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <p><b>No of Years:</b> @php echo $mou->num_of_years; @endphp</p>
                                                        </div>
                                                        @if($mou->mou_file)
                                                        <div class="col-md-6">
                                                            {{-- Storage::disk('uploads_dir')->url($mou->mou_file) --}}
                                                            <p><b>MOU:</b>
                                                                <a target="_blank" href="{{ url($mou->mou_file) }}">View File</a>
                                                            </p>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>

@endsection
