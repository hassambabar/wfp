@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{-- <div class="card-title">
                        <div class="title">Basic example</div>
                    </div> --}}
                </div>
                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form action="{{route('reporting.store')}}" class="needs-validation" novalidate method="POST" enctype="multipart/form-data">
                        @csrf
                       {{--  @if($ngoProfile->status == 1)
                            <input type="hidden" name="noc_apply" value="0" />
                        @endif
                          --}}
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-6">
                                <label for="report_duration">Report</label>
                                <select name="report_duration" id="report_duration" class="form-control">
                                    <option @if (old('report_duration') == 'monthly') selected="selected" @endif value="monthly">Monthly</option>
                                    <option @if (old('report_duration') == 'quarterly') selected="selected" @endif value="quarterly">Quarterly</option>
                                    <option @if (old('report_duration') == 'final') selected="selected" @endif value="final">Final</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="report_project">Project</label>
                                <select name="report_project" id="report_project" class="form-control">
                                    <option value="">Select</option>
                                    @foreach($ngoProjects as $project)
                                        <option @if (old('report_project') == $project->id) selected="selected" @endif value="{{$project->id}}">{{$project->project_title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="form-group col-md-6">
                                <label for="project_category">Context *</label>
                                <select id="project_category" name="project_category" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($projectCategories as $projCat)
                                        <option @if (old('project_category') == $projCat->id) selected="selected" @endif value="{{$projCat->id}}">{{$projCat->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="donor">Donor/Funding Organization *</label>
                                <select id="donor" name="donor" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($fundingAgency as $agency)
                                        <option @if (old('donor') == $agency->id) selected="selected" @endif value="{{$agency->id}}">{{$agency->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_code">Project Code *</label>
                                <input type="text" required class="form-control" id="project_code" placeholder="Project Code" value="{{old('project_code')}}"  name="project_code">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_modality">Project Modality *</label>
                                <select id="project_modality" name="project_modality" class="form-control" required>
                                    <option value="">Select</option>
                                    @foreach ($modalities as $modality)
                                        <option @if (old('project_modality') == $modality->id) selected="selected" @endif value="{{$modality->id}}">{{$modality->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_title">Project Title *</label>
                                <input type="text" required class="form-control" id="project_title" placeholder="Project Title" value="{{old('project_title')}}"  name="project_title">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_owner_name">Project Owner *</label>
                                <input type="text" class="form-control" id="project_owner_name" placeholder="Project Owner" value="{{old('project_owner_name')}}"  name="project_owner_name">
                            </div>



                            <div class="form-group col-md-6">
                                <label for="project_owner_org_type">Project Owner Organization Type *</label>
                                <input type="text" class="form-control" id="project_owner_org_type" required placeholder="Project Owner Organization Type" value="{{old('project_owner_org_type')}}"  name="project_owner_org_type">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="implementing_partner">Implementing Partner *</label>
                                <select id="implementing_partner" required name="implementing_partner" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($implementingPartner as $imPartner)
                                        <option @if (old('implementing_partner') == $imPartner->id) selected="selected" @endif value="{{$imPartner->id}}">{{$imPartner->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="implementing_org_type">Implementing Organization Type *</label>
                                <select id="implementing_org_type" required name="implementing_org_type" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($implementingOrgType as $imOrgType)
                                        <option @if (old('implementing_org_type') == $imOrgType->id) selected="selected" @endif value="{{$imOrgType->id}}">{{$imOrgType->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="project_sector">Sector / Cluster *</label>
                                <select id="project_sector" required name="project_sector" class="form-control">
                                    <option value="">Select</option>
                                    @foreach ($projectSector as $sector)
                                        <option @if (old('project_sector') == $sector->id) selected="selected" @endif value="{{$sector->id}}">{{$sector->title}}</option>
                                    @endforeach
                                </select>
                            </div>


                        </div>
                        <div class="form-group col-md-12">
                            <hr class="style1">
                        </div>
                        <div class="form-group col-md-12 activities">
                            <div class="activity">
                                <div class="col-md-12 row-head">
                                    <h5>Activity 1:</h5>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="activity_1">Activity *</label>
                                    <input required type="text" class="form-control" id="activity_1" placeholder="Activity" value=""  name="activity[activity][]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="description_1">Activity Description *</label>
                                    <input required type="text" class="form-control" id="description_1" placeholder="Activity Description" value=""  name="activity[description][]">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_start_date_1">Activity Start Date *</label>
                                    <input required type="text" class="form-control datepicker" autocomplete="off" id="activity_start_date_1" placeholder="Activity Start Date" value=""  name="activity[activity_start_date][]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="activity_end_date_1">Activity End Date *</label>
                                    <input required type="text" class="form-control datepicker" autocomplete="off" id="activity_end_date_1" placeholder="Activity End Date" value=""  name="activity[activity_end_date][]">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_province_1">Activity Province *</label>
                                    <select required id="activity_province_1" name="activity[activity_province][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($Province as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="activity_district_1">Activity District *</label>
                                    <select required id="activity_district_1" name="activity[activity_district][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($District as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_tehsil_1">Activity Tehsil *</label>
                                    <select required id="activity_tehsil_1" name="activity[activity_tehsil][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($Tehsil as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="activity_uc_1">Activity Union Council *</label>
                                    <select required id="activity_uc_1" name="activity[activity_uc][]" class="form-control">
                                        <option value="">Select</option>
                                        @foreach ($ucs as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="activity_village_1">Activity Village *</label>
                                    <input required type="text" class="form-control" id="activity_village_1" placeholder="Activity Village" value=""  name="activity[activity_village][]">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="latitude">Latitude</label>
                                    <input type="text" class="form-control" autocomplete="off" id="latitude" placeholder="Latitude" value=""  name="activity[latitude][]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="longitude">Longitude</label>
                                    <input type="text" class="form-control" autocomplete="off" id="longitude" placeholder="Longitude" value=""  name="activity[longitude][]">
                                </div>
                            </div>

                            <div class="add-more-activity">
                                <a href="javascript:void(0)" class="btn btn-primary">Add Activity</a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <hr class="style1">
                        </div>

                        <div class="form-group col-md-12">
                            <h5><b><u>Activity</u></b></h5>
                            <div class="form-group col-md-6">
                                <label for="target_number">Target (Number)</label>
                                <input type="number" class="form-control is_numeric" id="target_number" placeholder="Target (Number)" value="{{old('target_number')}}" name="target_number">
                            </div>
                            @if($ngoProfile->status == 0)
                                <div class="form-group col-md-6">
                                    <label for="achieved_number">Achieved (Number)</label>
                                    <input type="number" class="form-control is_numeric" id="achieved_number" placeholder="Achieved (Number)" value="{{old('achieved_number')}}" name="achieved_number">
                                </div>
                            @endif

                            <div class="form-group col-md-6">
                                <label for="activity_unit">Achieved Activity Unit</label>
                                <input type="text" class="form-control" id="activity_unit" placeholder="Achieved Activity Unit" value="{{old('activity_unit')}}" name="activity_unit" autocomplete="off">
                            </div>

                        </div>

                        <div class="form-group col-md-12">
                            <h5><b><u>Beneficiary Targeted</u></b></h5>
                            <div class="form-group col-md-6">
                                <label for="families_targeted">Families</label>
                                <input type="number" class="form-control is_numeric" id="families_targeted" placeholder="Families" value="{{old('families_targeted')}}" name="families_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="women_targeted">Women</label>
                                <input type="number" class="form-control is_numeric" id="women_targeted" placeholder="Women" value="{{old('women_targeted')}}" name="women_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="men_targeted">Men</label>
                                <input type="number" class="form-control is_numeric" id="men_targeted" placeholder="Men" value="{{old('men_targeted')}}" name="men_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="girls_targeted">Girls</label>
                                <input type="number" class="form-control is_numeric" id="girls_targeted" placeholder="Girls" value="{{old('girls_targeted')}}" name="girls_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="boys_targeted">Boys</label>
                                <input type="number" class="form-control is_numeric" id="boys_targeted" placeholder="Boys" value="{{old('boys_targeted')}}" name="boys_targeted">
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <h5><b><u>Disabled Targeted</u></b></h5>
                            <div class="form-group col-md-6">
                                <label for="disabled_women_targeted">Women</label>
                                <input type="number" class="form-control is_numeric" id="disabled_women_targeted" placeholder="Women" value="{{old('disabled_women_targeted')}}" name="disabled_women_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="disabled_men_targeted">Men</label>
                                <input type="number" class="form-control is_numeric" id="disabled_men_targeted" placeholder="Men" value="{{old('disabled_men_targeted')}}" name="disabled_men_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="disabled_boys_targeted">Girls</label>
                                <input type="number" class="form-control is_numeric" id="disabled_boys_targeted" placeholder="Girls" value="{{old('disabled_boys_targeted')}}" name="disabled_boys_targeted">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="disabled_girls_targeted">Boys</label>
                                <input type="number" class="form-control is_numeric" id="disabled_girls_targeted" placeholder="Boys" value="{{old('disabled_girls_targeted')}}" name="disabled_girls_targeted">
                            </div>
                        </div>

                         <div class="form-group col-md-12">
                             <h5><b><u>Beneficiary Reached/Achieved</u></b></h5>
                         </div>

                        @if($ngoProfile->status == 0)
                            <div class="form-group col-md-12">

                                <div class="form-group col-md-6">
                                    <label for="start_date">Start Date</label>
                                    <input type="text" class="form-control datepicker" id="start_date" autocomplete="off" placeholder="Start Date" value="{{old('start_date')}}"  name="start_date">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="end_date">End Date</label>
                                    <input type="text" class="form-control datepicker" id="end_date" autocomplete="off" placeholder="End Date" value="{{old('end_date')}}"  name="end_date">
                                </div>


                                <div class="form-group col-md-6">
                                    <label for="female_headed">Families Reached</label>
                                    <input type="number" class="form-control is_numeric" id="female_headed" placeholder="Families Reached" value="{{old('female_headed')}}" name="female_headed">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="women_reached">Women</label>
                                    <input type="number" class="form-control is_numeric" id="women_reached" placeholder="Women" value="{{old('women_reached')}}" name="women_reached">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="men_reached">Men</label>
                                    <input type="number" class="form-control is_numeric" id="men_reached" placeholder="Men" value="{{old('men_reached')}}" name="men_reached">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="girls_reached">Girls</label>
                                    <input type="number" class="form-control is_numeric" id="girls_reached" placeholder="Girls" value="{{old('girls_reached')}}" name="girls_reached">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="boys_reached">Boys</label>
                                    <input type="number" class="form-control is_numeric" id="boys_reached" placeholder="Boys" value="{{old('boys_reached')}}" name="boys_reached">
                                </div>
                            </div>
                        @endif

                        <div class="form-group col-md-12">
                            <h5><b><u>People with disabilities reached</u></b></h5>
                        </div>

                        <div class="form-group col-md-12">
                            <div class="form-group col-md-6">
                                <label for="women_disabled_reached">Women</label>
                                <input type="number" class="form-control is_numeric" id="women_disabled_reached"
                                       placeholder="Women" value="{{old('women_disabled_reached')}}" name="women_disabled_reached">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="men_disabled_reached">Men</label>
                                <input type="number" class="form-control is_numeric" id="men_disabled_reached" placeholder="Men"
                                       value="{{old('men_disabled_reached')}}" name="men_disabled_reached">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="girls_disabled_reached">Girls</label>
                                <input type="number" class="form-control is_numeric" id="girls_disabled_reached"
                                       placeholder="Girls" value="{{old('girls_disabled_reached')}}" name="girls_disabled_reached">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="boys_disabled_reached">Boys</label>
                                <input type="number" class="form-control is_numeric" id="boys_disabled_reached"
                                       placeholder="Boys" value="{{old('boys_disabled_reached')}}" name="boys_disabled_reached">
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <h5><b><u>Contacts</u></b></h5>
                            <div class="form-group col-md-6">
                                <label for="contact_person">Contact Person</label>
                                <input type="text" class="form-control" id="contact_person" placeholder="Contact Person" value="{{old('contact_person')}}" name="contact_person">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="contact_number">Contact Number</label>
                                <input type="text" class="form-control is_numeric" id="contact_number" placeholder="Contact Number" value="{{old('contact_number')}}" name="contact_number">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="contact_email">Email</label>
                                <input type="email" class="form-control" id="contact_email" placeholder="Email" value="{{old('contact_email')}}" name="contact_email">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="comments">Comments</label>
                                <input type="text" class="form-control" id="comments" placeholder="Comments" value="{{old('comments')}}" name="comments">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="comments">Date (date updated)</label>
                                <input type="text" class="form-control datepicker" id="updated_date" autocomplete="off" value="{{old('comments')}}" name="comments" placeholder="Updated Date">
                            </div>
                             <div class="form-group col-md-6">
                                <label for="narrative_reports">Narrative Report</label>
                                <input type="file" class="form-control uploadFileInput" id="narrative_reports" name="narrative_reports">
                            </div>
                             <div class="form-group col-md-6">
                                <label for="upload_image">Upload Image</label>
                                <input type="file" class="form-control uploadFileInput" id="upload_image" name="upload_image">
                            </div>

                        </div>


                        <div class="form-group col-md-12">
                            <label>Status</label>
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="ext_yes" name="report_status" checked value="1">
                                <label for="ext_yes">Complete</label>
                            </div>
                            <div class="radio3 radio-check radio-inline">
                                <input type="radio" id="ext_no" name="report_status" value="0">
                                <label for="ext_no">Ongoing</label>
                            </div>
                        </div>


                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-default triggerSubmit">Submit</button>
                            <a href="{{url('reporting')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('pagespecificscripts')
    <script>
        $( function() {
            $( ".datepicker" ).datepicker();
            $('.add-more-activity a').on('click', function () {
                let cloneActivity = $('.activities .activity:first-child').clone();
                let totalActivity = $('.activities .activity').length;
                $(cloneActivity).find('.row-head h5').html('Activity '+( totalActivity+ 1)+':');

                $(cloneActivity).insertBefore('.activities .add-more-activity');
            });

            <?php
            if(app('request')->old('report_project') !== null){ ?>
                $.ajax({
                    url: '/getProjectDetail/'+<?php echo app('request')->old('report_project') ?>,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        console.log('response', response)
                        var len = 0;
                        if(response != null){
                            //if(response.project_category != null){
                            $('#project_category').val(response.project_category).change().attr('readonly',true);
                            $('#donor').val(response.donor).change().attr('readonly',true);
                            $('#project_code').val(response.project_code).attr('readonly',true);
                            $('#project_modality').val(response.project_modality).change().attr('readonly',true);
                            $('#project_title').val(response.project_title).attr('readonly',true);
                            $('#project_owner_name').val(response.project_owner_name).attr('readonly',true);
                            $('#project_owner_org_type').val(response.project_owner_org_type).attr('readonly',true);
                            $('#implementing_partner').val(response.implementing_partner).change().attr('readonly',true);
                            $('#implementing_org_type').val(response.implementing_org_type).change().attr('readonly',true);
                            $('#project_sector').val(response.project_sector).change().attr('readonly',true);
                            $('#target_number').val(response.target_number).attr('readonly',true);
                            //}
                        }

                    }
                });

           <?php  } ?>

            $('#report_project').change(function(){
                // district id
                var id = $(this).val();
                // AJAX request
                $.ajax({
                    url: '/getProjectDetail/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response){
                        console.log('response', response)
                        var len = 0;
                        if(response != null){
                            //if(response.project_category != null){
                                $('#project_category').val(response.project_category).change().attr('readonly',true);
                                $('#donor').val(response.donor).change().attr('readonly',true);
                                $('#project_code').val(response.project_code).attr('readonly',true);
                                $('#project_modality').val(response.project_modality).change().attr('readonly',true);
                                $('#project_title').val(response.project_title).attr('readonly',true);
                                $('#project_owner_name').val(response.project_owner_name).attr('readonly',true);
                                $('#project_owner_org_type').val(response.project_owner_org_type).attr('readonly',true);
                                $('#implementing_partner').val(response.implementing_partner).change().attr('readonly',true);
                                $('#implementing_org_type').val(response.implementing_org_type).change().attr('readonly',true);
                                $('#project_sector').val(response.project_sector).change().attr('readonly',true);
                                $('#target_number').val(response.target_number).attr('readonly',true);
                            //}
                        }

                    }
                });
            });


            var MAX_FILE_SIZE = 5 * 1024 * 1024; // 5MB
            $('.uploadFileInput').change(function() {
                console.log(' uploadFileInput change ', this.files);
                if(this.files.length && this.files[0].size > MAX_FILE_SIZE){
                    this.setCustomValidity("File must not exceed 5 MB!");
                    this.reportValidity();
                    // this.value = this.defaultValue;
                }else {
                    this.setCustomValidity("");
                }

            });

        });
    </script>
    <script>
        // Disable form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {

                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    <link href="{{ asset('assets/css/checkbox3.min.css') }}" rel="stylesheet" >
@endsection
