@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-6">
                        <h3>Details</h3>
                    </div>
                    @if((checkProfileApproval() && checkEnableEdit()) || $ngoProfile->user->status === 0 )
                        <div class="col-md-6 text-right">
                            <a href="{{route('reporting.edit', $reporting->id)}}">Edit Details</a>
                        </div>
                    @endif
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <p><b>Report:</b>
                                {{ucfirst($reporting->report_duration)}}
                            </p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Start Date:</b> {{$reporting->start_date ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>End Date:</b> {{$reporting->end_date ? : ' -- '}}</p>
                        </div>
                    </div>
                    <div class="col-md-12 row-head">
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <p><b>Context:</b> {{$reporting->category ? $reporting->category->category_name : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Donor/Funding Organization:</b> {{$reporting->fundingAgency ? $reporting->fundingAgency->name : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Code:</b> {{$reporting->project_code ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Modality:</b> {{$reporting->modality ? $reporting->modality->title : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Title:</b> {{$reporting->project_title ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Owner:</b> {{$reporting->project_owner_name ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Project Owner Organization Type:</b> {{$reporting->project_owner_org_type ? : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Implementing Partner:</b> {{$reporting->implementingPartner ? $reporting->implementingPartner->title : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Implementing Organization Type:</b> {{$reporting->implementingOrgType ? $reporting->implementingOrgType->title : ' -- '}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><b>Sector / Cluster:</b> {{$reporting->projectSector ? $reporting->projectSector->title : ' -- '}}</p>
                        </div>
                        @if ($reporting->activities->count() > 0)
                            @foreach ($reporting->activities as $akey => $activity)
                                <div class="activity">
                                    <div class="col-md-12 row-head">
                                        <hr>
                                        <h4>Activity : {{$akey+1}}</h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity:</b> {{$activity->activity ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Description:</b> {{$activity->description ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Start Date:</b> {{$activity->activity_start_date ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity End Date:</b> {{$activity->activity_end_date ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Month of Assistance:</b> {{$activity->month_assistance ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Province:</b> {{$activity->province ? $activity->province->name : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity District:</b> {{$activity->district ? $activity->district->name : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Tehsil:</b> {{$activity->tehsil ? $activity->tehsil->name : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Union Council:</b> {{$activity->uc ? $activity->uc->name : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Activity Village:</b> {{$activity->activity_village ? : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Latitude:</b> {{$activity->latitude ?  : ' -- '}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><b>Longitude:</b> {{$activity->longitude ?  : ' -- '}}</p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-12 row-head">
                            <h4>Activity</h4>
                        </div>
                        <div class="col-md-4">
                            <p><b>Target (Number):</b> {{$reporting->target_number ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Achieved (Number):</b> {{$reporting->achieved_number ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Achieved Activity Unit:</b> {{$reporting->activity_unit ?  : ' -- '}}</p>
                        </div>

                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-12 row-head">
                            <h4>Beneficiary Targeted</h4>
                        </div>
                        <div class="col-md-4">
                            <p><b>Families:</b> {{$reporting->families_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Women:</b> {{$reporting->women_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Men:</b> {{$reporting->men_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Girls:</b> {{$reporting->girls_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Boys:</b> {{$reporting->boys_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-12 row-head">
                            <h4>Persons with disabilities Targeted</h4>
                        </div>
                        <div class="col-md-4">
                            <p><b>Women:</b> {{$reporting->disabled_women_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Men:</b> {{$reporting->disabled_men_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Girls:</b> {{$reporting->disabled_boys_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Boys:</b> {{$reporting->disabled_girls_targeted ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-12 row-head">
                            <h4>Beneficiary Reached</h4>
                        </div>
                        <div class="col-md-4">
                            <p><b>Female Headed Household:</b> {{$reporting->female_headed ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Women:</b> {{$reporting->women_reached ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Men:</b> {{$reporting->men_reached ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Girls:</b> {{$reporting->girls_reached ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Boys:</b> {{$reporting->boys_reached ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-12 row-head">
                            <h4>Contacts</h4>
                        </div>
                        <div class="col-md-4">
                            <p><b>Contact Person:</b> {{$reporting->contact_person ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Contact Number:</b> {{$reporting->contact_number ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Email:</b> {{$reporting->contact_email ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Comments:</b> {{$reporting->comments ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-4">
                            <p><b>Date (date updated):</b> {{$reporting->updated_date ?  : ' -- '}}</p>
                        </div>
                        <div class="col-md-12 row-head">
                            <hr>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <b>Narrative Report:</b>
                                @if (!empty($reporting->narrative_reports))
                                    <a target="_blank" href="{{ url($reporting->narrative_reports) }}">View File</a>
                                @else
                                    {{' -- '}}
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <b>Upload Image:</b>
                                @if (!empty($reporting->upload_image))
                                    <a target="_blank" href="{{ url($reporting->upload_image) }}">View File</a>
                                @else
                                    {{' -- '}}
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <b>Status:</b>
                                @if ($reporting->report_status == 1)
                                    {{'Complete'}}
                                @else
                                    {{'Ongoing'}}
                                @endif
                            </p>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <a href="{{url('/reporting')}}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
