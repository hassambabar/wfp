@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
                {!! displayMessage() !!}
            @endif
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporting
                </div>
                <div class="panel-body">
                    @if(isset($ngoProfile) && $ngoProfile == null)
                        <div>
                            You need to complete your profile first. Click <a href="{{url('/profile/create')}}">here</a> to fill in your details.
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-3">
                                <select name="report_duration" id="report_duration" class="form-control">
                                    <option value="">--Select Report Duration--</option>
                                    <option value="monthly">Monthly</option>
                                    <option value="quarterly">Quarterly</option>
                                    <option value="final">Final</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="project_title" id="project_title" class="form-control">
                                    <option value="">--Select Project--</option>

                                    @foreach ($ngoProjects  as $project)
                                        <option value="{!!$project->project_title!!}">{!!$project->project_title!!}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="projects-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Project Title</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Owner Name</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                    <th>Duration</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($reporting as $key => $item)
                                    <tr class="odd gradeX">
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->project_title}}</td>
                                        <td>{{$item->start_date}}</td>
                                        <td>{{$item->end_date}}</td>
                                        <td>{{$item->project_owner_name}}</td>
                                        <td>{{($item->report_status == '1')?'Current':'Ongoing'}}</td>
                                         <td>
                                             <a href="{{ route('reporting.show', $item->id) }}">View</a>
                                             @if(checkProfileApproval() && checkEnableEdit())
                                             <a href="{{ route('reporting.edit', $item->id)}}">Edit</a>
                                             @endif
                                             @if($ngoProfile->user->status === 0 )
                                                 - <a href="{{ url("/reporting/{$item->id}/delete") }}">Delete</a>
                                             @endif
                                         </td>
                                        <td>{{$item->report_duration}}</td>
                                    </tr>
                                @empty
                                    <tr class="gradeU">
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>

@endsection
@section('pagespecificscripts')
    <script src="{{ asset('assets/js/filterDropDown.js') }}"></script>
    <script>
        $(document).ready(function () {
            oTableContact = $('#projects-table').DataTable({
                "columnDefs": [
                    {
                        "targets": [7],
                        "visible": false
                    }
                ]
            });

            $('#report_duration').change(function () {
                oTableContact.column('7').search($(this).val()).draw();
            });
            $('#project_title').change(function () {
                oTableContact.column('1').search($(this).val()).draw();
            });
        });
    </script>
@endsection
