<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterResilienceColumnsInEmergencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emergency', function (Blueprint $table) {
            $table->renameColumn('resilience_district_id', 'support_district_id');
            $table->renameColumn('resilience_tehsil_id', 'support_tehsil_id');
            $table->renameColumn('resilience_uc_id', 'support_uc_id');
            $table->renameColumn('resilience_village', 'support_area_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emergency', function (Blueprint $table) {
            $table->renameColumn('support_district_id', 'resilience_district_id');
            $table->renameColumn('support_tehsil_id', 'resilience_tehsil_id');
            $table->renameColumn('support_uc_id', 'resilience_uc_id');
            $table->renameColumn('support_area_name', 'resilience_village');
        });
    }
}
