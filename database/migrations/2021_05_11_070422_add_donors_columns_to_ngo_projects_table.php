<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDonorsColumnsToNgoProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngo_projects', function (Blueprint $table) {
           $table->string('funds_donor_name')->after('source_of_funds');
           $table->string('funds_amount')->after('source_of_funds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_projects', function (Blueprint $table) {
            $table->dropColumn(['funds_donor_name','funds_amount']);
        });
    }
}
