<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MedicalFacilities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hospitals')->nullable();
            $table->string('dispensaries')->nullable();
            $table->string('bhus')->nullable();
            $table->string('medical_camps_established')->nullable();
            $table->string('focal_person_name')->nullable();
            $table->string('designation')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('relief_camp_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MedicalFacilities');
    }
}
