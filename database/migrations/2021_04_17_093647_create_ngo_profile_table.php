<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNgoProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ngo_profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_id')->constrained('users');
            $table->enum('legal_status', ['pk_org', 'intl_org','un_org']);
            $table->string('reg_law', 100);
            $table->date('reg_date');
            $table->integer('reg_no');
            $table->string('hq_origin_country', 100)->nullable();
            $table->string('hq_reg_law', 100)->nullable();
            $table->date('hq_reg_date')->nullable();
            $table->integer('hq_reg_no')->nullable();
            $table->text('profile');
            $table->boolean('have_mou');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ngo_profile');
    }
}
