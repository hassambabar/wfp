<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToReliefCampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relief_camps', function (Blueprint $table) {

            if (Schema::hasColumn('relief_camps', 'facilities')) {
                    $table->dropColumn('facilities');
            }
            

            $table->integer('tehsil_id')            ->after('uc_id')               ->nullable();

            $table->string('site_name')             ->after('capacity')          ->nullable();
            $table->integer('has_drinking_water')   ->after('site_name')          ->default(0)->nullable();
            $table->integer('has_electricity')      ->after('has_drinking_water') ->default(0)->nullable();
            $table->integer('has_washroom')         ->after('has_electricity')    ->default(0)->nullable();
            $table->integer('has_medical_aid')      ->after('has_washroom')       ->default(0)->nullable();
            $table->string('other_facility')        ->after('has_medical_aid')    ->nullable();
            
            $table->string('designation')           ->after('contact_no')         ->nullable();
            $table->string('focal_person_2')        ->after('designation')        ->nullable();
            $table->string('contact_no_2')          ->after('focal_person_2')     ->nullable();
            $table->string('designation_2')         ->after('contact_no_2')       ->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relief_camps', function (Blueprint $table) {
            //
        });
    }
}
