<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveClumsRelefcamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relief_camps', function (Blueprint $table) {
            //
            $table->dropColumn('focal_person');
            $table->dropColumn('contact_no');
            $table->dropColumn('designation');
            $table->dropColumn('focal_person_2');
            $table->dropColumn('contact_no_2');
            $table->dropColumn('designation_2');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relief_camps', function (Blueprint $table) {
            //
        });
    }
}
