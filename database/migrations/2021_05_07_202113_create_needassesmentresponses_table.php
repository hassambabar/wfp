<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeedassesmentresponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('needassesmentresponses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ngo_id')->nullable();
            $table->string('emergency_id')->nullable();
            $table->text('response')->nullable();
            $table->string('response_quantity')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('needassesmentresponses');
    }
}
