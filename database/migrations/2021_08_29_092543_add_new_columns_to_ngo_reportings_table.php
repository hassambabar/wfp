<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsToNgoReportingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngo_reportings', function (Blueprint $table) {
            $table->string('report_project')->nullable();
            $table->string('families_targeted')->nullable();
            $table->string('disabled_women_targeted')->nullable();
            $table->string('disabled_men_targeted')->nullable();
            $table->string('disabled_boys_targeted')->nullable();
            $table->string('disabled_girls_targeted')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_reportings', function (Blueprint $table) {
            $table->dropColumn('report_project');
            $table->dropColumn('families_targeted');
            $table->dropColumn('disabled_women_targeted');
            $table->dropColumn('disabled_men_targeted');
            $table->dropColumn('disabled_boys_targeted');
            $table->dropColumn('disabled_girls_targeted');
        });
    }
}
