<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAffectedHhsAndSupportColumnsToEmergencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emergency', function (Blueprint $table) {
            $table->string('affected_hhs')->nullable();
            $table->string('contact_person_one')->nullable();
            $table->string('contact_person_one_designation')->nullable();
            $table->string('contact_person_one_email')->nullable();
            $table->string('contact_person_one_phone')->nullable();
            $table->string('contact_person_two')->nullable();
            $table->string('contact_person_two_designation')->nullable();
            $table->string('contact_person_two_email')->nullable();
            $table->string('contact_person_two_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emergency', function (Blueprint $table) {
            $table->dropColumns([
                'affected_hhs',
                'contact_person_one',
                'contact_person_one_designation',
                'contact_person_one_email',
                'contact_person_one_phone',
                'contact_person_two',
                'contact_person_two_designation',
                'contact_person_two_email',
                'contact_person_two_phone',
            ]);
        });
    }
}
