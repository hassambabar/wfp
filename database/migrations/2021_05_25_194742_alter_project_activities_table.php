<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProjectActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_activities', function (Blueprint $table) {
           $table->string('project_activity')->nullable()->change(); 
           $table->text('project_description')->nullable()->change(); 
           $table->date('activity_start_date')->nullable()->change(); 
           $table->date('activity_end_date')->nullable()->change(); 
           $table->integer('activity_province')->nullable()->change(); 
           $table->integer('activity_district')->nullable()->change(); 
           $table->integer('activity_tehsil')->nullable()->change(); 
           $table->integer('activity_uc')->nullable()->change(); 
           $table->string('activity_village')->nullable()->change(); 

        }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
