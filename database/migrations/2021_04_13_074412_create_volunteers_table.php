<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('f_name')->nullable();
            $table->string('l_name')->nullable();
            $table->string('cnic')->nullable();
            $table->string('age')->nullable();
            $table->string('email')->nullable();
            $table->string('gender')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('tehsil_id')->nullable();
            $table->integer('uc_id')->nullable();
            $table->text('address')->nullable();
            $table->text('address_org')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('image')->nullable();
            $table->string('availability')->nullable();
            $table->string('area_of_expertise')->nullable();
            $table->string('physical_limitation')->nullable();
            $table->integer('ngo_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers');
    }
}
