<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddColumnsNgoProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('ngo_profile')) {
            Schema::table('ngo_profile', function (Blueprint $table) {
                $table->string('acronym')->after('user_id');
                $table->string('org_full_name')->after('user_id');
                DB::statement("ALTER TABLE ngo_profile CHANGE COLUMN legal_status legal_status ENUM('pk_org', 'intl_org', 'un_org', 'corp_firm', 'pk_charity') NOT NULL DEFAULT 'pk_org'");
                $table->text('profile_info')->after('have_mou')->nullable();
                $table->text('objectives')->after('have_mou')->nullable();
                $table->text('mission_statement')->after('have_mou')->nullable();
                $table->text('ngo_goal')->after('have_mou')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_profile', function (Blueprint $table) {
            $table->dropColumn('acronym');
            $table->dropColumn('org_full_name');
            $table->dropColumn('ngo_goal');
            $table->dropColumn('mission_statement');
            $table->dropColumn('objectives');
            $table->dropColumn('profile_info');
        });
    }
}
