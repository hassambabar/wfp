<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsHumanResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('human_resources', function (Blueprint $table) {
            // $table->integer('total_frn_national_staff')->after('id')->nullable();
            // $table->integer('total_national_staff')->after('id')->nullable();
            $table->string('temp_address')->after('address')->nullable();
            /*$table->string('frn_email')->after('phone_no')->nullable();
            $table->string('frn_phone')->after('phone_no')->nullable();
            $table->string('frn_nationality')->after('phone_no')->nullable();
            $table->string('frn_national_name')->after('phone_no')->nullable();*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('human_resources', function (Blueprint $table) {
            $table->dropColumn([
            //    'total_frn_national_staff' ,
            //    'total_national_staff' ,
               'temp_address' ,
               /*'frn_email' ,
               'frn_phone' ,
               'frn_nationality' ,
               'frn_national_name'*/
            ]);
        });
    }
}
