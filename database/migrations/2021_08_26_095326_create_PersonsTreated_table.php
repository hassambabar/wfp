<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsTreatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PersonsTreated', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('male')->nullable();
            $table->string('female')->nullable();
            $table->string('children')->nullable();
            $table->string('persons_with_disabilities')->nullable();
            $table->string('total')->nullable();
            $table->integer('relief_camp_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonsTreated');
    }
}
