<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNgoProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ngo_projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project_title');
            $table->foreignId('project_category')->constrained('project_category');
            $table->foreignId('project_type')->constrained('project_type');
            $table->string('project_owner_name');
            $table->foreignId('partner')->constrained('project_partners');
            $table->string('project_code');
            $table->string('project_budget');
            $table->string('project_deliverable');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('beneficiary_targets');
            $table->foreignId('thematic_area')->constrained('thematic_areas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ngo_projects');
    }
}
