<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNgoReportingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::table('ngo_reportings', function (Blueprint $table) {
           $table->string('project_code')->nullable()->change(); 
           $table->string('project_owner_name')->nullable()->change(); 
           $table->integer('implementing_partner')->nullable()->change(); 
           $table->integer('implementing_org_type')->nullable()->change(); 
           $table->integer('project_sector')->nullable()->change(); 
        });

        Schema::table('ngo_reporting_activities', function (Blueprint $table) {
           $table->string('activity')->nullable()->change(); 
           $table->text('description')->nullable()->change(); 
           $table->date('activity_start_date')->nullable()->change(); 
           $table->date('activity_end_date')->nullable()->change(); 
           $table->integer('activity_province')->nullable()->change(); 
           $table->integer('activity_district')->nullable()->change(); 
           $table->integer('activity_tehsil')->nullable()->change(); 
           $table->integer('activity_uc')->nullable()->change(); 
           $table->string('activity_village')->nullable()->change(); 
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
