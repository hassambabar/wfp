<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivestockCampTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LivestockCamp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('small_animal_vaccinated')->nullable();
            $table->string('large_animal_vaccinated')->nullable();
            $table->string('poultry_support')->nullable();
            $table->string('fodder_provided_for_animals')->nullable();
            $table->string('total')->nullable();
            $table->integer('relief_camp_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LivestockCamp');
    }
}
