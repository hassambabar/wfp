<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumsToNgoContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngo_contacts', function (Blueprint $table) {
            //
            $table->string('contact_person_name')->nullable();
            $table->string('designation')->nullable();
            $table->integer('can_pdma_contact')->default(1);
            $table->string('office')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_contacts', function (Blueprint $table) {
            //
        });
    }
}
