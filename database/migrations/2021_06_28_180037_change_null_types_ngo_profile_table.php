<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeNullTypesNgoProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngo_profile', function (Blueprint $table) {
            $table->string('reg_law')->nullable()->change();
            $table->date('reg_date')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_profile', function (Blueprint $table) {
            $table->string('reg_law')->change();
            $table->date('reg_date')->change();

        });
    }
}
