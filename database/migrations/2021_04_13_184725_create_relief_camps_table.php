<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReliefCampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relief_camps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('district_id')->nullable();
            $table->integer('uc_id')->nullable();
            $table->integer('capacity')->nullable();
            $table->text('facilities')->nullable();
            $table->string('focal_person')->nullable();
            $table->string('contact_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relief_camps');
    }
}
