<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffectedAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affected_areas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('province_id')->nullable();
            $table->string('affected_district')->nullable(); 
            $table->string('affected_talukas')->nullable();
            
            $table->string('animal_died')->nullable();
            $table->string('male_died')->nullable();
            $table->string('female_died')->nullable();
            $table->string('children_died')->nullable();
            $table->string('total_died')->nullable();

            $table->string('male_injured')->nullable();
            $table->string('female_injured')->nullable();
            $table->string('children_injured')->nullable();
            $table->string('total_injured')->nullable();

            $table->string('male_displaced')->nullable();
            $table->string('female_displaced')->nullable();
            $table->string('children_displaced')->nullable();
            $table->string('total_displaced')->nullable();

            $table->string('male_affected')->nullable();
            $table->string('female_affected')->nullable();
            $table->string('children_affected')->nullable();
            $table->string('total_affected')->nullable();
            
            $table->string('damage_district')->nullable();

            $table->string('partially_houses_damage')->nullable();
            $table->string('fully_houses_damage')->nullable();
            $table->string('total_houses_damage')->nullable();

            $table->string('partially_villages_affected')->nullable();
            $table->string('fully_villages_affected')->nullable(); 
            $table->string('total_villages_affected')->nullable(); 

            $table->string('crops_area_damaged')->nullable(); 
            $table->string('cattle_head_perished')->nullable(); 


            $table->string('partially_school_damaged')->nullable(); 
            $table->string('fully_school_damaged')->nullable(); 
            $table->string('total_school_damaged')->nullable(); 

            $table->string('partially_building_damaged')->nullable(); 
            $table->string('fully_building_damaged')->nullable(); 
            $table->string('total_building_damaged')->nullable();

            $table->string('roads_damaged')->nullable(); 
            $table->string('railways_damaged')->nullable(); 
            $table->string('bridges_damaged')->nullable(); 

            $table->string('damage_comments')->nullable();


            $table->string('relief_camp_established')->nullable();
            $table->string('relief_camp_operational')->nullable();
            $table->string('relief_camp_population_impace')->nullable();

            $table->string('medical_hospitals')->nullable();
            $table->string('medical_dispensaries')->nullable();
            $table->string('medical_bhus')->nullable();
            $table->string('persons_teated')->nullable();
            $table->string('quantity_of_equipment')->nullable();
            $table->string('other_info')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affected_areas');
    }
}
