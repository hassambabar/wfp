<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContactColumnToReliefCampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relief_camps', function (Blueprint $table) {
            if (!Schema::hasColumn('relief_camps', 'contact')) {
                $table->text('contact')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relief_camps', function (Blueprint $table) {
            $table->dropColumn('contact');
        });
    }
}
