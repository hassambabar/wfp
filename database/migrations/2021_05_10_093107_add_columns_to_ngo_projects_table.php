<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToNgoProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngo_projects', function (Blueprint $table) {
            $table->integer('donor');
            $table->integer('project_modality');
            $table->string('project_owner_org_type')->nullable();
            $table->integer('implementing_partner');
            $table->integer('implementing_org_type');
            $table->integer('project_sector');
            $table->integer('project_province');
            $table->integer('project_district');
            $table->integer('project_tehsil');
            $table->integer('project_uc');
            $table->integer('project_village');
            $table->integer('target_number')->nullable();
            $table->integer('achieved_number')->nullable();
            $table->integer('activity_unit')->nullable();
            $table->string('assist_type')->nullable();
            $table->integer('women_targeted')->nullable();
            $table->integer('men_targeted')->nullable();
            $table->integer('girls_targeted')->nullable();
            $table->integer('boys_targeted')->nullable();
            $table->integer('female_headed')->nullable();
            $table->integer('women_reached')->nullable();
            $table->integer('men_reached')->nullable();
            $table->integer('girls_reached')->nullable();
            $table->integer('boys_reached')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('contact_email')->nullable();
            $table->text('comments')->nullable();
            $table->string('source_of_funds')->nullable();
            $table->string('project_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_projects', function (Blueprint $table) {
            $table->dropColumn(
                [
                    'donor',
                    'project_modality',
                    'project_owner_org_type',
                    'implementing_partner',
                    'implementing_org_type',
                    'project_sector',
                    'project_province',
                    'project_district',
                    'project_tehsil',
                    'project_uc',
                    'project_village',
                    'target_number',
                    'achieved_number',
                    'activity_unit',
                    'assist_type',
                    'women_targeted',
                    'men_targeted',
                    'girls_targeted',
                    'boys_targeted',
                    'female_headed',
                    'women_reached',
                    'men_reached',
                    'girls_reached',
                    'boys_reached',
                    'contact_person',
                    'contact_number',
                    'contact_email',
                    'comments',
                    'source_of_funds',
                    'project_status'
                ]
            );
        });
    }
}
