<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAffectedAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affected_areas', function (Blueprint $table) {
            $table->string('affected_villages_name')->nullable();
            $table->string('relief_camps_established')->nullable();
            $table->string('population_affected')->nullable();
            $table->renameColumn('animal_died','affected_union_council_name');
            $table->renameColumn('male_affected','male_in_relief_camps');
            $table->renameColumn('female_affected','female_in_relief_camps');
            $table->renameColumn('children_affected','children_in_relief_camps');
            $table->renameColumn('total_affected','total_in_relief_camps');
            $table->renameColumn('relief_camp_established','shops_damaged');
            $table->renameColumn('relief_camp_operational','hotels_damaged');
            $table->renameColumn('relief_camp_population_impace','religious_places_damaged');
            $table->renameColumn('medical_hospitals','power_houses_damaged');
            $table->renameColumn('medical_dispensaries','max_persons_in_relief_camps');
            $table->dropColumn('medical_bhus');
            $table->dropColumn('quantity_of_equipment');
            $table->dropColumn('other_info');
            $table->dropColumn('damage_comments');
            $table->dropColumn('damage_district');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affected_areas', function (Blueprint $table) {
            //
        });
    }
}
