<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNonNullableColumnsInNgoProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngo_projects', function (Blueprint $table) {
            $table->string('project_title')->nullable()->change();
            $table->unsignedBigInteger('project_category')->nullable()->change();
            $table->unsignedBigInteger('thematic_area')->nullable()->change();
            $table->date('start_date')->nullable()->change();
            $table->date('end_date')->nullable()->change();
            $table->integer('donor')->nullable()->change();
            $table->integer('project_modality')->nullable()->change();
            $table->integer('project_province')->nullable()->change();
            $table->integer('project_district')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_projects', function (Blueprint $table) {
            $table->string('project_title')->nullable(false)->change();
            $table->unsignedBigInteger('project_category')->nullable(false)->change();
            $table->unsignedBigInteger('thematic_area')->nullable(false)->change();
            $table->date('start_date')->nullable(false)->change();
            $table->date('end_date')->nullable(false)->change();
            $table->integer('donor')->nullable(false)->change();
            $table->integer('project_modality')->nullable(false)->change();
            $table->integer('project_province')->nullable(false)->change();
            $table->integer('project_district')->nullable(false)->change();
        });
    }
}
