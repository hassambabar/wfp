<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReliefcampcontactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reliefcampcontact', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('focal_person')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('designation')->nullable();
            $table->string('relief_camp_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reliefcampcontact');
    }
}
