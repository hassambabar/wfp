<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmergenciesRespondedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emergencies_responded', function (Blueprint $table) {
            $table->id();
            $table->integer('ngo_id');
            $table->integer('project_id');
            $table->integer('year');
            $table->integer('type_of_emergency');
            $table->integer('province');
            $table->integer('district');
            $table->integer('tehsil');
            $table->integer('uc');
            $table->text('village_details')->nullable();
            $table->string('response_type')->nullable();
            $table->string('desc_deliverables')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergencies_responded');
    }
}
