<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNgoReportingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ngo_reportings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ngo_id');
            
            $table->foreignId('project_category')->constrained('project_category');
            $table->integer('donor');

            $table->string('project_code');
            $table->integer('project_modality');
            $table->string('project_title');
            $table->string('project_owner_name');
            $table->string('project_owner_org_type')->nullable();
            $table->integer('implementing_partner');
            $table->integer('implementing_org_type');
            $table->integer('project_sector');

            $table->integer('target_number')->nullable();
            $table->integer('achieved_number')->nullable();
            $table->integer('activity_unit')->nullable();

            $table->integer('women_targeted')->nullable();
            $table->integer('men_targeted')->nullable();
            $table->integer('girls_targeted')->nullable();
            $table->integer('boys_targeted')->nullable();

            $table->string('report_duration')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();

            $table->integer('female_headed')->nullable();
            $table->integer('women_reached')->nullable();
            $table->integer('men_reached')->nullable();
            $table->integer('girls_reached')->nullable();
            $table->integer('boys_reached')->nullable();

            $table->string('contact_person')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('contact_email')->nullable();
            $table->text('comments')->nullable();

            $table->date('updated_date')->nullable();
            $table->string('narrative_reports')->nullable();
            $table->string('upload_image')->nullable();
            $table->string('report_status')->nullable();


            $table->timestamps();
        });

        Schema::create('ngo_reporting_activities', function (Blueprint $table) {
            $table->id();
            $table->integer('report_id');
            $table->string('activity');
            $table->text('description');
            $table->date('activity_start_date');
            $table->date('activity_end_date');
            $table->integer('activity_province');
            $table->integer('activity_district');
            $table->integer('activity_tehsil');
            $table->integer('activity_uc');
            $table->string('activity_village');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ngo_reportings');
        Schema::dropIfExists('ngo_reporting_activities');
    }
}
