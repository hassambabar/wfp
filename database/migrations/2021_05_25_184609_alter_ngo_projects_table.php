<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNgoProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::table('ngo_projects', function (Blueprint $table) {
           $table->string('funds_donor_name')->nullable()->change(); 
           $table->string('funds_amount')->nullable()->change(); 
           $table->string('project_owner_name')->nullable()->change(); 
           $table->string('project_code')->nullable()->change(); 
           $table->integer('implementing_partner')->nullable()->change(); 
           $table->integer('implementing_org_type')->nullable()->change(); 
           $table->integer('project_sector')->nullable()->change(); 
           $table->integer('project_tehsil')->nullable()->change(); 
           $table->integer('project_uc')->nullable()->change(); 

        });
    

         
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
