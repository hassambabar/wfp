<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_activities', function (Blueprint $table) {
            $table->id();
            $table->integer('project_id');
            $table->string('project_activity');
            $table->text('project_description');
            $table->date('activity_start_date');
            $table->date('activity_end_date');
            $table->integer('activity_province');
            $table->integer('activity_district');
            $table->integer('activity_tehsil');
            $table->integer('activity_uc');
            $table->string('activity_village');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_activities');
    }
}
