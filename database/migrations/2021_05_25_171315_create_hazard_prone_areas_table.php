<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHazardProneAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hazard_prone_areas', function (Blueprint $table) {
            $table->id();
            $table->integer('hazard_type_id');
            $table->integer('district_id');
            $table->integer('tehsil_id');
            $table->integer('uc_id');
            $table->string('village');
            $table->date('expected_occurrence');
            $table->date('happened_on');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hazard_pron_areas');
    }
}
