<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsOfDisabledReachedToNgoReportingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngo_reportings', function (Blueprint $table) {
            $table->string('disabled_women_reached')->nullable();
            $table->string('disabled_men_reached')->nullable();
            $table->string('disabled_boys_reached')->nullable();
            $table->string('disabled_girls_reached')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_reportings', function (Blueprint $table) {
            $table->dropColumn('disabled_women_reached');
            $table->dropColumn('disabled_men_reached');
            $table->dropColumn('disabled_boys_reached');
            $table->dropColumn('disabled_girls_reached');
        });
    }
}
