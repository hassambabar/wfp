<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmergencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emergency', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type_of_emergency')->nullable();
            $table->string('type_of_hazard')->nullable();
            $table->string('district_id')->nullable();
            $table->string('tehsil_id')->nullable();
            $table->string('uc_id')->nullable();
            $table->string('taluka_id')->nullable();
            $table->string('village')->nullable();
            $table->string('affected_population')->nullable();
            $table->string('affected_livestock')->nullable();
            $table->string('area_affected')->nullable();
            $table->string('document')->nullable();
            $table->text('description')->nullable();
            $table->string('safe_places_after_rescue_for_camp')->nullable();
            $table->string('resilience_district_id')->nullable();
            $table->string('resilience_tehsil_id')->nullable();
            $table->string('resilience_uc_id')->nullable();
            $table->string('resilience_taluka_id')->nullable();
            $table->string('resilience_village')->nullable();
            $table->string('location_of_camp')->nullable();
            $table->string('rescue_items')->nullable();
            $table->string('quantity')->nullable();
            $table->string('requirement_location')->nullable();
            $table->string('when_needed')->nullable();
            $table->string('other_requirement')->nullable();
            $table->string('proposal_commets_from_ac_relevant')->nullable();
            $table->string('referred_to_for_technical_proposal_comments')->nullable();
            $table->string('expert_opinion')->nullable();
            $table->text('progress_status_update_by_dc_ac_pdma')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergency');
    }
}
