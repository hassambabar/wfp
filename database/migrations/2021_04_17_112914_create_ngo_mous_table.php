<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNgoMousTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ngo_mous', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ngo_id')->constrained('ngo_profile');
            $table->enum('mou_status', ['valid','invalid']);
            $table->string('mou_file')->nullable();
            $table->date('mou_signed_date');
            $table->date('mou_expiry_date');
            $table->integer('nature_of_org')->nullable();
            $table->integer('num_of_years')->nullable();
            $table->integer('num_of_employees')->nullable();
            $table->integer('funding_agency')->nullable();
            $table->string('donor')->nullable();
            $table->integer('donation_amount')->nullable();
            $table->string('description', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ngo_mous');
    }
}
