<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodPackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FoodPackage', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('male_hh')->nullable();
            $table->string('female_hh')->nullable();
            $table->string('child_headed')->nullable();
            $table->string('persons_with_disabilities')->nullable();
            $table->string('total')->nullable();
            $table->integer('relief_camp_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FoodPackage');
    }
}
