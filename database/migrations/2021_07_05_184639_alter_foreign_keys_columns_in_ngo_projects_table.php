<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterForeignKeysColumnsInNgoProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngo_projects', function (Blueprint $table) {
            $table->dropForeign('ngo_projects_project_category_foreign');
            $table->dropForeign('ngo_projects_thematic_area_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_projects', function (Blueprint $table) {
            $table->unsignedBigInteger('project_category')->change();
            $table->unsignedBigInteger('thematic_area')->change();
            $table->foreign('project_category','ngo_projects_project_category_foreign')->references('id')->on('project_category');
            $table->foreign('thematic_area','ngo_projects_thematic_area_foreign')->references('id')->on('thematic_areas');
        });
    }
}
