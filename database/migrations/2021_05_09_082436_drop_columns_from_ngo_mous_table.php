<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsFromNgoMousTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngo_mous', function (Blueprint $table) {
            $table->dropColumn(
                [
                    'nature_of_org',
                    'num_of_employees',
                    'funding_agency',
                    'donor',
                    'donation_amount',
                    'description'
                ]
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_mous', function (Blueprint $table) {
            $table->integer('nature_of_org')->nullable();
            $table->integer('num_of_employees')->nullable();
            $table->integer('funding_agency')->nullable();
            $table->string('donor')->nullable();
            $table->integer('donation_amount')->nullable();
            $table->string('description', 255)->nullable();
        });
    }
}
