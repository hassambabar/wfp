<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmergencyItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emergency_items', function (Blueprint $table) {
            $table->id();
            $table->string('emergency_id')->nullable();
            $table->string('rescue_items')->nullable();
            $table->string('quantity')->nullable();
            $table->text('description')->nullable();
            $table->string('when_needed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergency_items');
    }
}
