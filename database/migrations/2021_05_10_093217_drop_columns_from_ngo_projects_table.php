<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsFromNgoProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ngo_projects', function (Blueprint $table) {
            $table->dropForeign('ngo_projects_project_type_foreign');
            $table->dropForeign('ngo_projects_partner_foreign');
            $table->dropColumn(
                [
                    'project_type',
                    'partner',
                    'project_budget',
                    'project_deliverable',
                    'start_date',
                    'end_date',
                    'beneficiary_targets',
                ]
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ngo_projects', function (Blueprint $table) {
            //
        });
    }
}
